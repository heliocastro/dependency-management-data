package cmd

import (
	"fmt"

	"dmd.tanna.dev/internal/reports"
	"github.com/spf13/cobra"
)

var reportFunding = &cobra.Command{
	Use:   "funding",
	Short: "Report packages that are looking for funding",
	Long: `Report packages that you depend on, who have indicated they are looking for financial support.

This provides insight into how many of your dependencies are looking for financial support, which can be useful to understand how to make your software supply chain more sustainable long-term.

The resulting output prioritises dependencies that are more heavily used across all repositories that dependency-management-data is aware of, and does not have a limit of the number of packages returned.

This looks for known metadata in:

- The package manager, such as metadata that would appear when running ` + "`npm fund`" + `
- The source repository, for instance in a ` + "`FUNDING.yml`" + ` or similar

This data is retrieved through:

- https://ecosyste.ms

Requires running ` + "`db generate dependency-health`" + ` to seed the data.`,
	Run: func(cmd *cobra.Command, args []string) {
		sqlDB, err := openDatabase(databasePath)
		cobra.CheckErr(err)

		tw, err := reports.Funding(cmd.Context(), logger, sqlDB)
		cobra.CheckErr(err)

		if tw == nil {
			return
		}

		fmt.Println(tw.Render())
	},
}

func init() {
	reportCmd.AddCommand(reportFunding)
}
