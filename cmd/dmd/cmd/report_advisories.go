package cmd

import (
	"fmt"
	"os"
	"path/filepath"

	"dmd.tanna.dev/internal/advisory"
	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/spf13/cobra"
)

var reportAdvisoriesCmd = &cobra.Command{
	Use:   "advisories",
	Short: "Report advisories that are available for packages or dependencies in use",
	Long: `Report advisories that are available for packages or dependencies in use

This reports advisories from the following sources:

- the ` + "`" + `advisories` + "`" + ` table for custom or community-informed advisories
- End of Life package advisories, via endoflife.date, and highlights whether dependencies are lacking active support or are actively end-of-life.
- AWS infrastructure version advisories, via endoflife-checker

Requires running ` + "`" + `db generate advisories` + "`" + ` to seed the data.
	`,
	Run: func(cmd *cobra.Command, args []string) {
		sqlDB, err := openDatabase(databasePath)
		cobra.CheckErr(err)

		tw, err := advisory.ReportPackages(cmd.Context(), logger, sqlDB, platform, organisation, repo, owner, advisoryType, summaryOutput)
		cobra.CheckErr(err)
		err = outputTable(tw, outPath)
		cobra.CheckErr(err)
	},
}

func outputTable(tw table.Writer, reportName string) error {
	if csvOutput {
		err := os.MkdirAll(outPath, 0700)
		if err != nil {
			return err
		}

		err = os.WriteFile(filepath.Join(outPath, reportName), []byte(tw.RenderCSV()), 0600)
		if err != nil {
			return err
		}

		return nil
	}

	fmt.Println(tw.Render())
	return nil
}

func init() {
	reportCmd.AddCommand(reportAdvisoriesCmd)
	reportAdvisoriesCmd.Flags().BoolVar(&csvOutput, "csv", false, "Whether to output as a CSV file")
	reportAdvisoriesCmd.Flags().StringVar(&outPath, "out", "", "Where to output report(s)")
	reportAdvisoriesCmd.MarkFlagsRequiredTogether("csv", "out")
	reportAdvisoriesCmd.Flags().StringVar(&platform, "platform", "", "Whether to filter policy violations by the given platform that hosts the repository, for instance `gitlab`")
	reportAdvisoriesCmd.Flags().StringVar(&organisation, "organisation", "", "Whether to filter policy violations by the given organisation that hosts the repository")
	reportAdvisoriesCmd.Flags().StringVar(&repo, "repo", "", "Whether to filter policy violations by a specific repository name")
	reportAdvisoriesCmd.Flags().StringVar(&owner, "owner", "", "Whether to filter policy violations by the underlying owner of the repository")
	reportAdvisoriesCmd.Flags().StringVar(&advisoryType, "advisory-type", "", "Whether to filter the policy violations by a given Advisory Type (as noted in https://dmd.tanna.dev/concepts/advisory/)")
	reportAdvisoriesCmd.Flags().BoolVar(&summaryOutput, "summary", false, "Whether to report only a short summary")
}
