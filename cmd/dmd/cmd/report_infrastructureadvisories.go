package cmd

import (
	"dmd.tanna.dev/internal/advisory"
	"github.com/spf13/cobra"
)

var reportInfrastructureadvisoriesCmd = &cobra.Command{
	Use: "infrastructure-advisories",
	Aliases: []string{
		"infra-advisories",
	},
	Short: "Report infrastructure advisories",
	Long: `Report advisories that are available for infrastructure components

This reports advisories from the following sources:

- AWS infrastructure version advisories, via endoflife-checker

Requires running ` + "`" + `db generate advisories` + "`" + ` to seed the data.
	`,
	Run: func(cmd *cobra.Command, args []string) {
		sqlDB, err := openDatabase(databasePath)
		cobra.CheckErr(err)

		tw, err := advisory.ReportAWS(cmd.Context(), logger, sqlDB)
		cobra.CheckErr(err)
		err = outputTable(tw, outPath)
		cobra.CheckErr(err)
	},
}

func init() {
	reportCmd.AddCommand(reportInfrastructureadvisoriesCmd)
	reportInfrastructureadvisoriesCmd.Flags().BoolVar(&csvOutput, "csv", false, "Whether to output as a CSV file")
	reportInfrastructureadvisoriesCmd.Flags().StringVar(&outPath, "out", "", "Where to output report(s)")
	reportInfrastructureadvisoriesCmd.MarkFlagsRequiredTogether("csv", "out")
}
