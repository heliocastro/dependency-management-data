package cmd

import (
	"fmt"

	"dmd.tanna.dev/internal/licenses"
	"github.com/spf13/cobra"
)

var reportLicensesCmd = &cobra.Command{
	Use:     "licenses",
	Aliases: []string{"licences"},
	Short:   "Report license information for package dependencies",
	Long: `Report license information for package dependencies

This report utilises data from deps.dev

Requires running ` + "`" + `db generate advisories` + "`" + ` to seed the data.
	`,
	Run: func(cmd *cobra.Command, args []string) {
		sqlDB, err := openDatabase(databasePath)
		cobra.CheckErr(err)

		tw, err := licenses.ReportLicensingOverview(cmd.Context(), logger, sqlDB, platform, organisation, repo, owner)
		cobra.CheckErr(err)

		fmt.Println(tw.Render())
	},
}

func init() {
	reportCmd.AddCommand(reportLicensesCmd)
	reportLicensesCmd.Flags().StringVar(&platform, "platform", "", "Whether to filter advisories by the given platform that hosts the repository, for instance `gitlab`")
	reportLicensesCmd.Flags().StringVar(&organisation, "organisation", "", "Whether to filter licenses by the given organisation that hosts the repository")
	reportLicensesCmd.Flags().StringVar(&repo, "repo", "", "Whether to filter licenses by a specific repository name")
	reportLicensesCmd.Flags().StringVar(&owner, "owner", "", "Whether to filter licenses by the underlying owner of the repository")
}
