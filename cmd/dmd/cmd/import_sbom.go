package cmd

import (
	"fmt"
	"os"

	"dmd.tanna.dev/internal/datasources/sbom"
	"dmd.tanna.dev/internal/externaldata"
	"dmd.tanna.dev/internal/view"
	"github.com/spf13/cobra"
)

var importSBOMCmd = &cobra.Command{
	Use:   "sbom '/path/to/sbom.json' --platform github --organisation jamietanna --repo jamietanna",
	Short: "Import an SBOM",
	Long: `Imports a Software Bill of Materials (SBOM).
`,
	Example: `# taking an SBOM that was produced from the GitHub repo https://github.com/jamietanna/jamietanna
dmd import sbom '/path/to/sbom.json' --platform github --organisation jamietanna --repo jamietanna --db dmd.db
# taking an SBOM that was produced from the GitLab repo https://gitlab.com/tanna.dev/dependency-management-data
dmd import sbom '/path/to/sbom.json' --platform gitlab --organisation tanna.dev --repo dependency-management-data --db dmd.db
`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 0 {
			cobra.CheckErr(fmt.Errorf("Missing argument"))
		}

		db, err := openDatabaseForWrite(databasePath)
		cobra.CheckErr(err)

		pw := view.NewProgressWriter(os.Stdout, noProgress)

		parser := sbom.NewParser()
		deps, licenses, err := parser.ParseFile(args[0], platform, organisation, repo)
		cobra.CheckErr(err)

		importer := sbom.NewImporter()
		err = importer.ImportDependencies(cmd.Context(), deps, db, pw)
		cobra.CheckErr(err)

		err = externaldata.ImportLicenses(cmd.Context(), licenses, db, pw)
		cobra.CheckErr(err)
	},
}

func init() {
	importCmd.AddCommand(importSBOMCmd)
	addNoProgressFlag(importSBOMCmd)

	importSBOMCmd.Flags().StringVar(&platform, "platform", "", "The platform that hosts the repository that this given SBOM has been generated for, for instance `github`, `gitlab`")
	cobra.CheckErr(importSBOMCmd.MarkFlagRequired("platform"))

	importSBOMCmd.Flags().StringVar(&organisation, "organisation", "", "The organisation that hosts the repository that this given SBOM has been generated for, for instance `tanna.dev`, `gitlab-org/sbom`")
	cobra.CheckErr(importSBOMCmd.MarkFlagRequired("organisation"))

	importSBOMCmd.Flags().StringVar(&repo, "repo", "", "The repository that this given SBOM has been generated for")
	cobra.CheckErr(importSBOMCmd.MarkFlagRequired("repo"))
}
