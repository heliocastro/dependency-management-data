package cmd

import (
	"fmt"
	"os"

	"dmd.tanna.dev/internal/advisory"
	"dmd.tanna.dev/internal/contrib"
	"dmd.tanna.dev/internal/datasources/awselasticache"
	"dmd.tanna.dev/internal/datasources/awslambda"
	"dmd.tanna.dev/internal/datasources/awsrds"
	"dmd.tanna.dev/internal/dependencyhealth"
	"dmd.tanna.dev/internal/depsdev"
	"dmd.tanna.dev/internal/endoflifedate"
	"dmd.tanna.dev/internal/sensitivepackages/db"
	"dmd.tanna.dev/internal/view"
	"github.com/hashicorp/go-retryablehttp"
	"github.com/spf13/cobra"
)

var dbGenerateAdvisoriesCmd = &cobra.Command{
	Use:   "advisories",
	Short: "Seed the database with known package advisories",
	Long: `Seed the database with known advisories about packages' security or maintainence posture.

This uses information available in the Open Source ecosystem about known unmaintained packages or packages that are marked as deprecated and provides a free-form field to specify some reasoning as to why the advisory is present, and any remediation steps if necessary.

This determines whether the packages you are using are running/approaching End Of Life (EOL) versions, through integration with:

- EndOfLife.date (https://endoflife.date)

This includes the generation of licensing information (to determine i.e. "how many packages use AGPL-3.0 licensed code") as well as Common Vulnerabilities and Exposures (CVE) information, and integrates with:

- deps.dev (https://deps.dev)

Note that this may lead to the leakage of package names to external systems, which may be seen as a privacy or security issue, which can be avoided by following the documentation in the Avoiding the leakage of sensitive package names cookbook (https://dmd.tanna.dev/cookbooks/avoiding-sensitive-package-names/)

Known issues:

- Renovate data missing https://gitlab.com/tanna.dev/dependency-management-data/-/issues/77
`,
	Run: func(cmd *cobra.Command, args []string) {
		sqlDB, err := openDatabaseForWrite(databasePath)
		cobra.CheckErr(err)

		httpClient := retryablehttp.NewClient().HTTPClient

		pw := view.NewProgressWriter(os.Stdout, noProgress)

		numSensitivePackages, err := db.New(sqlDB).CountEntries(cmd.Context())
		cobra.CheckErr(err)

		if numSensitivePackages == 0 {
			logger.Warn("About to process advisories without any sensitive packages defined. This may be intended, but it's worth reviewing https://dmd.tanna.dev/cookbooks/avoiding-sensitive-package-names/ to be sure")
		} else {
			logger.Info(fmt.Sprintf("About to process advisories-data with %d sensitive packages defined", numSensitivePackages))
		}

		err = depsdev.Generate(cmd.Context(), logger, sqlDB, pw, httpClient)
		cobra.CheckErr(err)

		err = contrib.GenerateAdvisories(cmd.Context(), logger, sqlDB, pw)
		cobra.CheckErr(err)

		err = endoflifedate.Generate(cmd.Context(), logger, sqlDB, pw, httpClient)
		cobra.CheckErr(err)

		// then combine all package data advisories into the `advisories` table
		err = advisory.CombineTablesIntoAdvisoriesTable(cmd.Context(), logger, sqlDB, pw)
		cobra.CheckErr(err)

		err = dependencyhealth.GenerateAdvisories(cmd.Context(), logger, sqlDB, pw)
		cobra.CheckErr(err)

		err = awslambda.GenerateEndOfLife(cmd.Context(), logger, sqlDB, pw)
		cobra.CheckErr(err)

		err = awsrds.GenerateEndOfLife(cmd.Context(), logger, sqlDB, pw)
		cobra.CheckErr(err)

		err = awselasticache.GenerateEndOfLife(cmd.Context(), logger, sqlDB, pw)
		cobra.CheckErr(err)
	},
}

func init() {
	dbGenerateCmd.AddCommand(dbGenerateAdvisoriesCmd)
	addNoProgressFlag(dbGenerateAdvisoriesCmd)
}
