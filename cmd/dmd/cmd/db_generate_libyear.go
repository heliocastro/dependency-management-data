package cmd

import (
	"fmt"
	"os"

	"dmd.tanna.dev/internal/libyear"
	"dmd.tanna.dev/internal/sensitivepackages/db"
	"dmd.tanna.dev/internal/view"
	"github.com/hashicorp/go-retryablehttp"
	"github.com/spf13/cobra"
)

var dbGenerateLibyearCmd = &cobra.Command{
	Use:   "libyear",
	Short: "Generate the Libyear metric for known dependencies",
	Long: `Generate the Libyear metric for known dependencies

This uses the Libyear metric, which is defined as "how many years between the version we're currently using and the latest version released", and then totalled across all libraries used by the project.

NOTE: that this may not include all dependencies, so the number could be higher than shown.

For further reading on the Libyear metric, check out https://libyear.com/ and https://chaoss.community/kb/metric-libyears/ for more information.

Note that this may lead to the leakage of package names to external systems, which may be seen as a privacy or security issue, which can be avoided by following the documentation in the Avoiding the leakage of sensitive package names cookbook (https://dmd.tanna.dev/cookbooks/avoiding-sensitive-package-names/)

Known issues:

- Performance issues and 500s upstream - https://gitlab.com/tanna.dev/dependency-management-data/-/issues/459
`,
	Aliases: []string{"libyears"},
	Run: func(cmd *cobra.Command, args []string) {
		sqlDB, err := openDatabaseForWrite(databasePath)
		cobra.CheckErr(err)

		httpClient := retryablehttp.NewClient().HTTPClient

		pw := view.NewProgressWriter(os.Stdout, noProgress)
		go pw.Render()

		numSensitivePackages, err := db.New(sqlDB).CountEntries(cmd.Context())
		cobra.CheckErr(err)

		if numSensitivePackages == 0 {
			logger.Warn("About to calculate Libyears without any sensitive packages defined. This may be intended, but it's worth reviewing https://dmd.tanna.dev/cookbooks/avoiding-sensitive-package-names/ to be sure")
		} else {
			logger.Info(fmt.Sprintf("About to calculate Libyears with %d sensitive packages defined", numSensitivePackages))
		}

		err = libyear.Generate(cmd.Context(), logger, sqlDB, pw, httpClient)
		cobra.CheckErr(err)
	},
}

func init() {
	dbGenerateCmd.AddCommand(dbGenerateLibyearCmd)
	addNoProgressFlag(dbGenerateLibyearCmd)
}
