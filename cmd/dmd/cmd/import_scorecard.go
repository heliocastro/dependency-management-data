package cmd

import (
	"fmt"
	"net/http"
	"os"
	"sync"

	"dmd.tanna.dev/internal/dependencyhealth"
	"dmd.tanna.dev/internal/ecosystems"
	"dmd.tanna.dev/internal/securityscorecards"
	"dmd.tanna.dev/internal/view"
	"github.com/hashicorp/go-retryablehttp"
	"github.com/jedib0t/go-pretty/v6/progress"
	"github.com/spf13/cobra"
)

var importScorecardCmd = &cobra.Command{
	Use:   "scorecard",
	Short: "Import OpenSSF Security Scorecards data",
	Long: `Import the result of an OpenSSF Security Scorecards (https://securityscorecards.dev/) analysis of a repository into dependency-management-data's internal database

Similar to the insight that the ` + "`dmd db generate dependency-health`" + ` command provides in the ` + "`dependency_health`" + ` table, you can ingest pre-computed Security Scorecards reports into dependency-management-data.

This consumes the JSON report that has been output from the https://github.com/ossf/scorecard CLI, performing a lookup to https://ecosyste.ms to determine whether there are any package(s) that the repo corresponds to, and then imports it into the ` + "`dependency_health`" + ` table.

NOTE that this may lead to the leakage of package names to external systems, which may be seen as a privacy or security issue, but it DOES NOT currently have any ability to override the lookup. This will be possible as part of https://gitlab.com/tanna.dev/dependency-management-data/-/issues/480 but is not implemented.

`,
	Example: `# First, retrieve the data via the ` + "`scorecard`" + ` CLI
scorecard --repo github.com/oapi-codegen/runtime --output runtime.json      --format json
scorecard --repo gitlab.com/fdroid/fdroidclient  --output fdroidclient.json --format json
# etc ...
# then, import it
# NOTE quotes to prevent globbing
dmd db import scorecard --db dmd.db '*.json'
	`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 0 {
			cobra.CheckErr(fmt.Errorf("Missing argument"))
		}

		sqlDB, err := openDatabaseForWrite(databasePath)
		cobra.CheckErr(err)

		pw := view.NewProgressWriter(os.Stdout, noProgress)

		parser := securityscorecards.NewParser()
		scorecards, err := parser.ParseFiles(args[0], pw)
		cobra.CheckErr(err)

		tracker := progress.Tracker{
			Message: fmt.Sprintf("Looking up dependency names for %d Scorecards reports", len(scorecards)),
			Total:   int64(len(scorecards)),
		}
		pw.AppendTracker(&tracker)

		httpClient := retryablehttp.NewClient().HTTPClient
		ecosystemsClient, err := ecosystems.NewClientWithResponses(ecosystems.BaseURL, ecosystems.WithHTTPClient(httpClient))
		if err != nil {
			cobra.CheckErr(fmt.Errorf("failed to construct API client for Ecosyste.ms' Packages API: %w", err))
		}

		imports := make(map[string]securityscorecards.ScorecardResult)

		const maxGoroutines = 20
		var wg sync.WaitGroup
		var m sync.Mutex
		goroutines := make(chan struct{}, maxGoroutines)
		for _, row := range scorecards {
			wg.Add(1)

			go func(result securityscorecards.ScorecardResult) {
				defer func() {
					<-goroutines
					wg.Done()
					tracker.Increment(1)
				}()

				goroutines <- struct{}{}

				repoUrl := fmt.Sprintf("https://%s", result.Repo.Name)

				resp, err := ecosystemsClient.LookupPackageWithResponse(cmd.Context(), &ecosystems.LookupPackageParams{
					RepositoryUrl: &repoUrl,
				})
				if err != nil {
					logger.Error(err.Error(), "err", err)
					return
				}
				if resp.StatusCode() != http.StatusOK {
					logger.Warn(fmt.Sprintf("Failed lookup in Ecosystems: HTTP %d", resp.StatusCode()))
					return
				}
				if resp.JSON200 == nil || len(*resp.JSON200) == 0 {
					logger.Warn(fmt.Sprintf("Lookup for repo %s in Ecosystems returned no results", repoUrl))
					return
				}

				if len(*resp.JSON200) > 1 {
					logger.Warn(fmt.Sprintf("Lookup for repo URL %s in Ecosystems returned %d results, but was only expecting 1: Will import the Scorecard results for all possible dependency names", repoUrl, len(*resp.JSON200)))
				}

				for _, pack := range *resp.JSON200 {
					m.Lock()
					imports[pack.Purl] = result
					m.Unlock()

				}
			}(row)
		}

		wg.Wait()
		tracker.MarkAsDone()

		err = dependencyhealth.ImportRawScorecardsResults(cmd.Context(), logger, sqlDB, imports, pw, httpClient)
		cobra.CheckErr(err)
	},
}

func init() {
	importCmd.AddCommand(importScorecardCmd)
	addNoProgressFlag(importScorecardCmd)
}
