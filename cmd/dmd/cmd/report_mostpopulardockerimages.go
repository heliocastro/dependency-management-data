package cmd

import (
	"fmt"

	"dmd.tanna.dev/internal/datasources"
	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/spf13/cobra"
)

var reportMostPopularDockerImagesCmd = &cobra.Command{
	Use:   "mostPopularDockerImages",
	Short: "Query the most popular Docker registries, namespaces and images in use",
	Run: func(cmd *cobra.Command, args []string) {
		sqlDB, err := openDatabase(databasePath)
		cobra.CheckErr(err)

		results, err := datasources.QueryMostPopularDockerImages(cmd.Context(), sqlDB)
		cobra.CheckErr(err)
		warnIfNoResults(results)

		rowLimit := 10

		for k, v := range results {
			fmt.Println(k)

			registriesTw := table.NewWriter()
			registriesTw.AppendHeader(table.Row{
				"Registry", "#",
			})

			for i, count := range v.Registries {
				registriesTw.AppendRow(table.Row{
					count.Name, count.Count,
				})

				if i > rowLimit {
					break
				}
			}
			fmt.Println(registriesTw.Render())

			namespacesTw := table.NewWriter()
			namespacesTw.AppendHeader(table.Row{
				"Namespace", "#",
			})

			for i, count := range v.Namespaces {
				namespacesTw.AppendRow(table.Row{
					count.Name, count.Count,
				})

				if i > rowLimit {
					break
				}
			}
			fmt.Println(namespacesTw.Render())

			imagesTw := table.NewWriter()
			imagesTw.AppendHeader(table.Row{
				"Image", "#",
			})

			for i, count := range v.Images {
				imagesTw.AppendRow(table.Row{
					count.Name, count.Count,
				})

				if i > rowLimit {
					break
				}
			}
			fmt.Println(imagesTw.Render())
		}
	},
}

func init() {
	reportCmd.AddCommand(reportMostPopularDockerImagesCmd)
}
