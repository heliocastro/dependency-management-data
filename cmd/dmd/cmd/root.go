package cmd

import (
	"database/sql"
	"errors"
	"fmt"
	"os"

	"log/slog"

	"github.com/charmbracelet/log"
	"github.com/spf13/cobra"
)

var (
	databasePath  string
	outPath       string
	debug         bool
	noProgress    bool
	platform      string
	organisation  string
	repo          string
	owner         string
	advisoryType  string
	csvOutput     bool
	summaryOutput bool

	logger *slog.Logger

	versionInfo struct {
		version string
		commit  string
		short   string
	}
)

const (
	// logKeyDMDVersion defines the version of the `dmd` command that's currently running
	logKeyDMDVersion = "dmdVersion"
	// logKeyDatabaseVersion defines the version in the given database that describes the version of the `dmd` command that initialised the database
	logKeyDatabaseVersion = "dbVersion"
)

var rootCmd = &cobra.Command{
	Use:   "dmd",
	Short: "A set of tooling to interact with dependency-management-data",
	PersistentPreRun: func(cmd *cobra.Command, args []string) {
		var handler *log.Logger
		if debug {
			handler = log.NewWithOptions(os.Stderr, log.Options{
				Level: log.DebugLevel,
			})
			handler.Debug("Starting application in debug mode")
		} else {
			handler = log.New(os.Stderr)
		}
		logger = slog.New(handler.With(logKeyDMDVersion, versionInfo.short))
	},
}

func Command() *cobra.Command {
	return rootCmd
}

func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func SetVersionInfo(version, commit, short string) {
	versionInfo.version = version
	versionInfo.commit = commit
	versionInfo.short = short

	rootCmd.Version = fmt.Sprintf("%s (Built from Git SHA %s)", versionInfo.version, versionInfo.commit)
}

func addRequiredDbFlag(cmd *cobra.Command) {
	cmd.PersistentFlags().StringVar(&databasePath, "db", "", "the path to the input/output database")
	cmd.MarkPersistentFlagRequired("db")
}

func addNoProgressFlag(cmd *cobra.Command) {
	cmd.Flags().BoolVar(&noProgress, "no-progress", false, "prevent displaying progress of long-running tasks")
}

func init() {
	rootCmd.PersistentFlags().BoolVar(&debug, "debug", false, "whether to enable debug logging")
}

func openDatabase(databasePath string) (*sql.DB, error) {
	db, err := sql.Open("sqlite", databasePath)
	if err != nil {
		return nil, err
	}

	if databasePath != ":memory" {
		if _, err := os.Stat(databasePath); !errors.Is(err, os.ErrNotExist) {
			preFlightChecks(db)
		}
	}

	return db, nil
}

func openDatabaseForWrite(databasePath string) (*sql.DB, error) {
	db, err := openDatabase(databasePath)
	if err != nil {
		return nil, err
	}

	if databasePath != ":memory" {
		if _, err := os.Stat(databasePath); !errors.Is(err, os.ErrNotExist) {
			preFlightChecks(db)

			if isDatabaseFinalised(db) {
				logger.Error("You are modifying a database that has been finalised - are you sure you're meaning to update what should be treated as a read-only database?")
			}
		}
	}

	return db, nil
}
