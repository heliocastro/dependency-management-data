package cmd

import (
	"fmt"
	"os"

	"dmd.tanna.dev/internal/datasources/renovate"
	"dmd.tanna.dev/internal/view"
	"github.com/spf13/cobra"
)

var importRenovateCmd = &cobra.Command{
	Use:   "renovate '/path/to/*.json'",
	Short: "Import a data dump from renovate-graph or the Renovate debug logs",
	Long: `Takes a data export from https://gitlab.com/tanna.dev/renovate-graph/ or the debug logs that come from Renovate (https://dmd.tanna.dev/cookbooks/consuming-renovate-debug-logs) and converts it to the database model.

`,
	Example: `# To consume the output from ` + "`renovate-graph`" + `
# NOTE the escaping! Only takes one argument, which can be a glob
dmd import renovate '../out/*.json' --db out.db
# To consume the output from Renovate's debug logs (https://dmd.tanna.dev/cookbooks/consuming-renovate-debug-logs)
dmd import renovate renovate.log --db out.db
`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 0 {
			cobra.CheckErr(fmt.Errorf("Missing argument"))
		}

		db, err := openDatabaseForWrite(databasePath)
		cobra.CheckErr(err)

		pw := view.NewProgressWriter(os.Stdout, noProgress)

		parser := renovate.NewParser()
		deps, depUpdates, err := parser.ParseFiles(args[0], pw)
		cobra.CheckErr(err)

		importer := renovate.NewImporter()
		err = importer.ImportDependencies(cmd.Context(), deps, db, pw)
		cobra.CheckErr(err)

		if depUpdates != nil {
			err = importer.ImportDependencyUpdates(cmd.Context(), depUpdates, db, pw)
			cobra.CheckErr(err)
		}
	},
}

func init() {
	importCmd.AddCommand(importRenovateCmd)
	addNoProgressFlag(importRenovateCmd)
}
