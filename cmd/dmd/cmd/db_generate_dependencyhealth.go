package cmd

import (
	"fmt"
	"os"

	"dmd.tanna.dev/internal/dependencyhealth"
	"dmd.tanna.dev/internal/sensitivepackages/db"
	"dmd.tanna.dev/internal/view"
	"github.com/hashicorp/go-retryablehttp"
	"github.com/spf13/cobra"
)

var dbGenerateDependencyhealthCmd = &cobra.Command{
	Use:   "dependency-health",
	Short: "Generate insights into the health of dependencies",
	Long: `Generate insights into the health of (third-party) dependencies

This consumes data from different sources to augment the understanding of dependencies in use, for instance giving an indication of whether they are (well) maintained, have been recently released, or may have supply chain hygiene issues.

Currently, this data is derived from:

- OpenSSF Security Scorecards (https://api.securityscorecards.dev/)
- Ecosystems (https://ecosyste.ms)

This data is a best-efforts attempt to provide this insight, and may be stale at the time of fetching.

Note that this may lead to the leakage of package names to external systems, which may be seen as a privacy or security issue, which can be avoided by following the documentation in the Avoiding the leakage of sensitive package names cookbook (https://dmd.tanna.dev/cookbooks/avoiding-sensitive-package-names/)

Known issues:

- Performance issues and 500s upstream - https://gitlab.com/tanna.dev/dependency-management-data/-/issues/459
`,
	Run: func(cmd *cobra.Command, args []string) {
		sqlDB, err := openDatabaseForWrite(databasePath)
		cobra.CheckErr(err)

		httpClient := retryablehttp.NewClient().HTTPClient

		pw := view.NewProgressWriter(os.Stdout, noProgress)
		go pw.Render()

		numSensitivePackages, err := db.New(sqlDB).CountEntries(cmd.Context())
		cobra.CheckErr(err)

		if numSensitivePackages == 0 {
			logger.Warn("About to process dependency health without any sensitive packages defined. This may be intended, but it's worth reviewing https://dmd.tanna.dev/cookbooks/avoiding-sensitive-package-names/ to be sure")
		} else {
			logger.Info(fmt.Sprintf("About to process dependency health data with %d sensitive packages defined", numSensitivePackages))
		}

		err = dependencyhealth.Generate(cmd.Context(), logger, sqlDB, pw, httpClient)
		cobra.CheckErr(err)
	},
}

func init() {
	dbGenerateCmd.AddCommand(dbGenerateDependencyhealthCmd)
	addNoProgressFlag(dbGenerateDependencyhealthCmd)
}
