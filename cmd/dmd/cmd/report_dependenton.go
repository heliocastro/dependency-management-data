package cmd

import (
	"fmt"

	"dmd.tanna.dev/internal/reports"
	"github.com/spf13/cobra"
)

var reportDependenton = &cobra.Command{
	Use:   "dependenton",
	Short: "Report usage of a given dependency",
	Long: `Report usage of a given dependency, and optionally the specific version in use, across all known projects in the database.

This provides a view of the impact of the usage of a given dependency for instance when understanding an unmaintained/end-of-life dependency, security issues, or simply understanding the uptake of a given dependency.

The ` + "`package-current-version`" + ` is prioritised over ` + "`package-version`" + `
	`,
	Run: func(cmd *cobra.Command, args []string) {
		sqlDB, err := openDatabase(databasePath)
		cobra.CheckErr(err)

		packageName, err := cmd.Flags().GetString("package-name")
		cobra.CheckErr(err)

		packageManager, err := cmd.Flags().GetString("package-manager")
		cobra.CheckErr(err)

		packageVersion, err := cmd.Flags().GetString("package-version")
		cobra.CheckErr(err)

		packageCurrentVersion, err := cmd.Flags().GetString("package-current-version")
		cobra.CheckErr(err)

		tw, err := reports.DependentOn(cmd.Context(), logger, sqlDB, packageName, packageManager, packageVersion, packageCurrentVersion)
		cobra.CheckErr(err)
		fmt.Println(tw.Render())
	},
}

func init() {
	reportCmd.AddCommand(reportDependenton)
	reportDependenton.Flags().String("package-name", "", "The exact package name to search for")
	cobra.CheckErr(reportDependenton.MarkFlagRequired("package-name"))
	reportDependenton.Flags().String("package-manager", "", "The exact package manager to search for")
	cobra.CheckErr(reportDependenton.MarkFlagRequired("package-manager"))
	reportDependenton.Flags().String("package-version", "", "The exact package version to search for, which matches the `version` column")
	reportDependenton.Flags().String("package-current-version", "", "The exact package version to search for, which matches the `current_version` column. Takes preference over `version`")
}
