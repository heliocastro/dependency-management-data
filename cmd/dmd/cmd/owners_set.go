package cmd

import (
	"dmd.tanna.dev/internal/ownership"
	"github.com/spf13/cobra"
)

var ownersSetCmd = &cobra.Command{
	Use:   "set owner [notes]",
	Short: "Set the ownership of repo(s) in bulk",
	Long: `Set the ownership of repo(s) in bulk, by using flags to filter

At least one of the command-line flags ` + "`--platform`, `--organisation`, or `--repo` are required." + `

See the examples for an idea of how the filtering works.
`,
	Example: `# set the owner for a whole organisation, ` + "`jamietanna`" + `, (regardless of the ` + "`platform` or `repo`" + `) to be ` + "`Jamie Tanna`" + `.
dmd owners set 'Jamie Tanna' --organisation jamietanna
# Set a specific repository to have the owner name ` + "`Job DSL Plugin developers`, with the notes `https://github.com/orgs/jenkinsci/teams/job-dsl-plugin-developers`" + `,
dmd owners set --platform 'github' --organisation jenkinsci --repo job-dsl-plugin 'Job DSL Plugin developers' 'https://github.com/orgs/jenkinsci/teams/job-dsl-plugin-developers'
`,
	Args: cobra.RangeArgs(1, 2),
	Run: func(cmd *cobra.Command, args []string) {
		sqlDB, err := openDatabaseForWrite(databasePath)
		cobra.CheckErr(err)

		params := ownership.SetBulkOwnershipParams{
			Platform:     platform,
			Organisation: organisation,
			Repo:         repo,
			OwnerName:    args[0],
		}

		if len(args) > 1 {
			params.Notes = args[1]
		}

		err = ownership.SetBulkOwnership(cmd.Context(), params, sqlDB)
		cobra.CheckErr(err)
	},
}

func init() {
	ownersCmd.AddCommand(ownersSetCmd)
	ownersSetCmd.Flags().StringVar(&platform, "platform", "", "Set ownership information to the given owner, limited to the specific platform. Leaving empty will match against any platform")
	ownersSetCmd.Flags().StringVar(&organisation, "organisation", "", "Set ownership information to the given owner, limited to the specific organisation. Leaving empty will match against any organisation")
	ownersSetCmd.Flags().StringVar(&repo, "repo", "", "Set ownership information to the given owner, limited to the specific repo. Leaving empty will match against any repo")
}
