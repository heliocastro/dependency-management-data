package cmd

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	"dmd.tanna.dev/internal/metadata/db"
)

func preFlightChecks(sqlDB *sql.DB) {
	if sqlDB == nil {
		logger.Debug("Skipping preFlightChecks because provided *sql.DB was <nil>")
		return
	}

	dbVersion := getDBVersion(sqlDB)
	if dbVersion == "" {
		logger.Debug("No dbVersion could be determined")
		return
	}

	if dbVersion != versionInfo.short {
		// dmdVersion is already provided through the centrally configured logger
		logger.Warn("The version of `dmd` that you are running doesn't match the version that the database was created with. It is recommended you align these, in case of schema or package changes that may cause runtime issues, or invalid data.", logKeyDatabaseVersion, dbVersion)
	}
}

func getDBVersion(sqlDB *sql.DB) string {
	q := db.New(sqlDB)
	data, err := q.RetrieveDMDVersion(context.Background())
	if errors.Is(err, sql.ErrNoRows) {
		logger.Warn("No `dmd_version` key was found in the metadata table - you may be interacting with a very old version version of a DMD database, or something has gone wrong when initialising it")
	} else if err != nil {
		logger.Error(fmt.Sprintf("Failed to look up DMD CLI version in the metadata table: %v", err))
		return ""
	}

	return data.Value
}

func isDatabaseFinalised(sqlDB *sql.DB) bool {
	q := db.New(sqlDB)
	_, err := q.RetrieveFinalisedAt(context.Background())
	if errors.Is(err, sql.ErrNoRows) {
		return false
	} else if err != nil {
		logger.Error(fmt.Sprintf("Failed to look up `finalised_at` in the metadata table: %v", err))
		return false
	}

	return true
}
