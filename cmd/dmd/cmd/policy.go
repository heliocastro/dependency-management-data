package cmd

import (
	"github.com/spf13/cobra"
)

var policyCmd = &cobra.Command{
	Use:   "policy",
	Short: "Perform actions alongside user-defined Policies",
	Long: `Perform actions alongside user-defined Policies

Policies can be found further documented in https://dmd.tanna.dev/concepts/policy/
.`,
}

func init() {
	rootCmd.AddCommand(policyCmd)
}
