package cmd

import "github.com/spf13/cobra"

var dbMetaCmd = &cobra.Command{
	Use:   "meta",
	Short: "Perform operations on the database's metadata",
	Long:  "Perform actions on the database's `metadata` table, which provides insight into database-specific metadata",
}

func init() {
	dbCmd.AddCommand(dbMetaCmd)
}
