package cmd

import (
	"fmt"
	"os"

	"dmd.tanna.dev/internal/depsdev"
	"dmd.tanna.dev/internal/sensitivepackages/db"
	"dmd.tanna.dev/internal/view"
	"github.com/hashicorp/go-retryablehttp"
	"github.com/spf13/cobra"
)

var dbGenerateMissingdataCmd = &cobra.Command{
	Use:   "missing-data",
	Short: "Generate missing package data",
	Long: `Generate missing package data for some ecosystems, via https://deps.dev.

For a subset of package ecosystems supported by https://deps.dev, provide the full dependency tree for packages.

The supported ecosystems can be found documented on https://dmd.tanna.dev/features/#generating-missing-package-data

This is a "best efforts" lookup, and depends wholly on the deps.dev database's quality.

Note that this may lead to the leakage of package names to external systems, which may be seen as a privacy or security issue, which can be avoided by following the documentation in the Avoiding the leakage of sensitive package names cookbook (https://dmd.tanna.dev/cookbooks/avoiding-sensitive-package-names/)

Known issues:

- https://gitlab.com/tanna.dev/dependency-management-data/-/issues/193
- https://github.com/google/deps.dev/issues/34
`,
	Run: func(cmd *cobra.Command, args []string) {
		sqlDB, err := openDatabaseForWrite(databasePath)
		cobra.CheckErr(err)

		httpClient := retryablehttp.NewClient().HTTPClient

		pw := view.NewProgressWriter(os.Stdout, noProgress)

		numSensitivePackages, err := db.New(sqlDB).CountEntries(cmd.Context())
		cobra.CheckErr(err)

		if numSensitivePackages == 0 {
			logger.Warn("About to process missing-data without any sensitive packages defined. This may be intended, but it's worth reviewing https://dmd.tanna.dev/cookbooks/avoiding-sensitive-package-names/ to be sure")
		} else {
			logger.Info(fmt.Sprintf("About to process missing-data with %d sensitive packages defined", numSensitivePackages))
		}

		err = depsdev.GenerateMissingData(cmd.Context(), logger, sqlDB, pw, httpClient)
		cobra.CheckErr(err)
	},
}

func init() {
	dbGenerateCmd.AddCommand(dbGenerateMissingdataCmd)
	addNoProgressFlag(dbGenerateMissingdataCmd)
}
