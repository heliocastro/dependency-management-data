package cmd

import (
	"database/sql"
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"sync"

	"dmd.tanna.dev/internal/advisory/db"
	"dmd.tanna.dev/internal/policies"
	"github.com/open-policy-agent/opa/rego"
	"github.com/spf13/cobra"
)

var dbGeneratePolicyviolationsCmd = &cobra.Command{
	Use:   "policy-violations",
	Short: "Persist all Open Policy Agent policy violations",
	Long: `Persist to the database all Open Policy Agent policy violations

As an alternative to writing custom Advisories (https://dmd.tanna.dev/cookbooks/custom-advisories/), you can leverage the power of Open Policy Agent to create Policies (https://dmd.tanna.dev/concepts/policy/). See https://dmd.tanna.dev/cookbooks/custom-advisories-opa/ for more information on how to write them.

This takes all policies from ` + "`--policies-directory`" + ` and evaluates them as defined in https://dmd.tanna.dev/cookbooks/custom-advisories-opa/ and then writes them to ` + "`advisories`" + ` table.
`,
	Run: func(cmd *cobra.Command, args []string) {
		sqlDB, err := openDatabaseForWrite(databasePath)
		cobra.CheckErr(err)

		polDir, err := cmd.Flags().GetString("policies-directory")
		cobra.CheckErr(err)

		dir, err := os.Open(polDir)
		cobra.CheckErr(err)

		names, err := dir.Readdirnames(-1)
		cobra.CheckErr(err)

		modules := make([]policies.Module, len(names))
		var m sync.Mutex
		var errs []error
		var wg sync.WaitGroup

		for i, v := range names {
			wg.Add(1)

			go func(i int, v string) {
				defer wg.Done()

				path := filepath.Join(polDir, v)

				b, err := os.ReadFile(path)
				if err != nil {
					m.Lock()
					errs = append(errs, err)
					m.Unlock()
					return
				}

				modules[i] = policies.Module{
					Filepath: path,
					Contents: string(b),
				}
			}(i, v)
		}

		wg.Wait()

		err = errors.Join(unique(errs)...)
		if err != nil {
			cobra.CheckErr(fmt.Errorf("failed to read files in %s: %w", polDir, err))
		}

		var queries []rego.PreparedEvalQuery
		for _, m2 := range modules {
			query, err := policies.PreparePackageQuery(cmd.Context(), []policies.Module{m2})
			cobra.CheckErr(err)
			queries = append(queries, query)
		}

		inputs, err := policies.PreparePolicyEvaluationInputs(cmd.Context(), sqlDB)
		cobra.CheckErr(err)

		// NOTE that each OPA policy must be evaluated on its own. At some point we may allow processing multiple at once, but the design is to be a single file per policy, for now
		var allViolations []policies.PolicyViolation
		for _, query := range queries {
			violations, _, err := policies.EvaluatePolicies(cmd.Context(), query, inputs)
			cobra.CheckErr(err)

			allViolations = append(allViolations, violations...)
		}

		if len(allViolations) == 0 {
			return
		}

		tx, err := sqlDB.BeginTx(cmd.Context(), nil)
		cobra.CheckErr(err)

		defer tx.Rollback()

		q := db.New(sqlDB)

		errs = nil
		for _, pv := range allViolations {
			params := db.InsertAdvisoryParams{
				Platform:     pv.Platform,
				Organisation: pv.Organisation,
				Repo:         pv.Repo,
				PackageName:  pv.PackageName,
				Version:      pv.Version,
				// CurrentVersion below
				PackageManager:  pv.PackageManager,
				DepTypes:        pv.DepTypesAsString(),
				PackageFilePath: pv.PackageFilePath,
				Level:           pv.Level.String(),
				AdvisoryType:    pv.AdvisoryType,
				Description:     pv.Description,
			}
			if pv.CurrentVersion != nil {
				params.CurrentVersion = sql.NullString{
					String: *pv.CurrentVersion,
					Valid:  true,
				}
			}

			err = q.WithTx(tx).InsertAdvisory(cmd.Context(), params)
			if err != nil {
				errs = append(errs, err)
			}
		}
		err = errors.Join(unique(errs)...)
		if err != nil {
			cobra.CheckErr(err)
		}

		cobra.CheckErr(tx.Commit())
		fmt.Printf("Successfully persisted %d policy violations\n", len(allViolations))
	},
}

func init() {
	dbGenerateCmd.AddCommand(dbGeneratePolicyviolationsCmd)
	dbGeneratePolicyviolationsCmd.Flags().String("policies-directory", "", "The directory to source policies from")
	cobra.CheckErr(dbGeneratePolicyviolationsCmd.MarkFlagRequired("policies-directory"))
}

func unique(t []error) []error {
	m := make(map[string]error)

	var errs []error
	for _, v := range t {
		m[v.Error()] = v
	}

	for _, err := range m {
		errs = append(errs, err)
	}
	return errs
}
