package cmd

import (
	"context"
	"database/sql"
	"encoding/csv"
	"errors"
	"fmt"
	"os"
	"sync"

	"dmd.tanna.dev/internal/datasources"
	"dmd.tanna.dev/internal/datasources/sbom"
	"dmd.tanna.dev/internal/domain"
	"dmd.tanna.dev/internal/externaldata"
	"dmd.tanna.dev/internal/view"
	"github.com/jedib0t/go-pretty/v6/progress"
	"github.com/spf13/cobra"
)

var importBulkCmd = &cobra.Command{
	Use:   "bulk /path/to/csv.csv",
	Short: "Import many dependency data exports, via a CSV file",
	Long: `Import many dependency data exports, via metadata in a CSV file
Instead of requiring multiple invocations of the ` + "`dmd`" + ` CLI, using this subcommand makes it possible to prepare a CSV file with the relevant information, and paths to files to parse.

This simplifies the

The CSV must have rows that correspond with the following columns:

- platform: corresponds to https://dmd.tanna.dev/concepts/repo-key/#platform
- organisation: corresponds to https://dmd.tanna.dev/concepts/repo-key/#organisation
- repo: corresponds to https://dmd.tanna.dev/concepts/repo-key/#repo
- datasource: corresponds to https://dmd.tanna.dev/concepts/datasource/ and is one of the following values: [sbom]
- filename: a relative/absolute path to the file to parse

NOTE that the CSV does not require a header
`,
	Example: `$ cat imports.csv
# NOTE: no header should be provided, this is commented to indicate what's the columns are
# platform,organisation,repo,datasource,filename
github,snarfed,bridgy,sbom,example/sbom/snyk-bridgy-fed-cyclone.json

$ dmd import bulk imports.csv --db dmd.db
`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 0 {
			cobra.CheckErr(fmt.Errorf("Missing argument"))
		}

		db, err := openDatabaseForWrite(databasePath)
		cobra.CheckErr(err)

		pw := view.NewProgressWriter(os.Stdout, noProgress)
		go pw.Render()

		f, err := os.Open(args[0])
		cobra.CheckErr(err)

		defer f.Close()

		reader := csv.NewReader(f)
		reader.Comment = '#'

		records, err := reader.ReadAll()
		cobra.CheckErr(err)

		var sbomImports []datasources.BulkImportRow

		var errs []error

		for _, r := range records {
			if len(r) != 5 {
				errs = append(errs, fmt.Errorf("invalid row, should have 5 columns : %v", r))
				continue
			}

			if r[3] != "sbom" {
				errs = append(errs, fmt.Errorf("only `sbom` datasource supported, was given `%v`: %v", r[3], r))
				continue
			}

			sbomImports = append(sbomImports, newBulkImportRow(r))
		}

		err = errors.Join(errs...)
		if err != nil {
			cobra.CheckErr(fmt.Errorf("invalid CSV file given: %w", err))
		}

		err = bulkImportSBOMs(cmd.Context(), db, pw, sbomImports)
		cobra.CheckErr(err)
	},
}

func init() {
	importCmd.AddCommand(importBulkCmd)
	addNoProgressFlag(importBulkCmd)
}

func newBulkImportRow(r []string) datasources.BulkImportRow {
	return datasources.BulkImportRow{
		Platform:     r[0],
		Organisation: r[1],
		Repo:         r[2],
		// Datasource is r[3]
		Filename: r[4],
	}
}

func bulkImportSBOMs(ctx context.Context, db *sql.DB, pw progress.Writer, sbomImports []datasources.BulkImportRow) error {
	tracker := progress.Tracker{
		Message: fmt.Sprintf("Parsing %d SBOMs", len(sbomImports)),
		Total:   int64(len(sbomImports)),
	}
	pw.AppendTracker(&tracker)

	var m sync.Mutex
	var errs []error
	var allDeps []domain.SBOMDependency
	var allLicenses []domain.License

	var wg sync.WaitGroup
	for _, s := range sbomImports {
		wg.Add(1)

		go func(s datasources.BulkImportRow) {
			defer wg.Done()

			parser := sbom.NewParser()
			deps, licenses, err := parser.ParseFile(s.Filename, s.Platform, s.Organisation, s.Repo)
			if err != nil {
				m.Lock()
				errs = append(errs, err)
				m.Unlock()
				tracker.MarkAsErrored()
				return
			}
			m.Lock()
			allDeps = append(allDeps, deps...)
			allLicenses = append(allLicenses, licenses...)
			m.Unlock()
			tracker.Increment(1)
		}(s)
	}
	wg.Wait()

	err := errors.Join(errs...)
	if err != nil {
		return fmt.Errorf("failed to parse %d SBOMs: %w", len(sbomImports), err)
	}

	tracker.MarkAsDone()

	importer := sbom.NewImporter()
	err = importer.ImportDependencies(ctx, allDeps, db, pw)
	if err != nil {
		return err
	}

	err = externaldata.ImportLicenses(ctx, allLicenses, db, pw)
	if err != nil {
		return err
	}

	return nil
}
