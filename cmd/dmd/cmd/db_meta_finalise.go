package cmd

import (
	"time"

	"dmd.tanna.dev/internal/metadata/db"
	"github.com/spf13/cobra"
)

var dbMetaFinaliseCmd = &cobra.Command{
	Use:     "finalise",
	Aliases: []string{"finalize"},
	Short:   "Indicate that the database is now finalised",
	Long: `Set the ` + "`finalised_at`" + ` metadata in the database, which indicates that the database is now finalised, and should now be treated as "up-to-date" and read-only from this point onwards

This indicates that all datasources were imported and all enrichment (i.e. via Advisories, Dependency Health, etc) was complete.

This could indicate that all datasources' data is now up-to-date, but there are likely some that haven't been as recently scanned.
`,
	Example: `# to set the current time as the ` + "`finalised_at`" + ` metadata in the database
dmd db meta finalise --db dmd.db
`,
	Run: func(cmd *cobra.Command, args []string) {
		sqlDB, err := openDatabaseForWrite(databasePath)
		cobra.CheckErr(err)

		now := time.Now().Format(time.RFC3339)

		q := db.New(sqlDB)

		err = q.SetFinalisedAt(cmd.Context(), now)
		cobra.CheckErr(err)
	},
}

func init() {
	dbMetaCmd.AddCommand(dbMetaFinaliseCmd)
}
