package cmd

import (
	"dmd.tanna.dev/internal/libyear"
	"github.com/spf13/cobra"
)

var reportLibyearCmd = &cobra.Command{
	Use:   "libyear",
	Short: "Report the Libyears metric for each repository",
	Long: `Report how many Libyears behind the latest release repositories are

This uses the Libyear metric, which is defined as "how many years between the version we're currently using and the latest version released", and then totalled across all libraries used by the project.

Repositories with a Libyear value of 0 could indicate that either all dependencies are up-to-date, or that the Libyear could not be calculated and are not shown.

NOTE: that this may not include all dependencies, so the number could be higher than shown.

For further reading on the Libyear metric, check out:

- https://libyear.com/
- https://chaoss.community/kb/metric-libyears/

Requires running ` + "`db generate libyears`" + ` to seed the data.`,
	Example: `# To list all repositories
dmd report libyear --db dmd.db
# To list a specific repository, which shows a breakdown of the detected dependencies and calculated Libyear
dmd report libyear --db dmd.db --platform gitlab --organisation tanna.dev --repo dependency-management-data
`,
	Aliases: []string{"libyears"},
	Run: func(cmd *cobra.Command, args []string) {
		sqlDB, err := openDatabase(databasePath)
		cobra.CheckErr(err)

		tw, err := libyear.Report(cmd.Context(), logger, sqlDB, platform, organisation, repo, owner)
		cobra.CheckErr(err)

		if tw == nil {
			return
		}

		err = outputTable(tw, outPath)
		cobra.CheckErr(err)
	},
}

func init() {
	reportCmd.AddCommand(reportLibyearCmd)
	reportLibyearCmd.Flags().StringVar(&platform, "platform", "", "Whether to filter Libyears by the given platform that hosts the repository, for instance `gitlab`")
	reportLibyearCmd.Flags().StringVar(&organisation, "organisation", "", "Whether to filter Libyears by the given organisation that hosts the repository")
	reportLibyearCmd.Flags().StringVar(&repo, "repo", "", "Whether to filter Libyears by a specific repository name")
	reportLibyearCmd.Flags().StringVar(&owner, "owner", "", "Whether to filter Libyears by the underlying owner of the repository")

	reportLibyearCmd.Flags().BoolVar(&csvOutput, "csv", false, "Whether to output as a CSV file")
	reportLibyearCmd.Flags().StringVar(&outPath, "out", "", "Where to output report(s)")
	reportLibyearCmd.MarkFlagsRequiredTogether("csv", "out")
}
