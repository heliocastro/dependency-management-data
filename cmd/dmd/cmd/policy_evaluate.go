package cmd

import (
	"fmt"
	"os"

	"dmd.tanna.dev/internal/policies"
	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/mitchellh/go-wordwrap"
	"github.com/spf13/cobra"
)

var policyEvaluateCmd = &cobra.Command{
	Use:   "evaluate /path/to/policy.rego --db dmd.db",
	Short: "Evaluate an Open Policy Agent policy",
	Long: `Indicate dependencies that would be flagged by a given Open Policy Agent Policy

As an alternative to writing custom Advisories https://dmd.tanna.dev/cookbooks/custom-advisories/, you can leverage the power of Open Policy Agent to create Policies https://dmd.tanna.dev/concepts/policy/. See https://dmd.tanna.dev/cookbooks/custom-advisories-opa/ for more information on how to write them.

**NOTE** that this only performs the evaluation, and does not write the state to the database. To do so, use ` + "`dmd db generate policy-violations`." + `
`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 0 || args[0] == "" {
			cobra.CheckErr(fmt.Errorf("Missing path to policy to evaluate"))
		}

		limit, err := cmd.Flags().GetInt("limit")
		cobra.CheckErr(err)

		filepath := args[0]

		s, err := openDatabase(databasePath)
		cobra.CheckErr(err)

		module, err := os.ReadFile(filepath)
		cobra.CheckErr(err)

		modules := []policies.Module{
			{
				Filepath: filepath,
				Contents: string(module),
			},
		}

		regalInstance, err := policies.NewLinter(args)
		cobra.CheckErr(err)

		regalInstance = regalInstance.WithDisabledRules("line-length", "opa-fmt")

		lintingReport, err := regalInstance.Lint(cmd.Context())
		cobra.CheckErr(err)

		tw := table.NewWriter()
		if lintingReport.Summary.NumViolations != 0 {
			tw.AppendHeader(table.Row{
				"Rule",
				"Level",
				"Description",
				"Location",
				"Related",
			})

			for _, v := range lintingReport.Violations {
				related := ""
				for _, rr := range v.RelatedResources {
					related += fmt.Sprintf("%s: %s\n", rr.Description, rr.Reference)
				}
				loc := fmt.Sprintf("In %s line %d, row %d", v.Location.File, v.Location.Row, v.Location.Column)
				if v.Location.Text != nil {
					loc += fmt.Sprintf(":\n```\n%s\n```", *v.Location.Text)
				}
				if v.Location.File == "" {
					loc = ""
				}

				tw.AppendRow(table.Row{
					v.Title,
					v.Level,
					wordwrap.WrapString(v.Description, 60),
					wordwrap.WrapString(loc, 60),
					wordwrap.WrapString(related, 60),
				})
			}

			fmt.Println(tw.Render())
			cobra.CheckErr(fmt.Errorf("`%s` contained a number of linting violations. Please correct them and then re-run this command. For more detailed linting, you can also use `dmd policy lint`.", modules[0].Filepath))
		}

		query, err := policies.PreparePackageQuery(cmd.Context(), modules)
		cobra.CheckErr(err)

		inputs, err := policies.PreparePolicyEvaluationInputs(cmd.Context(), s)
		cobra.CheckErr(err)

		violations, numRows, err := policies.EvaluatePolicies(cmd.Context(), query, inputs)
		cobra.CheckErr(err)

		if len(violations) == 0 {
			logger.Warn(fmt.Sprintf("Processing `%s` against %d dependencies did not result in any new advisories - was that expected?", filepath, numRows))
			return
		}

		if limit == -1 || len(violations) <= limit {
			fmt.Printf("Processing %s resulted in %d policy violations, from %d dependencies:\n", filepath, len(violations), numRows)
		} else {
			fmt.Printf("Processing %s resulted in %d policy violations, from %d dependencies, but as `--limit` was set to %d, only showing that many results:\n", filepath, len(violations), numRows, limit)
		}

		tw = table.NewWriter()
		tw.AppendHeader(table.Row{
			"Platform",
			"Organisation",
			"Repo",
			"Package",
			"Version",
			"Dependency Types",
			"Filepath",
			"Level",
			"Advisory Type",
			"Description",
		})

		for i, violation := range violations {
			if limit != -1 && i >= limit {
				break
			}

			ver := violation.Version
			if violation.CurrentVersion != nil {
				ver = fmt.Sprintf("%s / %s", ver, *violation.CurrentVersion)
			}
			tw.AppendRow(table.Row{
				violation.Platform,
				violation.Organisation,
				violation.Repo,
				violation.PackageName,
				wordwrap.WrapString(ver, 10),
				violation.DepTypesAsString(),
				violation.PackageFilePath,
				violation.Level,
				violation.AdvisoryType,
				wordwrap.WrapString(violation.Description, 60),
			})
		}

		fmt.Println(tw.Render())
	},
}

func init() {
	policyCmd.AddCommand(policyEvaluateCmd)
	addRequiredDbFlag(policyEvaluateCmd)
	policyEvaluateCmd.Flags().Int("limit", 10, "How many violation(s) to output. Set to -1 to show all results.")
}
