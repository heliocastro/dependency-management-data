package cmd

import (
	"fmt"

	"dmd.tanna.dev/internal/metadata"
	"github.com/spf13/cobra"
)

var dbMetaListCmd = &cobra.Command{
	Use:   "list",
	Short: "List database metadata",
	Long: `List all the values in the ` + "`metadata`" + ` table

	Full definitions of each key can be found documented https://dmd.tanna.dev/schema/#internalmetadatadbschemasql
	`,
	Run: func(cmd *cobra.Command, args []string) {
		db, err := openDatabase(databasePath)
		cobra.CheckErr(err)

		tw, err := metadata.List(cmd.Context(), db)
		cobra.CheckErr(err)

		fmt.Println(tw.Render())
	},
}

func init() {
	dbMetaCmd.AddCommand(dbMetaListCmd)
}
