package cmd

import (
	"dmd.tanna.dev/internal/policies"
	"github.com/spf13/cobra"
)

var reportPolicyviolationsCmd = &cobra.Command{
	Use:   "policy-violations",
	Short: "Report policy violations that are found for packages or dependencies in use",
	Long: `Report policy violations that are found for packages or dependencies in use, based on Open Policy Agent Policies

As an alternative to writing custom Advisories https://dmd.tanna.dev/cookbooks/custom-advisories/, you can leverage the power of Open Policy Agent to create Policies https://dmd.tanna.dev/concepts/policy/. See https://dmd.tanna.dev/cookbooks/custom-advisories-opa/ for more information on how to write them.

Once the data is generated via ` + "`dmd db generate policy-violations`" + `, you can use this report to flag any non-compliance.
	`,
	Run: func(cmd *cobra.Command, args []string) {
		sqlDB, err := openDatabase(databasePath)
		cobra.CheckErr(err)

		level, err := cmd.Flags().GetString("level")
		cobra.CheckErr(err)

		tw, err := policies.ReportPackages(cmd.Context(), logger, sqlDB, platform, organisation, repo, owner, level)
		cobra.CheckErr(err)

		err = outputTable(tw, outPath)
		cobra.CheckErr(err)
	},
}

func init() {
	reportCmd.AddCommand(reportPolicyviolationsCmd)
	reportPolicyviolationsCmd.Flags().BoolVar(&csvOutput, "csv", false, "Whether to output as a CSV file")
	reportPolicyviolationsCmd.Flags().StringVar(&outPath, "out", "", "Where to output report(s)")
	reportPolicyviolationsCmd.MarkFlagsRequiredTogether("csv", "out")
	reportPolicyviolationsCmd.Flags().StringVar(&platform, "platform", "", "Whether to filter policy violations by the given platform that hosts the repository, for instance `gitlab`")
	reportPolicyviolationsCmd.Flags().StringVar(&organisation, "organisation", "", "Whether to filter policy violations by the given organisation that hosts the repository")
	reportPolicyviolationsCmd.Flags().StringVar(&repo, "repo", "", "Whether to filter policy violations by a specific repository name")
	reportPolicyviolationsCmd.Flags().StringVar(&owner, "owner", "", "Whether to filter policy violations by the underlying owner of the repository")
	reportPolicyviolationsCmd.Flags().String("level", "", "The violation level to report: [ERROR, WARN]. Empty string returns all")
}
