package cmd

import (
	"fmt"

	"dmd.tanna.dev/internal/policies"
	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/mitchellh/go-wordwrap"
	"github.com/spf13/cobra"
)

var policyLintCmd = &cobra.Command{
	Use:   "lint /path/to/policy.rego [/path/to/another-policy.rego ...]",
	Short: "Lint an Open Policy Agent policy",
	Long: `Lint an Open Policy Agent policy

This runs a little more validation than ` + "`dmd policy evaluate`" + `.
`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 0 || args[0] == "" {
			cobra.CheckErr(fmt.Errorf("Missing path to policy/policies to evaluate"))
		}

		regalInstance, err := policies.NewLinter(args)
		cobra.CheckErr(err)

		lintingReport, err := regalInstance.Lint(cmd.Context())
		cobra.CheckErr(err)

		tw := table.NewWriter()
		if lintingReport.Summary.NumViolations != 0 {
			tw.AppendHeader(table.Row{
				"File",
				"Rule",
				"Level",
				"Description",
				"Location",
				"Related",
			})

			for _, v := range lintingReport.Violations {
				related := ""
				for _, rr := range v.RelatedResources {
					related += fmt.Sprintf("%s: %s\n", rr.Description, rr.Reference)
				}
				loc := fmt.Sprintf("In %s line %d, row %d", v.Location.File, v.Location.Row, v.Location.Column)
				if v.Location.Text != nil {
					loc += fmt.Sprintf(":\n```\n%s\n```", *v.Location.Text)
				}
				if v.Location.File == "" {
					loc = ""
				}

				tw.AppendRow(table.Row{
					v.Location.File,
					v.Title,
					v.Level,
					wordwrap.WrapString(v.Description, 60),
					wordwrap.WrapString(loc, 60),
					wordwrap.WrapString(related, 60),
				})
			}

			fmt.Println(tw.Render())
			cobra.CheckErr(fmt.Errorf("`%v` contained a number of linting violations. Please correct them and then re-run this command.", args))
		}
	},
}

func init() {
	policyCmd.AddCommand(policyLintCmd)
}
