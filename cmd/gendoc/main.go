package main

import (
	"bytes"
	"log"
	"os"
	"path/filepath"
	"strings"
	"text/template"

	_ "embed"

	graphcmd "dmd.tanna.dev/cmd/dmd-graph/cmd"
	webcmd "dmd.tanna.dev/cmd/dmd-web/cmd"
	"dmd.tanna.dev/cmd/dmd/cmd"
	renovateToSBOMCmd "dmd.tanna.dev/cmd/renovate-to-sbom/cmd"
	"github.com/spf13/cobra"
)

//go:embed cobra.tmpl
var cobraCmdTemplate string

//go:embed dbSchemas.tmpl
var dbSchemasTemplate string

//go:embed graphql.tmpl
var gqlSchemasTemplate string

//go:embed commandlisting.tmpl
var commandListingTemplate string

func main() {
	generateDBSchemaDocumentation()
	generateGraphQLSchemaDocumentation()

	tmpl, err := template.New("cobra.tmpl").
		Funcs(template.FuncMap{
			"commandNameToFilename": CommandNameToFilename,
		}).
		Parse(cobraCmdTemplate)
	must(err)

	generateCommandListingDocumentation("dmd", "dmd-web", "dmd-graph", "renovate-to-sbom")
	generateCommandDocumentation(tmpl, cmd.Command())
	generateCommandDocumentation(tmpl, webcmd.Command())
	generateCommandDocumentation(tmpl, graphcmd.Command())
	generateCommandDocumentation(tmpl, renovateToSBOMCmd.Command())
}

type schema struct {
	Filename   string
	Contents   string
	Deprecated bool
}

type dbSchemaTemplateData struct {
	Schemas []schema
}

type gqlSchemaTemplateData struct {
	Schema string
}

func generateDBSchemaDocumentation() {
	filenames, err := globSchemas("internal")
	must(err)

	if len(filenames) == 0 {
		log.Fatal("No files named `schema.sql` could be found - `gendoc` may have a bug")
	}

	t := template.Must(template.New("schemas.md").Parse(dbSchemasTemplate))

	schemas := make([]schema, len(filenames))
	for i, v := range filenames {
		b, err := os.ReadFile(v)
		must(err)

		schemas[i] = schema{
			Filename:   v,
			Contents:   string(b),
			Deprecated: strings.Contains(string(b), "-- Deprecated:"),
		}
	}

	templateData := dbSchemaTemplateData{
		Schemas: schemas,
	}

	var buf bytes.Buffer

	err = t.Execute(&buf, templateData)
	must(err)

	err = os.WriteFile("public/content/schema.md", buf.Bytes(), 0644)
	must(err)
}

func generateGraphQLSchemaDocumentation() {
	schema, err := os.ReadFile("internal/graph/schema.graphqls")
	must(err)

	templateData := gqlSchemaTemplateData{
		Schema: string(schema),
	}

	var buf bytes.Buffer

	t := template.Must(template.New("graphql.md").Parse(gqlSchemasTemplate))

	err = t.Execute(&buf, templateData)
	must(err)

	err = os.WriteFile("public/content/graphql.md", buf.Bytes(), 0644)
	must(err)
}

func generateCommandDocumentation(tmpl *template.Template, cmd *cobra.Command) {
	must(GenTemplateTree(tmpl, cmd, "public/content/commands/"))
}

func generateCommandListingDocumentation(commands ...string) {
	templateData := map[string]any{
		"Commands": commands,
	}

	var buf bytes.Buffer

	t := template.Must(template.New("commandlisting.md").Parse(commandListingTemplate))

	err := t.Execute(&buf, templateData)
	must(err)

	err = os.WriteFile("public/content/commands/_listing.md", buf.Bytes(), 0644)
	must(err)

}

// adapted from https://stackoverflow.com/a/26809999
func globSchemas(dir string) ([]string, error) {
	files := []string{}
	err := filepath.Walk(dir, func(path string, f os.FileInfo, err error) error {
		if strings.HasSuffix(path, "schema.sql") {
			files = append(files, path)
		}
		return nil
	})

	return files, err
}

func must(err error) {
	if err != nil {
		log.Fatalf("There was an unexpected error: %s", err)
	}
}
