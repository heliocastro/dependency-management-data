package main

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"text/template"

	"github.com/spf13/cobra"
	doc "gitlab.com/tanna.dev/cobra-doc-template"
)

func CommandNameToFilename(name string) string {
	// https://www.jvt.me/posts/2019/11/11/gotcha-netlify-lowercase/
	name = strings.ToLower(name)
	filename := fmt.Sprintf("/commands/%s/", name)
	filename = strings.ReplaceAll(filename, " ", "_")
	return filename
}

// GenTemplateTree is the the same as GenMarkdownTree, but
// with custom filePrepender and linkHandler.
func GenTemplateTree(tmpl *template.Template, cmd *cobra.Command, dir string) error {
	for _, c := range cmd.Commands() {
		if !c.IsAvailableCommand() || c.IsAdditionalHelpTopicCommand() {
			continue
		}
		if err := GenTemplateTree(tmpl, c, dir); err != nil {
			return err
		}
	}

	basename := doc.CommandNameToMarkdownFilename(cmd.CommandPath())
	filename := filepath.Join(dir, basename)
	f, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer f.Close()

	if err := GenTemplate(tmpl, cmd, f); err != nil {
		return err
	}
	return nil
}

func GenTemplate(tmpl *template.Template, cmd *cobra.Command, f *os.File) error {
	d, err := doc.ParseCommand(cmd)
	if err != nil {
		return err
	}

	return tmpl.Execute(f, d)

}
