package cmd

import (
	"bytes"
	"fmt"

	"dmd.tanna.dev/internal/datasources/renovate"
	cdx "github.com/CycloneDX/cyclonedx-go"
)

func init() {
	sbomFormats["cyclonedx1.5+json"] = cyclonedxv1_5JSON{}
}

type cyclonedxv1_5JSON struct{}

func (f cyclonedxv1_5JSON) Name() string {
	return "CycloneDX v1.5 (JSON)"
}

func (f cyclonedxv1_5JSON) Build(deps []renovate.Dependency) any {
	componentName := fmt.Sprintf("%s/%s/%s", deps[0].Platform, deps[0].Organisation, deps[0].Repo)

	metadata := cdx.Metadata{
		Component: &cdx.Component{
			Type: cdx.ComponentTypeApplication,
			Name: componentName,
		},
	}

	var allDeps []string

	dependencies := []cdx.Dependency{
		{
			Ref:          componentName,
			Dependencies: &allDeps,
		},
	}

	components := []cdx.Component{}

	skipped := make(map[string]struct{})
	for _, dep := range deps {
		prl := renovate.ToPurl(dep)
		if onlyIncludeKnownPurlTypes && !renovate.IsKnownType(prl.Type) {
			skipped[prl.Type] = struct{}{}
			continue
		}

		components = append(components, cdx.Component{
			BOMRef:     prl.String(),
			Type:       cdx.ComponentTypeLibrary,
			Name:       prl.Name,
			Version:    prl.Version,
			PackageURL: prl.String(),
		})

		allDeps = append(allDeps, prl.String())
	}

	bom := cdx.NewBOM()
	bom.Metadata = &metadata
	bom.Components = &components
	bom.Dependencies = &dependencies

	if onlyIncludeKnownPurlTypes && len(skipped) > 0 {
		keys := make([]string, 0, len(skipped))
		for k := range skipped {
			keys = append(keys, k)
		}

		logger.Info(fmt.Sprintf("Skipping %d pURLs of unknown types: %v", len(skipped), keys), "repoKey", componentName)
	}

	return bom
}

func (cyclonedxv1_5JSON) Filename(f string) string {
	return f + ".cyclonedx1.5.json"
}

func (cyclonedxv1_5JSON) Marshal(a any) ([]byte, error) {
	bom, ok := a.(*cdx.BOM)
	if !ok {
		return nil, fmt.Errorf("Incorrect type provided to Marshal: %v", a)
	}

	w := bytes.NewBuffer(nil)
	err := cdx.NewBOMEncoder(w, cdx.BOMFileFormatJSON).Encode(bom)

	if err != nil {
		return nil, err
	}

	return w.Bytes(), nil
}
