package cmd

import (
	"encoding/json"
	"fmt"
	"time"

	"dmd.tanna.dev/internal/datasources/renovate"
	"github.com/google/uuid"
	"github.com/spdx/tools-golang/spdx/v2/common"
	"github.com/spdx/tools-golang/spdx/v2/v2_3"
)

func init() {
	sbomFormats["spdx2.3+json"] = spdxv2_3JSON{}
}

type spdxv2_3JSON struct{}

func (f spdxv2_3JSON) Name() string {
	return "SPDX v2.3 (JSON)"
}

func (f spdxv2_3JSON) Build(deps []renovate.Dependency) any {
	var d v2_3.Document
	d.SPDXVersion = v2_3.Version
	d.SPDXIdentifier = "DOCUMENT"
	d.DataLicense = v2_3.DataLicense
	d.DocumentName = fmt.Sprintf("%s/%s/%s", deps[0].Platform, deps[0].Organisation, deps[0].Repo)
	d.DocumentNamespace = fmt.Sprintf("http://localhost:12345/will-not-resolve/%s/%s", uuid.NewString(), d.DocumentName)

	d.CreationInfo = &v2_3.CreationInfo{
		Created: time.Now().Format(time.RFC3339),
		Creators: []common.Creator{
			{
				Creator:     "renovate-to-sbom (https://dmd.tanna.dev/commands/renovate-to-sbom/)",
				CreatorType: "Tool",
			},
		},
	}

	// TODO add Renovate/renovate-graph version https://gitlab.com/tanna.dev/dependency-management-data/-/issues/238
	d.CreationInfo.Creators = append(d.CreationInfo.Creators, common.Creator{
		Creator:     fmt.Sprintf("renovate-to-sbom (v%s, https://dmd.tanna.dev/)", versionInfo.short),
		CreatorType: "Tool",
	})
	d.CreationInfo.Creators = append(d.CreationInfo.Creators, common.Creator{
		Creator:     "Renovate (unknown version, https://github.com/renovatebot/renovate/)",
		CreatorType: "Tool",
	})

	d.Packages = append(d.Packages, &v2_3.Package{
		PackageSPDXIdentifier:   common.ElementID(d.DocumentName),
		PackageName:             d.DocumentName,
		PackageVersion:          "NOASSERTION",
		PackageDownloadLocation: "NOASSERTION",
	})

	skipped := make(map[string]struct{})
	for _, dep := range deps {
		p := v2_3.Package{}

		p.PackageSPDXIdentifier = common.ElementID(f.spdxRef(dep))

		prl := renovate.ToPurl(dep)
		if onlyIncludeKnownPurlTypes && !renovate.IsKnownType(prl.Type) {
			skipped[prl.Type] = struct{}{}
			continue
		}

		p.PackageName = prl.Type + ":" + dep.PackageName

		p.PackageDownloadLocation = "NOASSERTION"
		p.PackageCopyrightText = "NOASSERTION"

		p.PackageExternalReferences = append(p.PackageExternalReferences, &v2_3.PackageExternalReference{
			Category: "PACKAGE-MANAGER",
			Locator:  prl.String(),
			RefType:  "purl",
		})
		p.PackageVersion = depVersionInfo(dep)

		d.Packages = append(d.Packages, &p)
	}

	if onlyIncludeKnownPurlTypes && len(skipped) > 0 {
		keys := make([]string, 0, len(skipped))
		for k := range skipped {
			keys = append(keys, k)
		}

		logger.Info(fmt.Sprintf("Skipping %d pURLs of unknown types: %v", len(skipped), keys), "repoKey", d.DocumentName)
	}

	return d
}

func (spdxv2_3JSON) Filename(f string) string {
	return f + ".spdx2.3.json"
}

func (spdxv2_3JSON) spdxRef(dep renovate.Dependency) string {
	return renovate.ToPurl(dep).String()
}

func (spdxv2_3JSON) Marshal(a any) ([]byte, error) {
	return json.Marshal(a)
}
