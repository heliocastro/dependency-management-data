package cmd

import (
	"fmt"
	"log/slog"
	"os"
	"path"
	"path/filepath"
	"sync"

	"dmd.tanna.dev/internal/datasources/renovate"
	"dmd.tanna.dev/internal/view"
	"github.com/charmbracelet/log"
	"github.com/jedib0t/go-pretty/v6/progress"
	"github.com/spf13/cobra"
)

var (
	outPath                   string
	noProgress                bool
	outFormat                 string
	onlyIncludeKnownPurlTypes bool

	versionInfo struct {
		version string
		commit  string
		short   string
	}
)

var logger = slog.New(log.New(os.Stderr))

type sbomFormat interface {
	Name() string
	Build(deps []renovate.Dependency) any
	Marshal(a any) ([]byte, error)
	Filename(f string) string
}

var sbomFormats = make(map[string]sbomFormat)

var rootCmd = &cobra.Command{
	Use:   "renovate-to-sbom 'path/to/*.json'",
	Short: "Convert Renovate data exports to SBOMs",
	Long: `Convert Renovate data exports to Software Bill of Materials (SBOMs)

Takes a data export from https://gitlab.com/tanna.dev/renovate-graph/ or the debug logs that come from Renovate (https://dmd.tanna.dev/cookbooks/consuming-renovate-debug-logs) and converts it to a Software Bill of Materials (SBOM).
	`,
	Example: `# to convert file(s) from renovate-graph's output:
renovate-to-sbom '../out/*.json'      --out-format spdx2.3+json
# to convert file(s) from Renovate's debug logs (https://dmd.tanna.dev/cookbooks/consuming-renovate-debug-logs):
renovate-to-sbom renovate.log         --out-format cyclonedx1.5+json
# to only include known pURL types, for instance if the consumer of this SBOM may be stricter on the types it supports
renovate-to-sbom renovate-output.json	--out-format cyclonedx1.5+json --only-include-known-purl-types
`,
	RunE: func(cmd *cobra.Command, args []string) error {
		format, found := sbomFormats[outFormat]
		if !found {
			return fmt.Errorf("unsupported SBOM output format. Please check --help")
		}

		pw := view.NewProgressWriter(os.Stdout, noProgress)
		go pw.Render()

		err := os.MkdirAll(outPath, 0700)
		if err != nil {
			return fmt.Errorf("failed to create outPath %s: %w", outPath, err)
		}

		return processFiles(args[0], pw, "out", format)
	},
}

func depVersionInfo(dep renovate.Dependency) string {
	if dep.CurrentVersion != nil {
		return *dep.CurrentVersion
	}
	return dep.Version
}

func processFiles(glob string, pw progress.Writer, outDir string, format sbomFormat) error {
	p := renovate.NewParser()

	files, err := filepath.Glob(glob)
	if err != nil {
		return err
	}

	if files == nil {
		return fmt.Errorf("no files could be found for glob %s", glob)
	}

	parserTracker := progress.Tracker{
		Message: "Parsing Renovate files",
		Total:   int64(len(files)),
	}
	pw.AppendTracker(&parserTracker)

	exportTracker := progress.Tracker{
		Message: "Creating SBOMs",
		Total:   int64(len(files)),
	}
	pw.AppendTracker(&exportTracker)

	go pw.Render()

	var wg sync.WaitGroup

	for _, f := range files {
		wg.Add(1)

		go func(f string) {
			defer wg.Done()

			deps, _, err := p.ParseFile(f)
			if err != nil {
				log.Printf("Failed to parse %s: %v", f, err)
				parserTracker.IncrementWithError(1)
				return
			}

			if len(deps) == 0 {
				log.Printf("No dependencies discovered for %s, not generating an SBOM", f)
				parserTracker.Increment(1)
				return
			}

			parserTracker.Increment(1)

			doc := format.Build(deps)
			data, err := format.Marshal(doc)
			if err != nil {
				log.Printf("Failed to marshal data for %s's document: %v", f, err)
				return
			}

			outFilename := path.Join(outPath, format.Filename(path.Base(f)))
			err = os.WriteFile(outFilename, data, 0600)
			if err != nil {
				log.Printf("Failed to write contents for %s's document: %v", f, err)
				exportTracker.IncrementWithError(1)
				return
			}

			exportTracker.Increment(1)
		}(f)
	}

	wg.Wait()
	parserTracker.UpdateMessage(fmt.Sprintf("Parsed %d Renovate files", parserTracker.Total))
	parserTracker.MarkAsDone()

	return nil
}

func Command() *cobra.Command {
	return rootCmd
}

func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func SetVersionInfo(version, commit, short string) {
	versionInfo.version = version
	versionInfo.commit = commit
	versionInfo.short = short

	rootCmd.Version = fmt.Sprintf("%s (Built from Git SHA %s)", versionInfo.version, versionInfo.commit)
}

func init() {
	rootCmd.Flags().StringVar(&outPath, "out-path", "out", "Path to output generated SBOMs to")
	rootCmd.Flags().StringVar(&outFormat, "out-format", "", "Output SBOM format. Supported: [spdx2.3+json, cyclonedx1.5+json]")
	rootCmd.Flags().BoolVar(&noProgress, "no-progress", false, "Whether to display progress bar while processing file(s)")
	rootCmd.Flags().BoolVar(&onlyIncludeKnownPurlTypes, "only-include-known-purl-types", false, "Whether to remove any dependencies from the resulting SBOMs if the Package URL (pURL) is not a known type according to the underlying pURL library")

	cobra.CheckErr(rootCmd.MarkFlagRequired("out-format"))
}
