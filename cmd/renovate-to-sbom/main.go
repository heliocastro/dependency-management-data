package main

import (
	"dmd.tanna.dev/cmd/renovate-to-sbom/cmd"

	"github.com/carlmjohnson/versioninfo"
)

func main() {
	cmd.SetVersionInfo(versioninfo.Version, versioninfo.Revision, versioninfo.Short())
	cmd.Execute()
}
