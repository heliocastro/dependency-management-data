package cmd

import (
	"database/sql"
	"embed"
	"net/http"
	"net/http/httputil"
	"sync"

	"dmd.tanna.dev/internal/graph"
	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"
)

// DMDServer is the Web Application for dependency-management-data (DMD)
type DMDServer struct {
	sqlDB      *sql.DB
	reports    map[string]Report
	httpServer *http.Server
	userConfig UserConfig
	graphql    struct {
		enabled bool
	}
	dbMetadataFunc func() map[string]string
}

//go:embed static/*
var staticDir embed.FS

func newServer(sqlDB *sql.DB, datasetteProxy *httputil.ReverseProxy, datasetteProxyPath string, userConfig UserConfig, enableGraphQLPlayground bool) DMDServer {
	mux := http.NewServeMux()

	server := DMDServer{
		sqlDB:      sqlDB,
		userConfig: userConfig,
	}
	server.graphql.enabled = true

	gql := handler.NewDefaultServer(graph.NewExecutableSchema(graph.Config{
		Resolvers: graph.NewResolver(sqlDB, logger),
	}))

	staticFs := http.FileServer(http.FS(staticDir))
	mux.HandleFunc("/static/", staticFs.ServeHTTP)

	mux.HandleFunc("/", server.handleRoot)
	mux.HandleFunc(datasetteProxyPath, datasetteProxy.ServeHTTP)
	mux.Handle("/graphql", gql)

	if server.graphql.enabled {
		mux.Handle("/graphql-playground", playground.Handler("GraphQL playground", "/graphql"))
	}

	mux.HandleFunc("/report", server.handleReportRoot)
	server.registerReport(mux,
		"advisories", "Advisories", "Report advisories that are available for packages or dependencies in use",
		server.handleReportAdvisories)
	server.registerReport(mux,
		"infrastructure-advisories", "Infrastructure Advisories", "Report advisories that are available for infrastructure components",
		server.handleReportInfrastructureAdvisories)
	server.registerReport(mux,
		"policy-violations", "Policy Violations", "Report violations of organisation-specific policies",
		server.handleReportPolicyviolations)
	server.registerReport(mux,
		"golangCILint", "golangci-lint", "Query usages of golangci-lint, tracked as a source-based dependency",
		server.handleReportGolangCILint)
	server.registerReport(mux,
		"mostPopularDockerImages", "Most Popular Docker Images", "Query the most popular Docker namespaces and images in use",
		server.handleReportMostPopularDockerImages)
	server.registerReport(mux,
		"mostPopularPackageManagers", "Most Popular Package Managers", "Query the most popular package managers in use",
		server.handleReportMostPopularPackageManagers)
	server.registerReport(mux,
		"licenses", "Licenses", "Report license information for package dependencies",
		server.handleReportLicenses)
	server.registerReport(mux,
		"dependenton", "Report usage of a given dependency", "Report usage of a given dependency, and optionally the specific version in use, across all known projects in the database.",
		server.handleReportDependenton)
	server.registerReport(mux,
		"libyear", "Report the Libyears metric for each repository", "Report how many Libyears behind the latest release repositories are",
		server.handleReportLibyear)
	server.registerReport(mux,
		"funding", "Report packages that are looking for funding", "Report packages that you depend on, who have indicated they are looking for financial support",
		server.handleReportFunding)

	server.httpServer = &http.Server{
		Handler: mux,
	}

	server.dbMetadataFunc = sync.OnceValue(func() map[string]string {
		// pre-load any metadata to avoid unnecessary hits to the DB
		meta := map[string]string{
			"Version":     getVersion(),
			"DBVersion":   getDBVersion(server.sqlDB),
			"FinalisedAt": getFinalisedAt(server.sqlDB),
		}

		if server.userConfig.DatabaseMeta.GenerationTime != "" {
			logger.Warn("The provided UserConfig has the `DatabaseMeta.GenerationTime` field set, which is now deprecated. Please use the `dmd db meta finalise` command to set this metadata when building the database, instead", "dbMetadataFinalisedAt", meta["FinalisedAt"], "userConfigDatabaseMetaGenerationTime", server.userConfig.DatabaseMeta.GenerationTime)

			if meta["FinalisedAt"] != "" {
				logger.Warn("The provided UserConfig has the `DatabaseMeta.GenerationTime` field set, as well as the database's `metadata.finalised_at` field being set. This is not recommended, and the `DatabaseMeta.GenerationTime` will be ignored in preference of `metadata.finalised_at`", "dbMetadataFinalisedAt", meta["FinalisedAt"], "userConfigDatabaseMetaGenerationTime", server.userConfig.DatabaseMeta.GenerationTime)
			}
		}

		return meta
	})

	return server
}

func (s *DMDServer) handleRoot(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" {
		http.Error(w, "Not found", http.StatusNotFound)
		return
	}

	data := map[string]any{}

	s.renderTemplate(w, r, data, "templates/pages/index.html.tmpl")
}
