package cmd

import (
	"context"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log/slog"
	"os"

	"dmd.tanna.dev/internal/metadata/db"
	"github.com/carlmjohnson/versioninfo"
	"github.com/charmbracelet/log"

	"github.com/spf13/cobra"
)

func getVersion() string {
	v := versioninfo.Short()
	if v == "" {
		return "(unknown)"
	}

	return v
}

func getDBVersion(sqlDB *sql.DB) string {
	q := db.New(sqlDB)
	data, err := q.RetrieveDMDVersion(context.Background())
	if errors.Is(err, sql.ErrNoRows) {
		logger.Warn("No `dmd_version` key was found in the metadata table - you may be interacting with a very old version version of a DMD database, or something has gone wrong when initialising it")
	} else if err != nil {
		logger.Error(fmt.Sprintf("Failed to look up DMD CLI version in the metadata table: %v", err))
		return ""
	}

	return data.Value
}

func getFinalisedAt(sqlDB *sql.DB) string {
	q := db.New(sqlDB)
	d, err := q.RetrieveFinalisedAt(context.Background())
	if errors.Is(err, sql.ErrNoRows) {
		return ""
	} else if err != nil {
		return ""
	}

	return d.Value
}

func newLogger(w io.Writer, envLogFormat string) *slog.Logger {
	if envLogFormat == "json" {
		return slog.New(slog.NewJSONHandler(w, nil))
	}

	return slog.New(log.New(w))
}

var logger = newLogger(os.Stderr, os.Getenv("LOG_FORMAT"))
var config Config
var userConfigPath string

type Config struct {
	databasePath string
	port         string
	datasette    DatasetteConfig
	userConfig   UserConfig
	graphql      struct {
		enablePlayground bool
	}
}

type DatasetteConfig struct {
	route     string
	extraArgs string
}

func Command() *cobra.Command {
	return rootCmd
}

var rootCmd = &cobra.Command{
	Use:   "dmd-web",
	Short: "The web frontend for dependency-management-data",
	RunE: func(cmd *cobra.Command, args []string) error {
		// trailing slash to allow Datasette's static file serving to work
		config.datasette.route = "/datasette/"

		if userConfigPath != "" {
			data, err := os.ReadFile(userConfigPath)
			if err != nil {
				return fmt.Errorf("Failed to open configuration from file %s: %w", userConfigPath, err)
			}
			err = json.Unmarshal(data, &config.userConfig)
			if err != nil {
				return fmt.Errorf("Failed to parse configuration from file %s as JSON: %w", userConfigPath, err)
			}
		}

		sqlDB, err := sql.Open("sqlite", config.databasePath)
		if err != nil {
			return err
		}

		datasetteCmd, datasetteProxy, err := runDatasette(cmd.Context(), config.databasePath, config.datasette)
		if err != nil {
			return err
		}

		go func() {
			err = datasetteCmd.Run()
			if err != nil {
				logger.Error(fmt.Sprintf("An error occured with Datasette: %v", err))
				// TODO: https://gitlab.com/tanna.dev/dependency-management-data/-/issues/264
			}
		}()

		server := newServer(sqlDB, datasetteProxy, config.datasette.route, config.userConfig, config.graphql.enablePlayground)
		server.httpServer.Addr = ":" + config.port

		logger.Info(fmt.Sprintf("dmd-web started on http://localhost:%s", config.port))

		err = server.httpServer.ListenAndServe()

		// TODO: https://gitlab.com/tanna.dev/dependency-management-data/-/issues/264

		if err != nil {
			return err
		}

		return nil
	},
}

func init() {
	rootCmd.Flags().StringVar(&config.databasePath, "db", "", "Path to the dependency-management-data created SQLite database")
	rootCmd.Flags().StringVar(&config.port, "port", "8080", "Local port to bind to")
	rootCmd.Flags().StringVar(&userConfigPath, "config", "", "Path to configuration.json file")
	rootCmd.Flags().StringVar(&config.datasette.extraArgs, "datasette-extra-args", "", "Extra arguments to be passed to the datasette command via https://docs.datasette.io/en/stable/settings.html#using-setting")
	rootCmd.Flags().BoolVar(&config.graphql.enablePlayground, "enable-playground", false, "Whether to enable the GraphQL playground")

	err := rootCmd.MarkFlagRequired("db")
	if err != nil {
		logger.Error(fmt.Sprintf("Failed to mark `db` as required %v", err))
	}
}
