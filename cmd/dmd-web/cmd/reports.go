package cmd

import (
	"fmt"
	"html/template"
	"net/http"

	"dmd.tanna.dev/internal/advisory"
	"dmd.tanna.dev/internal/datasources"
	"dmd.tanna.dev/internal/libyear"
	"dmd.tanna.dev/internal/licenses"
	"dmd.tanna.dev/internal/policies"
	"dmd.tanna.dev/internal/reports"
	"github.com/jedib0t/go-pretty/v6/table"
)

type Report struct {
	Name        string
	Description string
}

func (s *DMDServer) handleReportRoot(w http.ResponseWriter, r *http.Request) {
	data := map[string]any{
		"Reports": s.reports,
	}

	s.renderTemplate(w, r, data, "templates/pages/report-root.html.tmpl")
}

func (s *DMDServer) registerReport(mux *http.ServeMux, reportRoute string, reportName string, reportDescription string, handlerFunc http.HandlerFunc) {
	if s.reports == nil {
		s.reports = make(map[string]Report)
	}

	s.reports["/report/"+reportRoute] = Report{
		Name:        reportName,
		Description: reportDescription,
	}
	mux.HandleFunc("/report/"+reportRoute, handlerFunc)
}

func (s *DMDServer) handleReportAdvisories(w http.ResponseWriter, r *http.Request) {
	report := `<h2>Package advisories</h2>`

	showAll := r.URL.Query().Get("showAll")
	platform := r.URL.Query().Get("platform")
	organisation := r.URL.Query().Get("organisation")
	repo := r.URL.Query().Get("repo")
	owner := r.URL.Query().Get("owner")
	advisoryType := r.URL.Query().Get("advisoryType")

	if platform == "" && organisation == "" && repo == "" && owner == "" && advisoryType == "" {
		// if we've just hit /report/advisories then we should load just the summary, and note that if you want to see everything, you now have to opt-in, as it's quite intensive for the server especially if done unnecessarily

		if showAll == "true" {
			tw, err := advisory.ReportPackages(r.Context(), logger, s.sqlDB, "", "", "", "", "", false)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			report += tw.RenderHTML()
		} else {
			report += `
			<p>By default, <code>dmd-web</code> doesn't show all the advisories it knows about, as this can result in a rather large query + page load. It's recommended you use the filters above to tune the reported advisories as necessary.</p>

			<p>However, if you'd still like to load everything, you can <a href="/report/advisories?showAll=true">click here</a>.</p>

			<p>A summary of the advisories for all repos can be seen below:</p>
`

			tw, err := advisory.ReportPackages(r.Context(), logger, s.sqlDB, "", "", "", "", "", true)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			report += tw.RenderHTML()
		}
	} else {
		tw, err := advisory.ReportPackages(r.Context(), logger, s.sqlDB, platform, organisation, repo, owner, advisoryType, false)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		report += tw.RenderHTML()
	}

	data := map[string]any{
		"Data":   template.HTML(report),
		"Report": s.reports[r.URL.Path],
		"Opts": map[string]string{
			"ShowAll":      showAll,
			"Platform":     platform,
			"Organisation": organisation,
			"Repo":         repo,
			"Owner":        owner,
			"AdvisoryType": advisoryType,
		},
	}

	s.renderTemplate(w, r, data, "templates/pages/reports/advisories.html.tmpl")
}

func (s *DMDServer) handleReportInfrastructureAdvisories(w http.ResponseWriter, r *http.Request) {
	report := `<h2>AWS Infrastructure advisories</h2>`

	tw, err := advisory.ReportAWS(r.Context(), logger, s.sqlDB)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	report += tw.RenderHTML()

	data := map[string]any{
		"Data":   template.HTML(report),
		"Report": s.reports[r.URL.Path],
	}

	s.renderTemplate(w, r, data, "templates/pages/report.html.tmpl")
}

func (s *DMDServer) handleReportPolicyviolations(w http.ResponseWriter, r *http.Request) {
	report := `<h2>Policy violations</h2>`

	showAll := r.URL.Query().Get("showAll")
	platform := r.URL.Query().Get("platform")
	organisation := r.URL.Query().Get("organisation")
	repo := r.URL.Query().Get("repo")
	owner := r.URL.Query().Get("owner")
	level := r.URL.Query().Get("level")

	if platform == "" && organisation == "" && repo == "" && owner == "" {
		// if we've just hit /report/policies then we should load just the
		// summary, and note that if you want to see everything, you now have to
		// opt-in, as it's quite intensive for the server especially if done
		// unnecessarily

		if showAll == "true" {
			tw, err := policies.ReportPackages(r.Context(), logger, s.sqlDB, "", "", "", "", "")
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			report += tw.RenderHTML()
		} else {
			report += `
			<p>By default, <code>dmd-web</code> doesn't show all the policy violations it knows about, as this can result in a rather large query + page load. It's recommended you use the filters above to tune the reported policy violations as necessary.</p>

			<p>However, if you'd still like to load everything, you can <a href="/report/policy-violations?showAll=true">click here</a>.</p>
			`
		}

	} else {
		tw, err := policies.ReportPackages(r.Context(), logger, s.sqlDB, platform, organisation, repo, owner, level)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		report += tw.RenderHTML()
	}

	data := map[string]any{
		"Data":   template.HTML(report),
		"Report": s.reports[r.URL.Path],
		"Opts": map[string]string{
			"ShowAll":      showAll,
			"Platform":     platform,
			"Organisation": organisation,
			"Repo":         repo,
			"Owner":        owner,
		},
	}

	s.renderTemplate(w, r, data, "templates/pages/reports/policyviolations.html.tmpl")
}

func (s *DMDServer) handleReportGolangCILint(w http.ResponseWriter, r *http.Request) {
	report := ""

	results, err := datasources.QueryGolangCILint(r.Context(), s.sqlDB)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	for k, v := range results {
		report += `<h3>` + k + `</h3>`
		if len(v.Direct) == 0 && len(v.Indirect) == 0 {
			report += "No source-tracked dependencies on golangci-lint"
		} else {

			if len(v.Direct) == 0 {
				report += "No direct dependencies found on golangci-lint"
			} else {
				report += fmt.Sprintf("Direct dependencies found in %d repos\n", len(v.Direct))

				tw := table.NewWriter()

				tw.AppendHeader(table.Row{
					"Platform", "Organisation", "Repo", "Owner",
				})

				for _, dep := range v.Direct {
					tw.AppendRow(table.Row{
						dep.Platform, dep.Organisation, dep.Repo, stringPointerToString(dep.Owner),
					})
				}

				report += tw.RenderHTML()
			}

			if len(v.Indirect) == 0 {
				report += "No indirect dependencies found on golangci-lint"
			} else {
				report += fmt.Sprintf("Indirect dependencies found in %d repos\n", len(v.Indirect))

				tw := table.NewWriter()

				tw.AppendHeader(table.Row{
					"Platform", "Organisation", "Repo", "Owner",
				})

				for _, dep := range v.Indirect {
					tw.AppendRow(table.Row{
						dep.Platform, dep.Organisation, dep.Repo, stringPointerToString(dep.Owner),
					})
				}

				report += tw.RenderHTML()
			}
		}
	}

	data := map[string]any{
		"Data":   template.HTML(report),
		"Report": s.reports[r.URL.Path],
	}

	s.renderTemplate(w, r, data, "templates/pages/report.html.tmpl")
}

func (s *DMDServer) handleReportMostPopularDockerImages(w http.ResponseWriter, r *http.Request) {
	report := ``

	results, err := datasources.QueryMostPopularDockerImages(r.Context(), s.sqlDB)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	rowLimit := 10

	for k, v := range results {
		report += `<h3>` + k + `</h3>`

		registriesTw := table.NewWriter()
		registriesTw.AppendHeader(table.Row{
			"Registry", "#",
		})

		for i, count := range v.Registries {
			registriesTw.AppendRow(table.Row{
				count.Name, count.Count,
			})

			if i > rowLimit {
				break
			}
		}
		report += registriesTw.RenderHTML()

		namespacesTw := table.NewWriter()
		namespacesTw.AppendHeader(table.Row{
			"Namespace", "#",
		})

		for i, count := range v.Namespaces {
			namespacesTw.AppendRow(table.Row{
				count.Name, count.Count,
			})

			if i > rowLimit {
				break
			}
		}
		report += namespacesTw.RenderHTML()

		imagesTw := table.NewWriter()
		imagesTw.AppendHeader(table.Row{
			"Image", "#",
		})

		for i, count := range v.Images {
			imagesTw.AppendRow(table.Row{
				count.Name, count.Count,
			})

			if i > rowLimit {
				break
			}
		}
		report += imagesTw.RenderHTML()
	}

	data := map[string]any{
		"Data":   template.HTML(report),
		"Report": s.reports[r.URL.Path],
	}

	s.renderTemplate(w, r, data, "templates/pages/report.html.tmpl")
}

func (s *DMDServer) handleReportMostPopularPackageManagers(w http.ResponseWriter, r *http.Request) {
	report := ``

	results, err := datasources.QueryMostPopularPackageManagers(r.Context(), s.sqlDB)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	for k, v := range results {
		report += `<h3>` + k + `</h3>`
		tw := table.NewWriter()
		tw.AppendHeader(table.Row{
			"Package Manager", "#",
		})

		for _, row := range v {
			tw.AppendRow(table.Row{
				row.PackageManager, row.Count,
			})
		}
		report += tw.RenderHTML()
	}

	data := map[string]any{
		"Data":   template.HTML(report),
		"Report": s.reports[r.URL.Path],
	}

	s.renderTemplate(w, r, data, "templates/pages/report.html.tmpl")
}

func (s *DMDServer) handleReportLicenses(w http.ResponseWriter, r *http.Request) {
	report := `<h2>Package licenses</h2>`

	platform := r.URL.Query().Get("platform")
	organisation := r.URL.Query().Get("organisation")
	repo := r.URL.Query().Get("repo")
	owner := r.URL.Query().Get("owner")

	tw, err := licenses.ReportLicensingOverview(r.Context(), logger, s.sqlDB, platform, organisation, repo, owner)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	report += tw.RenderHTML()

	data := map[string]any{
		"Data":   template.HTML(report),
		"Report": s.reports[r.URL.Path],
		"Opts": map[string]string{
			"Platform":     platform,
			"Organisation": organisation,
			"Repo":         repo,
			"Owner":        owner,
		},
	}

	s.renderTemplate(w, r, data, "templates/pages/reports/licenses.html.tmpl")
}

func (s *DMDServer) handleReportDependenton(w http.ResponseWriter, r *http.Request) {
	report := `<h2>List repositories dependent on a given package</h2>`

	packageName := r.URL.Query().Get("packageName")
	packageManager := r.URL.Query().Get("packageManager")
	packageVersion := r.URL.Query().Get("packageVersion")
	packageCurrentVersion := r.URL.Query().Get("packageCurrentVersion")

	if packageName == "" || packageManager == "" {
		data := map[string]any{
			"Data":   template.HTML(report),
			"Report": s.reports[r.URL.Path],
			"Opts": map[string]string{
				"PackageName":           packageName,
				"PackageManager":        packageManager,
				"PackageVersion":        packageVersion,
				"PackageCurrentVersion": packageCurrentVersion,
			},
		}

		s.renderTemplate(w, r, data, "templates/pages/reports/dependenton.html.tmpl")
		return
	}

	tw, err := reports.DependentOn(r.Context(), logger, s.sqlDB, packageName, packageManager, packageVersion, packageCurrentVersion)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	report += tw.RenderHTML()

	data := map[string]any{
		"Data":   template.HTML(report),
		"Report": s.reports[r.URL.Path],
		"Opts": map[string]string{
			"PackageName":           packageName,
			"PackageManager":        packageManager,
			"PackageVersion":        packageVersion,
			"PackageCurrentVersion": packageCurrentVersion,
		},
	}

	s.renderTemplate(w, r, data, "templates/pages/reports/dependenton.html.tmpl")
}

func (s *DMDServer) handleReportLibyear(w http.ResponseWriter, r *http.Request) {
	report := ``

	platform := r.URL.Query().Get("platform")
	organisation := r.URL.Query().Get("organisation")
	repo := r.URL.Query().Get("repo")
	owner := r.URL.Query().Get("owner")

	tw, err := libyear.Report(r.Context(), logger, s.sqlDB, platform, organisation, repo, owner)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if tw == nil {
		if platform != "" || organisation != "" || repo != "" || owner != "" {
			report += `<p>No results were found based on that filter.</p>`
		} else {
			report += `<p>No results were found - have you run <code>dmd db generate libyear</code>?</p>`
		}
	} else {
		report += tw.RenderHTML()
	}

	data := map[string]any{
		"Data":   template.HTML(report),
		"Report": s.reports[r.URL.Path],
		"Opts": map[string]string{
			"Platform":     platform,
			"Organisation": organisation,
			"Repo":         repo,
			"Owner":        owner,
		},
	}

	s.renderTemplate(w, r, data, "templates/pages/reports/libyear.html.tmpl")
}

func (s *DMDServer) handleReportFunding(w http.ResponseWriter, r *http.Request) {
	report := ``

	tw, err := reports.Funding(r.Context(), logger, s.sqlDB)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if tw == nil {
		report += `<p>No results were found - have you run <code>dmd db generate dependency-health</code>? This could also be that there are none of your dependencies looking for financial support, or we just can't detect them</p>`
	} else {
		report += tw.RenderHTML()
	}

	data := map[string]any{
		"Data":   template.HTML(report),
		"Report": s.reports[r.URL.Path],
	}

	s.renderTemplate(w, r, data, "templates/pages/report.html.tmpl")
}

func stringPointerToString(s *string) string {
	if s == nil {
		return ""
	}
	return *s
}
