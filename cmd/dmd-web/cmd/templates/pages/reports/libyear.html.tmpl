{{ block "main" . }}

<h1>{{ .Report.Name }} </h1>

<p>{{ .Report.Description }}</p>

{{ if or
  (ne .Opts.Platform "")
  (ne .Opts.Organisation "")
  (ne .Opts.Repo "")
  (ne .Opts.Owner "")
}}
<div class="card hint">
  <p>You are currently filtering by the following querystring parameters:</p>

  <table>
    <tr>
      <th>
        Name
      </th>
      <th>
        Value
      </th>
    </tr>

    <tr>
      <td>
        <code>platform</code>
      </td>
      <td>
        {{ .Opts.Platform }}
      </td>
    </tr>

    <tr>
      <td>
        <code>organisation</code>
      </td>
      <td>
        {{ .Opts.Organisation }}
      </td>
    </tr>

    <tr>
      <td>
        <code>repo</code>
      </td>
      <td>
        {{ .Opts.Repo }}
      </td>
    </tr>

    <tr>
      <td>
        <code>owner</code>
      </td>
      <td>
        {{ .Opts.Owner }}
      </td>
    </tr>

  </table>
</div>
{{ end }}

<p>
  You can filter the data by using the querystring, for instance:
</p>

<form action="" method="get">
  <div class=row>
    <div class=col>
      <label for="platform">
        <code>platform</code>
      </label>
    </div>
    <div class=col>
      <input type="text" name="platform" id="platform" value="{{ .Opts.Platform }}"/>
    </div>
  </div>

  <div class=row>
    <div class=col>
      <label for="organisation">
        <code>organisation</code>
      </label>
    </div>
    <div class=col>
      <input type="text" name="organisation" id="organisation" value="{{ .Opts.Organisation }}"/>
    </div>
  </div>

  <div class=row>
    <div class=col>
      <label for="repo">
        <code>repo</code>
      </label>
    </div>
    <div class=col>
      <input type="text" name="repo" id="repo" value="{{ .Opts.Repo }}"/>
    </div>
  </div>

  <div class=row>
    <div class=col>
      <label for="owner">
        <code>owner</code>
      </label>
    </div>
    <div class=col>
      <input type="text" name="owner" id="owner" value="{{ .Opts.Owner }}"/>
    </div>
  </div>

  <div class=row>
    <div class=col>
    </div>
    <div class=col>
      <input type=submit>
    </div>
  </div>
</form>

<details>
  <summary>Examples</summary>

  <ul>
    <li>
      <a href="/report/libyear?platform=github">Repos on GitHub</a>
    </li>
    <li>
      <a href="/report/libyear?organisation=*tanna*">Repos with an owner that contains the word <code>tanna</code></a>
    </li>
    <li>
      <a href="/report/libyear?platform=gitlab&organisation=tanna.dev&repo=dependency-management-data&owner=">Report advisories for a specific repository, which includes details around how the Libyear data was derived.</a>
    </li>
  </ul>
</details>

{{ .Data }}

{{ end }}
