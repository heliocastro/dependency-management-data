package main

import (
	"os"

	"dmd.tanna.dev/cmd/dmd-graph/cmd"
	_ "modernc.org/sqlite"
)

func main() {
	err := cmd.Command().Execute()
	if err != nil {
		os.Exit(1)
	}
}
