package cmd

import (
	"database/sql"
	"fmt"
	"log/slog"
	"net/http"
	"os"

	"dmd.tanna.dev/internal/graph"
	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"
	"github.com/charmbracelet/log"
	"github.com/spf13/cobra"
)

var logger = slog.New(log.New(os.Stderr))
var config Config

type Config struct {
	databasePath     string
	port             string
	enablePlayground bool
}

func Command() *cobra.Command {
	return rootCmd
}

var rootCmd = &cobra.Command{
	Use:   "dmd-graph",
	Short: "The GraphQL API for dependency-management-data",
	RunE: func(cmd *cobra.Command, args []string) error {
		sqlDB, err := sql.Open("sqlite", config.databasePath)
		if err != nil {
			return err
		}

		mux := http.NewServeMux()
		srv := handler.NewDefaultServer(graph.NewExecutableSchema(graph.Config{
			Resolvers: graph.NewResolver(sqlDB, logger),
		}))

		if config.enablePlayground {
			mux.Handle("/", playground.Handler("GraphQL playground", "/graphql"))
		}
		mux.Handle("/graphql", srv)

		logger.Info(fmt.Sprintf("dmd-graph started on http://localhost:%s", config.port))
		if config.enablePlayground {
			logger.Info(fmt.Sprintf("The GraphQL playground can be found at http://localhost:%s", config.port))
		}

		return http.ListenAndServe(":"+config.port, mux)
	},
}

func init() {
	rootCmd.Flags().StringVar(&config.databasePath, "db", "", "Path to the dependency-management-data created SQLite database")
	rootCmd.Flags().StringVar(&config.port, "port", "8080", "Local port to bind to")
	rootCmd.Flags().BoolVar(&config.enablePlayground, "enable-playground", false, "Whether to enable the GraphQL playground")

	err := rootCmd.MarkFlagRequired("db")
	if err != nil {
		logger.Error(fmt.Sprintf("Failed to mark `db` as required %v", err))
	}
}
