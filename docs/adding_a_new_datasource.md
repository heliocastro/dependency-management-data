# Adding a new datasource

## What is a Datasource?

Check out `internal/datasources/doc.go`'s godoc.

## Adding one

- Choose a package that feels appropriate
  - i.e. `internal/datasources/sbom`
- Define the database schema
  - Database-related code goes into `internal/datasources/sbom/db`, and uses [SQLc](https://sqlc.dev) to compile SQL to Go code
- Define the underlying datamodel
  - `Dependency` must embed a `domain.Dependency`
  - Provide a means to convert a database row to a `Dependency`
- Create a `Parser` that can take a glob of files
  - This can be sped up by using Goroutines to parse each file in the glob individually
- Create an `Importer` that takes the parsed `Dependency`s and inserts them into the database
- Create a new subcommand for importing the data
  - i.e. if this is a datasource that is allowing importing Software Bill of Materials (SBOMs) then `cmd/dmd/cmd/import_sbom.go`
  - wire this in with the `Parser` and `Importer`
