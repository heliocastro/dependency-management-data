# Adding a new repository

## What is a repository?

Check out `internal/repositories/doc.go`'s godoc.

## Adding one

- Choose a package that feels appropriate
  - i.e. if this is a datasource that is allowing importing Software Bill of Materials (SBOMs) then `internal/datasources/sbom`
- Create an exported struct in that package
  - i.e. `type SBOM struct`
- Implement the interface `repositories.Repository`
  - Database-related code goes into `internal/datasources/sbom/db`, and uses [SQLc](https://sqlc.dev) to compile SQL to Go code
  - Ensure that there can be a `context.Context` passed into appropriate methods
- Add the exported struct to `repositories.Repositories`

Depending on how the data get seeded, you may also want to add a new command + subcommand(s) to seed the data correctly.

Ideally, the generation can be a one-time thing, that can be re-executed to update the data, instead of generating the data i.e. each time a query is ran. This saves a lot of wasted execution time if we're i.e. hitting a RESTful API to perform data lookup.
