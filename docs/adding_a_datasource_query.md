# Adding a new datasource query

## Using an existing query

For instance, let's say that we want to add support for `dmd report golangCILint` for a new datasource called `foo`.

To do this, we can check the command for that query, i.e. `cmd/dmd/cmd/report_golangcilint.go`, and find a call to `datasources.Query..`.

If we go to the implementation there, we'll see there is an interface that needs to be implemented by datasource, `foo.Foo`.

Once this interface is implemented, with the underlying database logic to perform this query, `foo`'s query should appear in the output of the report.

## Adding a new query

- Define the data model for the query in `internal/datasources/queries`
  - i.e. for a report called `mostPopularDockerImages`, this would be `internal/datasources/queries/most_popular_docker_images.go`
- Define the query interface + method in `internal/datasources/queries.go`
  - Note that there's both an interface and a method, to allow selectively implementing this as above
- Define the command
  - i.e. for a report called `mostPopularDockerImages`, create a command `cmd/dmd/cmd/report_mostpopulardockerimages.go` which renders the output
  - output should be formatted using `go-pretty/vX/table`, and include at a minimum the `organisation` and `repo`
- (Optionally) implement the interface for a given datasource
