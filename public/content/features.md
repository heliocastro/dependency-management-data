---
title: Features
---
<style>
th {
  text-align: center;
  text-justify: inter-word;
}

/* only the second+ rows, via https://stackoverflow.com/a/15675231 */
td + td {
  text-align: center;
  text-justify: inter-word;
}
</style>

Below you can find a list of features that dependency-management-data supports.

<div class="card hint" style="width: 90%">

<p>Looking for an in-depth example of how this data can be used?</p>

<p>Check out the <a href="/case-studies/">Case Studies</a>!</p>

</div>

## Tooling

dependency-management-data produces three key tools to use:

- [`dmd`](/commands/dmd/): manage the creation of the database, ingesting of [Datasources](/concepts/datasource/), enriching with data such as [Advisories](/concepts/advisory/) or [Policies](/concepts/policy/). Also allows [reporting](#reports) on the data available.
  - Intended to be interacted with by automation on a regular cadence to re-import dependencies, re-enrich data and then publish the SQLite database for use by other tooling
  - Intended to be interacted with by a human when they're exploring the data, or want to run reports on a local copy of the data
- [`dmd-web`](/commands/dmd-web/): a web frontend for dependency-management-data, providing access to the [reports](#reports), as well as [the GraphQL API](/graphql/). Includes the GraphQL API for consumption by other systems.
  - Intended to be interacted with by humans, could be behind your VPN/internal network where it is unauthenticated, or available on the public Internet but behind an authenticating reverse proxy like oauth2-proxy
- [`dmd-graph`](/commands/dmd-graph/): only [the GraphQL API](/graphql/), useful for integrating dependency-management-data into other systems without directly querying the database.
  - Intended to be interacted with automation, generally behind your VPN/internal network, optionally with authentication.

## Feature

A directory of the features that dependency-management-data supports across different [datasources](/concepts/datasource/).

### SQL database

The most important result of the use of dependency-management-data is the resulting SQLite database, which can then be used by any other tooling, or interacted with over [the GraphQL API](/graphql/).

The [full database schema](/schema/) is documented to make it easier to understand what the columns indicate, and there is a [cookbook for understanding the data model](/cookbooks/data-model/) to provide more insight into how to get started with the data.

It may be worth reading the [design decisions](/design-decisions/).

### Advisories

You can use dependency-management-data to get more insight into your dependencies using [Advisories](/concepts/advisory/), which indicates where dependencies are in use that maybe they shouldn't be.

When [generating advisories](/commands/dmd_db_generate_advisories/), a number of data sources are used to discover dependency insights:

- EndOfLife.date (https://endoflife.date)
- deps.dev (https://deps.dev)

Additionally, if you use [Dependency Health insights](#dependency-health-insights), this will also create Advisories.

<table>
<tr>
<th>
Feature
</th>
<th>
Renovate
</th>
<th>
Software Bill <br> of Materials <br> (SBOM)
</th>
<th>
AWS<br>Infrastructure
</th>
</tr>
<tr>
</tr>

<tr>
<td>
    Advisories
</td>
<td>
✅
</td>
<td>
✅
</td>
<td>
✅
</td>
</tr>

<tr>
<td>
    End-of-Life lookups, via <a href="https://endoflife.date">EndOfLife.date</a>
</td>
<td>
✅
</td>
<td>
✅
</td>
<td>
</td>
</tr>

<tr>
<td>
    Licensing + CVE Lookups, via <a href="https://deps.dev">deps.dev</a>
</td>
<td>
✅
</td>
<td>
✅
</td>
<td>
</td>
</tr>

</table>

#### End-of-Life checking

When performing End-of-Life checking for programming languages or popular packages that have a defined support window, via [endoflife.date](https://endoflife.date), the following lookups are currently supported:

<table>
<tr>
<th>
Product
</th>
<th>
Renovate
</th>
<th>
Software Bill <br> of Materials <br> (SBOM)
</th>
</tr>

<tr>
<td>
    <a href=https://endoflife.date/go>Go</a>
</td>
<td>
✅
</td>
<td>
</td>
</tr>

<tr>
<td>
    <a href=https://endoflife.date/alpine>Alpine</a>
</td>
<td>
✅
</td>
<td>
</td>
</tr>

<tr>
<td>
    <a href=https://endoflife.date/nodejs>NodeJS</a>
</td>
<td>
✅
</td>
<td>
</td>
</tr>

<tr>
<td>
    <a href=https://endoflife.date/python>Python</a>
</td>
<td>
✅
</td>
<td>
</td>
</tr>

<tr>
<td>
    <a href=https://endoflife.date/redis>Redis</a>
</td>
<td>
✅
</td>
<td>
</td>
</tr>

<tr>
<td>
    <a href=https://endoflife.date/ruby>Ruby</a>
</td>
<td>
✅
</td>
<td>
</td>
</tr>

<tr>
<td>
    <a href=https://endoflife.date/rails>Ruby on Rails</a>
</td>
<td>
✅
</td>
<td>
✅
</td>
</tr>

</table>

Is there a product that EndOfLife.date supports that is missing? Drop a comment into [the evergreen tracking issue](https://gitlab.com/tanna.dev/dependency-management-data/-/issues/67) or raise a fresh issue [on the issue tracker](https://gitlab.com/tanna.dev/dependency-management-data/-/issues) to request it.

#### CVE + license checking

When performing Advisory checks, dependency-management-data uses [deps.dev](https://deps.dev) to look up any known CVEs for packages as well as information about the license, if it can be detected.

<table>
<tr>
<th>
Ecosystem
</th>
<th>
Renovate
</th>
<th>
Software Bill <br> of Materials <br> (SBOM)
</th>
</tr>

<tr>
<td>
npm
</td>
<td>
✅
</td>
<td>
✅
</td>
</tr>

<tr>
<td>
    Go
</td>
<td>
✅
</td>
<td>
✅
</td>
</tr>

<tr>
<td>
    Maven
</td>
<td>
✅
</td>
<td>
✅
</td>
</tr>

<tr>
<td>
    PyPI
</td>
<td>
✅
</td>
<td>
✅
</td>
</tr>

<tr>
<td>
    NuGet
</td>
<td>
</td>
<td>
</td>
</tr>

<tr>
<td>
    Cargo
</td>
<td>
✅
</td>
<td>
</td>
</tr>

</table>

Is there an ecosystem that deps.dev supports that is missing? Drop a comment into [the evergreen tracking issue](https://gitlab.com/tanna.dev/dependency-management-data/-/issues/69) or raise a fresh issue [on the issue tracker](https://gitlab.com/tanna.dev/dependency-management-data/-/issues) to request it.

## Reports

[Reports](/concepts/report/) are available through the command-line application, [`dmd`](/commands/dmd/), as well as when running the [`dmd-web`](/commands/dmd-web/) web frontend.

The following datasources are supported for each of the reports:

<table>
<tr>
<th>
Report
</th>
<th>
Renovate
</th>
<th>
Software Bill <br> of Materials <br> (SBOM)
</th>
<th>
AWS <br> Infrastructure
</th>
</tr>
<tr>
</tr>

<tr>
<td>
<a href="/commands/dmd_report_advisories">advisories</a>
</td>
<td>
✅
</td>
<td>
✅
</td>
<td>
</td>
</tr>

<tr>
<td>
<a href="/commands/dmd_report_dependenton">dependenton</a>
</td>
<td>
✅
</td>
<td>
✅
</td>
<td>
</td>
</tr>

<tr>
<td>
<a href="/commands/dmd_report_funding">funding</a>
</td>
<td>
✅
</td>
<td>
✅
</td>
<td>
</td>
</tr>

<tr>
<td>
<a href="/commands/dmd_report_golangcilint">golangCILint</a>
</td>
<td>
✅
</td>
<td>
✅
</td>
<td>
</td>
</tr>

<tr>
<td>
<a href="/commands/dmd_report_infrastructure-advisories">infrastructure-advisories</a>
</td>
<td>
</td>
<td>
</td>
<td>
✅
</td>
</tr>

<tr>
<td>
<a href="/commands/dmd_report_libyear">libyear</a>
</td>
<td>
✅
</td>
<td>
✅
</td>
<td>
</td>
</tr>

<tr>
<td>
<a href="/commands/dmd_report_licenses">licenses</a>
</td>
<td>
✅
</td>
<td>
✅
</td>
<td>
</td>
</tr>

<tr>
<td>
<a href="/commands/dmd_report_mostpopulardockerimages">mostPopularDockerImages</a>
</td>
<td>
✅
</td>
<td>
✅
</td>
<td>
</td>
</tr>

<tr>
<td>
<a href="/commands/dmd_report_mostpopularpackagemanagers">mostPopularPackageManagers</a>
</td>
<td>
✅
</td>
<td>
✅
</td>
<td>
</td>
</tr>

<tr>
<td>
<a href="/commands/dmd_report_policy-violations">policy-violations</a>
</td>
<td>
✅
</td>
<td>
✅
</td>
<td>
</td>
</tr>

</table>

## Generating missing package data

In some cases, you may be using a datasource that doesn't always provide all the data about your full dependency tree, and so want to pick up on any data that may not be present.

To do this, you can [discover that missing data](/commands/dmd_db_generate_missing-data/) using [deps.dev](https://deps.dev).

Note that there are cases where this may not directly line up with the version in use in your project, as noted in blog [One set of requirements, zillions of SBOMs](https://blog.deps.dev/zillions-of-sboms/) by the Open Source Insights team.

The following package ecosystems are supported:

<table>
<tr>
<th>
Ecosystem
</th>
<th>
Renovate
</th>
<th>
Software Bill <br> of Materials <br> (SBOM)
</th>
</tr>

<tr>
<td>
npm
</td>
<td>
</td>
<td>
</td>
</tr>

<tr>
<td>
    Go
</td>
<td>
</td>
<td>
</td>
</tr>

<tr>
<td>
    Maven
</td>
<td>
✅
</td>
<td>
✅
</td>
</tr>

<tr>
<td>
    PyPI
</td>
<td>
</td>
<td>
</td>
</tr>

<tr>
<td>
    NuGet
</td>
<td>
</td>
<td>
</td>
</tr>

<tr>
<td>
    Cargo
</td>
<td>
</td>
<td>
</td>
</tr>

</table>

### Policy violations via Open Policy Agent

As well as generating Advisories through external sources, as well as [writing your own custom Advisories](/cookbooks/custom-advisories/), you can use [Open Policy Agent's Policy Language, Rego](https://www.openpolicyagent.org/docs/latest/policy-language/) to write much more complex advisories, also called [Policies](/concepts/policy/).

Full details of supported data that can be used with policy management can be found in the [_Turning complex policies into custom Advisories using Open Policy Agent_ cookbook](/cookbooks/custom-advisories-opa/#requirements).

<table>
<tr>
<th>
Feature
</th>
<th>
Renovate
</th>
<th>
Software Bill <br> of Materials <br> (SBOM)
</th>
</tr>
<tr>
</tr>

<tr>
<td>
    Policy Violations
</td>
<td>
✅
</td>
<td>
✅
</td>
</tr>

</table>

### Dependency Health insights

It is possible to [generate insights into your dependencies' health](/commands/dmd_db_generate_dependency-health/) to gain additional information and data around your use of specific dependencies.

<!-- copied from /commands/dmd_db_generate_dependency-health/ -->

This consumes data from different sources to augment the understanding of dependencies in use, for instance giving an indication of whether they are (well) maintained, have been recently released, or may have supply chain hygiene issues.

Currently, this data is derived from:

- OpenSSF Security Scorecards (https://api.securityscorecards.dev/)
- Ecosystems (https://ecosyste.ms)

This data is a best-efforts attempt to provide this insight, and may be stale at the time of fetching.

<!-- </end copied from /commands/dmd_db_generate_dependency-health/ -->

Known issues:

- [Performance issues and 500s upstream](https://gitlab.com/tanna.dev/dependency-management-data/-/issues/459)

<table>
<tr>
<th>
Feature
</th>
<th>
Renovate
</th>
<th>
Software Bill <br> of Materials <br> (SBOM)
</th>
</tr>
<tr>
</tr>

<tr>
<td>
    Dependency Health
</td>
<td>
✅
</td>
<td>
✅
</td>
</tr>

</table>


### Libyear calculations

It is possible to [generate insights into dependencies' libyear metrics](/commands/dmd_db_generate_libyear/) to gain additional information and data around the age of your dependencies.

<!-- copied from /commands/dmd_db_generate_libyear/ -->

This uses the Libyear metric, which is defined as "how many years between the version we're currently using and the latest version released", and then totalled across all libraries used by the project.

NOTE: that this may not include all dependencies, so the number could be higher than shown.

For further reading on the Libyear metric, check out https://libyear.com/ and https://chaoss.community/kb/metric-libyears/ for more information.

<!-- </end copied from /commands/dmd_db_generate_libyear/ -->

<table>
<tr>
<th>
Feature
</th>
<th>
Renovate
</th>
<th>
Software Bill <br> of Materials <br> (SBOM)
</th>
</tr>
<tr>
</tr>

<tr>
<td>
    Libyear metrics
</td>
<td>
✅
</td>
<td>
✅
</td>
</tr>

</table>

Known issues:

- [Performance issues and 500s upstream](https://gitlab.com/tanna.dev/dependency-management-data/-/issues/459)

### Ownership

It is possible to store ownership information for the given [Repository Key](/concepts/repo-key/), allowing you to ask questions like "which team should I get in touch with about this unmaintained dependency".

For more information, check out the [cookbook for using repository ownership information](/cookbooks/ownership/) and the following Case Studies:

<ul>

<li>
<a href="/case-studies/library-spread-oapi-codegen/">Elastic and understanding the spread of versions of `oapi-codegen`</a>
</li>

<li>
<a href="/case-studies/deliveroo-kafka-sidecar/">Deliveroo and a potential race condition with a Kafka sidecar</a>
</li>

<li>
<a href="/case-studies/log4shell/">Responding to the Log4shell incident</a>
</li>

</ul>

### Repository metadata

In addition to storing ownership information, it is also possible to to store additional metadata around a source repository, such as whether it's a monorepo, or how critical it is to the business.

For more information, check out the following Case Studies:

<ul>

<li>
<a href="/case-studies/deliveroo-kafka-sidecar/">Deliveroo and a potential race condition with a Kafka sidecar</a>
</li>

<li>
<a href="/case-studies/log4shell/">Responding to the Log4shell incident</a>
</li>

</ul>
