---
title: Components
---
DMD consists of several components:

- [`dmd` CLI](/commands/dmd/)
- [`dmd-web` web application](/web/)
- [`dmd-graph` GraphQL API](/commands/dmd-graph/) and associated [GraphQL API schema](/graphql/)
- [Database schema for the underlying SQLite database](/schema/)
- [-contrib repository](https://gitlab.com/tanna.dev/dependency-management-data-contrib) which allows community-sourced configuration and snippets
- [Example repository](https://gitlab.com/tanna.dev/dependency-management-data-example/) which is deployed to [dependency-management-data-example.fly.dev](https://dependency-management-data-example.fly.dev/)

Also worth reading:

- [Design decisions](/design-decisions/)
