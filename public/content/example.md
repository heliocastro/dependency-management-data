---
title: Example
asciinema: true
---
<div class="card hint">

<p>Looking for an in-depth example of how this data can be used?</p>

<p>Check out the <a href="/case-studies/">Case Studies</a>!</p>

</div>

Below you can find screencasts of [the dependency-management-data CLI, `dmd`](/commands/dmd/) and some of the features built into it.

The below examples are based on [the example repo on GitLab.com](https://gitlab.com/tanna.dev/dependency-management-data-example/).

You can also check out the DMD web application which corresponds with this data at [dependency-management-data-example.fly.dev](https://dependency-management-data-example.fly.dev/).

It's recommended to have a look at the repo and have a play with the data, but an example of the different things you can do with the project can be found below.

## Initialise database and import datasources

{{< asciinema id="initAndImport" >}}

## Set ownership information

{{< asciinema id="setOwners" >}}

## Report for usages of golangci-lint which are tracked as a source dependency

{{< asciinema id="reportGolangCILint" >}}

## Report the most popular Docker namespaces and images

{{< asciinema id="mostPopularDockerImages" >}}

## Report the most popular package managers

{{< asciinema id="mostPopularPackageManagers" >}}

## Report package advisories

Includes custom advisories ("this package is deprecated", "our organisation doesn't want to use that library") as well as End-of-Life (EOL) checking

{{< asciinema id="generateAdvisory" >}}

Additionally, through the `report advisories` command (There's a lot to display here, so it disappears pretty quickly 😅):

{{< asciinema id="reportAdvisories" >}}

## Report infrastructure advisories

{{< asciinema id="reportInfrastructureAdvisories" >}}

## Report license information for package dependencies

{{< asciinema id="reportLicenses" >}}

## Perform a policy evaluation

{{< asciinema id="evaluatePolicy" >}}

## Generate policy evaluations + report them

{{< asciinema id="generatePolicyViolations" >}}

## Report usages of a specific dependency

{{< asciinema id="reportDependenton" >}}

## Report packages looking for financial funding

{{< asciinema id="reportFunding" >}}

## Report Libyears

For more detail on Libyears, check out [the docs for `dmd report libyear`](/commands/dmd_report_libyear/).

{{< asciinema id="reportLibyear" >}}

## Interact with the database's internal metadata

{{< asciinema id="metadata" >}}
