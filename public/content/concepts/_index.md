---
title: Concepts
---
Within dependency-management-data, there are certain names we give different concepts.

You can find their brief description below, and there is more information available on their linked pages.
