---
title: Policy
description: A way to flag organisational restrictions around the use of certain software.
---
A Policy is very similar to the [Advisory](/concepts/advisory/) concept, but is _generally_ more oriented towards organisational policies around usage of software, opposed to general use-cases like "this software has security vulnerabilities" or "this software is no longer maintained".

For instance "our organisation has a policy of not using Open Source produced by our competitors", or "our organisation has a policy of being at most one major version behind the current release".
