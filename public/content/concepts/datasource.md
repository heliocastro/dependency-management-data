---
title: Datasource
description: A source of data that can be queried by dependency-management-data.
---
Currently, we have the following datasources:

## Package data

### Renovate

- Produced by: [Renovate](https://docs.renovatebot.com/) either via [renovate-graph](https://gitlab.com/tanna.dev/renovate-graph/) or [via Renovate's debug logs](/cookbooks/consuming-renovate-debug-logs)
- Imported using: [`dmd import renovate`](/commands/dmd_import_renovate/)
- [Database schema](/schema/#internaldatasourcesrenovatedbschemasql)

### Software Bill of Materials (SBOMs)

- Produced by: Various Software Composition Analysis (SCA) tools, such as Snyk, or via [dependabot-graph](https://gitlab.com/tanna.dev/dependabot-graph/)
- Imported using: [`dmd import sbom`](/commands/dmd_import_sbom/)
- [Database schema](/schema/#internaldatasourcessbomdbschemasql)

The following SBOM formats are supported:

- CycloneDX v1.4 (JSON, XML)
- CycloneDX v1.5 (JSON, XML)
- SPDX v2.2 (JSON, YAML)
- SPDX v2.3 (JSON, YAML)

Is there an SBOM format missing? Drop a comment into [the evergreen tracking issue](https://gitlab.com/tanna.dev/dependency-management-data/-/issues/167) or raise a fresh issue [on the issue tracker](https://gitlab.com/tanna.dev/dependency-management-data/-/issues) to request it.

## AWS Infrastructure

### AWS Elasticache

- Produced by: [endoflife-checker](https://gitlab.com/tanna.dev/endoflife-checker)
- Imported using: [`dmd import awselasticache`](/commands/dmd_import_awslambda/)
- [Database schema](/schema/#internaldatasourcesawselasticachedbschemasql)

### AWS Lambda

- Produced by: [endoflife-checker](https://gitlab.com/tanna.dev/endoflife-checker)
- Imported using: [`dmd import awslambda`](/commands/dmd_import_awslambda/)
- [Database schema](/schema/#internaldatasourcesawslambdadbschemasql)

### AWS RDS

- Produced by: [endoflife-checker](https://gitlab.com/tanna.dev/endoflife-checker)
- Imported using: [`dmd import awsrds`](/commands/dmd_import_awsrds/)
- [Database schema](/schema/#internaldatasourcesawsrdsdbschemasql)
