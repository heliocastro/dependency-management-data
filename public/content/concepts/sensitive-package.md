---
title: Sensitive Package
description: "A way to mark a package as 'sensitive', such that the package's details should not be processed through any external systems."
---
For instance, if your organisation has a package or repository that has the name `my-org/plain-text-secrets`, it may be materially damaging if that name was known. Alternatively if you had a project called `my-org/custom-encryption-algorithm`, it may be of more interest to a bad actor who may want to try and attack that project or package.

To prevent this leakage, any package names deemed a Sensitive Package will not be processed externally through any of dependency-management-data's external calls.
