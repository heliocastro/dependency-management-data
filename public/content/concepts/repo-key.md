---
title: Repository Key
description: A unique identifier to trace a source repository, aka the "Repo Key".
---
The Repository Key is an identifier that is used across the data model to indicate the source repository for i.e. a given dependency.

It is a three-part key containing the following segments:

## Platform

The platform that hosts the source code.

For example:

- `https://github.com/deepmap/oapi-codegen` would have `platform: github` or `platform: github.com` (if you also had a self-hosted GitHub Enterprise)
- `https://gitlab.com/gitlab-org/sbom/reports/` would have `platform: gitlab` or `platform: gitlab.com` (if you interact with multiple GitLab servers)

In the case your organisation only has code on one `platform`, you can ignore it when querying the data.

## Organisation

The organisation that the code sits within, which can include nested organisations.

For example:

- `https://github.com/deepmap/oapi-codegen` would have `organisation: deepmap`
- `https://gitlab.com/gitlab-org/sbom/reports/` would have `organisation: gitlab-org/sbom`

In the case your organisation only has code under one `organisation`, you can ignore it when querying the data.

## Repo

The repository name.

For example:

- `https://github.com/deepmap/oapi-codegen` would have `repo: oapi-codegen`
- `https://gitlab.com/gitlab-org/sbom/reports` would have `repo: reports`
