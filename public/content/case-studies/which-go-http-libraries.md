---
title: What HTTP routers and web frameworks do our (Go) projects use?
description: How you could understand the different dependencies teams are using for their Go HTTP services.
---
## Context

As a team working on building internal tooling and libraries to support teams building Go services, for instance as part of a Go tooling or Developer Experience team, you may be interested in understanding which libraries teams are using.

In the aftermath of the [the Gorilla Toolkit archiving](/case-studies/gorilla-toolkit/), Deliveroo used the data within dependency-management-data to discover which HTTP libraries teams were using.

From here, they were able to understand any gaps with their libraries and tooling, as well as cases where there were a few services that were using routers that were no internally longer recommended.

## Problem

List the number of projects that are directly using:

- [Gin web framework](https://github.com/gin-gonic/gin)
- [Echo web framework](https://github.com/labstack/echo)
- [fasthttp router](https://github.com/valyala/fasthttp)
- [gorilla/mux router](https://github.com/gorilla/mux)
- [Chi router](https://github.com/go-chi/chi)
- [julienschmidt/httprouter router](https://github.com/julienschmidt/httprouter)
- [Fiber web framework](https://github.com/gofiber/fiber)
- [Iris web framework](https://github.com/kataras/iris)

_Note_ that this list is non-exhaustive of all possible routers or web frameworks that the Go ecosystem produces, but is a good look at some of the most popular.

## Data

For instance, if we had the following data in our `renovate` table:

| platform |  organisation   |             repo             |            package_name             |                version                |            current_version            | package_manager |      package_file_path       | datasource |  dep_types   |
|----------|-----------------|------------------------------|-------------------------------------|---------------------------------------|---------------------------------------|-----------------|------------------------------|------------|--------------|
| github   | deepmap         | oapi-codegen                 | github.com/gin-gonic/gin            | v1.9.1                                | v1.9.1                                | gomod           | examples/go.mod              | go         | ["require"]  |
| github   | deepmap         | oapi-codegen                 | github.com/go-chi/chi/v5            | v5.0.10                               | v5.0.10                               | gomod           | examples/go.mod              | go         | ["require"]  |
| github   | deepmap         | oapi-codegen                 | github.com/gofiber/fiber/v2         | v2.49.1                               | v2.49.1                               | gomod           | examples/go.mod              | go         | ["require"]  |
| github   | deepmap         | oapi-codegen                 | github.com/gorilla/mux              | v1.8.0                                | v1.8.0                                | gomod           | examples/go.mod              | go         | ["require"]  |
| github   | deepmap         | oapi-codegen                 | github.com/kataras/iris/v12         | v12.2.6-0.20230908161203-24ba4e8933b9 | v12.2.6-0.20230908161203-24ba4e8933b9 | gomod           | examples/go.mod              | go         | ["require"]  |
| github   | deepmap         | oapi-codegen                 | github.com/labstack/echo/v4         | v4.11.3                               | v4.11.3                               | gomod           | examples/go.mod              | go         | ["require"]  |
| github   | deepmap         | oapi-codegen                 | github.com/valyala/fasthttp         | v1.49.0                               | v1.49.0                               | gomod           | examples/go.mod              | go         | ["indirect"] |
| github   | deepmap         | oapi-codegen                 | github.com/gin-gonic/gin            | v1.9.1                                | v1.9.1                                | gomod           | internal/test/go.mod         | go         | ["require"]  |
| github   | deepmap         | oapi-codegen                 | github.com/go-chi/chi/v5            | v5.0.10                               | v5.0.10                               | gomod           | internal/test/go.mod         | go         | ["require"]  |
| github   | deepmap         | oapi-codegen                 | github.com/gofiber/fiber/v2         | v2.49.1                               | v2.49.1                               | gomod           | internal/test/go.mod         | go         | ["require"]  |
| github   | deepmap         | oapi-codegen                 | github.com/gorilla/mux              | v1.8.0                                | v1.8.0                                | gomod           | internal/test/go.mod         | go         | ["require"]  |
| github   | deepmap         | oapi-codegen                 | github.com/kataras/iris/v12         | v12.2.6-0.20230908161203-24ba4e8933b9 | v12.2.6-0.20230908161203-24ba4e8933b9 | gomod           | internal/test/go.mod         | go         | ["require"]  |
| github   | deepmap         | oapi-codegen                 | github.com/labstack/echo/v4         | v4.11.3                               | v4.11.3                               | gomod           | internal/test/go.mod         | go         | ["require"]  |
| github   | elastic         | beats                        | github.com/gorilla/mux              | v1.8.0                                | v1.8.0                                | gomod           | go.mod                       | go         | ["require"]  |
| github   | elastic         | go-elasticsearch             | github.com/valyala/fasthttp         | v1.34.0                               | v1.34.0                               | gomod           | _examples/fasthttp/go.mod    | go         | ["require"]  |
| github   | gravitational   | teleport                     | github.com/julienschmidt/httprouter | v1.3.0                                | v1.3.0                                | gomod           | go.mod                       | go         | ["require"]  |
| github   | jamietanna      | oapi-codegen-private         | github.com/gin-gonic/gin            | v1.8.1                                | v1.8.1                                | gomod           | go.mod                       | go         | ["require"]  |
| github   | jamietanna      | oapi-codegen-private         | github.com/go-chi/chi/v5            | v5.0.7                                | v5.0.7                                | gomod           | go.mod                       | go         | ["require"]  |
| github   | jamietanna      | oapi-codegen-private         | github.com/gorilla/mux              | v1.8.0                                | v1.8.0                                | gomod           | go.mod                       | go         | ["require"]  |
| github   | jamietanna      | oapi-codegen-private         | github.com/labstack/echo/v4         | v4.9.1                                | v4.9.1                                | gomod           | go.mod                       | go         | ["require"]  |
| github   | oapi-codegen    | echo-middleware              | github.com/labstack/echo/v4         | v4.11.1                               | v4.11.1                               | gomod           | go.mod                       | go         | ["require"]  |
| github   | oapi-codegen    | fiber-middleware             | github.com/gofiber/fiber/v2         | v2.49.1                               | v2.49.1                               | gomod           | go.mod                       | go         | ["require"]  |
| github   | oapi-codegen    | gin-middleware               | github.com/gin-gonic/gin            | v1.9.1                                | v1.9.1                                | gomod           | go.mod                       | go         | ["require"]  |
| github   | oapi-codegen    | iris-middleware              | github.com/kataras/iris/v12         | v12.2.6-0.20230908161203-24ba4e8933b9 | v12.2.6-0.20230908161203-24ba4e8933b9 | gomod           | go.mod                       | go         | ["require"]  |
| github   | oapi-codegen    | nethttp-middleware           | github.com/go-chi/chi/v5            | v5.0.10                               | v5.0.10                               | gomod           | internal/test/chi/go.mod     | go         | ["require"]  |

## Query

This allows us to write the following query:

```sql
select
  (
    select
      count(*)
    from
      renovate
    where
      (
        package_name = 'github.com/gin-gonic/gin'
        or package_name like 'github.com/gin-gonic/gin/%'
      )
      and dep_types = '["require"]'
  ) as gin,
  (
    select
      count(*)
    from
      renovate
    where
      (
        package_name = 'github.com/labstack/echo'
        or package_name like 'github.com/labstack/echo/%'
      )
      and dep_types = '["require"]'
  ) as echo,
  (
    select
      count(*)
    from
      renovate
    where
      (
        package_name = 'github.com/valyala/fasthttp'
        or package_name like 'github.com/valyala/fasthttp/%'
      )
      and dep_types = '["require"]'
  ) as fasthttp,
  (
    select
      count(*)
    from
      renovate
    where
      (
        package_name = 'github.com/gorilla/mux'
        or package_name like 'github.com/gorilla/mux/%'
      )
      and dep_types = '["require"]'
  ) as gorillamux,
  (
    select
      count(*)
    from
      renovate
    where
      (
        package_name = 'github.com/go-chi/chi'
        or package_name like 'github.com/go-chi/chi/%'
      )
      and dep_types = '["require"]'
  ) as chi,
  (
    select
      count(*)
    from
      renovate
    where
      (
        package_name = 'github.com/julienschmidt/httprouter'
        or package_name like 'github.com/julienschmidt/httprouter/%'
      )
      and dep_types = '["require"]'
  ) as httprouter,
  (
    select
      count(*)
    from
      renovate
    where
      (
        package_name = 'github.com/gofiber/fiber'
        or package_name like 'github.com/gofiber/fiber/%'
      )
      and dep_types = '["require"]'
  ) as fiber,
  (
    select
      count(*)
    from
      renovate
    where
      (
        package_name = 'github.com/kataras/iris'
        or package_name like 'github.com/kataras/iris/%'
      )
      and dep_types = '["require"]'
  ) as iris
```

Based on the above query, we receive the following data:

| gin | echo | fasthttp | gorillamux | chi | httprouter | fiber | iris |
|-----|------|----------|------------|-----|------------|-------|------|
| 5   | 5    | 2        | 7          | 5   | 1          | 3     | 4    |
