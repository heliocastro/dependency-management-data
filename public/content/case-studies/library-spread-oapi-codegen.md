---
title: Elastic and understanding the spread of versions of `oapi-codegen`
description: How Elastic were able to easily discover what versions of the `oapi-codegen` OpenAPI-to-Go code generator was used across the organisation.
---
## Context

At Elastic, there are several versions of the OpenAPI-to-Go code generator, [oapi-codegen](https://github.com/deepmap/oapi-codegen/) in use.

Jamie Tanna is a maintainer of oapi-codegen, and as part of joining the company, Jamie wanted to see how Elastic was using different versions of oapi-codegen.

This was also important to know due to ongoing work that Jamie was putting together for [the upcoming v2 release](https://github.com/deepmap/oapi-codegen/discussions/1309) of the project, which introduced minimally breaking changes, and would require action from teams to upgrade.

## Problem

- What versions of `oapi-codegen` is in use?
- How many of each version is in use?

Additionally:

- Who are the owners of each service using those versions?

(Note that this exact process has also been used for other internal libraries)

## Data

We have the following data in the `renovate` table:

| platform | organisation |                 repo                  |            package_name            |                version                |            current_version            | package_manager |               package_file_path               | datasource |  dep_types  |
|----------|--------------|---------------------------------------|------------------------------------|---------------------------------------|---------------------------------------|-----------------|-----------------------------------------------|------------|-------------|
| github   | deepmap      | oapi-codegen                          | github.com/deepmap/oapi-codegen/v2 | v2.0.0-00010101000000-000000000000    | v2.0.0-00010101000000-000000000000    | gomod           | examples/go.mod                               | go         | ["require"] |
| github   | deepmap      | oapi-codegen                          | github.com/deepmap/oapi-codegen/v2 | v2.0.0-00010101000000-000000000000    | v2.0.0-00010101000000-000000000000    | gomod           | internal/test/go.mod                          | go         | ["require"] |
| github   | incident-io  | catalog-importer                      | github.com/deepmap/oapi-codegen    | v1.12.4                               | v1.12.4                               | gomod           | go.mod                                        | go         | ["require"] |
| github   | incident-io  | singer-tap                            | github.com/deepmap/oapi-codegen    | v1.12.4                               | v1.12.4                               | gomod           | go.mod                                        | go         | ["require"] |
| github   | incident-io  | terraform-provider-incident           | github.com/deepmap/oapi-codegen    | v1.12.4                               | v1.12.4                               | gomod           | go.mod                                        | go         | ["require"] |
| github   | jamietanna   | renovate-repro-local-no-requireconfig | github.com/deepmap/oapi-codegen    | v1.14.0                               | v1.14.0                               | gomod           | go.mod                                        | go         | ["require"] |
| github   | jamietanna   | renovate-repro-local-onboarding       | github.com/deepmap/oapi-codegen    | v1.14.0                               | v1.14.0                               | gomod           | go.mod                                        | go         | ["require"] |
| gitlab   | jamietanna   | dotfiles-arch                         | github.com/deepmap/oapi-codegen    | v1.11.1-0.20220629212257-2cf7fcf5b26d | v1.11.1-0.20220629212257-2cf7fcf5b26d | gomod           | go/home/go/src/jvt.me/dotfiles/shorten/go.mod | go         | ["require"] |
| gitlab   | jamietanna   | oapi-codegen-example-project          | github.com/deepmap/oapi-codegen    | v1.12.4                               | v1.12.4                               | gomod           | go.mod                                        | go         | ["require"] |
| gitlab   | tanna.dev    | dependency-management-data            | github.com/deepmap/oapi-codegen/v2 | v2.0.0                                | v2.0.0                                | gomod           | go.mod                                        | go         | ["require"] |
| gitlab   | tanna.dev    | dmd-go-semver                         | github.com/deepmap/oapi-codegen    | v1.12.4                               | v1.12.4                               | gomod           | go.mod                                        | go         | ["require"] |
| gitlab   | tanna.dev    | endoflife-checker                     | github.com/deepmap/oapi-codegen    | v1.12.4                               | v1.12.4                               | gomod           | go.mod                                        | go         | ["require"] |

And the following data in the `owners` table:

| platform | organisation |                 repo                  |    owner    | notes |      updated_at      |
|----------|--------------|---------------------------------------|-------------|-------|----------------------|
| gitlab   | tanna.dev    | dependency-management-data            | Jamie Tanna |       | 2024-01-18T10:54:05Z |
| gitlab   | tanna.dev    | dmd-go-semver                         | Jamie Tanna |       | 2024-01-18T10:54:05Z |
| gitlab   | tanna.dev    | endoflife-checker                     | Jamie Tanna |       | 2024-01-18T10:54:05Z |
| github   | jamietanna   | renovate-repro-local-no-requireconfig | Jamie Tanna |       | 2024-01-18T10:54:05Z |
| github   | jamietanna   | renovate-repro-local-onboarding       | Jamie Tanna |       | 2024-01-18T10:54:05Z |
| gitlab   | jamietanna   | dotfiles-arch                         | Jamie Tanna |       | 2024-01-18T10:54:05Z |
| gitlab   | jamietanna   | oapi-codegen-example-project          | Jamie Tanna |       | 2024-01-18T10:54:05Z |
| github   | incident-io  | catalog-importer                      | Incident.io |       | 2024-01-21T11:44:11Z |
| github   | incident-io  | singer-tap                            | Incident.io |       | 2024-01-21T11:44:11Z |
| github   | incident-io  | terraform-provider-incident           | Incident.io |       | 2024-01-21T11:44:11Z |

## Query

To list the spread of versions, we could write the following query:

```sql
select
  package_name,
  version,
  count(*)
from
  renovate
where
  package_name in (
    'github.com/deepmap/oapi-codegen',
    -- due to the way that Go modules work
    'github.com/deepmap/oapi-codegen/v2'
  )
group by
  package_name, version
order by
  version asc
```

This would produce the following data:

|            package_name            |                version                | count(*) |
|------------------------------------|---------------------------------------|----------|
| github.com/deepmap/oapi-codegen    | v1.11.1-0.20220629212257-2cf7fcf5b26d | 1        |
| github.com/deepmap/oapi-codegen    | v1.12.4                               | 6        |
| github.com/deepmap/oapi-codegen    | v1.14.0                               | 2        |
| github.com/deepmap/oapi-codegen/v2 | v2.0.0                                | 1        |
| github.com/deepmap/oapi-codegen/v2 | v2.0.0-00010101000000-000000000000    | 2        |

Then, we can also add in ownership information like so:

```sql
select
  package_name,
  version,
  count(*),
  group_concat(distinct owner) as owners
from
  renovate
  left join owners on renovate.platform = owners.platform
  and renovate.organisation = owners.organisation
  and renovate.repo = owners.repo
where
  package_name in (
    'github.com/deepmap/oapi-codegen',
    -- due to the way that Go modules work
    'github.com/deepmap/oapi-codegen/v2'
  )
group by
  package_name, version
order by
  version asc
```

Which would produce the following data:

|            package_name            |                version                | count(*) |         owners          |
|------------------------------------|---------------------------------------|----------|-------------------------|
| github.com/deepmap/oapi-codegen    | v1.11.1-0.20220629212257-2cf7fcf5b26d | 1        | Jamie Tanna             |
| github.com/deepmap/oapi-codegen    | v1.12.4                               | 6        | Incident.io,Jamie Tanna |
| github.com/deepmap/oapi-codegen    | v1.14.0                               | 2        | Jamie Tanna             |
| github.com/deepmap/oapi-codegen/v2 | v2.0.0                                | 1        | Jamie Tanna             |
| github.com/deepmap/oapi-codegen/v2 | v2.0.0-00010101000000-000000000000    | 2        |                         |
