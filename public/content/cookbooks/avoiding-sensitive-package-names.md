---
title: Avoiding the leakage of sensitive package names
description: How to use the `sensitive_packages` table to reduce the risk of leaking private package names to external systems.
weight: 20
tags:
- howto
---
When using [subcommands from `dmb db generate`](/commands/dmd_db_generate/), we perform lookups across various external systems to discover further insights for your dependency data.

However, depending on how your organisation treats this information, this _potential leakage_ could be problematic, and you should instead be treating these packages as [Sensitive Packages](/concepts/sensitive-package/).

Therefore, to avoid this, from dependency-management-data v0.47.0, it's now possible to reduce the risk of this using the [`sensitive_packages` table](/schema/#internalsensitivepackagesdbschemasql).

This allows you to add the following data to your database:

```sql
INSERT INTO sensitive_packages VALUES('@my-org/*',NULL,'MATCHES');
INSERT INTO sensitive_packages VALUES('github.com/my-org/*',NULL,'MATCHES');
INSERT INTO sensitive_packages VALUES('github.com/my-org/this-is-open-source',NULL,'DOES_NOT_MATCH');
-- wildcards can also be anywhere in the string i.e.
INSERT INTO sensitive_packages VALUES('*tan*na*',NULL,'DOES_NOT_MATCH');
```

This will then result in:

- Any package names that are prefixed with `@my-org/` will be treated as sensitive, and therefore ignored
- Any package names that are prefixed with `github.com/my-org/` will be treated as sensitive, and therefore ignored
- Any package names that equal `github.com/my-org/this-is-open-source` will be treated as *not* sensitive, and therefore lookups will occur
- Any package names that include the string `tan` followed by any number of characters followed by `na` will be treated as *not* sensitive, and therefore lookups will occur

Note that if you are defining the `package_manager`, it needs to be an appropriate name for **all** [Datasources](/concepts/datasource/) you are using. For instance:

```sql
INSERT INTO sensitive_packages VALUES('github.com/my-org/*','gomod','MATCHES');
INSERT INTO sensitive_packages VALUES('github.com/my-org/*','golang','MATCHES');
```

There is currently [no CLI interface to this functionality](https://gitlab.com/tanna.dev/dependency-management-data/-/issues/220), so you will need to hand-craft the SQL to specify these.
