---
title: Understanding the data model
description: An introduction to how dependency-management-data's database is structured, some insight into the data model and common queries you may want to use.
weight: 1
tags:
- howto
---
One of the key benefits of using dependency-management-data is that under the hood, you have access to the SQLite database that stores the dependency data.

Although you can interact with the database via the [GraphQL API](/graphql/), one of the key selling points of using dependency-management-data is that the database's interface is treated as a first-class citizen, and a key part of the product offering.

However, the database has a fair bit of data in it, and it may be daunting to understand where to start with or how to `JOIN` tables to perform some common actions.

<div class="card hint">
<p>ℹ This page is a work in progress</p>
</div>

It's recommended to have the [database schema documentation](/schema/) open while reading through this, which provides more in-depth information about tables and columns, as well as indicating where there may be "foreign keys".

## Overview

Before reading on, it's recommended to read [project's design decisions](/design-decisions/) which will give a bit more insight into why the database model is the way it is, in particular:

- [The database is not as normalised as a well-designed database](/design-decisions/#the-database-is-not-as-normalised-as-a-well-designed-database)
- [No foreign keys](/design-decisions/#no-foreign-keys)

For the most part, querying data from the database relies on the use of the [Repository Key](/concepts/repo-key/).

As you read through the [database schema documentation](/schema/) you will notice that there are a few types of database table:

- **Utility table**: generally expected to be `JOIN`'d with other tables, instead of queried directly
- **Datasources**: The [Datasource](/concepts/datasource/) specific table(s)
- (otherwise): a table that can be used as-is, or together with datasources
