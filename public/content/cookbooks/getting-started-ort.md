---
title: Getting Started with OSS Review Toolkit's data
description: How to get started with dependency-management-data, when using OSS Review Toolkit (ORT).
weight: 3
tags:
- getting-started
- using-with-tool
---
If you're already using the [OSS Review Toolkit (ORT)](https://oss-review-toolkit.org/ort/) project for analysing your dependencies, for instance to perform policy decisions based on them, you can import the data that ORT discovers directly into dependency-management-data, without necessarily needing to look at [different tooling](/cookbooks/collecting-data/#what-tools-shall-i-use) to produce data that dependency-management-data can consume.

Among [the various output formats that ORT's reporter supports](https://oss-review-toolkit.org/ort/docs/tools/reporter) are the ability to produce CycloneDX and SPDX Software Bill of Materials (SBOMs). As noted in the [Getting Started with SBOM data](/cookbooks/getting-started-sbom/) cookbook, [several types of SBOMs](/concepts/datasource/#software-bill-of-materials-sboms) are supported by dependency-management-data.

Therefore, once you've processed your repositories, you can then export the results as an SBOM and utilise dependency-management-data's tooling to further visualise and report on the data, for instance flagging up [advisories](/concepts/advisory/).

## Generating data

First, we need to `analyse` a repository:

```sh
cd /path/to/a/repo
# it's recommended to pin the Docker images, instead of pulling :latest
docker run -ti -v $PWD:/app ghcr.io/oss-review-toolkit/ort-extended:latest analyze -f YAML -i /app -o /app
```

Once we've analysed it, we can then optionally `scan` the repository for further details:

```sh
# it's recommended to pin the Docker images, instead of pulling :latest
docker run -ti -v $PWD:/app ghcr.io/oss-review-toolkit/ort-extended:latest scan --output-dir /app/out --ort-file /app/out/analyzer-result.yml
```

Finally, we can produce the SBOMs via the `report` commands:

```sh
# as a CycloneDX SBOM
docker run -ti -v $PWD:/app ghcr.io/oss-review-toolkit/ort-extended:latest report --output-dir /app/out --ort-file /app/out/scan-result.yml -f CycloneDx
# as an SPDX SBOM
docker run -ti -v $PWD:/app ghcr.io/oss-review-toolkit/ort-extended:latest report --output-dir /app/out --ort-file /app/out/scan-result.yml -f SpdxDocument
```

## Consuming data

Once produced, the resulting SBOMs can then be imported using [`dmd import sbom`](/commands/dmd_import_sbom/), for instance:

```sh
dmd db init --db dmd.db
# whitespace added for readability only
dmd import sbom --db dmd.db \
  bom.cyclonedx.xml \
  --platform gitlab \
  --organisation tanna.dev \
  --repo dependency-management-data
# or
dmd import sbom --db dmd.db \
  bom.spdx.yml \
  --platform gitlab \
  --organisation tanna.dev \
  --repo dependency-management-data
```
