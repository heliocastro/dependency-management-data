---
title: Getting Started with SBOM data
description: How to get started with dependency-management-data, when consuming SBOMs.
weight: 2
tags:
- getting-started
- using-with-tool
---
If you're already using an existing Software Composition Analysis (SCA) platform, or are able to produce [SBOMs](https://about.gitlab.com/blog/2022/10/25/the-ultimate-guide-to-sboms/) from the software you work on, you can import them directly into dependency-management-data, without necessarily needing to look at [different tooling](/cookbooks/collecting-data/#what-tools-shall-i-use) to produce data that dependency-management-data can consume.

SBOMs are an excellent format, and are well integrated into dependency-management-data's [supported features](/features/), and the supported formats and versions of SBOMs can be found [on the features page](/concepts/datasource/#software-bill-of-materials-sboms).

## Generating SBOM data

This is left as an exercise to the reader, as this is very dependent on the tool you're using. If you're having trouble, feel free to [raise an issue](https://gitlab.com/tanna.dev/dependency-management-data/-/issues/new) and we can try and work towards it together.

## Consuming SBOM data

Once produced, the resulting SBOMs can then be imported using [`dmd import bulk`](/commands/dmd_import_bulk/), for instance:

```sh
# set up the database
dmd db init --db dmd.db
dmd import bulk --db dmd.db repos.csv
```

This consumes a CSV of the following format:

```csv
# NOTE: no header should be provided, this is commented to indicate what's the columns are
# platform,organisation,repo,type,filename
github,snarfed,bridgy,sbom,example/sbom/snyk-bridgy-fed-cyclone.json
```

Alternatively, you can call the [`dmd import sbom`](/commands/dmd_import_sbom/), for instance:

```sh
# set up the database
dmd db init --db dmd.db
# whitespace added for readability only
dmd import sbom --db dmd.db sbom/snyk-bridgy-fed-cyclone.json \
  --platform github \
  --organisation snarfed \
  --repo bridgy
dmd import sbom --db dmd.db sbom/snyk-dddem-web-spdx.json \
  --platform github \
  --organisation DDDEastMidlandsLimited \
  --repo dddem-web
dmd import sbom --db dmd.db sbom/snyk-pay-webhooks-container-cyclone.json \
  --platform github \
  --organisation alphagov \
  --repo pay-webhooks
```
