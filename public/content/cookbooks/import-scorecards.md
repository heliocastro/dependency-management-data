---
title: Importing OpenSSF Security Scorecard data into dependency-management-data
description: How to use pre-computed OpenSSF Security Scorecard data, instead of requiring dependency-management-data to produce it.
tags:
- howto
---
The [OpenSSF Security Scorecard](https://securityscorecards.dev/) makes it possible to understand at-a-glance the health of an Open Source project, and whether there are any concerns with your Supply Chain security.

Since dependency-management-data v0.76.0, it has been possible to enrich the data in the database with data from [the public API](https://api.securityscorecards.dev/) from Scorecards, via [`dmd db generate dependency-health`](/commands/dmd_db_generate_dependency-health/), so you can get insight into data alongside other insights that dependency-management-data provides.

However, because the Scorecard project only looks at [the 1 million most critical Open Source projects](https://github.com/ossf/scorecard?tab=readme-ov-file#public-data), your dependencies may not all be present.

Therefore, you may be wanting to manually fetch the data from the [`scorecard` CLI](https://github.com/ossf/scorecard) to cover all your dependencies' repositories.

Since dependency-management-data v0.84.0, there is now a [`dmd import scorecard`](/commands/dmd_import_scorecard/) command which allows you to take the results from the `scorecard` CLI as-is, instead of relying upon the public data that [`dmd db generate dependency-health`](/commands/dmd_db_generate_dependency-health/) will use.

For instance, let's say that we want to process the following repositories for our dependencies:

```sh
scorecard --repo github.com/oapi-codegen/runtime --output runtime.json      --format json
scorecard --repo gitlab.com/fdroid/fdroidclient  --output fdroidclient.json --format json
```

Once these have succeeded, we can then import the data:

```sh
# if you've not already prepared the DB
dmd db init --db dmd.db
# NOTE quotes to prevent globbing
dmd db import scorecard --db dmd.db '*.json'
```

Note that this uses [Ecosyste.ms' Packages API](https://packages.ecosyste.ms/) to look up dependency names for given repository URLs.

Once this is imported, we can then write queries to look up this data.

For instance we can write the following query to list all packages that have a total scorecard below 3:

```sql
select
  s.platform,
  s.organisation,
  s.repo,
  s.package_name,
  s.version,
  s.current_version,
  package_type as package_manager,
  -- as SBOMs don't make this available, default to an empty array
  '[]' as dep_types,
  -- as SBOMs don't make this available, default to an empty string
  '' as package_file_path,
  printf('%.2f', scorecard_score) as scorecard_score
from
  sboms s
  inner join dependency_health as h on s.package_name = h.package_name
  and s.package_type = h.package_manager
where
  scorecard_score < 3
union
select
  r.platform,
  r.organisation,
  r.repo,
  r.package_name,
  r.version,
  r.current_version,
  r.package_manager,
  r.dep_types,
  r.package_file_path,
  printf('%.2f', scorecard_score) as scorecard_score
from
  renovate r
  inner join dependency_health as h on r.package_name = h.package_name
  and r.package_manager = h.package_manager
where
  scorecard_score < 3
order by
  scorecard_score desc;
```

You can see the result of this query [on the example data](https://dependency-management-data-example.fly.dev/datasette/dmd?sql=select%0D%0A++s.platform%2C%0D%0A++s.organisation%2C%0D%0A++s.repo%2C%0D%0A++s.package_name%2C%0D%0A++s.version%2C%0D%0A++s.current_version%2C%0D%0A++package_type+as+package_manager%2C%0D%0A++--+as+SBOMs+don%27t+make+this+available%2C+default+to+an+empty+array%0D%0A++%27%5B%5D%27+as+dep_types%2C%0D%0A++--+as+SBOMs+don%27t+make+this+available%2C+default+to+an+empty+string%0D%0A++%27%27+as+package_file_path%2C%0D%0A++printf%28%27%25.2f%27%2C+scorecard_score%29+as+scorecard_score%0D%0Afrom%0D%0A++sboms+s%0D%0A++inner+join+dependency_health+as+h+on+s.package_name+%3D+h.package_name%0D%0A++and+s.package_type+%3D+h.package_manager%0D%0Awhere%0D%0A++scorecard_score+%3C+3%0D%0Aunion%0D%0Aselect%0D%0A++r.platform%2C%0D%0A++r.organisation%2C%0D%0A++r.repo%2C%0D%0A++r.package_name%2C%0D%0A++r.version%2C%0D%0A++r.current_version%2C%0D%0A++r.package_manager%2C%0D%0A++r.dep_types%2C%0D%0A++r.package_file_path%2C%0D%0A++printf%28%27%25.2f%27%2C+scorecard_score%29+as+scorecard_score%0D%0Afrom%0D%0A++renovate+r%0D%0A++inner+join+dependency_health+as+h+on+r.package_name+%3D+h.package_name%0D%0A++and+r.package_manager+%3D+h.package_manager%0D%0Awhere%0D%0A++scorecard_score+%3C+3%0D%0Aorder+by%0D%0A++scorecard_score+desc%3B).
