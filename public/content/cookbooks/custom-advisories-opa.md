---
title: Turning complex policies into custom Advisories using Open Policy Agent
description: How to leverage Open Policy Agent integration in dependency-management-data to write much more complex rules for flagging advisories in your dependencies.
weight: 99
tags:
- howto
---
As well as the inbuilt functionality to flag [Advisories](/concepts/advisory/) in your software, dependency-management-data provides the ability to write your own [custom advisories](/cookbooks/custom-advisories/), which can also be shared as [part of the -contrib project](https://gitlab.com/tanna.dev/dependency-management-data-contrib).

Although very useful, they lack the ability to perform much more complex rules against dependencies.

Since dependency-management-data v0.54.0, it is possible to use [Open Policy Agent's Policy Language, Rego](https://www.openpolicyagent.org/docs/latest/policy-language/) to write much more complex advisories, also called [Policies](/concepts/policy/).

## Requirements

The following technical requirements are true for writing an OPA policy:

- The `package` must be set to `policy`
- There may be a string variable, `msg`, which indicates why the violation has occurred. This is a user-facing description, so add as much detail as would be useful to diagnose, including links to supporting documentation
- There must be a variable, `deny` or `warn`:
  - The variable `deny`, which indicates whether there should be a policy violation flagged for the given dependency. Not setting `deny` means no violation will occur. Setting the `deny` variable to contain the value of `msg` (or a hardcoded error message) results in a violation. See below for more details
  - The variable `warn`, which indicates whether there should be a policy warning flagged for the given dependency. Not setting `warn` means no violation will occur. Setting the `warn` variable to contain the value of `msg` (or a hardcoded error message) results in a warning. See below for more details
- There must be a string variable `advisory_type`, which indicates the Advisory type, which aligns with one of the types from the underlying database. The default is `POLICY`, but can be adjusted to point to a more appropriate Advisory type by explicitly setting the `advisory_type` variable
- Each `.rego` file must be its own standalone policy

As of dependency-management-data v0.55.0, linting will occur when running [`dmd policy evaluate` command](/commands/dmd_policy_evaluate/), or can be run ad-hoc with [`dmd policy lint` command](/commands/dmd_policy_lint/).

With that in mind, the following is a good starting point for fresh policies:

```rego
package policy

import future.keywords.contains
import future.keywords.if

# can be any value that matches one of the valid `advisory_type`s in https://dmd.tanna.dev/schema/#internaladvisorydbschemasql
# if not set, will default to `POLICY`
default advisory_type := "POLICY"

deny contains msg if {
	false
	msg := "This will deny the dependency"
}

warn contains msg if {
	false
	msg := "This will warn about the usage of the dependency"
}
```

### Flagging a violation

```rego
package policy

import future.keywords.contains
import future.keywords.if

deny contains "This will trigger a violation, and set the respective error message as this string"
```

### No violation

```rego
package policy

default deny := []

default advisory_type := "UNMAINTAINED"
```

### Flagging a warning

```rego
package policy

import future.keywords.if
import future.keywords.in
import future.keywords.contains

warn contains msg if {
	some license in input.dependency.licenses
	license in ["non-standard"]
	msg := sprintf("Dependency uses a non-standard license (%s)", [license])
}
```

## Input data

When creating an OPA Policy, you'll receive consistently-formatted input data. For more detail, you can see [field-level documentation on pkg.go.dev](https://pkg.go.dev/dmd.tanna.dev/internal/policies#EvaluationInput).

For instance, if we had the following rows from the `sboms` table:

| platform | organisation |     repo     |        package_name         | version | current_version | package_type |
|----------|--------------|--------------|-----------------------------|---------|-----------------|--------------|
| github   | deepmap      | oapi-codegen | github.com/stretchr/testify | 1.8.4   | 1.8.4           | golang       |

Then we would receive the following input:

```json
{
  "project": {
    "organisation": "deepmap",
    "platform": "github",
    "repo": "oapi-codegen"
  },
  "dependency": {
    "current_version": "1.8.4",
    "dep_types": [],
    "package_file_path": "",
    "package_manager": "golang",
    "package_name": "github.com/stretchr/testify",
    "version": "1.8.4",
    "licenses": [
    ]
  }
}
```

And from the `renovate` table:

| platform | organisation |     repo     |        package_name         | version | current_version | package_manager |  package_file_path   | datasource |  dep_types  |
|----------|--------------|--------------|-----------------------------|---------|-----------------|-----------------|----------------------|------------|-------------|
| github   | deepmap      | oapi-codegen | github.com/stretchr/testify | v1.8.4  | v1.8.4          | gomod           | internal/test/go.mod | go         | ["require"] |

Then we would receive the following input:

```json
{
  "project": {
    "organisation": "deepmap",
    "platform": "github",
    "repo": "oapi-codegen"
  },
  "dependency": {
    "current_version": "v1.8.4",
    "dep_types": ["require"],
    "package_file_path": "internal/test/go.mod",
    "package_manager": "gomod",
    "package_name": "github.com/stretchr/testify",
    "version": "v1.8.4",
    "licenses": [
    ]
  }
}
```

### Licensing data

If you have generated licensing information via [`dmd db generate advisories` command](/commands/dmd_db_generate_advisories/), you can access that data in your OPA policy.

For instance, if the `depsdev_licenses` table contains the following data:

|           package_name           | version | license |      updated_at      |
|----------------------------------|---------|---------|----------------------|
| github.com/stretchr/testify      | v1.8.4  | MIT     | 2023-11-23T10:52:19Z |

You will then receive the following input:

```json
{
  "project": {
    "organisation": "deepmap",
    "platform": "github",
    "repo": "oapi-codegen"
  },
  "dependency": {
    "current_version": "v1.8.4",
    "dep_types": ["require"],
    "package_file_path": "internal/test/go.mod",
    "package_manager": "gomod",
    "package_name": "github.com/stretchr/testify",
    "version": "v1.8.4",
    "licenses": [
      "MIT"
    ]
  }
}
```

Note that not every package may have a license. In this case, you will receive the following input - note the empty `dependency.licenses` array:

```json
{
  "project": {
    "organisation": "deepmap",
    "platform": "github",
    "repo": "oapi-codegen"
  },
  "dependency": {
    "current_version": "v1.8.4",
    "dep_types": ["require"],
    "package_file_path": "internal/test/go.mod",
    "package_manager": "gomod",
    "package_name": "github.com/stretchr/testify",
    "version": "v1.8.4",
    "licenses": [
    ]
  }
}
```

### Repository metadata

If you're taking advantage of the ability to store [`repository_metadata`](/schema/#internalrepositorymetadatadbschemasql), you will be able to leverage the data as part of policies.

For instance, if you have the following repository metadata:

| platform | organisation |     repo     | is_monorepo | is_fork | repository_type | repository_usage | visibility | description |                    additional_metadata                    |
|----------|--------------|--------------|-------------|---------|-----------------|------------------|------------|-------------|-----------------------------------------------------------|
| github   | deepmap      | oapi-codegen | false       | false   | TOOL            | TOOL LIBRARY     | PUBLIC     |             | {"last_push":"2024-01-12T04:44:23Z","stability":"stable"} |

You will then receive the following input:

```json
{
  "project": {
    "organisation": "deepmap",
    "platform": "github",
    "repo": "oapi-codegen",
    "metadata": {
      "is_monorepo": false,
      "is_fork": false,
      "repository_type": "TOOL",
      "repository_usage": "TOOL LIBRARY",
      "visibility": "PUBLIC",
      "additional_metadata": {
        "last_push": "2024-01-12T04:44:23Z",
        "stability": "stable"
      }
    }
  },
  "dependency": {
    "current_version": "v1.8.4",
    "dep_types": ["require"],
    "package_file_path": "internal/test/go.mod",
    "package_manager": "gomod",
    "package_name": "github.com/stretchr/testify",
    "version": "v1.8.4",
    "licenses": [
    ]
  }
}
```

Note that not every package may have repository metadata. In this case, you will receive the following input - note that `project.metadata` is absent:


```json
{
  "project": {
    "organisation": "deepmap",
    "platform": "github",
    "repo": "oapi-codegen"
  },
  "dependency": {
    "current_version": "v1.8.4",
    "dep_types": ["require"],
    "package_file_path": "internal/test/go.mod",
    "package_manager": "gomod",
    "package_name": "github.com/stretchr/testify",
    "version": "v1.8.4",
    "licenses": [
    ]
  }
}
```

### Dependency health data

If you have fetched dependency health data via [`dmd db generate dependency-health`](/commands/dmd_db_generate_dependency-health/), then you may have collected health metadata around your dependencies into the [`dependency_health` table](/schema/#internaldependencyhealthdbschemasql):

|    package_name     | package_manager | scorecard_score  | scorecard_codereview | scorecard_maintained | scorecard_ciibestpractices | scorecard_license | scorecard_dangerousworkflow | scorecard_packaging | scorecard_tokenpermissions | scorecard_signedreleases | scorecard_branchprotection | scorecard_binaryartifacts | scorecard_fuzzing | scorecard_securitypolicy | scorecard_sast | scorecard_vulnerabilities | scorecard_pinneddependencies | ecosystems_repo_archived | ecosystems_repo_pushed_at | ecosystems_repo_updated_at | ecosystems_repo_last_synced_at | ecosystems_last_synced_at | ecosystems_latest_release_published_at | ecosystems_status |
|---------------------|-----------------|------------------|----------------------|----------------------|----------------------------|-------------------|-----------------------------|---------------------|----------------------------|--------------------------|----------------------------|---------------------------|-------------------|--------------------------|----------------|---------------------------|------------------------------|--------------------------|---------------------------|----------------------------|--------------------------------|---------------------------|----------------------------------------|-------------------|
| @codemirror/history | npm             | 3.40000009536743 | 0                    | 0                    | 0                          | 10                | 10                          | -1                  | 0                          | -1                       | 0                          | 10                        | 0                 | 0                        | 0              | 10                        | 0                            | 1                        | 2022-02-16T16:57:16.000Z  | 2024-01-16T20:35:36.300Z   | 2024-01-16T20:35:36.299Z       | 2024-01-17T01:45:03Z      | 2022-01-03T13:52:35Z                   |                   |

This would result in input of the form:

```json
{
  "project": {
    "platform": "github",
    "organisation": "DDDEastMidlandsLimited",
    "repo": "dddem-blog"
  },
  "dependency": {
    "package_name": "@codemirror/history",
    "version": "0.18.1",
    "current_version": "0.18.1",
    "package_manager": "npm",
    "package_file_path": "package.json",
    "dep_types": [
      "lockfile"
    ],
    "licenses": [
    ],
    "health": {
      "ecosystems": {
        "last_synced_at": "2024-01-17T01:45:03Z",
        "latest_release_published_at": "2022-01-03T13:52:35Z",
        "repo": {
          "archived": true,
          "last_synced_at": "2024-01-16T20:35:36.299Z",
          "pushed_at": "2022-02-16T16:57:16.000Z",
          "updated_at": "2024-01-16T20:35:36.300Z"
        }
      },
      "security_scorecard": {
        "score": 3.4000000953674316,
        "binary_artifacts": 10,
        "branch_protection": 0,
        "cii_best_practices": 0,
        "code_review": 0,
        "dangerous_workflow": 10,
        "fuzzing": 0,
        "license": 10,
        "maintained": 0,
        "packaging": -1,
        "pinned_dependencies": 0,
        "sast": 0,
        "security_policy": 0,
        "signed_releases": -1,
        "token_permissions": 0,
        "vulnerabilities": 10
      }
    }
  }
}
```

If a dependency does not have any dependency health information available, the `health` field will be absent.

A number of these fields are optional, and can be found further documented in the [field-level documentation on pkg.go.dev](https://pkg.go.dev/dmd.tanna.dev/internal/policies#EvaluationInput).

## Testing out policies

You can use the [`dmd policy evaluate` command](/commands/dmd_policy_evaluate/) to evaluate a policy, giving you the opportunity to understand the impact it may have when applied.

Below you can find example policies, and the result that using `dmd policy evaluate` would bring, based on the [example project's data](https://gitlab.com/tanna.dev/dependency-management-data-example/).

Once you've written the policies, you'll then need to [process + persist them](#processing-and-persisting-policies).

### Example: Flagging use of legacy GitLab server repositories

Let's say that as an organisation you're moving from a self-hosted GitLab server to GitLab.com, and want to find all usages of the old package URLs.

To do this, we could craft the following `policy.rego`:

```rego
package policy

import future.keywords.contains
import future.keywords.if

default advisory_type := "UNMAINTAINED"

deny contains "Use the new GitLab.com server" if startswith(input.dependency.package_name, "gitlab.example.com/")
```

With this applied, we can then run the [`policy evaluate` command](/commands/dmd_policy_evaluate/):

```sh
dmd policy evaluate --db /path/to/dmd.db policy.rego
```

This will then print out, for instance:

```
Processing policy.rego resulted in 10 policy violations, from ... dependencies:
+----------+--------------+---------------------------------------+-----------------------------------------------+-----------+------------------+-------------------+---------------+-------------------------------+
| PLATFORM | ORGANISATION | REPO                                  | PACKAGE                                       | VERSION   | DEPENDENCY TYPES | FILEPATH          | ADVISORY TYPE | DESCRIPTION                   |
+----------+--------------+---------------------------------------+-----------------------------------------------+-----------+------------------+-------------------+---------------+-------------------------------+
| github   | ........     | ...............                       | gitlab.example.com/..........                 | ........  | []               | go.mod            | UNMAINTAINED  | Use the new GitLab.com server |
|          |              |                                       |                                               |           |                  |                   |               |                               |
+----------+--------------+---------------------------------------+-----------------------------------------------+-----------+------------------+-------------------+---------------+-------------------------------+
```

### Example: Flagging use of `bytedance/sonic`

Let's say that you're working on applications that need to abide by US federal restrictions on the use of software produced by ByteDance.

If we wanted to:

- Flag uses of the Gin web framework that now include `bytedance/sonic` as a transitive dependency (since v1.9.0)
- Flag uses of any Bytedance Go libraries, with additional checks based on whether it's a direct or indirect usage
- Work with both the `renovate` and `sboms` [Datasources](/concepts/datasource/)

To do this, we could craft the following `policy.rego`:

```rego
package policy

import future.keywords.contains
import future.keywords.if
import future.keywords.in

prefix := "The US Federal government in June highlighted that usage of ByteDance software on government equipment or networks is prohibited (https://www.federalregister.gov/documents/2023/06/02/2023-11756/federal-acquisition-regulation-prohibition-on-a-bytedance-covered-application), which may extend as far as Open Source projects they have produced."

default versions := []

# prefer the `version` with Go modules, as versions are always exactly pinned, and means we don't need to handle the absense of the `current_version` i.e. https://gitlab.com/tanna.dev/dependency-management-data/-/issues/77
versions := split(input.dependency.version, ".")

deny contains msg if {
	input.dependency.package_manager in {"gomod", "golang"}
	input.dependency.package_name = "github.com/gin-gonic/gin"
	versions[0] in {"v1", "1"}
	to_number(versions[1]) >= 9
	msg := sprintf("%s. Versions of Gin since v1.9.0 have shipped ByteDance/sonic as an optional dependency, but it still appears as a dependency, and could be in use - more details in https://github.com/gin-gonic/gin/issues/3653", [prefix])
}

is_bytedance if {
	input.dependency.package_manager in {"gomod", "golang"}
	startswith(input.dependency.package_name, "github.com/bytedance/")
}

is_direct if {
	input.dependency.package_manager in {"gomod", "golang"}
	"require" in input.dependency.dep_types
}

is_indirect if {
	input.dependency.package_manager in {"gomod", "golang"}
	"indirect" in input.dependency.dep_types
}

deny contains msg if {
	is_bytedance
	is_direct
	msg := sprintf("%s. This is an explicit use of a ByteDance-owned dependency", [prefix])
}

deny contains msg if {
	is_bytedance
	is_indirect
	msg := sprintf("%s. This is a transitive use of a ByteDance-owned dependency", [prefix])
}

deny contains msg if {
	is_bytedance
	not is_direct
	not is_indirect
	msg := sprintf("%s. (Unclear whether a direct or transitive dependency)", [prefix])
}
```

With this applied, we can then run the [`policy evaluate` command](/commands/dmd_policy_evaluate/):

```sh
dmd policy evaluate --db /path/to/dmd.db policy.rego
```

This will then print out, for instance:

```
Processing policy.rego resulted in 10 policy violations, from 129268 dependencies:
+----------+--------------+----------------+----------------------------+-------------+------------------+----------------------+---------------+--------------------------------------------------------------------------------------------------------------------------------------------------+
| PLATFORM | ORGANISATION | REPO           | PACKAGE                    | VERSION     | DEPENDENCY TYPES | FILEPATH             | ADVISORY TYPE | DESCRIPTION                                                                                                                                      |
+----------+--------------+----------------+----------------------------+-------------+------------------+----------------------+---------------+--------------------------------------------------------------------------------------------------------------------------------------------------+
| github   | deepmap      | oapi-codegen   | github.com/bytedance/sonic | 1.10.0-rc3  | []               |                      | POLICY        | The US Federal government in June highlighted that usage of                                                                                      |
|          |              |                |                            | /           |                  |                      |               | ByteDance software on government equipment or networks is                                                                                        |
|          |              |                |                            | 1.10.0-rc3  |                  |                      |               | prohibited                                                                                                                                       |
|          |              |                |                            |             |                  |                      |               | (https://www.federalregister.gov/documents/2023/06/02/2023-11756/federal-acquisition-regulation-prohibition-on-a-bytedance-covered-application), |
|          |              |                |                            |             |                  |                      |               | which may extend as far as Open Source projects they have                                                                                        |
|          |              |                |                            |             |                  |                      |               | produced.. (Unclear whether a direct or transitive                                                                                               |
|          |              |                |                            |             |                  |                      |               | dependency)                                                                                                                                      |
| github   | deepmap      | oapi-codegen   | github.com/bytedance/sonic | v1.10.0-rc3 | []               | examples/go.mod      | POLICY        | The US Federal government in June highlighted that usage of                                                                                      |
|          |              |                |                            | /           |                  |                      |               | ByteDance software on government equipment or networks is                                                                                        |
|          |              |                |                            | v1.10.0-rc3 |                  |                      |               | prohibited                                                                                                                                       |
|          |              |                |                            |             |                  |                      |               | (https://www.federalregister.gov/documents/2023/06/02/2023-11756/federal-acquisition-regulation-prohibition-on-a-bytedance-covered-application), |
|          |              |                |                            |             |                  |                      |               | which may extend as far as Open Source projects they have                                                                                        |
|          |              |                |                            |             |                  |                      |               | produced.. (Unclear whether a direct or transitive                                                                                               |
|          |              |                |                            |             |                  |                      |               | dependency)                                                                                                                                      |
| github   | deepmap      | oapi-codegen   | github.com/bytedance/sonic | v1.10.0-rc3 | []               | internal/test/go.mod | POLICY        | The US Federal government in June highlighted that usage of                                                                                      |
|          |              |                |                            | /           |                  |                      |               | ByteDance software on government equipment or networks is                                                                                        |
|          |              |                |                            | v1.10.0-rc3 |                  |                      |               | prohibited                                                                                                                                       |
|          |              |                |                            |             |                  |                      |               | (https://www.federalregister.gov/documents/2023/06/02/2023-11756/federal-acquisition-regulation-prohibition-on-a-bytedance-covered-application), |
|          |              |                |                            |             |                  |                      |               | which may extend as far as Open Source projects they have                                                                                        |
|          |              |                |                            |             |                  |                      |               | produced.. (Unclear whether a direct or transitive                                                                                               |
|          |              |                |                            |             |                  |                      |               | dependency)                                                                                                                                      |
| github   | deepmap      | oapi-codegen   | github.com/gin-gonic/gin   | 1.9.1 /     | []               |                      | POLICY        | The US Federal government in June highlighted that usage of                                                                                      |
|          |              |                |                            | 1.9.1       |                  |                      |               | ByteDance software on government equipment or networks is                                                                                        |
|          |              |                |                            |             |                  |                      |               | prohibited                                                                                                                                       |
|          |              |                |                            |             |                  |                      |               | (https://www.federalregister.gov/documents/2023/06/02/2023-11756/federal-acquisition-regulation-prohibition-on-a-bytedance-covered-application), |
|          |              |                |                            |             |                  |                      |               | which may extend as far as Open Source projects they have                                                                                        |
|          |              |                |                            |             |                  |                      |               | produced.. Versions of Gin since v1.9.0 have shipped                                                                                             |
|          |              |                |                            |             |                  |                      |               | ByteDance/sonic as an optional dependency, but it still                                                                                          |
|          |              |                |                            |             |                  |                      |               | appears as a dependency, and could be in use - more details                                                                                      |
|          |              |                |                            |             |                  |                      |               | in https://github.com/gin-gonic/gin/issues/3653                                                                                                  |
| github   | deepmap      | oapi-codegen   | github.com/gin-gonic/gin   | v1.9.1 /    | []               | examples/go.mod      | POLICY        | The US Federal government in June highlighted that usage of                                                                                      |
|          |              |                |                            | v1.9.1      |                  |                      |               | ByteDance software on government equipment or networks is                                                                                        |
|          |              |                |                            |             |                  |                      |               | prohibited                                                                                                                                       |
|          |              |                |                            |             |                  |                      |               | (https://www.federalregister.gov/documents/2023/06/02/2023-11756/federal-acquisition-regulation-prohibition-on-a-bytedance-covered-application), |
|          |              |                |                            |             |                  |                      |               | which may extend as far as Open Source projects they have                                                                                        |
|          |              |                |                            |             |                  |                      |               | produced.. Versions of Gin since v1.9.0 have shipped                                                                                             |
|          |              |                |                            |             |                  |                      |               | ByteDance/sonic as an optional dependency, but it still                                                                                          |
|          |              |                |                            |             |                  |                      |               | appears as a dependency, and could be in use - more details                                                                                      |
|          |              |                |                            |             |                  |                      |               | in https://github.com/gin-gonic/gin/issues/3653                                                                                                  |
| github   | deepmap      | oapi-codegen   | github.com/gin-gonic/gin   | v1.9.1 /    | []               | internal/test/go.mod | POLICY        | The US Federal government in June highlighted that usage of                                                                                      |
|          |              |                |                            | v1.9.1      |                  |                      |               | ByteDance software on government equipment or networks is                                                                                        |
|          |              |                |                            |             |                  |                      |               | prohibited                                                                                                                                       |
|          |              |                |                            |             |                  |                      |               | (https://www.federalregister.gov/documents/2023/06/02/2023-11756/federal-acquisition-regulation-prohibition-on-a-bytedance-covered-application), |
|          |              |                |                            |             |                  |                      |               | which may extend as far as Open Source projects they have                                                                                        |
|          |              |                |                            |             |                  |                      |               | produced.. Versions of Gin since v1.9.0 have shipped                                                                                             |
|          |              |                |                            |             |                  |                      |               | ByteDance/sonic as an optional dependency, but it still                                                                                          |
|          |              |                |                            |             |                  |                      |               | appears as a dependency, and could be in use - more details                                                                                      |
|          |              |                |                            |             |                  |                      |               | in https://github.com/gin-gonic/gin/issues/3653                                                                                                  |
| github   | oapi-codegen | gin-middleware | github.com/bytedance/sonic | v1.9.1 /    | []               | go.mod               | POLICY        | The US Federal government in June highlighted that usage of                                                                                      |
|          |              |                |                            | v1.9.1      |                  |                      |               | ByteDance software on government equipment or networks is                                                                                        |
|          |              |                |                            |             |                  |                      |               | prohibited                                                                                                                                       |
|          |              |                |                            |             |                  |                      |               | (https://www.federalregister.gov/documents/2023/06/02/2023-11756/federal-acquisition-regulation-prohibition-on-a-bytedance-covered-application), |
|          |              |                |                            |             |                  |                      |               | which may extend as far as Open Source projects they have                                                                                        |
|          |              |                |                            |             |                  |                      |               | produced.. (Unclear whether a direct or transitive                                                                                               |
|          |              |                |                            |             |                  |                      |               | dependency)                                                                                                                                      |
| github   | oapi-codegen | runtime        | github.com/bytedance/sonic | v1.10.0-rc3 | []               | go.mod               | POLICY        | The US Federal government in June highlighted that usage of                                                                                      |
|          |              |                |                            | /           |                  |                      |               | ByteDance software on government equipment or networks is                                                                                        |
|          |              |                |                            | v1.10.0-rc3 |                  |                      |               | prohibited                                                                                                                                       |
|          |              |                |                            |             |                  |                      |               | (https://www.federalregister.gov/documents/2023/06/02/2023-11756/federal-acquisition-regulation-prohibition-on-a-bytedance-covered-application), |
|          |              |                |                            |             |                  |                      |               | which may extend as far as Open Source projects they have                                                                                        |
|          |              |                |                            |             |                  |                      |               | produced.. (Unclear whether a direct or transitive                                                                                               |
|          |              |                |                            |             |                  |                      |               | dependency)                                                                                                                                      |
| github   | oapi-codegen | runtime        | github.com/gin-gonic/gin   | v1.9.1 /    | []               | go.mod               | POLICY        | The US Federal government in June highlighted that usage of                                                                                      |
|          |              |                |                            | v1.9.1      |                  |                      |               | ByteDance software on government equipment or networks is                                                                                        |
|          |              |                |                            |             |                  |                      |               | prohibited                                                                                                                                       |
|          |              |                |                            |             |                  |                      |               | (https://www.federalregister.gov/documents/2023/06/02/2023-11756/federal-acquisition-regulation-prohibition-on-a-bytedance-covered-application), |
|          |              |                |                            |             |                  |                      |               | which may extend as far as Open Source projects they have                                                                                        |
|          |              |                |                            |             |                  |                      |               | produced.. Versions of Gin since v1.9.0 have shipped                                                                                             |
|          |              |                |                            |             |                  |                      |               | ByteDance/sonic as an optional dependency, but it still                                                                                          |
|          |              |                |                            |             |                  |                      |               | appears as a dependency, and could be in use - more details                                                                                      |
|          |              |                |                            |             |                  |                      |               | in https://github.com/gin-gonic/gin/issues/3653                                                                                                  |
| github   | oapi-codegen | gin-middleware | github.com/gin-gonic/gin   | v1.9.1 /    | []               | go.mod               | POLICY        | The US Federal government in June highlighted that usage of                                                                                      |
|          |              |                |                            | v1.9.1      |                  |                      |               | ByteDance software on government equipment or networks is                                                                                        |
|          |              |                |                            |             |                  |                      |               | prohibited                                                                                                                                       |
|          |              |                |                            |             |                  |                      |               | (https://www.federalregister.gov/documents/2023/06/02/2023-11756/federal-acquisition-regulation-prohibition-on-a-bytedance-covered-application), |
|          |              |                |                            |             |                  |                      |               | which may extend as far as Open Source projects they have                                                                                        |
|          |              |                |                            |             |                  |                      |               | produced.. Versions of Gin since v1.9.0 have shipped                                                                                             |
|          |              |                |                            |             |                  |                      |               | ByteDance/sonic as an optional dependency, but it still                                                                                          |
|          |              |                |                            |             |                  |                      |               | appears as a dependency, and could be in use - more details                                                                                      |
|          |              |                |                            |             |                  |                      |               | in https://github.com/gin-gonic/gin/issues/3653                                                                                                  |
+----------+--------------+----------------+----------------------------+-------------+------------------+----------------------+---------------+--------------------------------------------------------------------------------------------------------------------------------------------------+
```
### Example: Flagging uses of licenses

Let's say that as an organisation you want to flag the usage of licenses you'd prefer not to be used.

To do this, we could craft the following `policy.rego`:

```rego
package policy

import future.keywords.if
import future.keywords.in
import future.keywords.contains

deny contains msg if {
	some license in input.dependency.licenses
	license in ["AGPL-3.0", "non-standard"]
	msg := sprintf("Dependency uses a Banned license (%s)", [license])
}
```

With this applied, we can then run the [`policy evaluate` command](/commands/dmd_policy_evaluate/):

```sh
dmd policy evaluate --db /path/to/dmd.db policy.rego
```

This will then print out, for instance:

```
Processing licensing.rego resulted in 1593 policy violations, from 131070 dependencies, but as `--limit` was set to 10, only showing that many results:
+----------+------------------------+-------------------+-------------------------------------------------+-----------+---------------------------------------------+------------------+---------------+-------------------------------------------------+
| PLATFORM | ORGANISATION           | REPO              | PACKAGE                                         | VERSION   | DEPENDENCY TYPES                            | FILEPATH         | ADVISORY TYPE | DESCRIPTION                                     |
+----------+------------------------+-------------------+-------------------------------------------------+-----------+---------------------------------------------+------------------+---------------+-------------------------------------------------+
| github   | DDDEastMidlandsLimited | dddem-web         | regjsparser                                     | ^0.1.4 /  | ["lockfile","lockfile-yarn-pinning-^0.1.4"] | package.json     | POLICY        | Dependency uses a Banned license (non-standard) |
|          |                        |                   |                                                 | 0.1.5     |                                             |                  |               |                                                 |
| github   | IndiePass              | indiepass-android | jakarta.activation:jakarta.activation-api       | 1.2.1 /   | ["dependencies","missing-data"]             | build.gradle     | POLICY        | Dependency uses a Banned license (non-standard) |
|          |                        |                   |                                                 | 1.2.1     |                                             |                  |               |                                                 |
| github   | IndiePass              | indiepass-android | jakarta.xml.bind:jakarta.xml.bind-api           | 2.3.2 /   | ["dependencies","missing-data"]             | build.gradle     | POLICY        | Dependency uses a Banned license (non-standard) |
|          |                        |                   |                                                 | 2.3.2     |                                             |                  |               |                                                 |
| github   | IndiePass              | indiepass-android | javax.annotation:javax.annotation-api           | 1.3.2 /   | ["dependencies","missing-data"]             | build.gradle     | POLICY        | Dependency uses a Banned license (non-standard) |
|          |                        |                   |                                                 | 1.3.2     |                                             |                  |               |                                                 |
| github   | alphagov               | pay-publicapi     | com.google.auth:google-auth-library-credentials | 1.10.0 /  | ["test","missing-data"]                     | pom.xml          | POLICY        | Dependency uses a Banned license (non-standard) |
|          |                        |                   |                                                 | 1.10.0    |                                             |                  |               |                                                 |
| github   | IndiePass              | indiepass-android | org.bouncycastle:bcprov-jdk15on                 | 1.67 /    | ["dependencies","missing-data"]             | build.gradle     | POLICY        | Dependency uses a Banned license (non-standard) |
|          |                        |                   |                                                 | 1.67      |                                             |                  |               |                                                 |
| github   | IndiePass              | indiepass-android | org.glassfish.jaxb:txw2                         | 2.3.2 /   | ["dependencies","missing-data"]             | build.gradle     | POLICY        | Dependency uses a Banned license (non-standard) |
|          |                        |                   |                                                 | 2.3.2     |                                             |                  |               |                                                 |
| github   | IndiePass              | indiepass-android | org.hamcrest:hamcrest-core                      | 2.2 / 2.2 | ["dependencies","missing-data"]             | app/build.gradle | POLICY        | Dependency uses a Banned license (non-standard) |
| github   | IndiePass              | indiepass-android | org.hamcrest:hamcrest-library                   | 1.3 / 1.3 | ["dependencies","missing-data"]             | app/build.gradle | POLICY        | Dependency uses a Banned license (non-standard) |
| github   | alphagov               | pay-connector     | antlr:antlr                                     | 2.7.7 /   | ["test","missing-data"]                     | pom.xml          | POLICY        | Dependency uses a Banned license (non-standard) |
|          |                        |                   |                                                 | 2.7.7     |                                             |                  |               |                                                 |
+----------+------------------------+-------------------+-------------------------------------------------+-----------+---------------------------------------------+------------------+---------------+-------------------------------------------------+
```

## Processing and persisting policies

When using the [`dmd policy evaluate` command](/commands/dmd_policy_evaluate/) to evaluate a policy, you only see what _would_ happen if the policy was applied, but it doesn't actually write the data to the database.

Similar to how advisories data is generated, to actually process + persist the defined policies, you can use the [`dmd db generate policy-violations`](/commands/dmd_db_generate_policy-violations/) command.

This then writes data to the [`advisories` table](/schema/#internaladvisorydbschemasql) that can be queried manually, or reported through 👇

## Reporting violations

To report violations, you can use [`dmd report policy-violations`](/commands/dmd_report_policy-violations/), or use the [web application](/web/)'s reports viewer.

Additionally, any policy violations are exposed through [`dmd report advisories`](/commands/dmd_report_advisories/).
