---
title: Using custom Advisories to flag packages in use
description: "How to use custom advisories with dependency-management-data to track packages that your organisation may not want to use."
weight: 15
tags:
- howto
---
As well as the inbuilt functionality to flag [Advisories](/concepts/advisory/) in your software based on external APIs, dependency-management-data provides the ability to introduce your own Advisories.

With the custom Advisories functionality, you can define custom rules for dependencies to match against and flag your own reasons for the advisory, using any of the built-in Advisory types, such as `UNMAINTAINED`, `SECURITY` or `OTHER`.

Depending on what the advisories are, it may be worth sharing them upstream as [part of the -contrib project](https://gitlab.com/tanna.dev/dependency-management-data-contrib), so the whole community can benefit.

This gives you the ability to flag usage of internal or vendor libraries that maybe shouldn't be in use, as well as also pointing to any Open Source projects that your organisation may have issues with.

For instance, a couple of the advisories that the contrib defines can be seen below:

```sql
-- examples taken from https://gitlab.com/tanna.dev/dependency-management-data-contrib/-/blob/main/advisories/go.sql and https://gitlab.com/tanna.dev/dependency-management-data-contrib/-/blob/main/advisories/docker.sql respectively
INSERT INTO custom_advisories (
  package_pattern,
  package_manager,
  version,
  version_match_strategy,
  advisory_type,
  description
) VALUES
(
  'github.com/pkg/errors',
  'gomod',
  NULL,
  NULL,
  'UNMAINTAINED',
  'pkg/errors was archived in 2021, and is unmaintained since'
),
(
  'public.ecr.aws/lambda/go',
  'docker',
  '1',
  'EQUALS',
  'DEPRECATED',
  'Amazon does not recommend the use of the v1 Go image, which is based off of Amazon Linux (v1) https://docs.aws.amazon.com/lambda/latest/dg/go-image.html#go-image-v1'
)
;
```

This produces a report that looks like:

<table><thead><tr><th>Platform</th><th>Organisation</th><th>Repo</th><th>Package</th><th>Version</th><th>Dependency Types</th><th>Advisory Type</th><th>Description</th></tr></thead><tbody><tr><td>github</td><td>deepmap</td><td>oapi-codegen</td><td>github.com/pkg/errors</td><td>v0.9.1 / v0.9.1</td><td>["indirect"]</td><td>UNMAINTAINED</td><td>pkg/errors was archived in 2021, and is unmaintained since</td></tr></tbody></table>

You'll notice that we're using the [`custom_advisories` table](/schema/#internaladvisorydbschemasql), which is a generic table that you can insert data into as you please.

Looking for something slightly more complex? You may be interested in [turning complex policies into custom Advisories using Open Policy Agent](/cookbooks/custom-advisories-opa/).
