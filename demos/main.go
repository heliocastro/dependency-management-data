package main

import (
	"github.com/saschagrunert/demo"
)

func main() {
	d := demo.New()

	d.Add(initAndImport(), "demo-initAndImport", "")

	d.Add(setOwners(), "demo-setOwners", "")

	d.Add(reportGolangCILint(), "demo-reportGolangCILint", "")
	d.Add(reportMostPopularDockerImages(), "demo-mostPopularDockerImages", "")
	d.Add(reportMostPopularPackageManagers(), "demo-mostPopularPackageManagers", "")

	d.Add(generateMissingData(), "demo-generateMissingData", "")

	d.Add(evaluatePolicy(), "demo-evaluatePolicy", "")
	d.Add(generatePolicyViolations(), "demo-generatePolicyViolations", "")

	d.Add(generateAdvisory(), "demo-generateAdvisoryAndList", "")
	d.Add(reportAdvisories(), "demo-reportAdvisories", "")
	d.Add(reportInfrastructureAdvisories(), "demo-reportInfrastructureAdvisories", "")
	d.Add(reportLicenses(), "demo-reportLicenses", "")

	d.Add(reportDependenton(), "demo-reportDependenton", "")
	d.Add(reportLibyear(), "demo-reportLibyear", "")
	d.Add(reportFunding(), "demo-reportFunding", "")

	d.Add(metadata(), "demo-metadata", "")

	d.Run()
}

func initAndImport() *demo.Run {
	r := demo.NewRun(
		"Initialise database and import datasources",
	)

	r.Step(nil, demo.S(
		"dmd db init --db dmd.db",
	))

	r.Step(nil, demo.S(
		"dmd import renovate --db dmd.db 'renovate/*.json'",
	))

	r.Step(nil, demo.S(
		"sqlite3 dmd.db 'select count(*) from renovate'",
	))

	r.Step(nil, demo.S(
		"sqlite3 dmd.db 'select count(*) from renovate_updates'",
	))

	r.Step(nil, demo.S(
		"dmd import dependabot --db dmd.db 'dependabot/*.json'",
	))

	r.Step(nil, demo.S(
		"sqlite3 dmd.db 'select count(*) from sboms'",
	))

	r.Step(nil, demo.S(
		"dmd import sbom --db dmd.db sbom/snyk-bridgy-fed-cyclone.json --platform github --organisation snarfed --repo bridgy-----cyclone",
	))

	r.Step(nil, demo.S(
		"dmd import sbom --db dmd.db sbom/snyk-bridgy-fed-spdx.json --platform github --organisation snarfed --repo bridgy-----spdx",
	))

	r.Step(nil, demo.S(
		"dmd import sbom --db dmd.db sbom/snyk-dddem-web-cyclone.json --platform github --organisation DDDEastMidlandsLimited --repo dddem-web-----cyclone",
	))

	r.Step(nil, demo.S(
		"dmd import sbom --db dmd.db sbom/snyk-dddem-web-spdx.json --platform github --organisation DDDEastMidlandsLimited --repo dddem-web-----spdx",
	))

	r.Step(nil, demo.S(
		"dmd import sbom --db dmd.db sbom/snyk-pay-webhooks-cyclone.json --platform github --organisation alphagov --repo pay-webhooks-----cyclone",
	))

	r.Step(nil, demo.S(
		"dmd import sbom --db dmd.db sbom/snyk-pay-webhooks-spdx.json --platform github --organisation alphagov --repo pay-webhooks-----spdx",
	))

	r.Step(nil, demo.S(
		"dmd import sbom --db dmd.db sbom/snyk-pay-webhooks-container-cyclone.json --platform github --organisation alphagov --repo pay-webhooks-----container-cyclone",
	))

	r.Step(nil, demo.S(
		"dmd import sbom --db dmd.db sbom/snyk-pay-webhooks-container-spdx.json --platform github --organisation alphagov --repo pay-webhooks-----container-spdx",
	))

	r.Step(demo.S(
		"Alternatively, we can import multiple files with `import bulk`",
	), demo.S(
		"echo '#platform,organisation,repo,type,filename' > imports.csv",
		" && ",
		"echo 'github,snarfed,bridgy-----cyclone,sbom,sbom/snyk-bridgy-fed-cyclone.json' >> imports.csv",
		" && ",
		"dmd import bulk --db dmd.db imports.csv",
	))

	r.Step(nil, demo.S(
		"sqlite3 dmd.db 'select count(*) from sboms'",
	))

	r.Step(nil, demo.S(
		"dmd import awselasticache --db dmd.db 'aws-elasticache/*'",
	))

	r.Step(nil, demo.S(
		"sqlite3 dmd.db 'select count(*) from aws_elasticache_datastores'",
	))

	r.Step(nil, demo.S(
		"dmd import awslambda --db dmd.db 'aws-lambda/*'",
	))

	r.Step(nil, demo.S(
		"sqlite3 dmd.db 'select count(*) from aws_lambda_functions'",
	))

	r.Step(nil, demo.S(
		"dmd import awsrds --db dmd.db 'aws-rds/*'",
	))

	r.Step(nil, demo.S(
		"sqlite3 dmd.db 'select count(*) from aws_rds_databases'",
	))

	return r
}

func reportGolangCILint() *demo.Run {
	r := demo.NewRun(
		"Report for usages of golangci-lint which are tracked as a source dependency",
	)

	r.Step(nil, demo.S(
		"dmd report golangCILint --db dmd.db",
	))

	return r
}

func reportMostPopularDockerImages() *demo.Run {
	r := demo.NewRun(
		"Report the most popular Docker namespaces and images",
	)

	r.Step(nil, demo.S(
		"dmd report mostPopularDockerImages --db dmd.db",
	))

	return r
}

func reportMostPopularPackageManagers() *demo.Run {
	r := demo.NewRun(
		"Report the most popular package managers",
	)

	r.Step(nil, demo.S(
		"dmd report mostPopularPackageManagers --db dmd.db",
	))

	return r
}

func setOwners() *demo.Run {
	r := demo.NewRun(
		"Set ownership information",
		"Set bulk ownership information through the `dmd` CLI",
	)

	r.Step(demo.S(
		"Set Jamie Tanna as the owner of all repos in the `tanna.dev` and `jamietanna` organisations",
	), demo.S(
		"dmd owners set --db dmd.db --organisation '*tanna*' 'Jamie Tanna'",
	))

	r.Step(demo.S(
		"Set an owner for a specific organisation and repo",
	), demo.S(
		"dmd owners set --db dmd.db --platform 'github' --organisation jenkinsci --repo job-dsl-plugin 'Job DSL Plugin developers' 'https://github.com/orgs/jenkinsci/teams/job-dsl-plugin-developers'",
	))

	r.Step(demo.S(
		"Set an owner with wildcards (GDS Pay)",
	), demo.S(
		"dmd owners set 'GDS Pay' --organisation alphagov --repo 'pay-*' --db dmd.db",
	))

	r.Step(demo.S(
		"Set an owner with wildcards (GDS Digital Identity)",
	), demo.S(
		"dmd owners set 'GDS Digital Identity' --organisation alphagov --repo 'di-*' --db dmd.db",
	))

	r.StepCanFail(demo.S(
		"Must provide `--platform`, `--organisation` or `--repo`",
	), demo.S(
		"dmd owners set --db dmd.db 'Job DSL Plugin developers'",
	))

	r.Step(demo.S(
		"Show Ownership of repos",
		"Right now, there's no report for this",
	), demo.S(
		"sqlite3 dmd.db 'select platform, organisation, repo, package_name, owner, notes from renovate natural join owners limit 5;'",
	))

	return r
}

func generateMissingData() *demo.Run {
	r := demo.NewRun(
		"Generate missing package data",
		"Via https://deps.dev",
	)

	r.Step(nil, demo.S(
		"dmd db generate missing-data --db dmd.db",
	))

	return r
}

func generateAdvisory() *demo.Run {
	r := demo.NewRun(
		"Seed the database with known package advisories",
		"Uses the `advisories` table to allow adding custom deprecation/security/etc advisories",
		"Also includes support for:",
		"- End Of Life checking (via https://endoflife.date)",
		"- Licensing and Common Vulnerabilities and Exposures (CVE) information (via https://deps.dev)",
	)

	r.Step(demo.S(
		"Fetch the latest community-contributed data",
	), demo.S(
		"dmd contrib download",
	))

	r.Step(nil, demo.S(
		"dmd db generate advisories --db dmd.db",
	))

	return r
}

func reportAdvisories() *demo.Run {
	r := demo.NewRun(
		"Report advisories",
		"Uses the `custom_advisories` table to allow adding custom deprecation/security/etc advisories",
		"Also includes support for:",
		"- End Of Life checking (via https://endoflife.date)",
		"- Licensing and Common Vulnerabilities and Exposures (CVE) information (via https://deps.dev)",
	)

	r.Step(demo.S(
		"Report advisory information",
	), demo.S(
		"dmd report advisories --db dmd.db",
	))

	r.Step(demo.S(
		"Report summarised version of advisory information",
	), demo.S(
		"dmd report advisories --summary --db dmd.db",
	))

	r.Step(demo.S(
		"Report advisory information for only projects owned by GDS",
		"Note that the filtering does not currently work on AWS infrastructure advisories",
	), demo.S(
		"dmd report advisories --db dmd.db --owner 'GDS*'",
	))

	r.Step(demo.S(
		"Report advisory information for projects in organisations that contain the word `tanna`",
		"Note that the filtering does not currently work on AWS infrastructure advisories",
	), demo.S(
		"dmd report advisories --db dmd.db --organisation '%tanna*'",
	))

	r.Step(demo.S(
		"Report advisory information for a specific project",
		"Note that the filtering does not currently work on AWS infrastructure advisories",
	), demo.S(
		"dmd report advisories --db dmd.db --platform gitlab --organisation 'tanna.dev' --repo dependency-management-data",
	))

	r.Step(demo.S(
		"Report advisory information for a specific type of advisory",
		"Note that the filtering does not currently work on AWS infrastructure advisories",
	), demo.S(
		"dmd report advisories --db dmd.db --advisory-type DEPRECATED",
	))

	return r
}

func reportInfrastructureAdvisories() *demo.Run {
	r := demo.NewRun(
		"Report advisories that are available for infrastructure components",
		"Uses AWS infrastructure version advisories, via endoflife-checker",
	)

	r.Step(demo.S(
		"Report advisory information",
	), demo.S(
		"dmd report infrastructure-advisories --db dmd.db",
	))

	return r
}

func reportLicenses() *demo.Run {
	r := demo.NewRun(
		"Report licenses",
		"List the licenses in use by dependencies, as determnined by https://deps.dev",
	)

	r.Step(demo.S(
		"Report licensing information",
	), demo.S(
		"dmd report licenses --db dmd.db",
	))

	r.Step(demo.S(
		"Report licensing information for only projects owned by GDS",
	), demo.S(
		"dmd report licenses --db dmd.db --owner 'GDS*'",
	))

	r.Step(demo.S(
		"Report licensing information for projects in organisations that contain the word `tanna`",
	), demo.S(
		"dmd report licenses --db dmd.db --organisation '%tanna*'",
	))

	r.Step(demo.S(
		"Report licensing information for a specific project",
	), demo.S(
		"dmd report licenses --db dmd.db --platform gitlab --organisation 'tanna.dev' --repo dependency-management-data",
	))

	return r
}

func evaluatePolicy() *demo.Run {
	r := demo.NewRun(
		"Evaluate policy",
		"Perform an evaluation of an Open Policy Agent Policy",
	)

	r.Step(demo.S(
		"Evaluate policy",
	), demo.S(
		"dmd policy evaluate --db dmd.db policies/bytedance.rego",
	))

	return r
}

func generatePolicyViolations() *demo.Run {
	r := demo.NewRun(
		"Process + persist policy violations",
		"Process any organisation-specific Open Policy Agent Policies and then report them",
	)

	r.Step(demo.S(
		"Generate policy violations",
	), demo.S(
		"dmd db generate --db dmd.db policy-violations --policies-directory policies",
	))

	r.Step(demo.S(
		"Report policy violations",
	), demo.S(
		"dmd report policy-violations --db dmd.db",
	))

	r.Step(demo.S(
		"Report policy violations for only projects owned by GDS",
	), demo.S(
		"dmd report policy-violations --db dmd.db --owner 'GDS*'",
	))

	r.Step(demo.S(
		"Report policy violations for projects in organisations that contain the word `tanna`",
	), demo.S(
		"dmd report policy-violations --db dmd.db --organisation '%tanna*'",
	))

	r.Step(demo.S(
		"Report policy violations for a specific project",
	), demo.S(
		"dmd report policy-violations --db dmd.db --platform gitlab --organisation 'tanna.dev' --repo dependency-management-data",
	))

	return r
}

func reportDependenton() *demo.Run {
	r := demo.NewRun(
		"Report usage of a given dependency",
		"Report usage of a given dependency, and optionally the specific version in use, across all known projects in the database.",
	)

	r.Step(demo.S(
		"Report usage of a given dependency",
		"The use of package-manager=golang targets SBOM-derived data",
	), demo.S(
		"dmd report dependenton --db dmd.db --package-manager=golang --package-name=golang.org/x/oauth2",
	))

	r.Step(demo.S(
		"Report usage of a given dependency, at a specific `version`",
		"The use of package-manager=gomod targets Renovate-derived data",
	), demo.S(
		"dmd report dependenton --db dmd.db --package-manager=gomod --package-name=golang.org/x/oauth2 --package-version v0.8.0",
	))

	r.Step(demo.S(
		"Report usage of a given dependency, at a specific `version`",
		"Note that the `version` must match the exact value in the DB",
	), demo.S(
		"dmd report dependenton --db dmd.db --package-manager=npm --package-name=@jamietanna/spectral-test-harness --package-version '^0.3.0'",
	))

	r.Step(demo.S(
		"Report usage of a given dependency, at a specific `current_version`",
		"Note that the `current_version` must match the exact value in the DB",
	), demo.S(
		"dmd report dependenton --db dmd.db --package-manager=npm --package-name=@jamietanna/spectral-test-harness --package-current-version '0.3.0'",
	))

	return r
}

func reportLibyear() *demo.Run {
	r := demo.NewRun(
		"Report the Libyears metric for each repository",
		"Report how many Libyears behind the latest release repositories are",
	)

	r.Step(demo.S(
		"Report all Libyears",
	), demo.S(
		"dmd report libyear --db dmd.db",
	))

	r.Step(demo.S(
		"Report Libyears for a given repository",
		"This shows a breakdown of which libraries were detected, and how the Libyear was calcuated",
	), demo.S(
		"dmd report libyear --db dmd.db --platform github --organisation stoplightio --repo spectral",
	))

	r.Step(demo.S(
		"Report Libyears for a given repository (with no results)",
		"This repository has no external dependencies",
	), demo.S(
		"dmd report libyear --db dmd.db --platform gitlab --organisation tanna.dev --repo serve",
	))

	return r
}

func reportFunding() *demo.Run {
	r := demo.NewRun(
		"Report packages that are looking for funding",
		"Report packages that you depend on, who have indicated they are looking for financial support.",
	)

	r.Step(demo.S(
		"Report all packages looking for funding",
	), demo.S(
		"dmd report funding --db dmd.db",
	))

	return r
}

func metadata() *demo.Run {
	r := demo.NewRun(
		"Interact with the database's internal metadata",
	)

	r.Step(demo.S(
		"List all database metadata",
	), demo.S(
		"dmd db meta list --db dmd.db",
	))

	r.Step(demo.S(
		"Finalise the database",
		"To indicate the data is at most this up-to-date",
	), demo.S(
		"dmd db meta finalise --db dmd.db",
	))

	r.Step(demo.S(
		"List all database metadata",
		"but this time, we should see `finalised_at`",
	), demo.S(
		"dmd db meta list --db dmd.db",
	))

	return r
}
