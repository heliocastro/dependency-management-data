package advisory

// AdvisoryType is the type of package advisory that is present
type AdvisoryType = string

const (
	AdvisoryTypeDeprecated   = "DEPRECATED"
	AdvisoryTypeUnmaintained = "UNMAINTAINED"
	AdvisoryTypeSecurity     = "SECURITY"
	AdvisoryTypePolicy       = "POLICY"
	AdvisoryTypeOther        = "OTHER"
)
