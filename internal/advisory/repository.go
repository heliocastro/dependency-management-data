package advisory

import (
	"context"
	"database/sql"

	"dmd.tanna.dev/internal/advisory/db"
)

type Advisories struct{}

func (*Advisories) Name() string {
	return "Advisories"
}

func (*Advisories) CreateTables(ctx context.Context, sqlDB *sql.DB) error {
	_, err := sqlDB.ExecContext(ctx, db.CreateTablesQuery)
	return err
}
