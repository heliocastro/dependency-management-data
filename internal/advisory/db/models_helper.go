package db

import "strings"

// NewRetrievePackageAdvisoriesLikeParams constructs a RetrievePackageAdvisoriesLikeParams with the correct defaults, and replaces wildcard characters (`*`) with the appropriate SQL
func NewRetrievePackageAdvisoriesLikeParams(platform, org, repo, owner, advisoryType string) RetrievePackageAdvisoriesLikeParams {
	params := RetrievePackageAdvisoriesLikeParams{
		Platform:     "%",
		Org:          "%",
		Repo:         "%",
		Owner:        "%",
		AdvisoryType: "%",
	}

	if platform != "" {
		params.Platform = strings.ReplaceAll(platform, "*", "%")
	}

	if org != "" {
		params.Org = strings.ReplaceAll(org, "*", "%")
	}

	if repo != "" {
		params.Repo = strings.ReplaceAll(repo, "*", "%")
	}

	if owner != "" {
		params.Owner = strings.ReplaceAll(owner, "*", "%")
	}

	if advisoryType != "" {
		params.AdvisoryType = strings.ReplaceAll(advisoryType, "*", "%")
	}

	return params
}

func (p RetrievePackageAdvisoriesLikeParams) IsWildcard() bool {
	return p.Platform == "%" && p.Org == "%" && p.Repo == "%" && p.Owner == "%" && p.AdvisoryType == "%"
}

// NewCountPackageAdvisoriesLikeParams constructs a CountPackageAdvisoriesLikeParams with the correct defaults, and replaces wildcard characters (`*`) with the appropriate SQL
func NewCountPackageAdvisoriesLikeParams(platform, org, repo, owner, advisoryType string) CountPackageAdvisoriesLikeParams {
	params := CountPackageAdvisoriesLikeParams{
		Platform:     "%",
		Org:          "%",
		Repo:         "%",
		Owner:        "%",
		AdvisoryType: "%",
	}

	if platform != "" {
		params.Platform = strings.ReplaceAll(platform, "*", "%")
	}

	if org != "" {
		params.Org = strings.ReplaceAll(org, "*", "%")
	}

	if repo != "" {
		params.Repo = strings.ReplaceAll(repo, "*", "%")
	}

	if owner != "" {
		params.Owner = strings.ReplaceAll(owner, "*", "%")
	}

	if advisoryType != "" {
		params.AdvisoryType = strings.ReplaceAll(advisoryType, "*", "%")
	}

	return params
}

func (p CountPackageAdvisoriesLikeParams) IsWildcard() bool {
	return p.Platform == "%" && p.Org == "%" && p.Repo == "%" && p.Owner == "%" && p.AdvisoryType == "%"
}
