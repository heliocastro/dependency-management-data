-- name: RetrievePackageAdvisoriesFromSeparateTables :many
select
  s.platform as _plat,
  s.organisation as _org,
  s.repo as _repo,
  s.package_name,
  s.version,
  s.current_version,
  package_type as package_manager,
  -- as SBOMs don't make this available, default to an empty array
  '[]' as dep_types,
  -- as SBOMs don't make this available, default to an empty string
  '' as package_file_path,
  owners.owner,
  a.advisory_type,
  a.level,
  a.description,
  NULL as supported_until,
  NULL as eol_from
from
  sboms s
  inner join custom_advisories as a on s.package_name like replace(a.package_pattern, '*', '%')
  left join owners on
  s.platform = owners.platform and
  s.organisation = owners.organisation and
  s.repo = owners.repo
where
s.version IS NOT NULL and
s.current_version IS NOT NULL and
(
   case
      when version_match_strategy IS NULL           then true
      when version_match_strategy = 'ANY'           then true
      when version_match_strategy = 'EQUAL'         then current_version = a.version
      when version_match_strategy = 'LESS_THAN'     then current_version < a.version
      when version_match_strategy = 'LESS_EQUAL'    then current_version <= a.version
      when version_match_strategy = 'GREATER_THAN'  then current_version > a.version
      when version_match_strategy = 'GREATER_EQUAL' then current_version >= a.version
      else false
    end)
union
select
  r.platform as _plat,
  r.organisation as _org,
  r.repo as _repo,
  r.package_name,
  r.version,
  r.current_version,
  r.package_manager,
  r.dep_types,
  r.package_file_path,
  owners.owner,
  advis.advisory_type,
  advis.level,
  advis.description,
  NULL as supported_until,
  NULL as eol_from
from
  renovate r
  inner join custom_advisories as advis on r.package_name like replace(advis.package_pattern, '*', '%')
  left join owners on
  r.platform = owners.platform and
  r.organisation = owners.organisation and
  r.repo = owners.repo
where
(
   case
      when version_match_strategy IS NULL           then true
      when version_match_strategy = 'ANY'           then true
      when version_match_strategy = 'EQUAL'         then current_version = advis.version
      when version_match_strategy = 'LESS_THAN'     then current_version < advis.version
      when version_match_strategy = 'LESS_EQUAL'    then current_version <= advis.version
      when version_match_strategy = 'GREATER_THAN'  then current_version > advis.version
      when version_match_strategy = 'GREATER_EQUAL' then current_version >= advis.version
      else false
    end)
union
select
  renovate.platform as _plat,
  renovate.organisation as _org,
  renovate.repo as _repo,
  e.package_name,
  e.version,
  e.current_version,
  renovate.package_manager,
  renovate.dep_types,
  renovate.package_file_path,
  owners.owner,
  (
    case
      when eol_from is not null
      and (cast ((julianday(eol_from) - julianday('now')) as integer))
           < 0
          then
            'UNMAINTAINED'
      when eol_from is not null
      and (cast ((julianday(eol_from) - julianday('now')) as integer))
           <= 60
           then
             'DEPRECATED'
      when supported_until is not null
      and (cast ((julianday(supported_until) - julianday('now')) as integer))
           < 0
          then
             'DEPRECATED'
      when supported_until is not null
      and (cast ((julianday(supported_until) - julianday('now')) as integer))
           <= 60
           then
             'DEPRECATED'
    end
  ) as advisory_type,
  'ERROR' as level,
  (
    case
      when eol_from is not null
      and (cast ((julianday(eol_from) - julianday('now')) as integer))
           < 0
          then
             e.product_name || ' ' || e.cycle
               || ' has been End-of-Life for '
               || abs(cast ((julianday(eol_from) - julianday('now')) as integer))
               || ' days'
      when eol_from is not null
      and (cast ((julianday(eol_from) - julianday('now')) as integer))
           <= 60
           then
             e.product_name || ' ' || e.cycle
               || ' will be End-of-Life in '
               || abs(cast ((julianday(eol_from) - julianday('now')) as integer))
               || ' days'
      when supported_until is not null
      and (cast ((julianday(supported_until) - julianday('now')) as integer))
           < 0
          then
             e.product_name || ' ' || e.cycle
               || ' has been unsupported (usually only receiving critical security fixes) for '
               || abs(cast ((julianday(supported_until) - julianday('now')) as integer))
               || ' days'
      when supported_until is not null
      and (cast ((julianday(supported_until) - julianday('now')) as integer))
           <= 60
           then
             e.product_name || ' ' || e.cycle
               || ' will be unsupported (usually only receiving critical security fixes) in '
               || abs(cast ((julianday(supported_until) - julianday('now')) as integer))
               || ' days'
    end
  ) as description,
  supported_until,
  eol_from
from
  renovate_endoflife e
  inner join endoflifedate_products on (
    e.product_name = endoflifedate_products.product_name
    AND e.cycle = endoflifedate_products.cycle
  )
  inner join renovate on (
    e.package_name = renovate.package_name
    AND (
      e.version = renovate.version
      AND case
        when e.current_version is NULL then true
        when e.current_version is NOT NULL then e.current_version = renovate.current_version
      end
    )
    AND e.package_manager = renovate.package_manager
    AND e.datasource = renovate.datasource
  )
  left join owners on
  renovate.platform = owners.platform and
  renovate.organisation = owners.organisation and
  renovate.repo = owners.repo
where
  (supported_until is not null or eol_from is not null)
  AND
  (
    (
      cast ((julianday(supported_until) - julianday('now')) as integer)
      <= 60
    )
    OR
    (
      cast ((julianday(eol_from) - julianday('now')) as integer)
      <= 60
    )
  )
union
select
  sboms.platform as _plat,
  sboms.organisation as _org,
  sboms.repo as _repo,
  e.package_name,
  e.version,
  e.current_version,
  sboms.package_type as package_manager,
  -- as SBOMs don't make this available, default to an empty array
  '[]' as dep_types,
  -- as SBOMs don't make this available, default to an empty string
  '' as package_file_path,
  owners.owner,
  (
    case
      when eol_from is not null
      and (cast ((julianday(eol_from) - julianday('now')) as integer))
           < 0
          then
            'UNMAINTAINED'
      when eol_from is not null
      and (cast ((julianday(eol_from) - julianday('now')) as integer))
           <= 60
           then
             'DEPRECATED'
      when supported_until is not null
      and (cast ((julianday(supported_until) - julianday('now')) as integer))
           < 0
          then
             'DEPRECATED'
      when supported_until is not null
      and (cast ((julianday(supported_until) - julianday('now')) as integer))
           <= 60
           then
             'DEPRECATED'
    end
  ) as advisory_type,
  'ERROR' as level,
  (
    case
      when eol_from is not null
      and (cast ((julianday(eol_from) - julianday('now')) as integer))
           < 0
          then
             e.product_name || ' ' || e.cycle
               || ' has been End-of-Life for '
               || abs(cast ((julianday(eol_from) - julianday('now')) as integer))
               || ' days'
      when eol_from is not null
      and (cast ((julianday(eol_from) - julianday('now')) as integer))
           <= 60
           then
             e.product_name || ' ' || e.cycle
               || ' will be End-of-Life in '
               || abs(cast ((julianday(eol_from) - julianday('now')) as integer))
               || ' days'
      when supported_until is not null
      and (cast ((julianday(supported_until) - julianday('now')) as integer))
           < 0
          then
             e.product_name || ' ' || e.cycle
               || ' has been unsupported (usually only receiving critical security fixes) for '
               || abs(cast ((julianday(supported_until) - julianday('now')) as integer))
               || ' days'
      when supported_until is not null
      and (cast ((julianday(supported_until) - julianday('now')) as integer))
           <= 60
           then
             e.product_name || ' ' || e.cycle
               || ' will be unsupported (usually only receiving critical security fixes) in '
               || abs(cast ((julianday(supported_until) - julianday('now')) as integer))
               || ' days'
    end
  ) as description,
  supported_until,
  eol_from
from
  sboms_endoflife e
  inner join endoflifedate_products on (
    e.product_name = endoflifedate_products.product_name
    AND e.cycle = endoflifedate_products.cycle
  )
  inner join sboms on (
    e.package_name = sboms.package_name
    AND (
      e.version = sboms.version
      AND case
        when e.current_version is NULL then true
        when e.current_version is NOT NULL then e.current_version = sboms.current_version
      end
    )
    AND e.package_type = sboms.package_type
  )
  left join owners on
  sboms.platform = owners.platform and
  sboms.organisation = owners.organisation and
  sboms.repo = owners.repo
where
  (sboms.version is not null and sboms.current_version is not null)
  and
  (supported_until is not null or eol_from is not null)
  AND
  (
    (
      cast ((julianday(supported_until) - julianday('now')) as integer)
      <= 60
    )
    OR
    (
      cast ((julianday(eol_from) - julianday('now')) as integer)
      <= 60
    )
  )
union
select
  s.platform as _plat,
  s.organisation as _org,
  s.repo as _repo,
  s.package_name,
  s.version,
  s.current_version,
  s.package_type as package_manager,
  -- as SBOMs don't make this available, default to an empty array
  '[]' as dep_types,
  -- as SBOMs don't make this available, default to an empty string
  '' as package_file_path,
  owners.owner,
  'SECURITY' as advisory_type,
  'ERROR' as level,
  'Package is vulnerable to CVE ' || cve_id as description,
  NULL as supported_until,
  NULL as eol_from
from
  sboms s
  inner join depsdev_cves on s.package_name = depsdev_cves.package_name
  and (case
       when s.current_version is not null then s.current_version = depsdev_cves.version
       else s.version = depsdev_cves.version
       end)
  left join owners on
  s.platform = owners.platform and
  s.organisation = owners.organisation and
  s.repo = owners.repo
where
  s.current_version IS NOT NULL
  or
  s.version IS NOT NULL
union
select
  r.platform as _plat,
  r.organisation as _org,
  r.repo as _repo,
  r.package_name,
  r.version,
  r.current_version,
  r.package_manager,
  r.dep_types,
  r.package_file_path,
  owners.owner,
  'SECURITY' as advisory_type,
  'ERROR' as level,
  'Package is vulnerable to CVE ' || cve_id as description,
  NULL as supported_until,
  NULL as eol_from
from
  renovate r
  inner join depsdev_cves on r.package_name = depsdev_cves.package_name
  and (case
       when r.current_version is not null then r.current_version = depsdev_cves.version
       else r.version = depsdev_cves.version
       end)
  left join owners on
  r.platform = owners.platform and
  r.organisation = owners.organisation and
  r.repo = owners.repo
where
  r.current_version IS NOT NULL
order by advisory_type, _plat, _org, _repo, package_name;

-- name: RetrievePackageAdvisories :many
select
  advisories.platform as _plat,
  advisories.organisation as _org,
  advisories.repo as _repo,
  package_name,
  version,
  current_version,
  dep_types,
  package_file_path,
  owners.owner,
  advisory_type,
  description
from
  advisories
  left join owners on
  advisories.platform = owners.platform and
  advisories.organisation = owners.organisation and
  advisories.repo = owners.repo
order by advisory_type, _plat, _org, _repo, package_name;

-- name: RetrievePackageAdvisoriesLike :many
select
  advisories.platform as _plat,
  advisories.organisation as _org,
  advisories.repo as _repo,
  package_name,
  version,
  current_version,
  dep_types,
  package_file_path,
  owners.owner,
  advisory_type,
  description
from
  advisories
  left join owners on
  advisories.platform = owners.platform and
  advisories.organisation = owners.organisation and
  advisories.repo = owners.repo
where
  advisories.platform like sqlc.arg(platform) and
  advisories.organisation like sqlc.arg(org) and
  advisories.repo like sqlc.arg(repo) and
  (
    -- checking for the owner is a little more complicated due to the fact that
    -- it's a nullable field
    -- TODO We may be able to simplify this.
    (
      sqlc.arg(owner) == '%' and
      (
        owners.owner like sqlc.arg(owner) or
        owners.owner IS NULL
      )
    ) or
    (
      sqlc.arg(owner) != '%' and
      owners.owner like sqlc.arg(owner)
    )
  )
  and
  advisories.advisory_type like sqlc.arg(advisory_type)
order by advisory_type, _plat, _org, _repo, package_name;


-- name: CountPackageAdvisoriesLike :many
select
_plat,
_org,
_repo,
owner,
SUM(
  case
    when advisory_type = 'DEPRECATED' then 1
    else 0
  end
) AS total_deprecated,
SUM(
  case
    when advisory_type = 'UNMAINTAINED' then 1
    else 0
  end
) AS total_unmaintained,
SUM(
  case
    when advisory_type = 'SECURITY' then 1
    else 0
  end
) AS total_security,
SUM(
  case
    when advisory_type = 'POLICY' then 1
    else 0
  end
) AS total_policy,
SUM(
  case
    when advisory_type = 'OTHER' then 1
    else 0
  end
) AS total_other
from (
    select
      advisories.platform as _plat,
      advisories.organisation as _org,
      advisories.repo as _repo,
      package_name,
      version,
      current_version,
      dep_types,
      package_file_path,
      owners.owner,
      advisory_type,
      description
    from
      advisories
      left join owners on
      advisories.platform = owners.platform and
      advisories.organisation = owners.organisation and
      advisories.repo = owners.repo
    where
      advisories.platform like sqlc.arg(platform) and
      advisories.organisation like sqlc.arg(org) and
      advisories.repo like sqlc.arg(repo) and
      (
        -- checking for the owner is a little more complicated due to the fact that
        -- it's a nullable field
        -- TODO We may be able to simplify this.
        (
          sqlc.arg(owner) == '%' and
          (
            owners.owner like sqlc.arg(owner) or
            owners.owner IS NULL
          )
        ) or
        (
          sqlc.arg(owner) != '%' and
          owners.owner like sqlc.arg(owner)
        )
      )
      and
      advisories.advisory_type like sqlc.arg(advisory_type)
  order by advisory_type, _plat, _org, _repo, package_name
) as c
group by _plat, _org, _repo
;

-- name: InsertAdvisory :exec
insert into advisories (
  platform,
  organisation,
  repo,
  package_name,
  version,
  current_version,
  package_manager,
  package_file_path,
  dep_types,
  level,
  advisory_type,
  description,
  supported_until,
  eol_from
) VALUES (
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?
);

-- name: RetrieveAWSAdvisories :many
select
  db.arn,
  db.name,
  printf('%s %s', db.engine, db.engine_version) as runtime,
  (
    case
      when
          (cast ((julianday(aws_rds_databases_engines.deprecation) - julianday('now')) as integer))
           < 0
          then
            'UNMAINTAINED'
      when
          (cast ((julianday(aws_rds_databases_engines.deprecation) - julianday('now')) as integer))
           <= 60
           then
             'DEPRECATED'
    end
  ) as advisory_type,
  (
    case
      when
          (cast ((julianday(aws_rds_databases_engines.deprecation) - julianday('now')) as integer))
           < 0
          then
             db.engine || ' ' || db.engine_version
               || ' has been End-of-Life for '
               || abs(cast ((julianday(aws_rds_databases_engines.deprecation) - julianday('now')) as integer))
               || ' days'
      when
          (cast ((julianday(aws_rds_databases_engines.deprecation) - julianday('now')) as integer))
           <= 60
           then
             db.engine || ' ' || db.engine_version
               || ' will be End-of-Life in '
               || abs(cast ((julianday(aws_rds_databases_engines.deprecation) - julianday('now')) as integer))
               || ' days'
    end
  ) as description
from
  aws_rds_databases as db
  natural join aws_rds_databases_engines
where
  (
    cast ((julianday(aws_rds_databases_engines.deprecation) - julianday('now')) as integer)
    <= 100
  )
union
select
  f.arn,
  f.name,
  f.runtime as runtime,
  (
    case
      when r.end_of_life is not null
      and (cast ((julianday(r.end_of_life) - julianday('now')) as integer))
           < 0
          then
            'UNMAINTAINED'
      when r.end_of_life is not null
      and (cast ((julianday(r.end_of_life) - julianday('now')) as integer))
           <= 60
           then
             'DEPRECATED'
      when r.deprecation is not null
      and (cast ((julianday(r.deprecation) - julianday('now')) as integer))
           < 0
          then
             'DEPRECATED'
      when r.deprecation is not null
      and (cast ((julianday(r.deprecation) - julianday('now')) as integer))
           <= 60
           then
             'DEPRECATED'
    end
  ) as advisory_type,
  (
    case
      when r.end_of_life is not null
      and (cast ((julianday(r.end_of_life) - julianday('now')) as integer))
           < 0
          then
            f.runtime
               || ' has been End-of-Life for '
              || abs(cast ((julianday(r.end_of_life) - julianday('now')) as integer))
               || ' days'
      when r.end_of_life is not null
      and (cast ((julianday(r.end_of_life) - julianday('now')) as integer))
           <= 60
           then
            f.runtime
               || ' will be End-of-Life in '
              || abs(cast ((julianday(r.end_of_life) - julianday('now')) as integer))
               || ' days'
      when r.deprecation is not null
      and (cast ((julianday(r.deprecation) - julianday('now')) as integer))
           < 0
          then
            f.runtime
               || ' has been deprecated '
               || abs(cast ((julianday(r.deprecation) - julianday('now')) as integer))
               || ' days'
      when r.deprecation is not null
        and (cast ((julianday(r.deprecation) - julianday('now')) as integer))
           <= 60
           then
            f.runtime
               || ' has been deprecated '
               || abs(cast ((julianday(r.deprecation) - julianday('now')) as integer))
               || ' days'
    end
  ) as description
from
  aws_lambda_functions f
  inner join aws_lambda_function_runtimes r on f.runtime = r.runtime
where
  (
    cast ((julianday(r.deprecation) - julianday('now')) as integer)
    <= 100
  )
union
select
  d.arn,
  d.name,
  printf('%s %s', d.engine, d.engine_version) as runtime,
  (
    case
      when
          (cast ((julianday(e.deprecation) - julianday('now')) as integer))
           < 0
          then
            'UNMAINTAINED'
      when
          (cast ((julianday(e.deprecation) - julianday('now')) as integer))
           <= 60
           then
             'DEPRECATED'
    end
  ) as advisory_type,
  (
    case
      when
          (cast ((julianday(e.deprecation) - julianday('now')) as integer))
           < 0
          then
             d.engine || ' ' || d.engine_version
               || ' has been End-of-Life for '
               || abs(cast ((julianday(e.deprecation) - julianday('now')) as integer))
               || ' days'
      when
          (cast ((julianday(e.deprecation) - julianday('now')) as integer))
           <= 60
           then
             d.engine || ' ' || d.engine_version
               || ' will be End-of-Life in '
               || abs(cast ((julianday(e.deprecation) - julianday('now')) as integer))
               || ' days'
    end
  ) as description
from
  aws_elasticache_datastores d
inner join aws_elasticache_datastore_engines e
where
      d.engine = e.engine
  and d.engine_version like e.engine_version || '.%'
  and
      (
        cast ((julianday(e.deprecation) - julianday('now')) as integer)
        <= 100
      )
;
