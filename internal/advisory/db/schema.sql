-- advisories contains the resulting Advisories
-- (https://dmd.tanna.dev/concepts/advisory/) and Policy Violations
-- (https://dmd.tanna.dev/concepts/policy/) that have been discovered by
-- dependency-management-data when running `dmd db generate advisories` and
-- `dmd db generate policy-violations` respectively.

-- The intent of this table is to make it easier to query all relevant sources
-- of advisories data, without you as the user needing to understand the
-- various sources of data in the utility tables that exist, and perform a long
-- `JOIN`.
--
-- It is still possible to directly query the different tables that feed into
-- this table, but is recommended that for most cases, you start using
-- this `advisories` table.
--
-- `advisories` also makes it possible to include more specific information
-- around the dependency that has the advisory, without requiring the complex
-- `JOIN`s across multiple tables.
CREATE TABLE IF NOT EXISTS advisories (
  -- what platform hosts the source code that this Advisory was produced for?
  -- i.e. `github`, `gitlab`, `gitea`, etc
  --
  -- See also: https://dmd.tanna.dev/concepts/repo-key/#platform
  --
  -- Foreign keys:
  -- - `renovate.platform`
  -- - `sboms.platform`
  platform TEXT NOT NULL,
  -- what organisation manages the source code that this Advisory was produced
  -- for? Can include `/` for nested organisations
  --
  -- See also: https://dmd.tanna.dev/concepts/repo-key/#organisation
  --
  -- Foreign keys:
  -- - `renovate.organisation`
  -- - `sboms.organisation`
  organisation TEXT NOT NULL,
  -- what repo manages the source code that this Advisory was produced
  -- for?
  --
  -- See also: https://dmd.tanna.dev/concepts/repo-key/#repo
  --
  -- Foreign keys:
  -- - `renovate.repo`
  -- - `sboms.repo`
  repo TEXT NOT NULL,

  -- what package is this Advisory for?
  --
  -- Foreign keys:
  -- - `renovate.package_name`
  -- - `sboms.package_name`
  package_name TEXT NOT NULL,
  -- version indicates the version of `package_name` that this Advisory
  -- is for.
  --
  -- NOTE this could be a version constraint, such as any of:
  --
  --   <=1.3.4,>=1.3.0
  --   "~> 0.9"
  --   latest
  --   ^2.0.6
  --   =1.0.4
  --
  -- As well as a specific value, such as:
  --
  --   1.0.4
  --   10
  --   latest
  --
  -- This versioning will be implementation-specific for the `package_manager` in use.
  --
  -- Foreign keys:
  -- - `renovate.version`
  -- - `sboms.version`
  version TEXT NOT NULL,
  -- current_version defines the current version that this package's `version`
  -- resolves to.
  --
  -- If the `version` is an exact version number, such as `1.0.4`, then
  -- `current_version` will usually be the same value, `1.0.4`.
  --
  -- If the `version` is a version constraint, then this column MAY indicate
  -- the exact version that was resolved at the time of dependency analysis.
  --
  -- Foreign keys:
  -- - `renovate.current_version`
  -- - `sboms.current_version`
  current_version TEXT,
  -- package_manager indicates the package manager that the Advisory
  -- will correspond to.
  --
  -- Based on which datasource(s) (https://dmd.tanna.dev/concepts/datasource/)
  -- you are using, this will be a different value:
  --
  -- - for Renovate data, must exactly match `renovate.package_manager`.
  --   Note that there may be multiple `package_managers`, for instance `maven`
  --   and `gradle`, which would require two rows.
  -- - for Software Bill of Materials (SBOM) data, must exactly match `sboms.package_type`
  --
  -- If you are using multiple datasources, you will have one row per
  -- `package_manager` that this Advisory matches.
  --
  -- Foreign keys:
  -- - `renovate.package_manager`
  -- - `sboms.package_type`
  package_manager TEXT NOT NULL,
  -- package_file_path defines the path within `repo` that defines the
  -- `package_name` as a dependency. For example:
  --
  --   .github/workflows/build.yml
  --   go.mod
  --   build/Dockerfile
  --
  -- NOTE that this may be empty
  -- (https://gitlab.com/tanna.dev/dependency-management-data/-/issues/396)
  --
  -- Foreign keys:
  -- - `renovate.package_file_path`
  -- - `sboms` does not have this field
  package_file_path TEXT NOT NULL,
  -- dep_types defines the different dependency types that may be in use. This
  -- will always be a JSON array, with 0 or more string elements. For example:
  --
  --   []
  --   ["action"]
  --   ["dependencies","lockfile"]
  --   ["dependencies","missing-data"]
  --   ["lockfile","lockfile-yarn-pinning-^21.1.1"]
  --   ["engines"]
  --
  -- Based on which datasource(s) (https://dmd.tanna.dev/concepts/datasource/)
  -- you are using, this will have different values and meanings.
  --
  -- TODO Querying this column will be found documented in
  -- https://gitlab.com/tanna.dev/dependency-management-data/-/issues/288
  --
  -- NOTE that in the future these there will be a more consistent naming
  -- structure for these
  -- (https://gitlab.com/tanna.dev/dependency-management-data/-/issues/379)
  --
  -- Foreign keys:
  -- - `renovate.package_file_path`
  -- - `sboms` does not have this field
  dep_types TEXT NOT NULL,

  -- level defines the severity of the Advisory. This will be
  -- organisation-specific in terms of what you deem most critical, but an
  -- example of what this could look like is:
  --
  --   ERROR: "Use of AGPL-3.0 licensed dependencies anywhere is a high-severity"
  --   WARN:  "Using a dependency that hasn't been updated in 1 year should be avoided"
  level TEXT NOT NULL
    CHECK (
      level IN (
        "ERROR",
        "WARN"
      )
    ),
  -- advisory_type defines the type of Advisory
  -- (https://dmd.tanna.dev/concepts/advisory/) that this Advisory will
  -- flagged as.
  advisory_type TEXT NOT NULL
    CHECK (
      advisory_type IN (
        -- the dependency is deprecated, and should ideally be replaced
        "DEPRECATED",
        -- the dependency is no longer maintained
        "UNMAINTAINED",
        -- there is a security issue with this dependency
        "SECURITY",
        -- there is organisational policy that recommends awareness of the use
        -- of this dependency
        "POLICY",
        -- there is no other `advisory_type` that makes sense for this type. If
        -- you feel there should be, please raise an issue on the issue tracker
        -- (https://gitlab.com/tanna.dev/dependency-management-data/-/issues)
        "OTHER"
      )
    ),
  -- description is a human-readable explanation of why this advisory is being
  -- flagged. The contents will be shown verbatim to a user, and will not be
  -- interpreted as markup. This can be as long and detailed as you wish, and
  -- is recommended to include links to (internal) documentation around the
  -- finding, any known remediation actions, and communication channels to
  -- reach out to for information.
  description TEXT NOT NULL,

  -- supported_until describes the date that this dependency is (actively)
  -- supported until
  --
  -- NOTE: that this is only relevant for `UNMAINTAINED` or `DEPRECATED` advisories
  supported_until TEXT,
  -- eol_from describes the date that this dependency will be marked as End of
  -- Life, and will no longer be maintained from
  --
  -- NOTE: that this is only relevant for `UNMAINTAINED` or `DEPRECATED` advisories
  eol_from TEXT,

  UNIQUE (platform, organisation, repo, package_file_path, package_name, package_manager, dep_types, level, advisory_type, description) ON CONFLICT REPLACE
);

-- custom_advisories is a table that makes it possible to flag Advisories
-- (https://dmd.tanna.dev/concepts/advisory/) with packages, for instance to
-- indicate lack of maintainence upstream, security issues, etc.
--
-- These give you the ability to provide additional insight into packages that
-- your projects use.
--
-- This data can be sourced through the `dmd contrib` subcommand, for
-- community-provided advisories, or alternatively you can add your own by
-- directly inserting rows into this table. See
-- https://dmd.tanna.dev/cookbooks/custom-advisories/ for more details on how
-- to do so.
--
-- The custom_advisories table will be joined against any package data
-- (https://dmd.tanna.dev/concepts/datasource/#package-data), which may have
-- different definitions for some columns - please make sure you review each
-- column's documentation.
CREATE TABLE IF NOT EXISTS custom_advisories (
  -- package_pattern defines an exact package name, or a pattern that should
  -- match a package name, indicating which package the advisory is for. this
  -- can either be an exact match, such as:
  --   `dmd.tanna.dev`
  -- or it can include a `*` character to indicate a wildcard such as:
  --   `*/oapi-codegen`
  --   `@my-org/*'
  --   `*tanna*`
  --   `*tan*na*`
  --
  -- Foreign keys:
  -- - `renovate.package_name`
  -- - `sboms.package_name`
  package_pattern TEXT NOT NULL,
  -- package_manager indicates the package manager that the given `package_pattern` should match.
  --
  -- Based on which datasource(s) (https://dmd.tanna.dev/concepts/datasource/)
  -- you are using, this will be a different value:
  -- - for Renovate data, must exactly match `renovate.package_manager`.
  --   Note that there may be multiple `package_managers`, for instance `maven`
  --   and `gradle`, which would require two rows.
  -- - for Software Bill of Materials (SBOM) data, must exactly match
  -- `sboms.package_type`
  --
  -- If you are using multiple datasources, you will need to have one row per `package_manager`.
  --
  -- Foreign keys:
  -- - `renovate.package_manager`
  -- - `sboms.package_type`
  package_manager TEXT NOT NULL,
  -- version defines version(s) that this advisory relates to.
  -- If NULL, any instances of this package (at any version) will be flagged.
  -- If non-NULL, the `version_match_strategy` will be taken into account.
  --
  -- Foreign keys:
  -- - `renovate.current_version`
  -- - `sboms.current_version`
  version TEXT,
  -- version_match_strategy defines how the advisory's `version` column gets
  -- matched against the given dependency.
  -- If NULL, `version_match_strategy` will be treated as if it were set to `ANY`
  -- If non-NULL, will perform the corresponding match type, which are documented below.
  --
  -- NOTE: that this is performed with a lexicographical match, which is NOT
  -- likely to be what you are expecting to perform version constraint matching
  -- For example:
  --   Performing a `GREATER_THAN` v1.10 would result in:
  --     v1.2.3
  --     v1.20.3
  --   Which does NOT match the expectation that you would only see `v1.20.3`.
  --
  -- If you would like more control over advisory data, and to perform true
  -- version number calculations, it's worth writing Policies using Open Policy
  -- Agent (https://dmd.tanna.dev/cookbooks/custom-advisories-opa/)
  --
  -- If `version` is NULL, this column is ignored.
  version_match_strategy TEXT
    CHECK (
      version_match_strategy IN (
        -- any packages that match `package_pattern` and `package_manager` will
        -- be classed as an Advisory
        "ANY",
        -- any packages that match `package_pattern` and `package_manager`, and
        -- has a `current_version` which is exactly equal to `version` will be
        -- classed as an Advisory
        "EQUALS",
        -- any packages that match `package_pattern` and `package_manager`, and
        -- has a `version < current_version` (lexicographically compared) will
        -- be classed as an Advisory
        "LESS_THAN",
        -- any packages that match `package_pattern` and `package_manager`, and
        -- has a `version <= current_version` (lexicographically compared) will
        -- be classed as an Advisory
        "LESS_EQUAL",
        -- any packages that match `package_pattern` and `package_manager`, and
        -- has a `version > current_version` (lexicographically compared) will
        -- be classed as an Advisory
        "GREATER_THAN",
        -- any packages that match `package_pattern` and `package_manager`, and
        -- has a `version >= current_version` (lexicographically compared) will
        -- be classed as an Advisory
        "GREATER_EQUAL"
      )
    ),
  -- level defines the severity of the Advisory. This will be
  -- organisation-specific in terms of what you deem most critical, but an
  -- example of what this could look like is:
  --
  --   ERROR: "Use of AGPL-3.0 licensed dependencies anywhere is a high-severity"
  --   WARN:  "Using a dependency that hasn't been updated in 1 year should be avoided"
  level TEXT NOT NULL
    DEFAULT 'ERROR'
    CHECK (
      level IN (
        "ERROR",
        "WARN"
      )
    ),
  -- advisory_type defines the type of Advisory
  -- (https://dmd.tanna.dev/concepts/advisory/) that this dependency will
  -- flagged as.
  --
  -- NOTE that this field is an exact match for the
  -- `custom_advisories.advisory_type` column
  advisory_type TEXT NOT NULL
    CHECK (
      advisory_type IN (
        -- the dependency is deprecated, and should ideally be replaced
        "DEPRECATED",
        -- the dependency is no longer maintained
        "UNMAINTAINED",
        -- there is a security issue with this dependency
        "SECURITY",
        -- there is organisational policy that recommends awareness of the use
        -- of this dependency
        "POLICY",
        -- there is no other `advisory_type` that makes sense for this type. If
        -- you feel there should be, please raise an issue on the issue tracker
        -- (https://gitlab.com/tanna.dev/dependency-management-data/-/issues)
        "OTHER"
      )
    ),
  -- description is a human-readable explanation of why this advisory is
  -- being flagged. The contents will be shown verbatim to a user, and will
  -- not be interpreted as markup. This can be as long and detailed as you
  -- wish, and is recommended to include links to (internal) documentation
  -- around the finding, any known remediation actions, and communication
  -- channels to reach out to for information.
  description TEXT NOT NULL,

  UNIQUE (package_pattern, package_manager, version, version_match_strategy, advisory_type, level, description) ON CONFLICT REPLACE
);
