package advisory

import (
	"context"
	"database/sql"
	"fmt"
	"log/slog"

	"dmd.tanna.dev/internal/advisory/db"
	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/mitchellh/go-wordwrap"
)

func ReportPackages(ctx context.Context, logger *slog.Logger, sqlDB *sql.DB, platform, org, repo, owner, advisoryType string, summaryOutput bool) (table.Writer, error) {
	queries := db.New(sqlDB)

	if summaryOutput {
		return reportPackagesSummary(ctx, logger, queries, platform, org, repo, owner, advisoryType)
	}

	return reportPackagesFull(ctx, logger, queries, platform, org, repo, owner, advisoryType)
}

func reportPackagesSummary(ctx context.Context, logger *slog.Logger, queries *db.Queries, platform, org, repo, owner, advisoryType string) (table.Writer, error) {
	tw := table.NewWriter()

	tw.AppendHeader(table.Row{
		"Platform",
		"Organisation",
		"Repo",
		"Owner",
		"# Deprecated",
		"# Unmaintained",
		"# Security",
		"# Policy",
		"# Other",
	})

	params := db.NewCountPackageAdvisoriesLikeParams(platform, org, repo, owner, advisoryType)

	rows, err := queries.CountPackageAdvisoriesLike(ctx, params)
	if err != nil {
		return nil, err
	}

	for _, row := range rows {
		tw.AppendRow(table.Row{
			row.Platform,
			row.Organisation,
			row.Repo,
			row.Owner.String,
			row.TotalDeprecated.Float64,
			row.TotalUnmaintained.Float64,
			row.TotalSecurity.Float64,
			row.TotalPolicy.Float64,
			row.TotalOther.Float64,
		})
	}

	return tw, nil
}

func reportPackagesFull(ctx context.Context, logger *slog.Logger, queries *db.Queries, platform, org, repo, owner, advisoryType string) (table.Writer, error) {
	tw := table.NewWriter()

	tw.AppendHeader(table.Row{
		"Platform",
		"Organisation",
		"Repo",
		"Package",
		"Version",
		"Dependency Types",
		"Filepath",
		"Owner",
		"Advisory Type",
		"Description",
	})

	params := db.NewRetrievePackageAdvisoriesLikeParams(platform, org, repo, owner, advisoryType)

	if params.IsWildcard() {
		rows, err := queries.RetrievePackageAdvisories(ctx)
		if err != nil {
			return nil, err
		}

		for _, row := range rows {
			if row.Version == "" {
				continue
			}
			ver := row.Version
			if row.CurrentVersion.Valid {
				ver = fmt.Sprintf("%s / %s", ver, row.CurrentVersion.String)
			}
			tw.AppendRow(table.Row{
				row.Platform,
				row.Organisation,
				row.Repo,
				row.PackageName,
				wordwrap.WrapString(ver, 10),
				row.DepTypes,
				row.PackageFilePath,
				sqlNullStringToString(row.Owner),
				row.AdvisoryType,
				wordwrap.WrapString(row.Description, 60),
			})
		}
	} else {
		rows, err := queries.RetrievePackageAdvisoriesLike(ctx, params)
		if err != nil {
			return nil, err
		}

		for _, row := range rows {
			if row.Version == "" {
				continue
			}
			ver := row.Version
			if row.CurrentVersion.Valid {
				ver = fmt.Sprintf("%s / %s", ver, row.CurrentVersion.String)
			}
			tw.AppendRow(table.Row{
				row.Platform,
				row.Organisation,
				row.Repo,
				row.PackageName,
				wordwrap.WrapString(ver, 10),
				row.DepTypes,
				row.PackageFilePath,
				sqlNullStringToString(row.Owner),
				row.AdvisoryType,
				wordwrap.WrapString(row.Description, 60),
			})
		}
	}

	return tw, nil
}

func ReportAWS(ctx context.Context, logger *slog.Logger, sqlDB *sql.DB) (table.Writer, error) {
	queries := db.New(sqlDB)

	rows, err := queries.RetrieveAWSAdvisories(ctx)
	if err != nil {
		return nil, err
	}

	tw := table.NewWriter()

	tw.AppendHeader(table.Row{
		"ARN",
		"Name",
		"Runtime",
		"Advisory Type",
		"Description",
	})

	for _, row := range rows {
		tw.AppendRow(table.Row{
			row.Arn,
			row.Name,
			sqlNullStringToString(row.Runtime),
			row.AdvisoryType,
			row.Description,
		})
	}

	return tw, err
}

func sqlNullStringToString(s sql.NullString) string {
	return s.String
}
