package advisory

import (
	"context"
	"database/sql"
	"fmt"
	"log/slog"

	"dmd.tanna.dev/internal/advisory/db"
	"github.com/jedib0t/go-pretty/v6/progress"
)

// CombineTablesIntoAdvisoriesTable migrates package advisories from different tables within dependency-management-data into the `advisories` table, for easier retrieval of data
func CombineTablesIntoAdvisoriesTable(ctx context.Context, logger *slog.Logger, sqlDB *sql.DB, pw progress.Writer) error {
	tx, err := sqlDB.BeginTx(ctx, nil)
	if err != nil {
		return fmt.Errorf("failed to create a transaction for CombineTablesIntoAdvisoriesTable: %w", err)
	}
	defer tx.Rollback()

	queries := db.New(sqlDB)

	rows, err := queries.RetrievePackageAdvisoriesFromSeparateTables(ctx)
	if err != nil {
		return fmt.Errorf("failed to retrieve package data advisories from their respective tables: %w", err)
	}

	for _, row := range rows {
		if !row.Version.Valid {
			continue
		}

		params := db.InsertAdvisoryParams{
			Platform:     row.Platform,
			Organisation: row.Organisation,
			Repo:         row.Repo,
			PackageName:  row.PackageName,
			Version:      row.Version.String,
			// CurrentVersion handled below
			PackageManager:  row.PackageManager,
			PackageFilePath: row.PackageFilePath,
			DepTypes:        row.DepTypes,
			Level:           row.Level,
			AdvisoryType:    row.AdvisoryType,
			Description:     row.Description,
			// SupportedUntil handled below
			// EolFrom handled below
		}

		if row.CurrentVersion.Valid {
			params.CurrentVersion = sql.NullString{
				String: row.CurrentVersion.String,
				Valid:  true,
			}
		}

		if val, ok := row.SupportedUntil.(string); ok && val != "" {
			params.SupportedUntil = sql.NullString{
				String: val,
				Valid:  true,
			}
		}

		if val, ok := row.EolFrom.(string); ok && val != "" {
			params.EolFrom = sql.NullString{
				String: val,
				Valid:  true,
			}
		}

		err = queries.WithTx(tx).InsertAdvisory(ctx, params)
		if err != nil {
			return fmt.Errorf("failed to insert row %#v: %w", params, err)
		}
	}

	return tx.Commit()
}
