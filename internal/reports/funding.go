package reports

import (
	"context"
	"database/sql"
	"fmt"
	"log/slog"
	"strings"

	"dmd.tanna.dev/internal/reports/db"
	"github.com/jedib0t/go-pretty/v6/table"
)

func Funding(ctx context.Context, logger *slog.Logger, sqlDB *sql.DB) (table.Writer, error) {
	tw := table.NewWriter()

	q := db.New(sqlDB)

	rows, err := q.ReportFunding(ctx)
	if err != nil {
		return nil, fmt.Errorf("failed to report funding data: %v", err)
	}

	if len(rows) == 0 {
		logger.Warn("No rows returned when querying funding information - have you run `dmd db generate dependency-health`? This could also be that there are none of your dependencies looking for financial support, or we just can't detect them")
		return nil, nil
	}

	tw.SetTitle("Dependencies that are looking for financial support")

	tw.AppendHeader(table.Row{
		"Package Name",
		"Package Manager",
		"% of total repos",
		"Number repos using (total)",
		"% of total dependencies",
		"Number usages (total)",
		"Funding Platforms",
	})

	for _, row := range rows {
		fundingRows := make([]string, 0, len(row.Funding))
		for k, v := range row.Funding {
			fundingRows = append(fundingRows, fmt.Sprintf("- %s: %s", k, v))
		}

		tw.AppendRow(table.Row{
			row.PackageName,
			row.PackageManager,
			row.PercentageRepos,
			fmt.Sprintf("%s (%s)", row.NumberRepos, row.TotalRepos),
			row.PercentageUsage,
			fmt.Sprintf("%s (%s)", row.NumberUsages, row.TotalDependencies),
			strings.Join(fundingRows, "\n"),
		})
	}

	return tw, nil
}
