package reports

import (
	"context"
	"database/sql"
	"fmt"
	"log/slog"

	"dmd.tanna.dev/internal/reports/db"
	"github.com/jedib0t/go-pretty/v6/table"
)

func DependentOn(ctx context.Context, logger *slog.Logger, sqlDB *sql.DB, packageName string, packageManager, packageVersion, packageCurrentVersion string) (table.Writer, error) {
	if packageName == "" {
		return nil, fmt.Errorf("the packageName provided to DependentOn was empty, but the value is required")
	}
	if packageManager == "" {
		return nil, fmt.Errorf("the packageManager provided to DependentOn was empty, but the value is required")
	}

	tw := table.NewWriter()

	q := db.New(sqlDB)
	if packageVersion == "" && packageCurrentVersion == "" {
		tw.AppendHeader(table.Row{
			"Platform",
			"Organisation",
			"Repo",
			"Version",
			"Current Version",
			"Dependency Types",
			"Filepath",
			"Owner",
		})

		params := db.RetrieveReposDependentOnParams{
			PackageName:    packageName,
			PackageManager: packageManager,
		}

		results, err := q.RetrieveReposDependentOn(ctx, params)
		if err != nil {
			return nil, err
		}

		if len(results) == 0 {
			logger.Warn("Expected the report to contain results, but there weren't any - could be an implementation issue, worth raising an issue for!")
		}

		for _, row := range results {
			tw.AppendRow(table.Row{
				row.Platform,
				row.Organisation,
				row.Repo,
				row.Version.String,
				row.CurrentVersion.String,
				row.DepTypes,
				row.PackageFilePath,
				row.Owner.String,
			})
		}
	} else if packageCurrentVersion != "" {
		tw.AppendHeader(table.Row{
			"Platform",
			"Organisation",
			"Repo",
			"Version",
			"Dependency Types",
			"Filepath",
			"Owner",
		})

		params := db.RetrieveReposDependentOnForCurrentVersionParams{
			PackageName:    packageName,
			PackageManager: packageManager,
			CurrentVersion: sql.NullString{
				String: packageCurrentVersion,
				Valid:  true,
			},
		}

		results, err := q.RetrieveReposDependentOnForCurrentVersion(ctx, params)
		if err != nil {
			return nil, err
		}

		if len(results) == 0 {
			logger.Warn("Expected the report to contain results, but there weren't any - could be an implementation issue, worth raising an issue for!")
		}

		for _, row := range results {
			tw.AppendRow(table.Row{
				row.Platform,
				row.Organisation,
				row.Repo,
				row.Version.String,
				row.DepTypes,
				row.PackageFilePath,
				row.Owner.String,
			})
		}
	} else if packageVersion != "" {
		tw.AppendHeader(table.Row{
			"Platform",
			"Organisation",
			"Repo",
			"Current Version",
			"Dependency Types",
			"Filepath",
			"Owner",
		})

		params := db.RetrieveReposDependentOnForVersionParams{
			PackageName:    packageName,
			PackageManager: packageManager,
			Version: sql.NullString{
				String: packageVersion,
				Valid:  true,
			},
		}

		results, err := q.RetrieveReposDependentOnForVersion(ctx, params)
		if err != nil {
			return nil, err
		}

		if len(results) == 0 {
			logger.Warn("Expected the report to contain results, but there weren't any - could be an implementation issue, worth raising an issue for!")
		}

		for _, row := range results {
			tw.AppendRow(table.Row{
				row.Platform,
				row.Organisation,
				row.Repo,
				row.CurrentVersion.String,
				row.DepTypes,
				row.PackageFilePath,
				row.Owner.String,
			})
		}
	}

	return tw, nil
}
