package db

import (
	"context"
	"encoding/json"
)

const queryReportFundingRow = `select
  package_name,
  package_manager,
  num_usages,
  printf('%.2f',
    (
    num_usages * 1.0 / (total_renovate + total_sboms)
  ) * 100) as percentage_usages,
  num_repos,
  printf('%.2f',
    (
    num_repos * 1.0 / (repos_renovate + repos_sboms)
  ) * 100) as percentage_repos,
  (total_renovate + total_sboms) as total_deps,
  (repos_renovate + repos_sboms) as total_repos,
  ecosystems_funding
from
  (
    select
      distinct renovate.package_name,
      renovate.package_manager as package_manager,
      count(*) as num_usages,
      count(distinct repo) as num_repos,
      ecosystems_funding
    from
      renovate
      inner join dependency_health on renovate.package_name = dependency_health.package_name
      and renovate.package_manager = dependency_health.package_manager
    where
      ecosystems_funding IS NOT NULL
    group by
      renovate.package_name,
      renovate.package_manager
    union
    select
      distinct sboms.package_name,
      sboms.package_type as package_manager,
      count(*) as num_usages,
      count(distinct repo) as num_repos,
      ecosystems_funding
    from
      sboms
      inner join dependency_health on sboms.package_name = dependency_health.package_name
      and sboms.package_type = dependency_health.package_manager
    where
      ecosystems_funding IS NOT NULL
    group by
      sboms.package_name,
      sboms.package_type
  ),
  (
    select
      count(*) as total_renovate
    from
      renovate
  ),
  (
    select
      count(*) as total_sboms
    from
      sboms
  ),
  (
    select
      count(distinct repo) as repos_renovate
    from
      renovate
  ),
  (
    select
      count(distinct repo) as repos_sboms
    from
      sboms
  )
group by
	package_name,
	package_manager
order by
  num_repos desc
`

type ReportFundingRow struct {
	PackageName       string
	PackageManager    string
	NumberUsages      string
	PercentageUsage   string
	TotalDependencies string
	NumberRepos       string
	PercentageRepos   string
	TotalRepos        string
	rawFunding        string
	Funding           map[string]string
}

func (q *Queries) ReportFunding(ctx context.Context) ([]ReportFundingRow, error) {
	rows, err := q.db.QueryContext(ctx, queryReportFundingRow)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var items []ReportFundingRow
	for rows.Next() {
		var i ReportFundingRow
		if err := rows.Scan(
			&i.PackageName,
			&i.PackageManager,
			&i.NumberUsages,
			&i.PercentageUsage,
			&i.NumberRepos,
			&i.PercentageRepos,
			&i.TotalDependencies,
			&i.TotalRepos,
			&i.rawFunding,
		); err != nil {
			return nil, err
		}

		// ignore the error, hopefully it won't fail, but if it does, it'll default to an empty map
		_ = json.Unmarshal([]byte(i.rawFunding), &i.Funding)

		items = append(items, i)
	}
	if err := rows.Close(); err != nil {
		return nil, err
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return items, nil
}
