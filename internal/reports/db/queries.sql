-- name: RetrieveReposDependentOn :many
select
  distinct
  s.platform,
  s.organisation,
  s.repo,
  s.version,
  s.current_version,
  -- as SBOMs don't make this available, default to an empty array
  '[]' as dep_types,
  -- as SBOMs don't make this available, default to an empty string
  '' as package_file_path,
  owners.owner,
  owners.notes as owner_notes
from
  sboms s
  left join owners on
  s.platform = owners.platform and
  s.organisation = owners.organisation and
  s.repo = owners.repo
where
s.version IS NOT NULL and
s.current_version IS NOT NULL and
s.package_name = sqlc.arg(package_name) and
s.package_type = sqlc.arg(package_manager)
union
select
  r.platform,
  r.organisation,
  r.repo,
  r.version,
  r.current_version,
  r.dep_types,
  r.package_file_path,
  owners.owner,
  owners.notes as owner_notes
from
  renovate r
  left join owners on
  r.platform = owners.platform and
  r.organisation = owners.organisation and
  r.repo = owners.repo
where
r.package_name = sqlc.arg(package_name) and
r.package_manager = sqlc.arg(package_manager)
;

-- name: RetrieveReposDependentOnForVersion :many
select
  distinct
  s.platform,
  s.organisation,
  s.repo,
  s.current_version,
  -- as SBOMs don't make this available, default to an empty array
  '[]' as dep_types,
  -- as SBOMs don't make this available, default to an empty string
  '' as package_file_path,
  owners.owner,
  owners.notes as owner_notes
from
  sboms s
  left join owners on
  s.platform = owners.platform and
  s.organisation = owners.organisation and
  s.repo = owners.repo
where
s.version IS NOT NULL and
s.current_version IS NOT NULL and
s.package_name = sqlc.arg(package_name) and
s.package_type = sqlc.arg(package_manager) and
s.version = sqlc.arg(version)
union
select
  r.platform,
  r.organisation,
  r.repo,
  r.current_version,
  r.dep_types,
  r.package_file_path,
  owners.owner,
  owners.notes as owner_notes
from
  renovate r
  left join owners on
  r.platform = owners.platform and
  r.organisation = owners.organisation and
  r.repo = owners.repo
where
r.package_name = sqlc.arg(package_name) and
r.package_manager = sqlc.arg(package_manager) and
r.version = sqlc.arg(version)
;

-- name: RetrieveReposDependentOnForCurrentVersion :many
select
  distinct
  s.platform,
  s.organisation,
  s.repo,
  s.version,
  -- as SBOMs don't make this available, default to an empty array
  '[]' as dep_types,
  -- as SBOMs don't make this available, default to an empty string
  '' as package_file_path,
  owners.owner,
  owners.notes as owner_notes
from
  sboms s
  left join owners on
  s.platform = owners.platform and
  s.organisation = owners.organisation and
  s.repo = owners.repo
where
s.version IS NOT NULL and
s.current_version IS NOT NULL and
s.package_name = sqlc.arg(package_name) and
s.package_type = sqlc.arg(package_manager) and
s.current_version = sqlc.arg(current_version)
union
select
  r.platform,
  r.organisation,
  r.repo,
  r.version,
  r.dep_types,
  r.package_file_path,
  owners.owner,
  owners.notes as owner_notes
from
  renovate r
  left join owners on
  r.platform = owners.platform and
  r.organisation = owners.organisation and
  r.repo = owners.repo
where
r.package_name = sqlc.arg(package_name) and
r.package_manager = sqlc.arg(package_manager) and
r.current_version = sqlc.arg(current_version)
;
