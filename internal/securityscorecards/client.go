package securityscorecards

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

type Client struct {
	httpClient *http.Client
}

func NewClient(httpClient *http.Client) Client {
	if httpClient == nil {
		httpClient = http.DefaultClient
	}

	return Client{
		httpClient: httpClient,
	}
}

const baseURL = "https://api.securityscorecards.dev"

// GetResult retrieves a ScorecardResult for a given repository.
//
// `repoPath` should be the URL to a repository with no scheme in it, for instance `https://gitlab.com/gitlab-org/sbom/generator` would become `gitlab.com/gitlab-org/sbom/generator`
func (c *Client) GetResult(ctx context.Context, repoPath string) (*ScorecardResult, error) {
	// NOTE that we're being a little lazy here, directly passing the repoPath
	u := fmt.Sprintf("%s/projects/%s", baseURL, repoPath)

	resp, err := c.httpClient.Get(u)
	if err != nil {
		return nil, fmt.Errorf("failed to look up Scorecards data for %s: %w", repoPath, err)
	}

	if resp.StatusCode != http.StatusOK {
		return nil, nil
	}

	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read response body: %w", err)
	}

	var r ScorecardResult
	err = json.Unmarshal(body, &r)
	if err != nil {
		return nil, fmt.Errorf("failed to unmarshal response body: %w", err)
	}

	return &r, nil
}
