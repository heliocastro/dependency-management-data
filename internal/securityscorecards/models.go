package securityscorecards

type ScorecardResult struct {
	Score  float32          `json:"score" yaml:"score"`
	Checks []ScorecardCheck `json:"checks" yaml:"checks"`
	Repo   ScorecardRepo    `json:"repo"`
}

type ScorecardCheck struct {
	Name  string `json:"name"`
	Score int    `json:"score"`
}

type ScorecardRepo struct {
	Name string `json:"name"`
}
