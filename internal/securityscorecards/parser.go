package securityscorecards

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"sync"

	"github.com/jedib0t/go-pretty/v6/progress"
)

type Parser struct{}

func NewParser() Parser {
	return Parser{}
}

func (p Parser) ParseFile(filename string) (ScorecardResult, error) {
	data, err := os.ReadFile(filename)
	if err != nil {
		return ScorecardResult{}, err
	}

	var result ScorecardResult
	err = json.Unmarshal(data, &result)
	if err != nil {
		return ScorecardResult{}, err
	}

	return result, nil
}

func (p Parser) ParseFiles(glob string, pw progress.Writer) ([]ScorecardResult, error) {
	files, err := filepath.Glob(glob)
	if err != nil {
		return nil, err
	}

	if files == nil {
		return nil, fmt.Errorf("no files could be found for glob %s", glob)
	}

	tracker := progress.Tracker{
		Message: "Parsing Security Scorecards reports",
		Total:   int64(len(files)),
	}
	pw.AppendTracker(&tracker)

	go pw.Render()

	var wg sync.WaitGroup
	results := make([]ScorecardResult, len(files))

	for i, f := range files {
		wg.Add(1)

		go func(i int, filename string) {
			defer wg.Done()
			defer tracker.Increment(1)

			f, err := p.ParseFile(filename)
			if err != nil {
				log.Printf("Failed to parse %s: %v", filename, err)
				return
			}
			results[i] = f
		}(i, f)
	}

	wg.Wait()
	tracker.UpdateMessage(fmt.Sprintf("Parsed %d Security Scorecards reports", tracker.Total))
	tracker.MarkAsDone()

	return results, nil
}
