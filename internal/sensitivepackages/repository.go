package sensitivepackages

import (
	"context"
	"database/sql"

	"dmd.tanna.dev/internal/sensitivepackages/db"
)

type SensitivePackages struct{}

func (s *SensitivePackages) CreateTables(ctx context.Context, sqlDB *sql.DB) error {
	_, err := sqlDB.ExecContext(ctx, db.CreateTablesQuery)
	return err
}
