-- Sensitive Packages are a way to mark a package as 'sensitive', such that the
-- package's details should not be processed through any external systems,
-- which limits the risk of sensitive metadata exposure for your organisation.
-- More details about usage can be found in
-- https://dmd.tanna.dev/cookbooks/avoiding-sensitive-package-names/
CREATE TABLE IF NOT EXISTS sensitive_packages (
  -- package_pattern defines an exact package name, or a pattern that should
  -- match a package name, indicating which package(s) are or are not
  -- sensitive. this can either be an exact match, such as:
  --   `dmd.tanna.dev`
  -- or it can include a `*` character to indicate a wildcard such as:
  --   `*/oapi-codegen`
  --   `@my-org/*'
  --   `*tanna*`
  --   `*tan*na*`
  package_pattern TEXT NOT NULL,
  -- package_manager indicates the package manager that the given
  -- `package_pattern` should match.
  --
  -- Based on which datasource(s) (https://dmd.tanna.dev/concepts/datasource/)
  -- you are using, this will be a different value:
  -- - for Renovate data, must exactly match `renovate.package_manager`.
  --   Note that there may be multiple `package_managers`, for instance `maven`
  --   and `gradle`, which would require two rows.
  -- - for Software Bill of Materials (SBOM) data, must exactly match `sboms.package_type`
  --
  -- If you are using multiple datasources, you will need to have one row per
  -- `package_manager`.
  --
  -- Foreign keys:
  -- - `renovate.package_manager`
  -- - `sboms.package_type`
  package_manager TEXT,
  -- match_strategy defines how to mark the package's sensitivity.
  -- If `MATCHES`, any packages that match `package_pattern` and `package_manager` (if set) will be marked as sensitive
  -- If `DOES_NOT_MATCH`, any packages that do not match `package_pattern` and `package_manager` (if set) will be marked as sensitive
  match_strategy TEXT NOT NULL
    CHECK (
      match_strategy IN (
        "MATCHES",
        "DOES_NOT_MATCH"
      )
    ),

  UNIQUE (package_pattern, package_manager, match_strategy)
);
