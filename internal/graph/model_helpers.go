package graph

import (
	"encoding/json"

	"dmd.tanna.dev/internal/graph/db"
	"dmd.tanna.dev/internal/graph/model"
)

func newAdvisoryFromRetrievePackageAdvisoriesForRepoRow(row db.RetrievePackageAdvisoriesForRepoRow) model.Advisory {
	advisory := model.Advisory{
		Dependency: &model.Dependency{
			PackageName: row.PackageName,
			Version:     row.Version,
			// CurrentVersion is handled below
			PackageManager: row.PackageManager,
			// PackageFilePath is handled below
			// DepTypes is handled below
		},
		AdvisoryType: model.AdvisoryType(row.AdvisoryType),
		Description:  row.Description,
		// SupportedUntil is handled below
		// EndOfLifeFrom is handled below
	}

	if row.CurrentVersion.Valid {
		// advisory.Dependency.CurrentVersion = copyValue(row.CurrentVersion.String)
		advisory.Dependency.CurrentVersion = &row.CurrentVersion.String
	}
	if row.PackageFilePath != "" {
		advisory.Dependency.PackageFilePath = &row.PackageFilePath
	}
	// ignore errors, as it'll be treated as an empty array
	_ = json.Unmarshal([]byte(row.DepTypes), &advisory.Dependency.DepTypes)

	if row.SupportedUntil.Valid {
		advisory.SupportedUntil = &row.SupportedUntil.String
	}
	if row.EolFrom.Valid {
		advisory.EndOfLifeFrom = &row.EolFrom.String
	}
	return advisory
}

func newPolicyViolationFromRetrievePolicyViolationsForRepoRow(row db.RetrievePolicyViolationsForRepoRow) model.PolicyViolation {
	violation := model.PolicyViolation{
		Dependency: &model.Dependency{
			PackageName: row.PackageName,
			Version:     row.Version,
			// CurrentVersion is handled below
			PackageManager: row.PackageManager,
			// PackageFilePath is handled below
			// DepTypes is handled below
		},
		Level:        model.PolicyViolationLevel(row.Level),
		AdvisoryType: model.AdvisoryType(row.AdvisoryType),
		Description:  row.Description,
	}

	if row.CurrentVersion.Valid {
		// advisory.Dependency.CurrentVersion = copyValue(row.CurrentVersion.String)
		violation.Dependency.CurrentVersion = &row.CurrentVersion.String
	}
	if row.PackageFilePath != "" {
		violation.Dependency.PackageFilePath = &row.PackageFilePath
	}
	// ignore errors, as it'll be treated as an empty array
	_ = json.Unmarshal([]byte(row.DepTypes), &violation.Dependency.DepTypes)

	return violation
}
