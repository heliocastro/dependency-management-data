package db

//go:generate go run dmd.tanna.dev/cmd/table-joiner ../../datasources/renovate/db/schema.sql ../../datasources/sbom/db/schema.sql ../../ownership/db/schema.sql ../../advisory/db/schema.sql ../../repositorymetadata/db/schema.sql ../../dependencyhealth/db/schema.sql ../../libyear/db/schema.sql
//go:generate go run github.com/sqlc-dev/sqlc/cmd/sqlc generate
