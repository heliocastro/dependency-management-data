-- name: RetrieveOwner :one
select
  owner,
  notes
from
  owners
  where
  owners.platform = sqlc.arg(platform) and
  owners.organisation = sqlc.arg(organisation) and
  owners.repo = sqlc.arg(repo)
;

-- name: ListReposLike :many
select
	distinct
	platform,
	organisation,
	repo
from
	renovate
where
  renovate.platform like sqlc.arg(platform) and
  renovate.organisation like sqlc.arg(organisation) and
  renovate.repo like sqlc.arg(repo)
union
select
	distinct
	platform,
	organisation,
	repo
from
	sboms
where
  sboms.platform like sqlc.arg(platform) and
  sboms.organisation like sqlc.arg(organisation) and
  sboms.repo like sqlc.arg(repo)
;

-- name: RetrieveDependenciesFor :many
select
  renovate.package_name,
  renovate.version as version,
  current_version as current_version,
  renovate.package_manager as package_manager,
  dep_types as dep_types,
  package_file_path as package_file_path
from
  renovate
where
  renovate.platform = sqlc.arg(platform) and
  renovate.organisation = sqlc.arg(organisation) and
  renovate.repo = sqlc.arg(repo)
union
select
  sboms.package_name as package_name,
  sboms.version as version,
  current_version as current_version,
  package_type as package_manager,
  -- as SBOMs don't make this available, default to an empty array
  '[]' as dep_types,
  -- as SBOMs don't make this available, default to an empty string
  '' as package_file_path
from
  sboms
where
  (
    sboms.version is not null
    or
    sboms.current_version is not null
  )
  and
  sboms.platform = sqlc.arg(platform) and
  sboms.organisation = sqlc.arg(organisation) and
  sboms.repo = sqlc.arg(repo)
;

-- name: CountDependenciesFor :many
select
  count(*)
from
  renovate
where
  renovate.platform = sqlc.arg(platform) and
  renovate.organisation = sqlc.arg(organisation) and
  renovate.repo = sqlc.arg(repo)
union
select
  count(*)
from
  sboms
where
  (
    sboms.version is not null
    or
    sboms.current_version is not null
  )
  and
  sboms.platform = sqlc.arg(platform) and
  sboms.organisation = sqlc.arg(organisation) and
  sboms.repo = sqlc.arg(repo)
;

-- name: RetrievePackageAdvisoriesForRepo :many
select
  package_name,
  version,
  current_version,
  package_manager,
  dep_types,
  package_file_path,
  advisory_type,
  description,
  supported_until,
  eol_from
from
  advisories
where
  advisories.platform = sqlc.arg(platform) and
  advisories.organisation = sqlc.arg(org) and
  advisories.repo = sqlc.arg(repo)
order by advisory_type;

-- name: CountPackageAdvisoriesForRepo :many
select
  count(*)
from
  advisories
where
  advisories.platform = sqlc.arg(platform) and
  advisories.organisation = sqlc.arg(org) and
  advisories.repo = sqlc.arg(repo)
;

-- name: RetrievePolicyViolationsForRepo :many
select
  package_name,
  version,
  current_version,
  package_manager,
  dep_types,
  package_file_path,
  level,
  advisory_type,
  description
from advisories
where
  platform = sqlc.arg(platform) and
  organisation = sqlc.arg(org) and
  repo = sqlc.arg(repo) and
  advisory_type = 'POLICY'
order by
  platform,
  organisation,
  repo,
  level,
  advisory_type;

-- name: CountPolicyViolationsForRepo :many
select
  count(*)
from advisories
where
  platform = sqlc.arg(platform) and
  organisation = sqlc.arg(org) and
  repo = sqlc.arg(repo) and
  advisory_type = 'POLICY'
;

-- name: RetrieveRepositoryMetadata :one
select
  is_monorepo,
  is_fork,
  repository_type,
  repository_usage,
  visibility,
  description,
  additional_metadata
from repository_metadata
where
  platform = sqlc.arg(platform) and
  organisation = sqlc.arg(org) and
  repo = sqlc.arg(repo)
;

-- name: RetrieveDependencyHealthFor :one
select
  *
from
  dependency_health
where
  dependency_health.package_name = sqlc.arg(package_name)
  and
  dependency_health.package_manager = sqlc.arg(package_manager)
;

-- name: RetrieveLibyearsForRepo :one
select
  sum(libyear) as total_libyears
from
  renovate
  inner join libyears
  on  renovate.package_name    = libyears.package_name
  and renovate.version         = libyears.version
  and renovate.current_version = libyears.current_version
  and renovate.package_manager = libyears.package_manager
where
  renovate.platform     like sqlc.arg(platform) and
  renovate.organisation like sqlc.arg(org) and
  renovate.repo         like sqlc.arg(repo)
group by
  renovate.platform,
  renovate.organisation,
  renovate.repo
union
select
  sum(libyear) as total_libyears
from
  sboms
  inner join libyears
  on  sboms.package_name    = libyears.package_name
  and sboms.version         = libyears.version
  and sboms.current_version = libyears.current_version
  and sboms.package_type    = libyears.package_manager
where
  sboms.platform     like sqlc.arg(platform) and
  sboms.organisation like sqlc.arg(org) and
  sboms.repo         like sqlc.arg(repo)
group by
  sboms.platform,
  sboms.organisation,
  sboms.repo
;
