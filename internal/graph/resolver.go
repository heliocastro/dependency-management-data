package graph

import (
	"database/sql"
	"log/slog"
)

type Resolver struct {
	db     *sql.DB
	logger *slog.Logger
}

func NewResolver(db *sql.DB, logger *slog.Logger) *Resolver {
	return &Resolver{
		db:     db,
		logger: logger,
	}
}
