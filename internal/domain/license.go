package domain

type License struct {
	PackageName    string
	Version        string
	PackageManager string
	License        string
}

func (l License) Valid() bool {
	return l.PackageName != "" && l.Version != "" && l.PackageManager != "" && l.License != ""
}
