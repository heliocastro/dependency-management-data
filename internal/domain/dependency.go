package domain

import (
	"fmt"
)

type Dependency struct {
	Platform     string
	Organisation string
	Repo         string

	PackageName    string
	Version        string
	CurrentVersion *string

	PackageManager  string
	PackageFilePath string
}

type DependencyUpdate struct {
	Dependency

	NewVersion string
	UpdateType string
}

func (d Dependency) DependencyDetails() string {
	if d.CurrentVersion != nil {
		return fmt.Sprintf("(%s@%s/%s)", d.PackageName, d.Version, *d.CurrentVersion)
	}
	return fmt.Sprintf("(%s@%s/<nil>)", d.PackageName, d.Version)
}
