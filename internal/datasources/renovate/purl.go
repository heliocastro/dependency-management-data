package renovate

import (
	"slices"

	"github.com/package-url/packageurl-go"
)

type PurlTypeDeriver func(packageManager string, datasource string) (purlType string, found bool)

var PurlTypeDerivers []PurlTypeDeriver

type PurlModifierOpts struct {
	PurlType  string
	Namespace string
	Name      string
	Version   string
}

type PurlModifier func(in PurlModifierOpts, dep Dependency) (result PurlModifierOpts, found bool)

var PurlModifiers []PurlModifier

var knownPurlTypes []string

func init() {
	knownPurlTypes = make([]string, 0, len(packageurl.KnownTypes))
	for k := range packageurl.KnownTypes {
		knownPurlTypes = append(knownPurlTypes, k)
	}

	PurlTypeDerivers = append(PurlTypeDerivers, func(packageManager, datasource string) (purlType string, found bool) {
		if packageManager != "gomod" {
			return "", false
		}

		if datasource == "golang-version" {
			return datasource, true
		}

		return "golang", true
	})

	PurlTypeDerivers = append(PurlTypeDerivers, func(packageManager, datasource string) (purlType string, found bool) {
		if packageManager != "github-actions" {
			return "", false
		}

		if datasource == "github-runners" {
			return datasource, true
		}

		return "github", true // TODO lowercase name
	})

	PurlTypeDerivers = append(PurlTypeDerivers, func(packageManager, datasource string) (purlType string, found bool) {
		if datasource != "hex" {
			return "", false
		}

		return "hex", true
	})

	PurlTypeDerivers = append(PurlTypeDerivers, func(packageManager, datasource string) (purlType string, found bool) {
		if packageManager != "mix" {
			return "", false
		}

		return "hex", true
	})

	PurlTypeDerivers = append(PurlTypeDerivers, func(packageManager, datasource string) (purlType string, found bool) {
		if packageManager == "dockerfile" {
			return "docker", true
		}

		if packageManager == "docker-compose" {
			return "docker", true
		}

		return "", false
	})

	PurlTypeDerivers = append(PurlTypeDerivers, func(packageManager, datasource string) (purlType string, found bool) {
		if packageManager != "bundler" {
			return "", false
		}

		return "gem", true
	})

	PurlTypeDerivers = append(PurlTypeDerivers, func(packageManager, datasource string) (purlType string, found bool) {
		if datasource != "helm" {
			return "", false
		}

		return "helm", true
	})

	PurlTypeDerivers = append(PurlTypeDerivers, func(packageManager, datasource string) (purlType string, found bool) {
		if packageManager != "sbt" {
			return "", false
		}

		return "maven", true
	})

	PurlTypeDerivers = append(PurlTypeDerivers, func(packageManager, datasource string) (purlType string, found bool) {
		packageManagers := []string{"pep621", "pip_requirements", "pip_setup", "pipenv", "poetry", "pyenv"}
		if !slices.Contains(packageManagers, packageManager) {
			return "", false
		}

		return "pypi", true
	})

	PurlTypeDerivers = append(PurlTypeDerivers, func(packageManager, datasource string) (purlType string, found bool) {
		if packageManager != "bun" {
			return "", false
		}

		return "npm", true
	})

	PurlTypeDerivers = append(PurlTypeDerivers, func(packageManager, datasource string) (purlType string, found bool) {
		if packageManager != "gradle" {
			return "", false
		}

		return "maven", true
	})

	PurlTypeDerivers = append(PurlTypeDerivers, func(packageManager, datasource string) (purlType string, found bool) {
		if packageManager != "deps-edn" || packageManager == "leiningen" {
			return "", false
		}

		return "clojars", true
	})

	PurlTypeDerivers = append(PurlTypeDerivers, func(packageManager, datasource string) (purlType string, found bool) {
		if packageManager != "gitlabci" && datasource != "docker" {
			return "", false
		}

		return "docker", true
	})

	PurlTypeDerivers = append(PurlTypeDerivers, func(packageManager, datasource string) (purlType string, found bool) {
		// fall back to the packageManager
		return packageManager, true
	})

	PurlModifiers = append(PurlModifiers, func(in PurlModifierOpts, dep Dependency) (result PurlModifierOpts, found bool) {
		if dep.PackageManager != "jenkins" {
			return in, false
		}

		result.PurlType = "maven"
		result.Namespace = "org.jenkins-ci.plugins"
		return result, true
	})
}

func ToPurl(dep Dependency) *packageurl.PackageURL {
	var purlType string
	var namespace string
	var name string
	var version string

	for _, ptd := range PurlTypeDerivers {
		t, found := ptd(dep.PackageManager, dep.Datasource)

		if found {
			purlType = t
			break
		}
	}

	// TODO revisit during https://gitlab.com/tanna.dev/dependency-management-data/-/issues/232
	name = dep.PackageName

	version = depVersionInfo(dep)

	in := PurlModifierOpts{
		PurlType:  purlType,
		Namespace: namespace,
		Name:      name,
		Version:   version,
	}

	for _, m := range PurlModifiers {
		if out, ok := m(in, dep); ok {
			if out.PurlType != "" {
				purlType = out.PurlType
			}
			if out.Namespace != "" {
				namespace = out.Namespace
			}
			if out.Name != "" {
				name = out.Name
			}
			if out.Version != "" {
				version = out.Version
			}
			break
		}
	}

	return packageurl.NewPackageURL(purlType, namespace, name, version, nil, "")
}

func IsKnownType(purlType string) bool {
	return slices.Contains(knownPurlTypes, purlType)
}

func depVersionInfo(dep Dependency) string {
	if dep.CurrentVersion != nil {
		return *dep.CurrentVersion
	}
	return dep.Version
}
