package renovate

import (
	"bufio"
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"sync"

	"dmd.tanna.dev/internal/domain"
	"github.com/jedib0t/go-pretty/v6/progress"
)

type parser struct{}

func NewParser() parser {
	return parser{}
}

type packageData map[string][]struct {
	Deps []struct {
		DepName     string `json:"depName"`
		PackageName string `json:"packageName"`
		// PackageManager comes from the map key
		Datasource     string   `json:"datasource"`
		DepType        string   `json:"depType"`
		DepTypes       []string `json:"depTypes"`
		CurrentValue   string   `json:"currentValue"`
		LockedVersion  string   `json:"lockedVersion"`
		FixedVersion   string   `json:"fixedVersion"`
		CurrentVersion string   `json:"currentVersion"`
		Updates        []struct {
			NewVersion string `json:"newVersion"`
			NewValue   string `json:"newValue"`
			UpdateType string `json:"updateType"`
		} `json:"updates"`
	} `json:"deps"`
	PackageFile string `json:"packageFile"`
}

type maybeRenovateDebugLogEntry struct {
	LogContext string `json:"logContext"`
	Message    string `json:"msg"`
}

type renovateDebugLogEntry struct {
	Repository string      `json:"repository"`
	Config     packageData `json:"config"`
}

type renovateGraphData struct {
	Organisation string      `json:"organisation"`
	Repo         string      `json:"repo"`
	PackageData  packageData `json:"packageData"`
	Metadata     struct {
		Renovate struct {
			Platform string `json:"platform"`
		} `json:"renovate"`
	} `json:"metadata"`
}

// maxLineLength is the number of characters in a log line that we're able to parse. Right now this is 32MB which is a significantly large set of dependencies, and is based on 11MB containing ~50000 dependencies and ~4000 dependency upgrades.
const maxLineLength = 32 * 1024 * 1024

func (p parser) ParseFile(filename string) ([]Dependency, []DependencyUpdate, error) {
	data, err := os.ReadFile(filename)
	if err != nil {
		return nil, nil, err
	}

	deps, depUpdates, err := p.parseRenovateDebugLogLines(data)
	if err != nil {
		return nil, nil, fmt.Errorf("failed to parse %s as Renovate debug log lines: %v", filename, err)
	}

	if len(deps) > 0 || len(depUpdates) > 0 {
		return deps, depUpdates, nil
	}

	return p.parseRenovateGraphData(data)
}

func (p parser) parseRenovateDebugLogLines(data []byte) ([]Dependency, []DependencyUpdate, error) {
	var deps []Dependency
	var depUpdates []DependencyUpdate
	var errs []error
	var m sync.Mutex

	scanner := bufio.NewScanner(bytes.NewReader(data))
	scanner.Split(bufio.ScanLines)
	buf := make([]byte, maxLineLength)
	scanner.Buffer(buf, maxLineLength)

	var wg sync.WaitGroup
	for scanner.Scan() {
		wg.Add(1)

		go func(data []byte) {
			defer wg.Done()

			d, du, err := p.parseRenovateDebugLogLine(data)
			if err != nil {
				m.Lock()
				errs = append(errs, fmt.Errorf("failed to parse a Renovate debug log line: %v", err))
				m.Unlock()
				return
			}
			if len(d) > 0 {
				m.Lock()
				deps = append(deps, d...)
				m.Unlock()
			}
			if len(du) > 0 {
				m.Lock()
				depUpdates = append(depUpdates, du...)
				m.Unlock()
			}
		}(scanner.Bytes())
	}

	wg.Wait()

	err := errors.Join(errs...)
	if err != nil {
		return nil, nil, err
	}

	err = scanner.Err()
	if errors.Is(err, bufio.ErrTooLong) {
		return nil, nil, fmt.Errorf("the Renovate debug log line was too long: amend `renovate.maxLineLength` to handle larger log lines than %dMB", maxLineLength/1024/1024)
	} else if err != nil {
		return nil, nil, fmt.Errorf("failed to read the Renovate debug log lines: %v", err)
	}

	return deps, depUpdates, nil
}

func (p parser) parseRenovateDebugLogLine(data []byte) ([]Dependency, []DependencyUpdate, error) {
	var tmpEntry maybeRenovateDebugLogEntry
	err := json.Unmarshal(data, &tmpEntry)
	if err != nil {
		return nil, nil, fmt.Errorf("failed to unmarshal line from JSON: %v", err)
	}

	// if it's not got a `LogContext`, it's probably not a Renovate debug log
	if tmpEntry.LogContext == "" {
		return nil, nil, nil
	}

	// if the log message is `packageFiles with updates`, it's the line we want, via https://github.com/renovatebot/renovate/discussions/13150#discussioncomment-1823497
	// TODO: doesn't handle when using `--dry-run=extract` https://gitlab.com/tanna.dev/dependency-management-data/-/issues/496
	if tmpEntry.Message != "packageFiles with updates" {
		return nil, nil, nil
	}

	var logEntry renovateDebugLogEntry
	err = json.Unmarshal(data, &logEntry)
	if err != nil {
		return nil, nil, fmt.Errorf("failed to unmarshal line from JSON: %v", err)
	}

	org, repo := parseRepoWithOrg(logEntry.Repository)
	deps, depUpdates, err := p.parsePackageData("unknown", org, repo, logEntry.Config)
	if err != nil {
		return nil, nil, fmt.Errorf("failed to parse package data from line: %v", err)
	}

	return deps, depUpdates, nil
}

func (p parser) parseRenovateGraphData(data []byte) ([]Dependency, []DependencyUpdate, error) {
	var rgData renovateGraphData
	err := json.Unmarshal(data, &rgData)
	if err != nil {
		return nil, nil, err
	}

	return p.parsePackageData(rgData.Metadata.Renovate.Platform, rgData.Organisation, rgData.Repo, rgData.PackageData)
}

func (parser) parsePackageData(platform string, organisation string, repo string, data packageData) ([]Dependency, []DependencyUpdate, error) {
	var deps []Dependency
	var depUpdates []DependencyUpdate
	for packageManager, v := range data {
		for _, container := range v {
			for _, dep := range container.Deps {
				dep := dep

				versions := strings.Split(dep.CurrentValue, "\n")
				for _, version := range versions {
					d := Dependency{
						Dependency: domain.Dependency{
							Platform:     platform,
							Organisation: organisation,
							Repo:         repo,

							// PackageName is being added below
							Version: version,
							// CurrentVersion is being added below

							PackageManager:  packageManager,
							PackageFilePath: container.PackageFile,
						},
						Datasource: dep.Datasource,

						// DepTypes is being added below
					}

					if dep.DepName != "" && !strings.Contains(dep.DepName, "\n") {
						d.PackageName = dep.DepName
					} else if dep.PackageName != "" && !strings.Contains(dep.PackageName, "\n") {
						d.PackageName = dep.PackageName
					}

					if dep.LockedVersion != "" && dep.FixedVersion == "" {
						d.CurrentVersion = &dep.LockedVersion
					} else if dep.LockedVersion == "" && dep.FixedVersion != "" {
						d.CurrentVersion = &dep.FixedVersion
					} else if dep.LockedVersion != "" && dep.FixedVersion != "" {
						d.CurrentVersion = &dep.LockedVersion
						if dep.LockedVersion != dep.FixedVersion {
							// TODO warn that they're not the same, but we're only using one of them
						}
					} else if dep.CurrentVersion != "" {
						d.CurrentVersion = &dep.CurrentVersion
					}

					if dep.DepTypes != nil {
						d.DepTypes = dep.DepTypes
					} else if dep.DepType != "" {
						d.DepTypes = []string{dep.DepType}
					}

					deps = append(deps, d)

					for _, update := range dep.Updates {
						depUpdate := DependencyUpdate{
							DependencyUpdate: domain.DependencyUpdate{
								Dependency: d.Dependency,

								NewVersion: update.NewVersion,
								UpdateType: update.UpdateType,
							},
							Datasource: d.Datasource,
						}

						if depUpdate.NewVersion == "" {
							depUpdate.NewVersion = update.NewValue
						}

						depUpdates = append(depUpdates, depUpdate)
					}
				}
			}
		}
	}

	return deps, depUpdates, nil
}

func (p parser) ParseFiles(glob string, pw progress.Writer) ([]Dependency, []DependencyUpdate, error) {
	files, err := filepath.Glob(glob)
	if err != nil {
		return nil, nil, err
	}

	if files == nil {
		return nil, nil, fmt.Errorf("no files could be found for glob %s", glob)
	}

	tracker := progress.Tracker{
		Message: "Parsing Renovate files",
		Total:   int64(len(files)),
	}
	pw.AppendTracker(&tracker)

	go pw.Render()

	var wg sync.WaitGroup
	allDeps := make([][]Dependency, len(files))
	allDepUpdates := make([][]DependencyUpdate, len(files))

	for i, f := range files {
		wg.Add(1)

		go func(i int, f string) {
			defer wg.Done()
			defer tracker.Increment(1)

			deps, depUpdates, err := p.ParseFile(f)
			if err != nil {
				log.Printf("Failed to parse %s: %v", f, err)
				return
			}
			allDeps[i] = deps
			allDepUpdates[i] = depUpdates
		}(i, f)
	}

	wg.Wait()
	tracker.UpdateMessage(fmt.Sprintf("Parsed %d Renovate files", tracker.Total))
	tracker.MarkAsDone()

	return flatten(allDeps), flatten(allDepUpdates), nil
}

func flatten[T any](s [][]T) []T {
	var out []T
	for _, v := range s {
		out = append(out, v...)
	}
	return out
}

func parseRepoWithOrg(s string) (org string, repo string) {
	return filepath.Dir(s), filepath.Base(s)
}
