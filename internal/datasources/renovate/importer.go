package renovate

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"

	"dmd.tanna.dev/internal/datasources/renovate/db"
	"github.com/jedib0t/go-pretty/v6/progress"
)

type importer struct{}

func NewImporter() importer {
	return importer{}
}

func (importer) ImportDependencies(ctx context.Context, deps []Dependency, sqlDB *sql.DB, pw progress.Writer) error {
	d := db.New(sqlDB)

	tx, err := sqlDB.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	tracker := progress.Tracker{
		Message: fmt.Sprintf("Importing %d Renovate dependencies", len(deps)),
		Total:   int64(len(deps)),
	}
	pw.AppendTracker(&tracker)

	go pw.Render()

	for _, dep := range deps {
		arg := db.InsertPackageParams{
			Platform:        dep.Platform,
			Organisation:    dep.Organisation,
			Repo:            dep.Repo,
			PackageName:     dep.PackageName,
			Version:         dep.Version,
			PackageManager:  dep.PackageManager,
			PackageFilePath: dep.PackageFilePath,
			Datasource:      dep.Datasource,
		}

		if dep.CurrentVersion != nil {
			arg.CurrentVersion = sql.NullString{
				String: *dep.CurrentVersion,
				Valid:  true,
			}
		}

		arg.DepTypes = serialiseDepTypes(dep.DepTypes)

		err := d.WithTx(tx).InsertPackage(ctx, arg)
		if err != nil {
			tracker.MarkAsErrored()
			return err
		}

		tracker.Increment(1)
	}

	tracker.MarkAsDone()
	tracker.UpdateMessage(fmt.Sprintf("Imported %d Renovate dependencies", tracker.Total))

	return tx.Commit()
}

func (importer) ImportDependencyUpdates(ctx context.Context, depUpdates []DependencyUpdate, sqlDB *sql.DB, pw progress.Writer) error {
	d := db.New(sqlDB)

	tx, err := sqlDB.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	tracker := progress.Tracker{
		Message: fmt.Sprintf("Importing %d Renovate dependency updates", len(depUpdates)),
		Total:   int64(len(depUpdates)),
	}
	pw.AppendTracker(&tracker)

	go pw.Render()

	for _, dep := range depUpdates {
		arg := db.InsertPackageUpdateParams{
			Platform:        dep.Platform,
			Organisation:    dep.Organisation,
			Repo:            dep.Repo,
			PackageName:     dep.PackageName,
			Version:         dep.Version,
			PackageManager:  dep.PackageManager,
			PackageFilePath: dep.PackageFilePath,
			Datasource:      dep.Datasource,

			NewVersion: dep.NewVersion,
			UpdateType: dep.UpdateType,
		}

		if dep.CurrentVersion != nil {
			arg.CurrentVersion = sql.NullString{
				String: *dep.CurrentVersion,
				Valid:  true,
			}
		}

		err := d.WithTx(tx).InsertPackageUpdate(ctx, arg)
		if err != nil {
			tracker.MarkAsErrored()
			return err
		}

		tracker.Increment(1)
	}

	tracker.MarkAsDone()
	tracker.UpdateMessage(fmt.Sprintf("Imported %d Renovate dependency updates", tracker.Total))

	return tx.Commit()
}

func serialiseDepTypes(depTypes []string) string {
	if depTypes == nil {
		// as json.Marshal will return `null` not `[]`
		return "[]"
	}

	data, err := json.Marshal(depTypes)
	if err != nil || data == nil {
		// default to an empty array to allow for parsing more easily
		return "[]"
	}

	return string(data)
}
