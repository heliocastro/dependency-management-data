-- name: InsertDatabase :exec
INSERT INTO aws_rds_databases (
  account_id,
  region,
  arn,
  name,
  engine,
  engine_version,
  tags
) VALUES (
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?
);

-- name: RetrieveAll :many
select * from aws_rds_databases;

-- name: InsertDatabaseEngine :exec
INSERT INTO aws_rds_databases_engines (
  engine,
  engine_version,
  deprecation
) VALUES (
  ?,
  ?,
  ?
);

-- name: RetrieveAllWithDeprecation :many
select
  arn,
  name,
  db.engine,
  db.engine_version,
  deprecation
from
  aws_rds_databases as db
  natural join aws_rds_databases_engines
;
