package awsrds

import (
	"encoding/json"
	"log"

	"dmd.tanna.dev/internal/datasources/awsrds/db"
)

type Database struct {
	AccountId     string
	Region        string
	ARN           string
	Name          string
	Engine        string
	EngineVersion string
	Tags          map[string]string
}

func newDatabaseFromDB(dbRow db.AwsRdsDatabase) Database {
	f := Database{
		AccountId:     dbRow.AccountID,
		Region:        dbRow.Region,
		ARN:           dbRow.Arn,
		Name:          dbRow.Name,
		Engine:        dbRow.Engine,
		EngineVersion: dbRow.EngineVersion,
	}

	err := json.Unmarshal([]byte(dbRow.Tags), &f.Tags)
	if err != nil {
		log.Printf("Failed to unmarshal `Tags` from DB row for RDS Database ARN %s", f.ARN)
	}

	return f
}
