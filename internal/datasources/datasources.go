package datasources

import (
	"dmd.tanna.dev/internal/datasources/awselasticache"
	"dmd.tanna.dev/internal/datasources/awslambda"
	"dmd.tanna.dev/internal/datasources/awsrds"
	"dmd.tanna.dev/internal/datasources/renovate"
	"dmd.tanna.dev/internal/datasources/sbom"
)

type Datasource interface{}

var datasources = []Datasource{
	&renovate.Renovate{},
	&sbom.SBOMs{},
	&awselasticache.AWSElasticache{},
	&awslambda.AWSLambda{},
	&awsrds.AWSRds{},
}
