package awselasticache

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"

	"dmd.tanna.dev/internal/datasources/awselasticache/db"
	"github.com/jedib0t/go-pretty/v6/progress"
)

type importer struct{}

func NewImporter() importer {
	return importer{}
}

func (importer) ImportDatastores(ctx context.Context, datastores []Datastore, sqlDB *sql.DB, pw progress.Writer) error {
	d := db.New(sqlDB)

	tx, err := sqlDB.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	tracker := progress.Tracker{
		Message: fmt.Sprintf("Importing %d aws-elasticache-endoflife datastores", len(datastores)),
		Total:   int64(len(datastores)),
	}
	pw.AppendTracker(&tracker)

	go pw.Render()

	for _, f := range datastores {
		arg := db.InsertDatastoreParams{
			AccountID:     f.AccountId,
			Region:        f.Region,
			Arn:           f.ARN,
			Name:          f.Name,
			Engine:        f.Engine,
			EngineVersion: f.EngineVersion,
		}

		data, err := json.Marshal(f.Tags)
		if err != nil {
			tracker.MarkAsErrored()
			return err
		}

		arg.Tags = string(data)

		err = d.WithTx(tx).InsertDatastore(ctx, arg)
		if err != nil {
			tracker.MarkAsErrored()
			return err
		}

		tracker.Increment(1)
	}

	tracker.MarkAsDone()
	tracker.UpdateMessage(fmt.Sprintf("Imported %d aws-elasticache-endoflife datastores", tracker.Total))

	return tx.Commit()
}
