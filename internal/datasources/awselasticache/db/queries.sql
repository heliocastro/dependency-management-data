-- name: InsertDatastore :exec
INSERT INTO aws_elasticache_datastores (
  account_id,
  region,
  arn,
  name,
  engine,
  engine_version,
  tags
) VALUES (
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?
);

-- name: RetrieveAll :many
select * from aws_elasticache_datastores;

-- name: InsertDatastoreEngine :exec
INSERT INTO aws_elasticache_datastore_engines (
  engine,
  engine_version,
  deprecation
) VALUES (
  ?,
  ?,
  ?
);

-- name: RetrieveAllWithDeprecation :many
select
  arn,
  name,
  d.engine,
  d.engine_version,
  e.deprecation
from
  aws_elasticache_datastores d
inner join aws_elasticache_datastore_engines e
where d.engine = e.engine
and d.engine_version like e.engine_version || '.%'
