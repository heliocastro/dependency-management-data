package awselasticache

import (
	"context"
	"database/sql"

	"dmd.tanna.dev/internal/datasources/awselasticache/db"
)

type AWSElasticache struct{}

func (*AWSElasticache) Name() string {
	return "AWS RDS"
}

func (*AWSElasticache) CreateTables(ctx context.Context, sqlDB *sql.DB) error {
	_, err := sqlDB.ExecContext(ctx, db.CreateTablesQuery)
	return err
}
