package sbom

import (
	"dmd.tanna.dev/internal/datasources/sbom/cyclonedx"
	"dmd.tanna.dev/internal/datasources/sbom/spdx"
	"dmd.tanna.dev/internal/domain"
)

type SBOMFormat interface {
	Matches(body []byte) bool
	Name() string
	Parse(body []byte, platform string, org string, repo string) ([]domain.SBOMDependency, []domain.License)
}

func Formats() []SBOMFormat {
	return []SBOMFormat{
		&spdx.SPDXv2_2JSONFormat{},
		&spdx.SPDXv2_2YAMLFormat{},
		&spdx.SPDXv2_3JSONFormat{},
		&spdx.SPDXv2_3YAMLFormat{},
		&cyclonedx.CycloneDXv1_4JSONFormat{},
		&cyclonedx.CycloneDXv1_4XMLFormat{},
		&cyclonedx.CycloneDXv1_5JSONFormat{},
		&cyclonedx.CycloneDXv1_5XMLFormat{},
	}
}

func Identify(body []byte) (SBOMFormat, bool) {
	for _, format := range Formats() {
		if format.Matches(body) {
			return format, true
		}
	}

	return nil, false
}
