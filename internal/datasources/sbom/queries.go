package sbom

import (
	"context"
	"database/sql"
	"sort"
	"strings"

	"dmd.tanna.dev/internal/datasources/queries"
	"dmd.tanna.dev/internal/datasources/sbom/db"
)

func (*SBOMs) QueryMostPopularPackageManagers(ctx context.Context, sqlDB *sql.DB) ([]queries.MostPopularPackageManagersRow, error) {
	q := db.New(sqlDB)

	rows, err := q.QueryMostPopularPackageManagers(ctx)
	if err != nil {
		return nil, err
	}

	results := make([]queries.MostPopularPackageManagersRow, len(rows))
	for i, row := range rows {
		results[i] = queries.MostPopularPackageManagersRow{
			PackageManager: row.PackageType,
			Count:          row.Count,
		}
	}
	return results, nil
}

func (*SBOMs) QueryMostPopularDockerImages(ctx context.Context, sqlDB *sql.DB) (result queries.MostPopularDockerImagesResult, err error) {
	q := db.New(sqlDB)

	rows, err := q.QueryMostPopularDockerImages(ctx)
	if err != nil {
		return
	}

	registries := make(map[string]int64)
	namespaces := make(map[string]int64)
	images := make(map[string]int64)

	for _, row := range rows {
		if len(row.PackageName) > 2 && strings.HasPrefix(row.PackageName, "_/") {
			row.PackageName = strings.Replace(row.PackageName, "_/", "library/", 1)
		}

		images[row.PackageName] = row.Count

		parts := strings.Split(row.PackageName, "/")

		var registry string

		if strings.Contains(parts[0], ".") {
			registry = parts[0]
		} else {
			registry = "docker.io"
		}

		current := registries[registry]
		registries[registry] = current + row.Count

		var namespace string
		if len(parts) == 1 {
			namespace = "_"
		} else if len(parts) == 2 {
			namespace = parts[0]
		} else if strings.Contains(parts[0], ".") {
			namespace = strings.Join(parts[:len(parts)-1], "/")
		} else {
			// this ideally shouldn't get hit, but if it does, return the full package name
			namespace = row.PackageName
		}

		if namespace == "_" {
			namespace = "library"
		}

		current = namespaces[namespace]
		namespaces[namespace] = current + row.Count
	}

	result.Registries = descendingSort(registries)
	result.Namespaces = descendingSort(namespaces)
	result.Images = descendingSort(images)

	return
}

func (*SBOMs) QueryGolangCILint(ctx context.Context, sqlDB *sql.DB) (result queries.GolangCILintResult, err error) {
	q := db.New(sqlDB)

	directRows, err := q.QueryGolangCILint(ctx)
	if err != nil {
		return
	}

	result.Direct = make([]queries.RepoWithOwner, len(directRows))

	for i, row := range directRows {
		result.Direct[i] = queries.RepoWithOwner{
			Platform:     row.Platform,
			Organisation: row.Organisation,
			Repo:         row.Repo,
			Owner:        sqlNullStringToStringPointer(row.Owner),
		}
	}

	return
}

func descendingSort(m map[string]int64) []queries.Count {
	keys := make([]string, 0, len(m))
	for k := range m {
		keys = append(keys, k)
	}

	sort.SliceStable(keys, func(i, j int) bool {
		return m[keys[i]] > m[keys[j]]
	})

	result := make([]queries.Count, len(keys))
	for i, key := range keys {
		result[i] = queries.Count{
			Name:  key,
			Count: m[key],
		}
	}

	return result
}

func sqlNullStringToStringPointer(s sql.NullString) *string {
	if !s.Valid {
		return nil
	}

	return &s.String
}
