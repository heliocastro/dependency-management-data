package sbom

import (
	"fmt"
	"os"

	"dmd.tanna.dev/internal/domain"
)

type Parser struct{}

func NewParser() Parser {
	return Parser{}
}

func (Parser) ParseFile(filename string, platform string, org string, repo string) ([]domain.SBOMDependency, []domain.License, error) {
	body, err := os.ReadFile(filename)
	if err != nil {
		return nil, nil, fmt.Errorf("failed to open %s: %w", filename, err)
	}

	format, found := Identify(body)
	if !found {
		return nil, nil, fmt.Errorf("the SBOM format provided isn't supported at this time, please raise an issue!")
	}

	deps, licenses := format.Parse(body, platform, org, repo)
	return deps, licenses, nil
}
