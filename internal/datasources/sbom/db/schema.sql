-- sboms contains all dependency data that an external tool has detected and
-- exported into a Software Bill of Materials (SBOM).
--
-- This is one of the first-class package Datasources
-- (https://dmd.tanna.dev/concepts/datasource/) that dependency-management-data
-- supports.
CREATE TABLE IF NOT EXISTS sboms (
  -- what platform hosts the source code that this SBOM was produced for? i.e.
  -- `github`, `gitlab`, `gitea`, etc
  --
  -- See also: https://dmd.tanna.dev/concepts/repo-key/#platform
  platform TEXT NOT NULL,
  -- what organisation manages the source code that this SBOM was produced for?
  -- Can include `/` for nested organisations
  --
  -- See also: https://dmd.tanna.dev/concepts/repo-key/#organisation
  organisation TEXT NOT NULL,
  -- what repo manages the source code that this SBOM was produced for?
  --
  -- See also: https://dmd.tanna.dev/concepts/repo-key/#repo
  repo TEXT NOT NULL,

  -- what package is this?
  package_name TEXT NOT NULL,
  -- version indicates the version of this dependency.
  --
  -- NOTE this could be a version constraint, such as any of:
  --
  --   <=1.3.4,>=1.3.0
  --   "~> 0.9"
  --   latest
  --   ^2.0.6
  --   =1.0.4
  --                   (NULL)
  --
  -- As well as a specific value, such as:
  --
  --   1.0.4
  --   10
  --   latest
  --
  -- This versioning will be implementation-specific for the `package_manager`
  -- in use.
  --
  -- NOTE that due to the quality of the tool producing the SBOM, this field
  -- may be NULL.
  version TEXT,
  -- current_version defines the current version that this package's `version`
  -- resolves to.
  --
  -- If the `version` is an exact version number, such as `1.0.4`, then
  -- `current_version` will usually be the same value, `1.0.4`.
  --
  -- NOTE that due to the quality of the tool producing the SBOM, this field
  -- may be NULL, or this may not be an exact value, but a version constraint
  -- similar to `version`.
  current_version TEXT,

  -- package_type most commonly relates to the "Type" field of a Package URL
  -- (as defined by https://github.com/package-url/purl-spec), which may be a
  -- package ecosystem or package manager type
  package_type TEXT NOT NULL,

  UNIQUE (platform, organisation, repo, package_name, package_type) ON CONFLICT REPLACE
);
