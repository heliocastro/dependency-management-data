-- name: InsertPackage :exec
INSERT INTO sboms (
  platform,
  organisation,
  repo,
  package_name,
  version,
  current_version,
  package_type
) VALUES (
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?
);

-- name: RetrieveDistinctPackages :many
select
  distinct
  package_name,
  version,
  current_version,
  package_type
from
  sboms
where
  version is not null
  or
  current_version is not null
;

-- name: RetrieveDistinctPackagesWithoutSensitivePackages :many
select
  distinct package_name,
  version,
  current_version,
  sboms.package_type
from
  sboms
  left join sensitive_packages as sp on
    sboms.package_name like replace(sp.package_pattern, '*', '%')
where
  (
    version is not null
    or
    current_version is not null
  )
  and
  (
    case
      when sp.package_manager IS NOT NULL then sp.package_manager == sboms.package_type
      else true
    end
  )
  and (
    case
      when sp.match_strategy = 'MATCHES' then false
      when sp.match_strategy = 'DOES_NOT_MATCH' then true
      else true
    end
  );

-- name: RetrieveReposAndPackagesForMissingData :many
select
  platform,
  organisation,
  repo,
  package_name,
  version,
  current_version,
  package_type
from
  sboms
  left join sensitive_packages as sp on
    sboms.package_name like replace(sp.package_pattern, '*', '%')
where
  version is not null
  or
  current_version is not null
  and
  (
    case
      when sp.package_manager IS NOT NULL then sp.package_manager == sboms.package_type
      else true
    end
  )
  and (
    case
      when sp.match_strategy = 'MATCHES' then false
      when sp.match_strategy = 'DOES_NOT_MATCH' then true
      else true
    end
  );

-- name: QueryMostPopularPackageManagers :many
SELECT
package_type,
count(*)
from sboms
group by
package_type
order by count(*) desc;

-- name: QueryMostPopularDockerImages :many
select
package_name, count(package_name)
from sboms
where package_type = 'docker'
and package_name != ''
group by package_name;

-- name: QueryGolangCILint :many
select
distinct
sboms.platform,
sboms.organisation,
sboms.repo,
owner
from
sboms
left join owners on
sboms.platform = owners.platform and
sboms.organisation = owners.organisation and
sboms.repo = owners.repo
where
package_name = 'github.com/golangci/golangci-lint'
and package_type = 'golang'
group by package_name;
