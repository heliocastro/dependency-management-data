package spdx

import (
	"bytes"
	"encoding/json"
	"strings"

	"dmd.tanna.dev/internal/domain"
	"github.com/package-url/packageurl-go"
	"github.com/spdx/tools-golang/spdx/v2/v2_2"
	spdxJSON "github.com/spdx/tools-golang/yaml"
	spdxYAML "github.com/spdx/tools-golang/yaml"
	"gopkg.in/yaml.v3"
)

type spdxv2_2Body struct {
	SPDXIdentifier string `json:"SPDXID" yaml:"SPDXID"`
	SPDXVersion    string `json:"spdxVersion" yaml:"spdxVersion"`
}

type SPDXv2_2JSONFormat struct{}

func (s *SPDXv2_2JSONFormat) Matches(body []byte) bool {
	var b spdxv2_2Body
	err := json.Unmarshal(body, &b)
	if err != nil {
		return false
	}

	if b.SPDXIdentifier != "SPDXRef-DOCUMENT" || b.SPDXVersion != "SPDX-2.2" {
		return false
	}

	var doc v2_2.Document
	err = spdxJSON.ReadInto(bytes.NewReader(body), &doc)
	if err != nil {
		return false
	}
	return true
}

func (s *SPDXv2_2JSONFormat) Name() string {
	return "SPDX-2.2"
}

func (s *SPDXv2_2JSONFormat) Parse(body []byte, platform string, org string, repo string) ([]domain.SBOMDependency, []domain.License) {
	var doc v2_2.Document
	err := spdxJSON.ReadInto(bytes.NewReader(body), &doc)
	if err != nil {
		return nil, nil
	}

	return s.parseInternal(&doc, platform, org, repo)
}

func (s *SPDXv2_2JSONFormat) parseInternal(doc *v2_2.Document, platform string, org string, repo string) ([]domain.SBOMDependency, []domain.License) {
	var deps []domain.SBOMDependency
	var licenses []domain.License
	for i, v := range doc.Packages {
		// skip the first element, as it's the project itself
		if i == 0 {
			continue
		}

		var packageName, packageType string

		for _, ref := range v.PackageExternalReferences {
			if ref.Category != "PACKAGE-MANAGER" {
				continue
			}

			purl, err := packageurl.FromString(ref.Locator)
			if err != nil {
				continue
			}

			packageName = purlToPackageName(purl)
			packageType = purl.Type
		}

		if packageName == "" || packageType == "" {
			// TODO log an error here to note that we can't determine the packageName or packageManager
			continue
		}

		d := domain.SBOMDependency{
			Platform:       platform,
			Organisation:   org,
			Repo:           repo,
			PackageName:    strings.ReplaceAll(packageName, "%40", "@"),
			Version:        toNilIfEmpty(v.PackageVersion),
			CurrentVersion: currentVersion(v.PackageVersion),
			PackageType:    packageType,
		}

		deps = append(deps, d)

		if v.PackageLicenseDeclared != "NOASSERTION" {
			l := domain.License{
				PackageName: d.PackageName,
				// Version is done below
				PackageManager: packageType,
				License:        v.PackageLicenseDeclared,
			}
			if d.CurrentVersion != nil {
				l.Version = *d.CurrentVersion
			} else if d.Version != nil {
				l.Version = *d.Version
			}

			if !l.Valid() {
				// TODO log an error here to note that the determined License object wasn't valid
				continue
			}

			licenses = append(licenses, l)
		}
	}
	return deps, licenses
}

type SPDXv2_2YAMLFormat struct{}

func (s *SPDXv2_2YAMLFormat) Matches(body []byte) bool {
	var b spdxv2_2Body
	err := yaml.Unmarshal(body, &b)
	if err != nil {
		return false
	}

	if b.SPDXIdentifier != "SPDXRef-DOCUMENT" || b.SPDXVersion != "SPDX-2.2" {
		return false
	}

	var doc v2_2.Document
	err = spdxYAML.ReadInto(bytes.NewReader(body), &doc)
	if err != nil {
		return false
	}
	return true
}

func (s *SPDXv2_2YAMLFormat) Name() string {
	return "SPDX-2.2"
}

func (s *SPDXv2_2YAMLFormat) Parse(body []byte, platform string, org string, repo string) ([]domain.SBOMDependency, []domain.License) {
	var doc v2_2.Document
	err := spdxYAML.ReadInto(bytes.NewReader(body), &doc)
	if err != nil {
		return nil, nil
	}

	return s.parseInternal(&doc, platform, org, repo)
}

func (s *SPDXv2_2YAMLFormat) parseInternal(doc *v2_2.Document, platform string, org string, repo string) ([]domain.SBOMDependency, []domain.License) {
	var deps []domain.SBOMDependency
	var licenses []domain.License
	for i, v := range doc.Packages {
		// skip the first element, as it's the project itself
		if i == 0 {
			continue
		}

		var packageName, packageType string

		for _, ref := range v.PackageExternalReferences {
			if ref.Category != "PACKAGE-MANAGER" {
				continue
			}

			purl, err := packageurl.FromString(ref.Locator)
			if err != nil {
				continue
			}

			packageName = purlToPackageName(purl)
			packageType = purl.Type
		}

		if packageName == "" || packageType == "" {
			// TODO log an error here to note that we can't determine the packageName or packageManager
			continue
		}

		d := domain.SBOMDependency{
			Platform:       platform,
			Organisation:   org,
			Repo:           repo,
			PackageName:    strings.ReplaceAll(packageName, "%40", "@"),
			Version:        toNilIfEmpty(v.PackageVersion),
			CurrentVersion: currentVersion(v.PackageVersion),
			PackageType:    packageType,
		}

		deps = append(deps, d)

		if v.PackageLicenseDeclared != "NOASSERTION" {
			l := domain.License{
				PackageName: d.PackageName,
				// Version is done below
				PackageManager: packageType,
				License:        v.PackageLicenseDeclared,
			}
			if d.CurrentVersion != nil {
				l.Version = *d.CurrentVersion
			} else if d.Version != nil {
				l.Version = *d.Version
			}

			if !l.Valid() {
				// TODO log an error here to note that the determined License object wasn't valid
				continue
			}

			licenses = append(licenses, l)
		}
	}
	return deps, licenses
}
