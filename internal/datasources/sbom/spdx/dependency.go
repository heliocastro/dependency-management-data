package spdx

import (
	"regexp"
)

var validVersionNumber = regexp.MustCompile("^[a-z0-9]")
