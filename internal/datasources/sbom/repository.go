package sbom

import (
	"context"
	"database/sql"

	"dmd.tanna.dev/internal/datasources/sbom/db"
)

type SBOMs struct{}

func (*SBOMs) Name() string {
	return "SBOMs"
}

func (*SBOMs) CreateTables(ctx context.Context, sqlDB *sql.DB) error {
	_, err := sqlDB.ExecContext(ctx, db.CreateTablesQuery)
	return err
}
