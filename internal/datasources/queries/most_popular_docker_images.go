package queries

type MostPopularDockerImagesRow struct {
	Image string
	Count int64
}

type MostPopularDockerImagesResult struct {
	Registries []Count
	Namespaces []Count
	Images     []Count
}
