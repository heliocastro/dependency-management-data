package queries

type GolangCILintResult struct {
	Direct   []RepoWithOwner
	Indirect []RepoWithOwner
}
