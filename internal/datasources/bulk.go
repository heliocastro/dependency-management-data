package datasources

type BulkImportRow struct {
	Platform     string
	Organisation string
	Repo         string

	Filename string
}
