-- name: InsertLambdaFunction :exec
INSERT INTO aws_lambda_functions (
  account_id,
  region,
  arn,
  name,
  runtime,
  last_modified,
  tags
) VALUES (
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?
);

-- name: RetrieveAll :many
select * from aws_lambda_functions;

-- name: InsertLambdaFunctionRuntime :exec
INSERT INTO aws_lambda_function_runtimes (
  runtime,
  deprecation,
  end_of_life
) VALUES (
  ?,
  ?,
  ?
);

-- name: RetrieveAllWithDeprecation :many
select
  arn,
  name,
  f.runtime,
  r.deprecation,
  r.end_of_life
from
  aws_lambda_functions f
  inner join aws_lambda_function_runtimes r on f.runtime = r.runtime
;
