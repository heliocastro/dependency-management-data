package awslambda

import (
	"encoding/json"
	"log"

	"dmd.tanna.dev/internal/datasources/awslambda/db"
)

type LambdaFunction struct {
	AccountId    string
	Region       string
	ARN          string
	Name         string
	Runtime      string
	LastModified *string
	Tags         map[string]string
}

func newLambdaFunctionFromDB(dbRow db.AwsLambdaFunction) LambdaFunction {
	f := LambdaFunction{
		AccountId: dbRow.AccountID,
		Region:    dbRow.Region,
		ARN:       dbRow.Arn,
		Name:      dbRow.Name,
		Runtime:   dbRow.Runtime,
	}

	if dbRow.LastModified.Valid {
		f.LastModified = &dbRow.LastModified.String
	}

	err := json.Unmarshal([]byte(dbRow.Tags), &f.Tags)
	if err != nil {
		log.Printf("Failed to unmarshal `Tags` from DB row for function ARN %s", f.ARN)
	}

	return f
}
