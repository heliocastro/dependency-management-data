package dependencyhealth

import (
	"context"
	"database/sql"
	"fmt"
	"log/slog"
	"net/http"
	"net/url"
	"strings"

	"dmd.tanna.dev/internal/dependencyhealth/db"
	"dmd.tanna.dev/internal/securityscorecards"
	"github.com/jedib0t/go-pretty/v6/progress"
	"github.com/package-url/packageurl-go"
)

func ImportRawScorecardsResults(ctx context.Context, logger *slog.Logger, sqlDB *sql.DB, results map[string]securityscorecards.ScorecardResult, pw progress.Writer, httpClient *http.Client) error {
	q := db.New(sqlDB)

	tx, err := sqlDB.BeginTx(ctx, nil)
	if err != nil {
		return fmt.Errorf("failed to prepare a database transaction: %w", err)
	}

	defer tx.Rollback()

	tracker := progress.Tracker{
		Message: fmt.Sprintf("Persisting %d Security Scorecards", len(results)),
		Total:   int64(len(results)),
	}
	pw.AppendTracker(&tracker)
	defer tracker.MarkAsDone()

	for k, sr := range results {
		purl, err := packageurl.FromString(k)
		if err != nil {
			logger.Warn(fmt.Sprintf("Received an invalid pURL from Ecosystems for repo %s, with value: %v. Will skip the package", sr.Repo.Name, k))
			tracker.IncrementWithError(1)
			continue
		}

		params := db.InsertRowParams{
			PackageName:    purlToPackageName(purl),
			PackageManager: purl.Type,
		}
		res := securityScorecardsResult{
			Result: sr,
		}
		res.Copy(&params)

		err = q.WithTx(tx).InsertRow(ctx, params)
		if err != nil {
			tracker.MarkAsErrored()
			return fmt.Errorf("failed to insert row: %w", err)
		}
		tracker.Increment(1)
	}

	return tx.Commit()
}

// purlToPackageName converts a purl to a package name.
// Adapted from https://github.com/package-url/packageurl-go/blob/358f10025d4a923f7f9babc766f0968720e82240/packageurl.go#L176-L189, with better support for Maven coordinates
func purlToPackageName(p packageurl.PackageURL) string {
	var purl string

	// Add namespaces if provided
	if p.Namespace != "" {
		var ns []string
		for _, item := range strings.Split(p.Namespace, "/") {
			ns = append(ns, url.QueryEscape(item))
		}
		if p.Type == "maven" {
			purl = purl + strings.Join(ns, ":") + ":"
		} else {
			purl = purl + strings.Join(ns, "/") + "/"
		}
	}
	// The name is always required and must be a percent-encoded string
	// Use url.QueryEscape instead of PathEscape, as it handles @ signs
	purl = purl + url.QueryEscape(p.Name)

	return purl
}
