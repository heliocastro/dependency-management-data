package dependencyhealth

import (
	"context"
	"database/sql"

	"dmd.tanna.dev/internal/dependencyhealth/db"
)

type DependencyHealth struct{}

func (*DependencyHealth) Name() string {
	return "DependencyHealth"
}

func (*DependencyHealth) CreateTables(ctx context.Context, sqlDB *sql.DB) error {
	_, err := sqlDB.ExecContext(ctx, db.CreateTablesQuery)
	return err
}
