package dependencyhealth

import (
	"context"
	"database/sql"
	"fmt"
	"log/slog"

	"dmd.tanna.dev/internal/advisory"
	advisoryDB "dmd.tanna.dev/internal/advisory/db"
	"dmd.tanna.dev/internal/dependencyhealth/db"
	"github.com/jedib0t/go-pretty/v6/progress"
)

func GenerateAdvisories(ctx context.Context, logger *slog.Logger, sqlDB *sql.DB, pw progress.Writer) error {
	advisoryQueries := advisoryDB.New(sqlDB)

	q := db.New(sqlDB)

	tx, err := sqlDB.BeginTx(ctx, nil)
	if err != nil {
		return fmt.Errorf("failed to prepare a database transaction: %w", err)
	}

	defer tx.Rollback()

	var results []advisoryDB.InsertAdvisoryParams

	rows, err := q.RetrieveUnmaintainedPackages(ctx)
	if err != nil {
		return fmt.Errorf("failed to list unmaintained dependencies: %w", err)
	}

	for _, row := range rows {
		adv := advisoryDB.InsertAdvisoryParams{
			Platform:     row.Platform,
			Organisation: row.Organisation,
			Repo:         row.Repo,
			PackageName:  row.PackageName,
			Version:      row.Version.String,
			// CurrentVersion handled below
			PackageManager:  row.PackageManager,
			PackageFilePath: row.PackageFilePath,
			DepTypes:        row.DepTypes,
			// Level handled below
			// AdvisoryType handled below
			// Description handled below
			// SupportedUntil handled below
			// EolFrom handled below
		}

		if row.CurrentVersion.Valid {
			adv.CurrentVersion = sql.NullString{
				String: row.CurrentVersion.String,
				Valid:  true,
			}
		}

		// NOTE that there can only be one advisory, per AdvisoryType and Level, so we need to combine descriptions together where there are multiple sources indicating we're UNMAINTAINED
		adv.AdvisoryType = advisory.AdvisoryTypeUnmaintained
		adv.Level = "ERROR"

		if row.EcosystemsRepoArchived.Valid && row.EcosystemsRepoArchived.Bool {
			adv.Description += "The repository that hosts the source code for this dependency is archived, implying no more code changes or releases will be issued"
			if row.EcosystemsRepoLastSyncedAt.Valid {
				adv.Description += " (as Ecosyste.ms' last query of the dependency's Repository data on " + row.EcosystemsRepoLastSyncedAt.String + " )"
			}
			adv.Description += "\n"
		}

		if row.EcosystemsStatus.Valid && row.EcosystemsStatus.String == "deprecated" {
			adv.Description += "The dependency has been marked as deprecated in " + adv.PackageManager + ", implying no more code changes or releases will be issued"
			if row.EcosystemsLastSyncedAt.Valid {
				adv.Description += " (as Ecosyste.ms' last query of the dependency's Package data on " + row.EcosystemsLastSyncedAt.String + " )"
			}
			adv.Description += "\n"
		}

		if row.ScorecardMaintained.Valid && row.ScorecardMaintained.Int64 == 0 {
			adv.Description += "The repository that hosts the source code for this dependency has been deemed unmaintained, according to OpenSSF Security Scorecards. See also https://github.com/ossf/scorecard/blob/main/docs/checks.md#maintained for more details"
			adv.Description += "\n"
		}

		results = append(results, adv)
	}

	for _, advisory := range results {
		err = advisoryQueries.WithTx(tx).InsertAdvisory(ctx, advisory)
		if err != nil {
			return fmt.Errorf("failed to insert row: %w", err)
		}
	}

	return tx.Commit()
}
