-- name: RetrieveDistinctPackagesWithoutSensitivePackages :many
select
  distinct package_name,
  renovate.package_manager,
  datasource
from
  renovate
  left join sensitive_packages as sp on
    renovate.package_name like replace(sp.package_pattern, '*', '%')
where
  (
    case
      when sp.package_manager IS NOT NULL then sp.package_manager == renovate.package_manager
      else true
    end
  )
  and (
    case
      when sp.match_strategy = 'MATCHES' then false
      when sp.match_strategy = 'DOES_NOT_MATCH' then true
      else true
    end
  )
UNION
select
  distinct package_name,
  sboms.package_type as package_manager,
  '' as datasource
from
  sboms
  left join sensitive_packages as sp on
    sboms.package_name like replace(sp.package_pattern, '*', '%')
where
  (
    case
      when sp.package_manager IS NOT NULL then sp.package_manager == sboms.package_type
      else true
    end
  )
  and (
    case
      when sp.match_strategy = 'MATCHES' then false
      when sp.match_strategy = 'DOES_NOT_MATCH' then true
      else true
    end
  );

-- name: InsertRow :exec
INSERT INTO dependency_health (
  package_name,
  package_manager,

  scorecard_score,
  scorecard_codereview,
  scorecard_maintained,
  scorecard_ciibestpractices,
  scorecard_license,
  scorecard_dangerousworkflow,
  scorecard_packaging,
  scorecard_tokenpermissions,
  scorecard_signedreleases,
  scorecard_branchprotection,
  scorecard_binaryartifacts,
  scorecard_fuzzing,
  scorecard_securitypolicy,
  scorecard_sast,
  scorecard_vulnerabilities,
  scorecard_pinneddependencies,

  ecosystems_repo_archived,
  ecosystems_repo_pushed_at,
  ecosystems_repo_updated_at,
  ecosystems_repo_last_synced_at,
  ecosystems_last_synced_at,
  ecosystems_latest_release_published_at,
  ecosystems_status,
  ecosystems_funding
) VALUES (
	?,
	?,

	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,

	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?
);

-- name: RetrieveUnmaintainedPackages :many
select
  -- data for the advisory
  s.platform,
  s.organisation,
  s.repo,
  s.package_name,
  s.version,
  s.current_version,
  package_type as package_manager,
  -- as SBOMs don't make this available, default to an empty array
  '[]' as dep_types,
  -- as SBOMs don't make this available, default to an empty string
  '' as package_file_path,

  -- data to feed into whether the package is unmaintained
  scorecard_maintained,
  ecosystems_repo_archived,
  ecosystems_status,

  -- metadata
  ecosystems_last_synced_at,
  ecosystems_repo_last_synced_at
from
  sboms s
  inner join dependency_health as h on
		s.package_name = h.package_name
		and
		s.package_type = h.package_manager
where
  scorecard_maintained = 0 OR
  ecosystems_repo_archived = 1 OR
  ecosystems_status = 'deprecated'
union
select
  -- data for the advisory
  r.platform,
  r.organisation,
  r.repo,
  r.package_name,
  r.version,
  r.current_version,
  r.package_manager,
  r.dep_types,
  r.package_file_path,

  -- data to feed into whether the package is unmaintained
  scorecard_maintained,
  ecosystems_repo_archived,
  ecosystems_status,

  -- metadata
  ecosystems_last_synced_at,
  ecosystems_repo_last_synced_at
from
  renovate r
  inner join dependency_health as h on
		r.package_name = h.package_name
		and
		r.package_manager = h.package_manager
where
  scorecard_maintained = 0 OR
  ecosystems_repo_archived = 1 OR
  ecosystems_status = 'deprecated'
;
