-- dependency_health contains information about the health of given
-- dependencies.
--
-- This consumes data from different sources to augment the understanding of
-- dependencies in use, for instance giving an indication of whether they are
-- (well) maintained, have been recently released, or may have supply chain
-- hygiene issues.
--
-- Currently, this data is derived from:
--
-- - OpenSSF Security Scorecards (https://api.securityscorecards.dev/)
-- - Ecosystems (https://ecosyste.ms)
--
-- This data is a best-efforts attempt to provide this insight, and may be
-- stale at the time of fetching, via `dmd db generate dependency-health`.

-- The intent of this table is to provide additional insight into the
-- dependencies in use, either to feed into Policies
-- (https://dmd.tanna.dev/concepts/policy/) or purely for informational
-- purposes.
--
-- This is a utility table that is expected to be JOIN'd with the relevant
-- datasources you are using.
CREATE TABLE IF NOT EXISTS dependency_health (
  -- what package is this dependency health for?
  --
  -- Foreign keys:
  -- - `renovate.package_name`
  -- - `sboms.package_name`
  package_name TEXT NOT NULL,
  -- package_manager indicates the package manager that the dependency health
  -- corresponds to.
  --
  -- Based on which datasource(s) (https://dmd.tanna.dev/concepts/datasource/)
  -- you are using, this will be a different value:
  --
  -- - for Renovate data, must exactly match `renovate.package_manager`.
  --   Note that there may be multiple `package_managers`, for instance `maven`
  --   and `gradle`, which would require two rows.
  -- - for Software Bill of Materials (SBOM) data, must exactly match `sboms.package_type`
  --
  -- If you are using multiple datasources, you will have one row per
  -- `package_manager` that this dependency health regards.
  --
  -- Foreign keys:
  -- - `renovate.package_manager`
  -- - `sboms.package_type`
  package_manager TEXT NOT NULL,

  --------------------------------------------------------------------------
  -------- OpenSSF Scorecards derived data

  -- scorecard_score is the overall calculated value of the OpenSSF Security
  -- Scorecards' checks for the given dependency.
  --
  -- May be NULL, as the repository may not have been scanned.
  scorecard_score REAL
    CHECK (
      scorecard_score >= -1 AND scorecard_score <= 10
    ),

  -- scorecard_codereview is the value of the OpenSSF Security Scorecards'
  -- `Code-Review` check.
  --
  -- More details on the calculation can be found at
  -- https://github.com/ossf/scorecard/blob/main/docs/checks.md#code-review
  --
  -- May be NULL, as the repository may not have been scanned.
  scorecard_codereview INTEGER
    CHECK (
      scorecard_codereview >= -1 AND scorecard_codereview <= 10
    ),

  -- scorecard_maintained is the value of the OpenSSF Security Scorecards'
  -- `Maintained` check.
  --
  -- More details on the calculation can be found at
  -- https://github.com/ossf/scorecard/blob/main/docs/checks.md#maintained
  --
  -- May be NULL, as the repository may not have been scanned.
  scorecard_maintained INTEGER
    CHECK (
      scorecard_maintained >= -1 AND scorecard_maintained <= 10
    ),

  -- scorecard_ciibestpractices is the value of the OpenSSF Security
  -- Scorecards' `CII-Best-Practices` check.
  --
  -- More details on the calculation can be found at
  -- https://github.com/ossf/scorecard/blob/main/docs/checks.md#cii-best-practices
  --
  -- May be NULL, as the repository may not have been scanned.
  scorecard_ciibestpractices INTEGER
    CHECK (
      scorecard_ciibestpractices >= -1 AND scorecard_ciibestpractices <= 10
    ),

  -- scorecard_license is the value of the OpenSSF Security Scorecards'
  -- `License` check.
  --
  -- More details on the calculation can be found at
  -- https://github.com/ossf/scorecard/blob/main/docs/checks.md#license
  --
  -- May be NULL, as the repository may not have been scanned.
  scorecard_license INTEGER
    CHECK (
      scorecard_license >= -1 AND scorecard_license <= 10
    ),

  -- scorecard_dangerousworkflow is the value of the OpenSSF Security
  -- Scorecards' `Dangerous-Workflow` check.
  --
  -- More details on the calculation can be found at
  -- https://github.com/ossf/scorecard/blob/main/docs/checks.md#dangerous-workflow
  --
  -- May be NULL, as the repository may not have been scanned.
  scorecard_dangerousworkflow INTEGER
    CHECK (
      scorecard_dangerousworkflow >= -1 AND scorecard_dangerousworkflow <= 10
    ),

  -- scorecard_packaging is the value of the OpenSSF Security Scorecards'
  -- `Packaging` check.
  --
  -- More details on the calculation can be found at
  -- https://github.com/ossf/scorecard/blob/main/docs/checks.md#packaging
  --
  -- May be NULL, as the repository may not have been scanned.
  scorecard_packaging INTEGER
    CHECK (
      scorecard_packaging >= -1 AND scorecard_packaging <= 10
    ),

  -- scorecard_tokenpermissions is the value of the OpenSSF Security
  -- Scorecards' `Token-Permissions` check.
  --
  -- More details on the calculation can be found at
  -- https://github.com/ossf/scorecard/blob/main/docs/checks.md#token-permissions
  --
  -- May be NULL, as the repository may not have been scanned.
  scorecard_tokenpermissions INTEGER
    CHECK (
      scorecard_tokenpermissions >= -1 AND scorecard_tokenpermissions <= 10
    ),

  -- scorecard_signedreleases is the value of the OpenSSF Security Scorecards'
  -- `Signed-Releases` check.
  --
  -- More details on the calculation can be found at
  -- https://github.com/ossf/scorecard/blob/main/docs/checks.md#signed-releases
  --
  -- May be NULL, as the repository may not have been scanned.
  scorecard_signedreleases INTEGER
    CHECK (
      scorecard_signedreleases >= -1 AND scorecard_signedreleases <= 10
    ),

  -- scorecard_branchprotection is the value of the OpenSSF Security
  -- Scorecards' `Branch-Protection` check.
  --
  -- More details on the calculation can be found at
  -- https://github.com/ossf/scorecard/blob/main/docs/checks.md#branch-protection
  --
  -- May be NULL, as the repository may not have been scanned.
  scorecard_branchprotection INTEGER
    CHECK (
      scorecard_branchprotection >= -1 AND scorecard_branchprotection <= 10
    ),

  -- scorecard_binaryartifacts is the value of the OpenSSF Security Scorecards'
  -- `Binary-Artifacts` check.
  --
  -- More details on the calculation can be found at
  -- https://github.com/ossf/scorecard/blob/main/docs/checks.md#binary-artifacts
  --
  -- May be NULL, as the repository may not have been scanned.
  scorecard_binaryartifacts INTEGER
    CHECK (
      scorecard_binaryartifacts >= -1 AND scorecard_binaryartifacts <= 10
    ),

  -- scorecard_fuzzing is the value of the OpenSSF Security Scorecards'
  -- `Fuzzing` check.
  --
  -- More details on the calculation can be found at
  -- https://github.com/ossf/scorecard/blob/main/docs/checks.md#fuzzing
  --
  -- May be NULL, as the repository may not have been scanned.
  scorecard_fuzzing INTEGER
    CHECK (
      scorecard_fuzzing >= -1 AND scorecard_fuzzing <= 10
    ),

  -- scorecard_securitypolicy is the value of the OpenSSF Security Scorecards'
  -- `Security-Policy` check.
  --
  -- More details on the calculation can be found at
  -- https://github.com/ossf/scorecard/blob/main/docs/checks.md#security-policy
  --
  -- May be NULL, as the repository may not have been scanned.
  scorecard_securitypolicy INTEGER
    CHECK (
      scorecard_securitypolicy >= -1 AND scorecard_securitypolicy <= 10
    ),

  -- scorecard_sast is the value of the OpenSSF Security Scorecards' `SAST`
  -- check.
  --
  -- More details on the calculation can be found at
  -- https://github.com/ossf/scorecard/blob/main/docs/checks.md#sast
  --
  -- May be NULL, as the repository may not have been scanned.
  scorecard_sast INTEGER
    CHECK (
      scorecard_sast >= -1 AND scorecard_sast <= 10
    ),

  -- scorecard_vulnerabilities is the value of the OpenSSF Security Scorecards'
  -- `Vulnerabilities` check.
  --
  -- More details on the calculation can be found at
  -- https://github.com/ossf/scorecard/blob/main/docs/checks.md#vulnerabilities
  --
  -- May be NULL, as the repository may not have been scanned.
  scorecard_vulnerabilities INTEGER
    CHECK (
      scorecard_vulnerabilities >= -1 AND scorecard_vulnerabilities <= 10
    ),

  -- scorecard_pinned-dependencies is the value of the OpenSSF Security
  -- Scorecards' `Pinned-Dependencies` check.
  --
  -- More details on the calculation can be found at
  -- https://github.com/ossf/scorecard/blob/main/docs/checks.md#pinned-dependencies
  --
  -- May be NULL, as the repository may not have been scanned.
  scorecard_pinneddependencies INTEGER
    CHECK (
      scorecard_pinneddependencies >= -1 AND scorecard_pinneddependencies <= 10
    ),

  --------------------------------------------------------------------------
  -------- Ecosyste.ms derived data

  -- ecosystems_repo_archived indicates whether the source code repository that
  -- the package is hosted at has been archived.
  --
  -- Sourced via Ecosystems (https://ecosyste.ms) and corresponds with the
  -- field `$.repo_metadata.archived` in the Packages API.
  --
  -- May be NULL, as the repository may not have been scanned.
  ecosystems_repo_archived BOOLEAN,
  -- ecosystems_repo_pushed_at indicates the last push to the default branch of
  -- the source code repository that manages this package.
  --
  -- Sourced via Ecosystems (https://ecosyste.ms) and corresponds with the
  -- field `$.repo_metadata.pushed_at` in the Packages API.
  --
  -- May be NULL, as the repository may not have been scanned.
  --
  -- Note that this may not always be up-to-date
  -- https://github.com/ecosyste-ms/repos/issues/442#issuecomment-1911818704
  ecosystems_repo_pushed_at TEXT,
  -- ecosystems_repo_updated_at indicates the last activity in the source code
  -- repository that manages this package.
  --
  -- Depending on the host of the source code, this could indicate a comments,
  -- push to a branch, or otherwise.
  --
  -- Sourced via Ecosystems (https://ecosyste.ms) and corresponds with the
  -- field `$.repo_metadata.updated_at` in the Packages API.
  --
  -- May be NULL, as the repository may not have been scanned.
  --
  -- Note that this may not always be up-to-date
  -- https://github.com/ecosyste-ms/repos/issues/442#issuecomment-1911818704
  ecosystems_repo_updated_at TEXT,
  -- ecosystems_repo_last_synced_at indicates the last date that Ecosystems
  -- synced repository data.
  --
  -- This is useful when used in conjunction with decisioning based on other
  -- fields, as it indicates how stale the data may be.
  --
  -- Sourced via Ecosystems (https://ecosyste.ms) and corresponds with the
  -- field `$.repo_metadata.last_synced_at` in the Packages API.
  --
  -- May be NULL, as the repository may not have been scanned.
  --
  -- Note that this may not always be up-to-date
  -- https://github.com/ecosyste-ms/repos/issues/442#issuecomment-1911818704
  ecosystems_repo_last_synced_at TEXT,
  -- ecosystems_last_synced_at indicates the last date that Ecosystems
  -- synced package data.
  --
  -- This is useful when used in conjunction with decisioning based on other
  -- fields, as it indicates how stale the data may be.
  --
  -- Sourced via Ecosystems (https://ecosyste.ms) and corresponds with the
  -- field `$.repo_metadata.last_synced_at` in the Packages API.
  --
  -- May be NULL, as the repository may not have been scanned.
  --
  -- Note that this may not always be up-to-date
  -- https://github.com/ecosyste-ms/repos/issues/442#issuecomment-1911818704
  ecosystems_last_synced_at TEXT,
  -- ecosystems_latest_release_published_at indicates when the last release for
  -- this package was published.
  --
  -- Sourced via Ecosystems (https://ecosyste.ms) and corresponds with the
  -- field `$.latest_release_published_at` in the Packages API.
  --
  -- May be NULL, as the repository may not have been scanned.
  --
  -- Note that this may include retracted releases
  -- https://github.com/ecosyste-ms/packages/issues/619
  ecosystems_latest_release_published_at TEXT,
  -- ecosystems_status indicates the status of the package.
  --
  -- Some possible values:
  --
  --   yanked
  --   deprecated
  --   discontinued
  --
  -- Sourced via Ecosystems (https://ecosyste.ms) and corresponds with the
  -- field `$.status` in the Packages API.
  --
  -- May be NULL, as the repository may not have been scanned.
  ecosystems_status TEXT,
  -- ecosystems_funding indicates the platform(s) that a given package is
  -- onboarded to, for supporters to financially support the project and its
  -- ongoing maintainence.
  --
  -- This is a a key-value JSON object of the different platforms.
  --
  -- Some possible values:
  --
  --   {}
  --
  --   {
  --     "github": "eslint",
  --     "tidelift": "npm/eslint",
  --     "package_metadata": "https://opencollective.com/eslint"
  --   }
  --
  -- Sourced via Ecosystems (https://ecosyste.ms) and corresponds with the following fields from the Packages API:
  --
  -- - the object `$.repo_metadata.metadata.funding`
  -- - the string `$.metadata.funding` (stored in the `package_metadata` field in the resulting object)
  --
  -- May be NULL, as the repository may not have been scanned.
  ecosystems_funding TEXT,

  UNIQUE (package_name, package_manager) ON CONFLICT REPLACE
);
