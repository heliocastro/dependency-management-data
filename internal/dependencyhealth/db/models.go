// Code generated by sqlc. DO NOT EDIT.
// versions:
//   sqlc v1.23.0

package db

import (
	"database/sql"
)

type DependencyHealth struct {
	PackageName                        string
	PackageManager                     string
	ScorecardScore                     sql.NullFloat64
	ScorecardCodereview                sql.NullInt64
	ScorecardMaintained                sql.NullInt64
	ScorecardCiibestpractices          sql.NullInt64
	ScorecardLicense                   sql.NullInt64
	ScorecardDangerousworkflow         sql.NullInt64
	ScorecardPackaging                 sql.NullInt64
	ScorecardTokenpermissions          sql.NullInt64
	ScorecardSignedreleases            sql.NullInt64
	ScorecardBranchprotection          sql.NullInt64
	ScorecardBinaryartifacts           sql.NullInt64
	ScorecardFuzzing                   sql.NullInt64
	ScorecardSecuritypolicy            sql.NullInt64
	ScorecardSast                      sql.NullInt64
	ScorecardVulnerabilities           sql.NullInt64
	ScorecardPinneddependencies        sql.NullInt64
	EcosystemsRepoArchived             sql.NullBool
	EcosystemsRepoPushedAt             sql.NullString
	EcosystemsRepoUpdatedAt            sql.NullString
	EcosystemsRepoLastSyncedAt         sql.NullString
	EcosystemsLastSyncedAt             sql.NullString
	EcosystemsLatestReleasePublishedAt sql.NullString
	EcosystemsStatus                   sql.NullString
	EcosystemsFunding                  sql.NullString
}

type Renovate struct {
	Platform        string
	Organisation    string
	Repo            string
	PackageName     string
	Version         string
	CurrentVersion  sql.NullString
	PackageManager  string
	PackageFilePath string
	Datasource      string
	DepTypes        string
}

type RenovateUpdate struct {
	Platform        string
	Organisation    string
	Repo            string
	PackageName     string
	Version         string
	CurrentVersion  sql.NullString
	PackageManager  string
	PackageFilePath string
	Datasource      string
	NewVersion      string
	UpdateType      string
}

type Sbom struct {
	Platform       string
	Organisation   string
	Repo           string
	PackageName    string
	Version        sql.NullString
	CurrentVersion sql.NullString
	PackageType    string
}

type SensitivePackage struct {
	PackagePattern string
	PackageManager sql.NullString
	MatchStrategy  string
}
