package dependencyhealth

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"log/slog"
	"net/http"
	"strings"
	"sync"
	"time"

	"dmd.tanna.dev/internal/datasources/renovate"
	"dmd.tanna.dev/internal/dependencyhealth/db"
	"dmd.tanna.dev/internal/ecosystems"
	"dmd.tanna.dev/internal/securityscorecards"
	"github.com/jedib0t/go-pretty/v6/progress"
	"github.com/package-url/packageurl-go"
)

const maxGoroutines = 250

func Generate(ctx context.Context, logger *slog.Logger, sqlDB *sql.DB, pw progress.Writer, httpClient *http.Client) error {
	q := db.New(sqlDB)

	rows, err := q.RetrieveDistinctPackagesWithoutSensitivePackages(ctx)
	if err != nil {
		return fmt.Errorf("failed to query datasources for distinct packages: %w", err)
	}

	tx, err := sqlDB.BeginTx(ctx, nil)
	if err != nil {
		return fmt.Errorf("failed to prepare a database transaction: %w", err)
	}

	defer tx.Rollback()

	ecosystemsClient, err := ecosystems.NewClientWithResponses(ecosystems.BaseURL, ecosystems.WithHTTPClient(httpClient))
	if err != nil {
		return fmt.Errorf("failed to construct API client for Ecosyste.ms' Packages API: %w", err)
	}

	scorecardsClient := securityscorecards.NewClient(httpClient)

	tracker := progress.Tracker{
		Message: fmt.Sprintf("Retrieving dependency health details for %d dependencies", len(rows)),
		Total:   int64(len(rows)),
	}
	pw.AppendTracker(&tracker)

	var wg sync.WaitGroup
	var m sync.Mutex
	goroutines := make(chan struct{}, maxGoroutines)

	var results []db.InsertRowParams
	for _, row := range rows {
		wg.Add(1)

		go func(row db.RetrieveDistinctPackagesWithoutSensitivePackagesRow) {
			defer func() {
				<-goroutines
				wg.Done()
				tracker.Increment(1)
			}()

			goroutines <- struct{}{}

			logger := logger.With("packageName", row.PackageName, "packageManager", row.PackageManager, "datasource", row.Datasource)

			result := db.InsertRowParams{
				PackageName:    row.PackageName,
				PackageManager: row.PackageManager,
				// everything else is done conditionally
			}

			e, err := queryEcosystems(ctx, ecosystemsClient, logger, row)
			if err != nil {
				logger.Error(err.Error(), "err", err)
				return
			}

			if e == nil {
				return
			}

			e.Copy(&result)

			if e.RepoHTMLUrl == nil {
				logger.Debug("Did not find a repository for the given dependency, skipping Security Scorecards check")
			} else {
				res, err := querySecurityScorecards(ctx, &scorecardsClient, logger, strings.Replace(*e.RepoHTMLUrl, "https://", "", 1))
				if err != nil {
					logger.Error(err.Error(), "err", err)
					return
				}
				if res == nil {
					return
				}

				res.Copy(&result)
			}
			m.Lock()
			results = append(results, result)
			m.Unlock()
		}(row)
	}
	wg.Wait()
	tracker.MarkAsDone()

	tracker = progress.Tracker{
		Message: fmt.Sprintf("Persisting %d dependency health details for %d dependencies", len(results), len(rows)),
		Total:   int64(len(rows)),
	}
	pw.AppendTracker(&tracker)
	defer tracker.MarkAsDone()

	for _, dh := range results {
		err = q.WithTx(tx).InsertRow(ctx, dh)
		if err != nil {
			tracker.MarkAsErrored()
			return fmt.Errorf("failed to insert row: %w", err)
		}
		tracker.Increment(1)
	}

	return tx.Commit()
}

type ecosystemsResult struct {
	RepoArchived             *bool
	RepoPushedAt             *string
	RepoUpdatedAt            *string
	RepoLastSyncedAt         *string
	LastSyncedAt             *string
	LatestReleasePublishedAt *string
	Status                   *string
	Funding                  map[string]string

	RepoHTMLUrl *string
}

func (e ecosystemsResult) Copy(m *db.InsertRowParams) {
	if e.RepoArchived != nil {
		m.EcosystemsRepoArchived = sql.NullBool{
			Bool:  *e.RepoArchived,
			Valid: true,
		}
	}

	if e.RepoPushedAt != nil {
		m.EcosystemsRepoPushedAt = sql.NullString{
			String: *e.RepoPushedAt,
			Valid:  true,
		}
	}

	if e.RepoUpdatedAt != nil {
		m.EcosystemsRepoUpdatedAt = sql.NullString{
			String: *e.RepoUpdatedAt,
			Valid:  true,
		}
	}

	if e.RepoLastSyncedAt != nil {
		m.EcosystemsRepoLastSyncedAt = sql.NullString{
			String: *e.RepoLastSyncedAt,
			Valid:  true,
		}
	}

	if e.LastSyncedAt != nil {
		m.EcosystemsLastSyncedAt = sql.NullString{
			String: *e.LastSyncedAt,
			Valid:  true,
		}
	}

	if e.LatestReleasePublishedAt != nil {
		m.EcosystemsLatestReleasePublishedAt = sql.NullString{
			String: *e.LatestReleasePublishedAt,
			Valid:  true,
		}
	}

	if e.Funding != nil && len(e.Funding) > 0 {
		m.EcosystemsFunding = sql.NullString{
			String: "{}",
			Valid:  true,
		}
		b, err := json.Marshal(e.Funding)
		if err == nil {
			m.EcosystemsFunding.String = string(b)
		}
		// TODO: log errors?
	}
}

func queryEcosystems(ctx context.Context, client *ecosystems.ClientWithResponses, logger *slog.Logger, row db.RetrieveDistinctPackagesWithoutSensitivePackagesRow) (*ecosystemsResult, error) {
	asPurl := packageAsPurl(row)

	logger = logger.With("pURL", asPurl.String())

	resp, err := client.LookupPackageWithResponse(ctx, &ecosystems.LookupPackageParams{
		Purl: ptr(asPurl.String()),
	})
	if err != nil {
		return nil, fmt.Errorf("failed lookup in Ecosystems: %v", err)
	}

	if resp.StatusCode() != http.StatusOK {
		logger.Warn(fmt.Sprintf("Failed lookup in Ecosystems: HTTP %d", resp.StatusCode()))
		return nil, nil
	}

	if resp.JSON200 == nil || len(*resp.JSON200) == 0 {
		logger.Warn(fmt.Sprintf("Lookup for pURL %s in Ecosystems returned no results", asPurl.String()))
		return nil, nil
	}

	if len(*resp.JSON200) > 1 {
		logger.Warn(fmt.Sprintf("Lookup for pURL %s in Ecosystems returned %d results, but was only expecting 1", asPurl.String(), len(*resp.JSON200)))
	}

	pack := (*resp.JSON200)[0]

	r := ecosystemsResult{
		Funding: make(map[string]string),
	}
	if pack.LatestReleasePublishedAt != nil {
		r.LatestReleasePublishedAt = ptr(pack.LatestReleasePublishedAt.Format(time.RFC3339))
	}
	if pack.LastSyncedAt != nil {
		r.LastSyncedAt = ptr(pack.LastSyncedAt.Format(time.RFC3339))
	}
	if pack.Metadata != nil {
		funding := (*pack.Metadata)["funding"]
		if funding, ok := funding.(string); ok {
			r.Funding["package_metadata"] = funding
		}
	}

	if pack.RepoMetadata != nil {
		m := *pack.RepoMetadata
		// metadata to be put into the database
		if v, ok := maybeBool(m, "archived"); ok {
			r.RepoArchived = v
		}
		if v, ok := maybeString(m, "pushed_at"); ok {
			r.RepoPushedAt = v
		}
		if v, ok := maybeString(m, "updated_at"); ok {
			r.RepoUpdatedAt = v
		}
		if v, ok := maybeString(m, "last_synced_at"); ok {
			r.RepoLastSyncedAt = v
		}

		// additional metadata
		if v, ok := maybeString(m, "html_url"); ok {
			r.RepoHTMLUrl = v
		}

		if funding, ok := maybeRepositoryMetadataFunding(m); ok {
			r.Funding = funding
		}
	}

	// we may receive some empty values from Ecosystems, which we don't want to retain
	for k, v := range r.Funding {
		if v == "" {
			delete(r.Funding, k)
		}
	}

	return &r, nil
}

func packageAsPurl(row db.RetrieveDistinctPackagesWithoutSensitivePackagesRow) *packageurl.PackageURL {
	var purlType string
	var namespace string
	var name string
	var version string

	for _, ptd := range renovate.PurlTypeDerivers {
		t, found := ptd(row.PackageManager, row.Datasource)

		if found {
			purlType = t
			break
		}
	}

	// TODO revisit during https://gitlab.com/tanna.dev/dependency-management-data/-/issues/232
	name = strings.ReplaceAll(row.PackageName, "%40", "@")

	return packageurl.NewPackageURL(purlType, namespace, name, version, nil, "")
}

func querySecurityScorecards(ctx context.Context, client *securityscorecards.Client, logger *slog.Logger, repoPath string) (*securityScorecardsResult, error) {
	res, err := client.GetResult(ctx, repoPath)
	if err != nil {
		// no wrapping as it's wrapped nicely in the client
		return nil, err
	}

	if res == nil {
		return nil, nil
	}

	return &securityScorecardsResult{
		Result: *res,
	}, nil
}

type securityScorecardsResult struct {
	Result securityscorecards.ScorecardResult
}

func (e securityScorecardsResult) Copy(m *db.InsertRowParams) {
	m.ScorecardScore = sql.NullFloat64{
		Float64: float64(e.Result.Score),
		Valid:   true,
	}
	for _, sc := range e.Result.Checks {
		switch sc.Name {
		case "Code-Review":
			m.ScorecardCodereview = sql.NullInt64{
				Int64: int64(sc.Score),
				Valid: true,
			}
		case "Maintained":
			m.ScorecardMaintained = sql.NullInt64{
				Int64: int64(sc.Score),
				Valid: true,
			}

		case "CII-Best-Practices":
			m.ScorecardCiibestpractices = sql.NullInt64{
				Int64: int64(sc.Score),
				Valid: true,
			}
		case "License":
			m.ScorecardLicense = sql.NullInt64{
				Int64: int64(sc.Score),
				Valid: true,
			}
		case "Dangerous-Workflow":
			m.ScorecardDangerousworkflow = sql.NullInt64{
				Int64: int64(sc.Score),
				Valid: true,
			}
		case "Packaging":
			m.ScorecardPackaging = sql.NullInt64{
				Int64: int64(sc.Score),
				Valid: true,
			}
		case "Token-Permissions":
			m.ScorecardTokenpermissions = sql.NullInt64{
				Int64: int64(sc.Score),
				Valid: true,
			}
		case "Signed-Releases":
			m.ScorecardSignedreleases = sql.NullInt64{
				Int64: int64(sc.Score),
				Valid: true,
			}
		case "Branch-Protection":
			m.ScorecardBranchprotection = sql.NullInt64{
				Int64: int64(sc.Score),
				Valid: true,
			}
		case "Binary-Artifacts":
			m.ScorecardBinaryartifacts = sql.NullInt64{
				Int64: int64(sc.Score),
				Valid: true,
			}
		case "Fuzzing":
			m.ScorecardFuzzing = sql.NullInt64{
				Int64: int64(sc.Score),
				Valid: true,
			}
		case "Security-Policy":
			m.ScorecardSecuritypolicy = sql.NullInt64{
				Int64: int64(sc.Score),
				Valid: true,
			}
		case "SAST":
			m.ScorecardSast = sql.NullInt64{
				Int64: int64(sc.Score),
				Valid: true,
			}
		case "Vulnerabilities":
			m.ScorecardVulnerabilities = sql.NullInt64{
				Int64: int64(sc.Score),
				Valid: true,
			}
		case "Pinned-Dependencies":
			m.ScorecardPinneddependencies = sql.NullInt64{
				Int64: int64(sc.Score),
				Valid: true,
			}
		}
	}
}

func ptr[T any](t T) *T {
	return &t
}

func maybeString(m map[string]interface{}, key string) (*string, bool) {
	if f, ok := m[key]; ok {
		if v, ok := f.(string); ok {
			return ptr(v), true
		}
	}
	return nil, false
}

func maybeBool(m map[string]interface{}, key string) (*bool, bool) {
	if f, ok := m[key]; ok {
		if v, ok := f.(bool); ok {
			return ptr(v), true
		}
	}
	return nil, false
}

func maybeRepositoryMetadataFunding(m map[string]interface{}) (map[string]string, bool) {
	var model struct {
		Metadata struct {
			Funding map[string]string `json:"funding"`
		} `json:"metadata"`
	}

	data, err := json.Marshal(m)
	if err != nil {
		return nil, false
	}

	err = json.Unmarshal(data, &model)
	if err != nil {
		return nil, false
	}

	return model.Metadata.Funding, true
}
