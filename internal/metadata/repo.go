package metadata

import (
	"context"
	"database/sql"

	"dmd.tanna.dev/internal/metadata/db"
)

type Metadata struct{}

func (*Metadata) Name() string {
	return "Metadata"
}

func (*Metadata) CreateTables(ctx context.Context, sqlDB *sql.DB) error {
	_, err := sqlDB.ExecContext(ctx, db.CreateTablesQuery)
	return err
}
