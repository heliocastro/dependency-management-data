-- metadata contains internal metadata about the dependency-management-data
-- database.
--
-- This provides a common location for key-value information to be stored that
-- affects the whole database.
--
-- Known keys:
--
--  dmd_version - the version of the `dmd` CLI that was used to create the
--  underlying database
--
--  finalised_at - the date at which the database was "finalised", and should
--  now be treated as "up-to-date" and read-only from this point onwards. This
--  indicates that all datasources were imported and all enrichment (i.e. via
--  Advisories, Dependency Health, etc) was complete. This could indicate
--  that all datasources' data is now up-to-date, but there are likely some
--  that haven't been as recently scanned.
--
-- Consuming applications MAY add their own data to this table, but must note
-- that there are no guarantees about the data not being overwritten by
-- dependency-management-data's own requirements in the future.
CREATE TABLE IF NOT EXISTS metadata (
  name TEXT NOT NULL,
  value TEXT NOT NULL,

  UNIQUE (name) ON CONFLICT REPLACE
);
