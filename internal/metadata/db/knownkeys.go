package db

// KnownKeys returns the known keys (`name` column) for the `metadata` table.
//
// This must stay aligned with the values in the `schema.sql`
func KnownKeys() []string {
	return []string{
		"dmd_version",
		"finalised_at",
	}
}
