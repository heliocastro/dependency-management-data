-- name: RetrieveAll :many
select
  name,
  value
from
  metadata
;

-- name: RetrieveDMDVersion :one
select
  name,
  value
from
  metadata
where
  name = 'dmd_version';

-- name: SetDMDVersion :exec
insert into metadata (
  name,
  value
) VALUES (
  'dmd_version',
  ?
);

-- name: RetrieveFinalisedAt :one
select
  name,
  value
from
  metadata
where
  name = 'finalised_at';

-- name: SetFinalisedAt :exec
insert into metadata (
  name,
  value
) VALUES (
  'finalised_at',
  ?
);
