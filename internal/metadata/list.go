package metadata

import (
	"context"
	"database/sql"
	"fmt"
	"slices"

	"dmd.tanna.dev/internal/metadata/db"
	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/jedib0t/go-pretty/v6/text"
)

func List(ctx context.Context, sqlDB *sql.DB) (table.Writer, error) {
	q := db.New(sqlDB)

	rows, err := q.RetrieveAll(ctx)
	if err != nil {
		return nil, fmt.Errorf("failed to list all `metadata` keys: %w", err)
	}

	tw := table.NewWriter()
	tw.SetTitle("Database metadata")

	tw.AppendHeader(table.Row{
		"Key",
		"Value",
	})

	anyUnknown := false
	for _, m := range rows {
		name := m.Name
		if !slices.Contains(db.KnownKeys(), name) {
			anyUnknown = true
			name = text.FgRed.Sprint(name)
		}

		tw.AppendRow(table.Row{
			name,
			m.Value,
		})
	}

	if anyUnknown {
		tw.SetCaption("NOTE: Fields in red are not known to dependency-management-data")
	}

	return tw, nil
}
