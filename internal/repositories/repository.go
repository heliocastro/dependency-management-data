package repositories

import (
	"context"
	"database/sql"

	"dmd.tanna.dev/internal/advisory"
	"dmd.tanna.dev/internal/datasources/awselasticache"
	"dmd.tanna.dev/internal/datasources/awslambda"
	"dmd.tanna.dev/internal/datasources/awsrds"
	"dmd.tanna.dev/internal/datasources/renovate"
	"dmd.tanna.dev/internal/datasources/sbom"
	"dmd.tanna.dev/internal/dependencyhealth"
	"dmd.tanna.dev/internal/depsdev"
	"dmd.tanna.dev/internal/endoflifedate"
	"dmd.tanna.dev/internal/externaldata"
	"dmd.tanna.dev/internal/libyear"
	"dmd.tanna.dev/internal/metadata"
	"dmd.tanna.dev/internal/ownership"
	"dmd.tanna.dev/internal/repositorymetadata"
	"dmd.tanna.dev/internal/sensitivepackages"
)

type Repository interface {
	CreateTables(ctx context.Context, sqlDB *sql.DB) error
}

var repositories = []Repository{
	&renovate.Renovate{},
	&awselasticache.AWSElasticache{},
	&awslambda.AWSLambda{},
	&awsrds.AWSRds{},
	&endoflifedate.EndOfLifeDate{},
	&ownership.Ownership{},
	&depsdev.DepsDev{},
	&advisory.Advisories{},
	&sbom.SBOMs{},
	&metadata.Metadata{},
	&sensitivepackages.SensitivePackages{},
	&externaldata.ExternalData{},
	&repositorymetadata.RepoMetadata{},
	&dependencyhealth.DependencyHealth{},
	&libyear.Libyear{},
}

func CreateTables(ctx context.Context, db *sql.DB) (err error) {
	for _, r := range repositories {
		err = r.CreateTables(ctx, db)
		if err != nil {
			return err
		}
	}

	return
}
