package repositorymetadata

import (
	"context"
	"database/sql"

	"dmd.tanna.dev/internal/repositorymetadata/db"
)

type RepoMetadata struct{}

func (*RepoMetadata) Name() string {
	return "RepoMetadata"
}

func (*RepoMetadata) CreateTables(ctx context.Context, sqlDB *sql.DB) error {
	_, err := sqlDB.ExecContext(ctx, db.CreateTablesQuery)
	return err
}
