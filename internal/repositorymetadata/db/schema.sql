-- repository_metadata allows tracking custom metadata around repositories,
-- separate to the `owners` table.
--
-- This metadata can provide some additional context to your repositories for
-- instance converting:
--
-- > {repo} has 8 outstanding policy violations, and is running 3 end-of-life
--   pieces of software
--
-- But with the metadata inside `repository_metadata`, we can instead reframe
-- this with:
--
-- > The public-facing repo {repo}, which is deployed to highly sensitive PCI
--   environments has 8 outstanding policy violations, and is running 3
--   end-of-life pieces of software
--
-- This additional metadata can make it possible to better understand and
-- action data within dependency-management-data.
--
-- As well as first-class metadata, it's possible to track custom
-- organisation-specific metadata that can be queried via SQLite's JSON
-- functions.
CREATE TABLE IF NOT EXISTS repository_metadata (
  -- what platform hosts the source code that this repo is for? i.e. `github`,
  -- `gitlab`, `gitea`, etc
  --
  -- See also: https://dmd.tanna.dev/concepts/repo-key/#platform
  --
  -- Foreign keys:
  -- - `renovate.platform`
  -- - `sboms.platform`
  platform TEXT NOT NULL,
  -- what organisation manages the source code for this repo? Can include `/`
  -- for nested organisations
  --
  -- See also: https://dmd.tanna.dev/concepts/repo-key/#organisation
  --
  -- Foreign keys:
  -- - `renovate.organisation`
  -- - `sboms.organisation`
  organisation TEXT NOT NULL,
  -- what is the repo name?
  --
  -- See also: https://dmd.tanna.dev/concepts/repo-key/#repo
  --
  -- Foreign keys:
  -- - `renovate.repo`
  -- - `sboms.repo`
  repo TEXT NOT NULL,

  -- is_monorepo indicates whether the repository is treated as a monorepo
  is_monorepo boolean not null,

  -- is_fork indicates whether this is a forked repository. This could indicate
  -- that this is a temporary repository, a long-standing fork for security +
  -- supply-chain hygiene purposes, or some other reason.
  is_fork boolean not null,

  -- repository_type is a free-form field to create enum-style data, for
  -- instance `LIBRARY` or `SERVICE`, or `EXAMPLE_CODE`.
  --
  -- This may track with your Developer Portal's own definition of a
  -- repository's type.
  --
  -- For additional usage, you can use the `repository_usage` field, or for
  -- further key-value data, use the`additional_metadata` field
  repository_type text not null,

  -- repository_usage is a free-form field to note additional information
  -- around the repository's usage, which is organisation-specific.
  --
  -- For instance, this may be enum-style data, a space-separated list of
  -- enum-style data, or a long human-readable description.
  --
  -- For additional usage, you can use the `additional_metadata` field
  repository_usage text,

  -- visibility indicates the repository's visibility in the source forge
  --
  -- NOTE that this may be straightforward if you're using a publicly hosted
  -- source forge, but if you're running on an internally run, i.e. VPN'd off
  -- source force, this field may have a slightly different interpretation
  visibility TEXT NOT NULL
    CHECK (
      visibility IN (
        "PUBLIC",
        "PRIVATE",
        "INTERNAL"
      )
    ),

  -- description is a textual description of the repo for more context, which
  -- can include links out to other systems i.e. a Service Catalog. The
  -- contents will be shown verbatim to a user, and will not be interpreted as
  -- markup
  description TEXT,

  -- additional_metadata is a JSON object of additional key-value data that can
  -- be used to provide custom organisation-specific configuration, and augment
  -- any queries for data with information around this additional metadata for
  -- instance:
  --
  -- - `last_commit_date` - the last commit date to the project
  -- - `pci_environment` - the PCI environment the application is deployed to
  -- - `customer_type` - i.e. whether it's used for government, financial
  --   users, etc
  --
  -- NOTE this must be a JSON object of key-value strings
  additional_metadata TEXT,

  UNIQUE (platform, organisation, repo) ON CONFLICT REPLACE
);
