-- name: InsertCve :exec
INSERT INTO depsdev_cves (
  package_name,
  version,

  cve_id,

  updated_at
  ) VALUES (
  ?,
  ?,

  ?,

  ?
);

-- name: InsertLicense :exec
INSERT INTO depsdev_licenses (
  package_name,
  version,

  license,

  updated_at
  ) VALUES (
  ?,
  ?,

  ?,

  ?
);
