-- depsdev_cves contains Common Vulnerabilities and Exposures (CVE) information
-- as derived from the API of https://deps.dev for given package dependencies.
--
-- This table is a utility table, and is expected to be `JOIN`'d with
-- Datasources (https://dmd.tanna.dev/concepts/datasource/) rather than used on
-- its own.
--
-- TODO: Add package_manager
-- https://gitlab.com/tanna.dev/dependency-management-data/-/issues/397
CREATE TABLE IF NOT EXISTS depsdev_cves (
  -- the package that this CVE describes
  --
  -- Foreign keys:
  -- - `renovate.package_name`
  -- - `sboms.package_name`
  package_name TEXT NOT NULL,
  -- version is the exact version that this CVE describes
  --
  -- Foreign keys:
  -- - `renovate.current_version`
  -- - `sboms.current_version`
  version TEXT NOT NULL,

  -- the external identifier of the CVE
  cve_id TEXT NOT NULL,

  -- updated_at indicates when this CVE was last updated in
  -- dependency-management-data, not when the CVE was last updated
  updated_at TEXT NOT NULL,

  UNIQUE (package_name, version, cve_id) ON CONFLICT REPLACE
);

-- depsdev_cves contains licensing information as derived from the API of
-- https://deps.dev for given package dependencies.
--
-- This table is a utility table, and is expected to be `JOIN`'d with
-- Datasources (https://dmd.tanna.dev/concepts/datasource/) rather than used on
-- its own.
--
-- TODO: Add package_manager
-- https://gitlab.com/tanna.dev/dependency-management-data/-/issues/397
CREATE TABLE IF NOT EXISTS depsdev_licenses (
  -- the package that this license covers
  --
  -- Foreign keys:
  -- - `renovate.package_name`
  -- - `sboms.package_name`
  package_name TEXT NOT NULL,
  -- version is the exact version of this package that this license applies to.
  -- For example:
  --   1.2.3
  --   v5.6.7-somelonghash
  --
  -- Foreign keys:
  -- - `renovate.current_version`
  -- - `sboms.current_version`
  version TEXT NOT NULL,

  -- license is the SPDX Identifier or SPDX License Expression
  -- (https://spdx.dev/learn/handling-license-info/) that declares this
  -- package's license
  license TEXT NOT NULL,

  -- updated_at indicates when this CVE was last updated in
  -- dependency-management-data, not when the CVE was last updated
  updated_at TEXT NOT NULL,

  UNIQUE (package_name, version, license) ON CONFLICT REPLACE
);
