package depsdev

import (
	"context"
	"database/sql"

	"dmd.tanna.dev/internal/depsdev/db"
)

type DepsDev struct{}

func (d *DepsDev) CreateTables(ctx context.Context, sqlDB *sql.DB) error {
	_, err := sqlDB.ExecContext(ctx, db.CreateTablesQuery)
	return err
}
