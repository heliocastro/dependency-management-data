package libyear

import (
	"math"
	"time"
)

func Calculate(t1, t2 time.Time) float64 {
	diff := t1.Sub(t2)
	years := diff.Hours() / 24.0 / 365.0
	return math.Abs(years)
}
