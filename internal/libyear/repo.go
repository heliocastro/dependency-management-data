package libyear

import (
	"context"
	"database/sql"

	"dmd.tanna.dev/internal/libyear/db"
)

type Libyear struct{}

func (*Libyear) Name() string {
	return "Libyear"
}

func (*Libyear) CreateTables(ctx context.Context, sqlDB *sql.DB) error {
	_, err := sqlDB.ExecContext(ctx, db.CreateTablesQuery)
	return err
}
