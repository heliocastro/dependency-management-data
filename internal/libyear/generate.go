package libyear

import (
	"context"
	"database/sql"
	"fmt"
	"log/slog"
	"net/http"
	"strings"
	"sync"
	"time"

	"dmd.tanna.dev/internal/datasources/renovate"
	"dmd.tanna.dev/internal/ecosystems"
	"dmd.tanna.dev/internal/libyear/db"
	"github.com/jedib0t/go-pretty/v6/progress"
	"github.com/package-url/packageurl-go"
)

const maxGoroutines = 250

type ecosystemsResult struct {
	LastSyncedAt             *string
	LatestReleasePublishedAt *time.Time
	LatestRelease            *string

	CurrentReleasePublishedAt *time.Time
}

func Generate(ctx context.Context, logger *slog.Logger, sqlDB *sql.DB, pw progress.Writer, httpClient *http.Client) error {
	q := db.New(sqlDB)

	rows, err := q.RetrieveDistinctPackagesAndVersionsWithoutSensitivePackages(ctx)
	if err != nil {
		return fmt.Errorf("failed to query datasources for distinct packages: %w", err)
	}

	tx, err := sqlDB.BeginTx(ctx, nil)
	if err != nil {
		return fmt.Errorf("failed to prepare a database transaction: %w", err)
	}

	defer tx.Rollback()

	ecosystemsClient, err := ecosystems.NewClientWithResponses(ecosystems.BaseURL, ecosystems.WithHTTPClient(httpClient))
	if err != nil {
		return fmt.Errorf("failed to construct API client for Ecosyste.ms' Packages API: %w", err)
	}

	tracker := progress.Tracker{
		Message: fmt.Sprintf("Calculating Libyear stats for %d dependencies", len(rows)),
		Total:   int64(len(rows)),
	}
	pw.AppendTracker(&tracker)

	var wg sync.WaitGroup
	var m sync.Mutex
	goroutines := make(chan struct{}, maxGoroutines)

	var results []db.InsertRowParams
	for _, row := range rows {
		wg.Add(1)

		go func(row db.RetrieveDistinctPackagesAndVersionsWithoutSensitivePackagesRow) {
			defer func() {
				<-goroutines
				wg.Done()
				tracker.Increment(1)
			}()

			goroutines <- struct{}{}

			e, err := retrieveLatestReleaseMetadata(ctx, ecosystemsClient, logger, row)
			if err != nil {
				logger.Error(err.Error(), "err", err)
				return
			}

			if e == nil {
				return
			}

			if e.LastSyncedAt == nil {
				logger.Warn("Skipping Libyear calculation, as no `last_synced_at` date was found")
				return
			}

			if e.CurrentReleasePublishedAt == nil || e.LatestReleasePublishedAt == nil || e.LatestRelease == nil {
				if e.LatestRelease != nil {
					logger = logger.With("latestRelease", *e.LatestRelease)
				}

				logger.Warn("Skipping Libyear calculation, as publish dates were missing", "currentReleasePublishedAt", e.CurrentReleasePublishedAt, "latestReleasePublishedAt", e.LatestReleasePublishedAt)
				return
			}

			if e.CurrentReleasePublishedAt.After(*e.LatestReleasePublishedAt) {
				if e.LatestRelease != nil {
					logger = logger.With("latestRelease", *e.LatestRelease)
				}

				logger.Warn("Skipping Libyear calculation, as publish dates weren't quite right - seeing the version you're using as published before the latest release", "currentReleasePublishedAt", e.CurrentReleasePublishedAt, "latestReleasePublishedAt", e.LatestReleasePublishedAt)
				return
			}

			ly := Calculate(*e.CurrentReleasePublishedAt, *e.LatestReleasePublishedAt)

			result := db.InsertRowParams{
				PackageName:              row.PackageName,
				Version:                  row.Version,
				CurrentVersion:           row.CurrentVersion,
				PackageManager:           row.PackageManager,
				Libyear:                  ly,
				VersionReleaseDate:       e.CurrentReleasePublishedAt.Format(time.RFC3339),
				LatestVersion:            *e.LatestRelease,
				LatestVersionReleaseDate: e.LatestReleasePublishedAt.Format(time.RFC3339),
				LastSyncedAt:             *e.LastSyncedAt,
			}

			if row.CurrentVersion.Valid {
				result.CurrentVersion = row.CurrentVersion
			}

			m.Lock()
			results = append(results, result)
			m.Unlock()
		}(row)
	}

	wg.Wait()

	tracker = progress.Tracker{
		Message: fmt.Sprintf("Persisting Libyear stats for %d dependencies", len(results)),
		Total:   int64(len(results)),
	}
	pw.AppendTracker(&tracker)

	for _, row := range results {
		err = q.WithTx(tx).InsertRow(ctx, row)
		if err != nil {
			tracker.MarkAsErrored()
			return fmt.Errorf("failed to insert row: %w", err)
		}
		tracker.Increment(1)
	}

	tracker.MarkAsDone()

	return tx.Commit()
}

func retrieveLatestReleaseMetadata(ctx context.Context, client *ecosystems.ClientWithResponses, logger *slog.Logger, row db.RetrieveDistinctPackagesAndVersionsWithoutSensitivePackagesRow) (*ecosystemsResult, error) {
	// without version, as the lookup endpoint doesn't do version-based lookups - https://github.com/ecosyste-ms/packages/issues/644
	asPurl := packageAsPurlWithoutVersion(row)

	logger = logger.With("pURL", asPurl.String())

	resp, err := client.LookupPackageWithResponse(ctx, &ecosystems.LookupPackageParams{
		Purl: ptr(asPurl.String()),
	})
	if err != nil {
		return nil, fmt.Errorf("failed lookup in Ecosystems: %v", err)
	}

	if resp.StatusCode() != http.StatusOK {
		logger.Warn(fmt.Sprintf("Failed lookup in Ecosystems: HTTP %d", resp.StatusCode()))
		return nil, nil
	}

	if resp.JSON200 == nil || len(*resp.JSON200) == 0 {
		logger.Warn(fmt.Sprintf("Lookup for pURL %s in Ecosystems returned no results", asPurl.String()))
		return nil, nil
	}

	if len(*resp.JSON200) > 1 {
		logger.Warn(fmt.Sprintf("Lookup for pURL %s in Ecosystems returned %d results, but was only expecting 1", asPurl.String(), len(*resp.JSON200)))
	}

	pack := (*resp.JSON200)[0]

	r := ecosystemsResult{}
	if pack.LatestReleasePublishedAt != nil {
		r.LatestReleasePublishedAt = pack.LatestReleasePublishedAt
	}
	if pack.LastSyncedAt != nil {
		r.LastSyncedAt = ptr(pack.LastSyncedAt.Format(time.RFC3339))
		logger = logger.With("lastSyncedAt", *pack.LastSyncedAt)
	}

	if pack.LatestReleaseNumber != nil {
		logger = logger.With("latestReleaseNumber", *pack.LatestReleaseNumber)
	}

	r.LatestRelease = pack.LatestReleaseNumber
	r.CurrentReleasePublishedAt = ptr(pack.CreatedAt)

	// then look up the specific version we're on

	version := row.Version
	if row.CurrentVersion.Valid {
		version = row.CurrentVersion.String
	}

	vResp, err := client.GetRegistryPackageVersionWithResponse(ctx, pack.Registry.Name, asPurl.Name, version)
	if err != nil {
		logger.Warn(fmt.Sprintf("failed to get dependency version information from Ecosystems: %v", err), "packageName", asPurl.Name, "version", version, "registryName", pack.Registry.Name)
		return &r, nil
	}

	if vResp.StatusCode() != http.StatusOK {
		logger.Warn(fmt.Sprintf("Failed lookup in Ecosystems: HTTP %d", vResp.StatusCode()), "packageName", asPurl.Name, "version", version, "registryName", pack.Registry.Name)
		return &r, nil
	}

	if vResp.JSON200.PublishedAt == nil {
		logger.Warn("Ecosystems returned package data for package, but the `published_at` was <nil>", "packageName", asPurl.Name, "version", version, "registryName", pack.Registry.Name)
		return &r, nil
	}

	parsed, err := time.Parse(time.RFC3339, *vResp.JSON200.PublishedAt)
	if err != nil {
		logger.Warn(fmt.Sprintf("Ecosystems returned a `published_at` that wasn't a valid RFC3339 date: was %v", *vResp.JSON200.PublishedAt), "packageName", asPurl.Name, "version", version, "registryName", pack.Registry.Name)
	}

	r.CurrentReleasePublishedAt = ptr(parsed)
	logger = logger.With("currentReleasePublishedAt", r.CurrentReleasePublishedAt)

	logger.Debug(fmt.Sprintf("Retrieved package metadata for version %#v", asPurl.Version))
	return &r, nil
}

func packageAsPurlWithoutVersion(row db.RetrieveDistinctPackagesAndVersionsWithoutSensitivePackagesRow) *packageurl.PackageURL {
	var purlType string
	var namespace string
	var name string

	for _, ptd := range renovate.PurlTypeDerivers {
		t, found := ptd(row.PackageManager, row.Datasource)

		if found {
			purlType = t
			break
		}
	}

	// TODO revisit during https://gitlab.com/tanna.dev/dependency-management-data/-/issues/232
	name = strings.ReplaceAll(row.PackageName, "%40", "@")

	return packageurl.NewPackageURL(purlType, namespace, name, "", nil, "")
}

func ptr[T any](t T) *T {
	return &t
}
