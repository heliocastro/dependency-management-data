package db

import (
	"cmp"
	"strings"
)

func NewRetrieveAllLibyearsParams(platform string, org string, repo string, owner string) RetrieveAllLibyearsParams {
	p := RetrieveAllLibyearsParams{
		Platform: strings.ReplaceAll(cmp.Or(platform, "%"), "*", "%"),
		Org:      strings.ReplaceAll(cmp.Or(org, "%"), "*", "%"),
		Repo:     strings.ReplaceAll(cmp.Or(repo, "%"), "*", "%"),
		Owner:    strings.ReplaceAll(cmp.Or(owner, "%"), "*", "%"),
	}

	return p
}
