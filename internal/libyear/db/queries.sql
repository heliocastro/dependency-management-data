-- name: RetrieveDistinctPackagesAndVersionsWithoutSensitivePackages :many
select
  platform,
  organisation,
  repo,
  package_name,
  version,
  current_version,
  renovate.package_manager,
  datasource
from
  renovate
  left join sensitive_packages as sp on
    renovate.package_name like replace(sp.package_pattern, '*', '%')
where
  (
    case
      when sp.package_manager IS NOT NULL then sp.package_manager == renovate.package_manager
      else true
    end
  )
  and (
    case
      when sp.match_strategy = 'MATCHES' then false
      when sp.match_strategy = 'DOES_NOT_MATCH' then true
      else true
    end
  )
UNION
select
  platform,
  organisation,
  repo,
  package_name,
  version,
  current_version,
  package_type as package_manager,
  '' as datasource
from
  sboms
  left join sensitive_packages as sp on
    sboms.package_name like replace(sp.package_pattern, '*', '%')
where
  (
    case
      when sp.package_manager IS NOT NULL then sp.package_manager == sboms.package_type
      else true
    end
  )
  and (
    case
      when sp.match_strategy = 'MATCHES' then false
      when sp.match_strategy = 'DOES_NOT_MATCH' then true
      else true
    end
  );

-- name: InsertRow :exec
insert into libyears (
  package_name,
  version,
  current_version,
  package_manager,

  libyear,

  version_release_date,
  latest_version,
  latest_version_release_date,
  last_synced_at
) VALUES (
  ?,
  ?,
  ?,
  ?,

  ?,

  ?,
  ?,
  ?,
  ?
);

-- name: RetrieveAllLibyears :many
select
  renovate.platform,
  renovate.organisation,
  renovate.repo,
  owner,
  sum(libyear) as total_libyears
from
  renovate
  inner join libyears
  on  renovate.package_name    = libyears.package_name
  and renovate.version         = libyears.version
  and renovate.current_version = libyears.current_version
  and renovate.package_manager = libyears.package_manager
  left join owners
  on  renovate.platform     = owners.platform
  and renovate.organisation = owners.organisation
  and renovate.repo         = owners.repo
where
  renovate.platform     like sqlc.arg(platform) and
  renovate.organisation like sqlc.arg(org) and
  renovate.repo         like sqlc.arg(repo) and
  (
    -- checking for the owner is a little more complicated due to the fact that
    -- it's a nullable field
    -- TODO We may be able to simplify this.
    (
      sqlc.arg(owner) == '%' and
      (
        owners.owner like sqlc.arg(owner) or
        owners.owner IS NULL
      )
    ) or
    (
      sqlc.arg(owner) != '%' and
      owners.owner like sqlc.arg(owner)
    )
  )
group by
  renovate.platform,
  renovate.organisation,
  renovate.repo
union
select
  sboms.platform,
  sboms.organisation,
  sboms.repo,
  owner,
  sum(libyear) as total_libyears
from
  sboms
  inner join libyears
  on  sboms.package_name    = libyears.package_name
  and sboms.version         = libyears.version
  and sboms.current_version = libyears.current_version
  and sboms.package_type    = libyears.package_manager
  left join owners
  on  sboms.platform     = owners.platform
  and sboms.organisation = owners.organisation
  and sboms.repo         = owners.repo
where
  sboms.platform     like sqlc.arg(platform) and
  sboms.organisation like sqlc.arg(org) and
  sboms.repo         like sqlc.arg(repo) and
  (
    -- checking for the owner is a little more complicated due to the fact that
    -- it's a nullable field
    -- TODO We may be able to simplify this.
    (
      sqlc.arg(owner) == '%' and
      (
        owners.owner like sqlc.arg(owner) or
        owners.owner IS NULL
      )
    ) or
    (
      sqlc.arg(owner) != '%' and
      owners.owner like sqlc.arg(owner)
    )
  )
group by
  sboms.platform,
  sboms.organisation,
  sboms.repo
order by total_libyears desc
  ;

-- name: RetrieveLibyearForSpecificRepo :many
select
  libyears.package_name,
  libyears.version,
  libyears.current_version,
  libyears.package_manager,
  libyear,
  version_release_date,
  latest_version,
  latest_version_release_date,
  owner
from
  renovate
  inner join libyears
  on  renovate.package_name    = libyears.package_name
  and renovate.version         = libyears.version
  and renovate.current_version = libyears.current_version
  and renovate.package_manager = libyears.package_manager
  left join owners
  on  renovate.platform     = owners.platform
  and renovate.organisation = owners.organisation
  and renovate.repo         = owners.repo
where
  renovate.platform     = sqlc.arg(platform) and
  renovate.organisation = sqlc.arg(org) and
  renovate.repo         = sqlc.arg(repo)
union
select
  libyears.package_name,
  libyears.version,
  libyears.current_version,
  libyears.package_manager,
  libyear,
  version_release_date,
  latest_version,
  latest_version_release_date,
  owner
from
  sboms
  inner join libyears
  on  sboms.package_name    = libyears.package_name
  and sboms.version         = libyears.version
  and sboms.current_version = libyears.current_version
  and sboms.package_type    = libyears.package_manager
  left join owners
  on  sboms.platform     = owners.platform
  and sboms.organisation = owners.organisation
  and sboms.repo         = owners.repo
where
  sboms.platform     = sqlc.arg(platform) and
  sboms.organisation = sqlc.arg(org) and
  sboms.repo         = sqlc.arg(repo)
order by libyear desc
  ;
