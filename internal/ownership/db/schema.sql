-- owners allows defining which team / person / vendor / part of your
-- organisation / etc owns a given repository. This data is expected to be
-- sourced through a Service Catalog or through some other means, and will be
-- organisation-specific.
--
-- More detail can be found in https://dmd.tanna.dev/cookbooks/ownership/
-- around how to source + query this data.
--
-- Each platform/organisation/repo combination should have one owner.
--
-- NOTE that in the future, multiple owners may be allowed
-- https://gitlab.com/tanna.dev/dependency-management-data/-/issues/112
CREATE TABLE IF NOT EXISTS owners (
  -- what platform hosts the source code that this repo is for? i.e. `github`,
  -- `gitlab`, `gitea`, etc
  --
  -- See also: https://dmd.tanna.dev/concepts/repo-key/#platform
  --
  -- Foreign keys:
  -- - `renovate.platform`
  -- - `sboms.platform`
  platform TEXT NOT NULL,
  -- what organisation manages the source code for this repo? Can include `/`
  -- for nested organisations
  --
  -- See also: https://dmd.tanna.dev/concepts/repo-key/#organisation
  --
  -- Foreign keys:
  -- - `renovate.organisation`
  -- - `sboms.organisation`
  organisation TEXT NOT NULL,
  -- what is the repo name?
  --
  -- See also: https://dmd.tanna.dev/concepts/repo-key/#repo
  --
  -- Foreign keys:
  -- - `renovate.repo`
  -- - `sboms.repo`
  repo TEXT NOT NULL,

  -- owner is a free-form identifier for who owns the repository. This could be
  -- an email address, a team name, Slack channel name, etc, but should ideally
  -- be clear from this column who should be contacted about any queries about
  -- the repository
  owner TEXT NOT NULL,
  -- notes allows adding additional, optional, context around the ownership,
  -- for instance a link to a Slack channel, Confluence page, internal Service
  -- Catalog, etc. The contents will be shown verbatim to a user, and will not
  -- be interpreted as markup.
  notes TEXT,

  -- updated_at indicates when this ownership information was last updated in
  -- dependency-management-data, not when the ownership last changed between
  -- teams
  updated_at TEXT NOT NULL,

  UNIQUE (platform, organisation, repo) ON CONFLICT REPLACE
);
