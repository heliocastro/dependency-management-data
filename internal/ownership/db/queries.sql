-- name: InsertOwner :exec
INSERT INTO owners (
  platform,
  organisation,
  repo,

  owner,
  notes,

  updated_at
  ) VALUES (
  ?,
  ?,
  ?,

  ?,
  ?,

  ?
);
