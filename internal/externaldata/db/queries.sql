-- name: InsertLicense :exec
INSERT INTO external_licenses (
  package_name,
  version,
  package_manager,
  license
) VALUES (
  ?,
  ?,
  ?,
  ?
);
