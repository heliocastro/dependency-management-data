// Code generated by sqlc. DO NOT EDIT.
// versions:
//   sqlc v1.23.0
// source: queries.sql

package db

import (
	"context"
)

const insertLicense = `-- name: InsertLicense :exec
INSERT INTO external_licenses (
  package_name,
  version,
  package_manager,
  license
) VALUES (
  ?,
  ?,
  ?,
  ?
)
`

type InsertLicenseParams struct {
	PackageName    string
	Version        string
	PackageManager string
	License        string
}

func (q *Queries) InsertLicense(ctx context.Context, arg InsertLicenseParams) error {
	_, err := q.db.ExecContext(ctx, insertLicense,
		arg.PackageName,
		arg.Version,
		arg.PackageManager,
		arg.License,
	)
	return err
}
