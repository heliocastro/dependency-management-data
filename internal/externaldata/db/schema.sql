-- external_licenses defines a table that can consume user-generated licensing
-- information for packages.
--
-- The prescence of external_licenses will take priority over
-- `depsdev_licenses` in reporting, as the assumption is that this
-- user-generated data is of higher quality.

-- This table will only be populated by DMD if:
--
-- - an SBOM defines a license
--
-- Otherwise, this table should be populated by your own tooling.
CREATE TABLE IF NOT EXISTS external_licenses (
  -- the package that this license covers
  --
  -- Foreign keys:
  -- - `renovate.package_name`
  -- - `sboms.package_name`
  package_name TEXT NOT NULL,
  -- version is the exact version of this package that this license applies to.
  -- For example:
  --   1.2.3
  --   v5.6.7-somelonghash
  --
  -- Foreign keys:
  -- - `renovate.current_version`
  -- - `sboms.current_version`
  version TEXT NOT NULL,
  -- package_manager indicates the package manager that the given
  -- `package_pattern` should match.
  --
  -- Based on which datasource(s) (https://dmd.tanna.dev/concepts/datasource/)
  -- you are using, this will be a different value:
  -- - for Renovate data, must exactly match `renovate.package_manager`.
  --   Note that there may be multiple `package_managers`, for instance `maven`
  --   and `gradle`, which would require two rows.
  -- - for Software Bill of Materials (SBOM) data, must exactly match `sboms.package_type`
  --
  -- If you are using multiple datasources, you will need to have one row per
  -- `package_manager`.
  --
  -- Foreign keys:
  -- - `renovate.package_manager`
  -- - `sboms.package_type`
  package_manager TEXT NOT NULL,

  -- license is the SPDX Identifier or SPDX License Expression
  -- (https://spdx.dev/learn/handling-license-info/) that declares this
  -- package's license
  license TEXT NOT NULL,

  UNIQUE (package_name, version, package_manager, license) ON CONFLICT REPLACE,
  CHECK(package_name <> ''),
  CHECK(version <> ''),
  CHECK(package_manager <> ''),
  CHECK(license <> '')
);
