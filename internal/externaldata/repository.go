package externaldata

import (
	"context"
	"database/sql"

	"dmd.tanna.dev/internal/externaldata/db"
)

type ExternalData struct{}

func (*ExternalData) Name() string {
	return "ExternalData"
}

func (*ExternalData) CreateTables(ctx context.Context, sqlDB *sql.DB) error {
	_, err := sqlDB.ExecContext(ctx, db.CreateTablesQuery)
	return err
}
