package externaldata

import (
	"context"
	"database/sql"
	"fmt"

	"dmd.tanna.dev/internal/domain"
	"dmd.tanna.dev/internal/externaldata/db"
	"github.com/jedib0t/go-pretty/v6/progress"
)

func ImportLicenses(ctx context.Context, licenses []domain.License, sqlDB *sql.DB, pw progress.Writer) error {
	q := db.New(sqlDB)

	tx, err := sqlDB.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	tracker := progress.Tracker{
		Message: fmt.Sprintf("Importing %d externally-defined licenses", len(licenses)),
		Total:   int64(len(licenses)),
	}

	for _, l := range licenses {
		p := db.InsertLicenseParams{
			PackageName:    l.PackageName,
			Version:        l.Version,
			PackageManager: l.PackageManager,
			License:        l.License,
		}
		err := q.WithTx(tx).InsertLicense(ctx, p)

		if err != nil {
			tracker.MarkAsErrored()
			return err
		}

		tracker.Increment(1)
	}

	tracker.MarkAsDone()
	tracker.UpdateMessage(fmt.Sprintf("Imported %d externally-defined licenses", tracker.Total))
	return tx.Commit()
}
