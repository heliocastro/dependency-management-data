# METADATA
# description: |
#   There must be at least one string rule named `advisory_type`
# related_resources:
# - description: documentation
#   ref: https://dmd.tanna.dev/cookbooks/custom-advisories-opa/#requirements
# schemas:
# - input: schema.regal.ast
package custom.regal.rules.organizational["dmd-var-advisorytype"]

import future.keywords.contains
import future.keywords.if
import future.keywords.in

import data.regal.ast
import data.regal.result

var_exists if {
    some rule in ast.rules

    ast.name(rule) == "advisory_type"
}

var_correct if {
    some rule in ast.rules

    ast.name(rule) == "advisory_type"
    rule["default"] == true
    rule.head.value.type == "string"
}

report contains violation if {
	not var_exists

	violation := result.fail(rego.metadata.chain(), result.location(input.regal.file.name))
}

report contains violation if {
	var_exists
	not var_correct

	violation := result.fail(rego.metadata.chain(), result.location(input.regal.file.name))
}
