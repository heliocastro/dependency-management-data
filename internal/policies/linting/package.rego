# METADATA
# description: All policies must have the package name `policy`
# related_resources:
# - description: documentation
#   ref: https://dmd.tanna.dev/cookbooks/custom-advisories-opa/#requirements
# schemas:
# - input: schema.regal.ast
package custom.regal.rules.naming["dmd-package-name"]

import future.keywords.contains
import future.keywords.if

import data.regal.result

report contains violation if {
	not policy_package

	violation := result.fail(rego.metadata.chain(), result.location(input["package"].path[1]))
}

policy_package if {
	count(input["package"].path) == 2
	input["package"].path[1].value == "policy"
}
