package policies

import (
	"context"
	"database/sql"
	"embed"
	"encoding/json"
	"errors"
	"fmt"
	"strings"
	"sync"

	"dmd.tanna.dev/internal/domain"
	"dmd.tanna.dev/internal/policies/db"
	"github.com/open-policy-agent/opa/ast"
	"github.com/open-policy-agent/opa/rego"
	"github.com/styrainc/regal/pkg/linter"
	"github.com/styrainc/regal/pkg/rules"
)

type Module struct {
	Filepath string
	Contents string
}

type EvaluationInput struct {
	Project    EvaluationInputProject    `json:"project"`
	Dependency EvaluationInputDependency `json:"dependency"`
}

func (i EvaluationInput) AsAstValue() ast.Value {
	proj := i.Project.asAstValue()
	dep := i.Dependency.asAstValue()

	astValue := ast.NewObject()

	astValue.Insert(ast.StringTerm("project"), ast.NewTerm(proj))
	astValue.Insert(ast.StringTerm("dependency"), ast.NewTerm(dep))

	return astValue
}

type EvaluationInputProject struct {
	// Platform describes the source hosting platform this dependency's repository is found on, i.e. `github`, `gitlab`, `gitea`, etc
	Platform string `json:"platform"`
	// Organisation describes the organisation this dependency's repository is found on. Can include `/` for nested organisations
	Organisation string `json:"organisation"`
	// Repo is the repo name
	Repo string `json:"repo"`
	// Metadata tracks metadata about the repository, which is sourced from the `repository_metadata` table
	Metadata *EvaluationInputRepositoryMetadata `json:"metadata,omitempty"`
}

func (p EvaluationInputProject) asAstValue() ast.Value {
	proj := ast.NewObject()
	proj.Insert(ast.StringTerm("platform"), ast.StringTerm(p.Platform))
	proj.Insert(ast.StringTerm("organisation"), ast.StringTerm(p.Organisation))
	proj.Insert(ast.StringTerm("repo"), ast.StringTerm(p.Repo))
	if p.Metadata != nil {
		proj.Insert(ast.StringTerm("metadata"), ast.NewTerm(p.Metadata.asAstValue()))
	}

	return proj
}

type EvaluationInputRepositoryMetadata struct {
	// IsMonorepo indicates whether the repository is treated as a monorepo
	IsMonorepo bool `json:"is_monorepo"`
	// IsFork indicates whether this is a forked repository. This could indicate that this is a temporary repository, a long-standing fork for security + supply-chain hygiene purposes, or some other reason.
	IsFork bool `json:"is_fork"`
	// RepositoryType is a free-form field to create enum-style data, for instance `LIBRARY` or `SERVICE`, or `EXAMPLE_CODE`.
	//
	// This may track with your Developer Portal's own definition of a repository's type.
	RepositoryType string `json:"repository_type"`
	// RepositoryUsage is a free-form field to note additional information around the repository's usage, which is organisation-specific.
	//
	// For instance, this may be enum-style data, a space-separated list of enum-style data, or a long human-readable description.
	RepositoryUsage *string `json:"repository_usage,omitempty"`
	// Visibility indicates the repository's visibility in the source forge
	//
	// NOTE that this may be straightforward if you're using a publicly hosted source forge, but if you're running on an internally run, i.e. VPN'd off source force, this field may have a slightly different interpretation
	Visibility string `json:"visibility"`
	// Description is a textual description of the repo for more context, which can include links out to other systems i.e. a Service Catalog. The contents will be shown verbatim to a user, and will not be interpreted as markup
	Description *string `json:"description,omitempty"`
	// AdditionalMetadata contains additional key-value data that can be used to provide custom organisation-specific configuration, and augment any queries for data with information around this additional metadata.
	//
	// For instance:
	//
	// - `last_commit_date` - the last commit date to the project
	// - `pci_environment` - the PCI environment the application is deployed to
	// - `customer_type` - i.e. whether it's used for government, financial
	//   users, etc
	AdditionalMetadata map[string]string `json:"additional_metadata,omitempty"`
}

func (p EvaluationInputRepositoryMetadata) asAstValue() ast.Value {
	m := ast.NewObject()
	m.Insert(ast.StringTerm("is_monorepo"), ast.BooleanTerm(p.IsMonorepo))
	m.Insert(ast.StringTerm("is_fork"), ast.BooleanTerm(p.IsFork))
	m.Insert(ast.StringTerm("repository_type"), ast.StringTerm(p.RepositoryType))
	if p.RepositoryUsage != nil {
		m.Insert(ast.StringTerm("repository_usage"), ast.StringTerm(*p.RepositoryUsage))
	}
	m.Insert(ast.StringTerm("visibility"), ast.StringTerm(p.Visibility))
	if p.Description != nil {
		m.Insert(ast.StringTerm("description"), ast.StringTerm(*p.Description))
	}

	additionalMetadata := ast.NewObject()
	for k, v := range p.AdditionalMetadata {
		additionalMetadata.Insert(ast.StringTerm(k), ast.StringTerm(v))
	}

	m.Insert(ast.StringTerm("additional_metadata"), ast.NewTerm(additionalMetadata))

	return m
}

type EvaluationInputDependency struct {
	// PackageName contains the name of the package
	PackageName string `json:"package_name"`
	// Version indicates the version of this dependency
	//
	// NOTE this could be a version constraint, such as any of:
	//
	//     <=1.3.4,>=1.3.0
	//     "~> 0.9"
	//     latest
	//     ^2.0.6
	//     =1.0.4
	//
	// As well as a specific value, such as:
	//
	//     1.0.4
	//     10
	//     latest
	//
	// This versioning will be implementation-specific for the `packageManager` in use
	Version string `json:"version"`
	// CurrentVersion defines the current version that this package's `version` resolves to
	//
	// If the `version` is an exact version number, such as `1.0.4`, then `currentVersion` will usually be the same value, `1.0.4`
	//
	// If the `version` is a version constraint, then this column MAY indicate the exact version that was resolved at the time of dependency analysis
	//
	// CurrentVersion may be empty
	CurrentVersion string `json:"current_version"`
	// PackageManager indicates the package manager that the Policy Violation will correspond to
	PackageManager string `json:"package_manager"`
	// packageFilePath defines the path within `repo` that defines the `packageName` as a dependency. For example:
	//
	//     .github/workflows/build.yml
	//     go.mod
	//     build/Dockerfile
	//
	// NOTE that this may be empty (https://gitlab.com/tanna.dev/dependency-management-data/-/issues/396)
	PackageFilePath string `json:"package_file_path"`
	// DepTypes defines the different dependency types that may be in use. This will always be a JSON array, with 0 or more string elements. For example:
	//
	//    []
	//    ["action"]
	//    ["dependencies","lockfile"]
	//    ["dependencies","missing-data"]
	//    ["lockfile","lockfile-yarn-pinning-^21.1.1"]
	//    ["engines"]
	//
	// Based on which datasource(s) (https://dmd.tanna.dev/concepts/datasource/) you are using, this will have different values and meanings
	//
	// NOTE that in the future these there will be a more consistent naming structure for these (https://gitlab.com/tanna.dev/dependency-management-data/-/issues/379)
	DepTypes []string `json:"dep_types"`
	// Licenses contains the SPDX Identifier(s) or SPDX License Expression(s) (https://spdx.dev/learn/handling-license-info/) that declares this package's license
	Licenses []string `json:"licenses"`

	Health *EvaluationInputDependencyHealth `json:"health,omitempty"`
}

func (d EvaluationInputDependency) asAstValue() ast.Value {
	dep := ast.NewObject()

	dep.Insert(ast.StringTerm("package_name"), ast.StringTerm(d.PackageName))
	dep.Insert(ast.StringTerm("version"), ast.StringTerm(d.Version))
	dep.Insert(ast.StringTerm("current_version"), ast.StringTerm(d.CurrentVersion))
	dep.Insert(ast.StringTerm("package_manager"), ast.StringTerm(d.PackageManager))
	dep.Insert(ast.StringTerm("package_file_path"), ast.StringTerm(d.PackageFilePath))
	var depTypes []*ast.Term
	for _, depType := range d.DepTypes {
		depTypes = append(depTypes, ast.StringTerm(depType))
	}
	dep.Insert(ast.StringTerm("dep_types"), ast.ArrayTerm(depTypes...))

	var licenses []*ast.Term
	for _, license := range d.Licenses {
		licenses = append(licenses, ast.StringTerm(license))
	}
	dep.Insert(ast.StringTerm("licenses"), ast.ArrayTerm(licenses...))

	if d.Health != nil {
		dep.Insert(ast.StringTerm("health"), ast.NewTerm(d.Health.asAstValue()))
	}

	return dep
}

type EvaluationInputDependencyHealth struct {
	SecurityScorecard *EvaluationInputDependencyHealthScorecard         `json:"security_scorecard,omitempty"`
	Ecosystems        *EvaluationInputDependencyHealthEcosystemsPackage `json:"ecosystems,omitempty"`
}

func (d EvaluationInputDependencyHealth) asAstValue() ast.Value {
	o := ast.NewObject()
	if d.SecurityScorecard != nil {
		o.Insert(ast.StringTerm("security_scorecard"), ast.NewTerm(d.SecurityScorecard.asAstValue()))
	}
	if d.Ecosystems != nil {
		o.Insert(ast.StringTerm("ecosystems"), ast.NewTerm(d.Ecosystems.asAstValue()))
	}
	return o
}

type EvaluationInputDependencyHealthScorecard struct {
	Score              *float64 `json:"score"`
	CodeReview         *int     `json:"code_review,omitempty"`
	Maintained         *int     `json:"maintained"`
	CiiBestPractices   *int     `json:"cii_best_practices"`
	License            *int     `json:"license"`
	DangerousWorkflow  *int     `json:"dangerous_workflow"`
	Packaging          *int     `json:"packaging"`
	TokenPermissions   *int     `json:"token_permissions"`
	SignedReleases     *int     `json:"signed_releases"`
	BranchProtection   *int     `json:"branch_protection"`
	BinaryArtifacts    *int     `json:"binary_artifacts"`
	Fuzzing            *int     `json:"fuzzing"`
	SecurityPolicy     *int     `json:"security_policy"`
	Sast               *int     `json:"sast"`
	Vulnerabilities    *int     `json:"vulnerabilities"`
	PinnedDependencies *int     `json:"pinned_dependencies"`
}

func (d EvaluationInputDependencyHealthScorecard) asAstValue() ast.Value {
	o := ast.NewObject()
	if d.Score != nil {
		o.Insert(ast.StringTerm("score"), ast.FloatNumberTerm(*d.Score))
	}
	if d.CodeReview != nil {
		o.Insert(ast.StringTerm("code_review"), ast.IntNumberTerm(*d.CodeReview))
	}
	if d.Maintained != nil {
		o.Insert(ast.StringTerm("maintained"), ast.IntNumberTerm(*d.Maintained))
	}
	if d.CiiBestPractices != nil {
		o.Insert(ast.StringTerm("cii_best_practices"), ast.IntNumberTerm(*d.CiiBestPractices))
	}
	if d.License != nil {
		o.Insert(ast.StringTerm("license"), ast.IntNumberTerm(*d.License))
	}
	if d.DangerousWorkflow != nil {
		o.Insert(ast.StringTerm("dangerous_workflow"), ast.IntNumberTerm(*d.DangerousWorkflow))
	}
	if d.Packaging != nil {
		o.Insert(ast.StringTerm("packaging"), ast.IntNumberTerm(*d.Packaging))
	}
	if d.TokenPermissions != nil {
		o.Insert(ast.StringTerm("token_permissions"), ast.IntNumberTerm(*d.TokenPermissions))
	}
	if d.SignedReleases != nil {
		o.Insert(ast.StringTerm("signed_releases"), ast.IntNumberTerm(*d.SignedReleases))
	}
	if d.BranchProtection != nil {
		o.Insert(ast.StringTerm("branch_protection"), ast.IntNumberTerm(*d.BranchProtection))
	}
	if d.BinaryArtifacts != nil {
		o.Insert(ast.StringTerm("binary_artifacts"), ast.IntNumberTerm(*d.BinaryArtifacts))
	}
	if d.Fuzzing != nil {
		o.Insert(ast.StringTerm("fuzzing"), ast.IntNumberTerm(*d.Fuzzing))
	}
	if d.SecurityPolicy != nil {
		o.Insert(ast.StringTerm("security_policy"), ast.IntNumberTerm(*d.SecurityPolicy))
	}
	if d.Sast != nil {
		o.Insert(ast.StringTerm("sast"), ast.IntNumberTerm(*d.Sast))
	}
	if d.Vulnerabilities != nil {
		o.Insert(ast.StringTerm("vulnerabilities"), ast.IntNumberTerm(*d.Vulnerabilities))
	}
	if d.PinnedDependencies != nil {
		o.Insert(ast.StringTerm("pinned_dependencies"), ast.IntNumberTerm(*d.PinnedDependencies))
	}

	return o
}

type EvaluationInputDependencyHealthEcosystemsPackage struct {
	Repo                     *EvaluationInputDependencyHealthEcosystemsRepo `json:"repo,omitempty"`
	LatestReleasePublishedAt *string                                        `json:"latest_release_published_at,omitempty"`
	LastSyncedAt             *string                                        `json:"last_synced_at,omitempty"`
	Status                   *string                                        `json:"status,omitempty"`
}

func (d EvaluationInputDependencyHealthEcosystemsPackage) asAstValue() ast.Value {
	o := ast.NewObject()
	if d.Repo != nil {
		o.Insert(ast.StringTerm("repo"), ast.NewTerm(d.Repo.asAstValue()))
	}
	if d.LatestReleasePublishedAt != nil {
		o.Insert(ast.StringTerm("latest_release_published_at"), ast.StringTerm(*d.LatestReleasePublishedAt))
	}
	if d.LastSyncedAt != nil {
		o.Insert(ast.StringTerm("last_synced_at"), ast.StringTerm(*d.LastSyncedAt))
	}
	if d.Status != nil {
		o.Insert(ast.StringTerm("status"), ast.StringTerm(*d.Status))
	}

	return o
}

type EvaluationInputDependencyHealthEcosystemsRepo struct {
	Archived     *bool   `json:"archived,omitempty"`
	PushedAt     *string `json:"pushed_at,omitempty"`
	UpdatedAt    *string `json:"updated_at,omitempty"`
	LastSyncedAt *string `json:"last_synced_at,omitempty"`
}

func (d EvaluationInputDependencyHealthEcosystemsRepo) asAstValue() ast.Value {
	o := ast.NewObject()
	if d.Archived != nil {
		o.Insert(ast.StringTerm("archived"), ast.BooleanTerm(*d.Archived))
	}
	if d.PushedAt != nil {
		o.Insert(ast.StringTerm("pushed_at"), ast.StringTerm(*d.PushedAt))
	}
	if d.UpdatedAt != nil {
		o.Insert(ast.StringTerm("updated_at"), ast.StringTerm(*d.UpdatedAt))
	}
	if d.LastSyncedAt != nil {
		o.Insert(ast.StringTerm("last_synced_at"), ast.StringTerm(*d.LastSyncedAt))
	}
	return o
}

func newEvaluationInput(row db.RetrieveDistinctReposAndPackagesRow, metadatum *db.RepositoryMetadatum) EvaluationInput {
	i := EvaluationInput{
		Project: EvaluationInputProject{
			Platform:     row.Platform,
			Organisation: row.Organisation,
			Repo:         row.Repo,
		},
		Dependency: EvaluationInputDependency{
			PackageName: row.PackageName,
			Version:     row.Version,
			// this could be empty
			CurrentVersion:  row.CurrentVersion.String,
			PackageManager:  row.PackageManager,
			PackageFilePath: row.PackageFilePath,
			DepTypes:        []string{},
			Licenses:        strings.Split(row.Licenses, ","),
		},
	}

	var depTypes []string
	err := json.Unmarshal([]byte(row.DepTypes), &depTypes)
	if err == nil {
		i.Dependency.DepTypes = depTypes
	}

	if metadatum != nil {
		i.Project.Metadata = &EvaluationInputRepositoryMetadata{
			IsMonorepo:     metadatum.IsMonorepo,
			IsFork:         metadatum.IsFork,
			RepositoryType: metadatum.RepositoryType,
			// RepositoryUsage handled below
			Visibility: metadatum.Visibility,
			// Description handled below
			// AdditionalMetadata handled below
		}

		if metadatum.RepositoryUsage.Valid {
			i.Project.Metadata.RepositoryUsage = &metadatum.RepositoryUsage.String
		}

		if metadatum.Description.Valid {
			i.Project.Metadata.Description = &metadatum.Description.String
		}

		if metadatum.AdditionalMetadata.Valid {
			var additionalMetadata map[string]string

			err = json.Unmarshal([]byte(metadatum.AdditionalMetadata.String), &additionalMetadata)
			if err == nil {
				i.Project.Metadata.AdditionalMetadata = additionalMetadata
			}
		}
	}

	// if we have these, it means we've got `dependency_health` data
	if row.PackageName_2.Valid && row.PackageManager_2.Valid {
		i.Dependency.Health = &EvaluationInputDependencyHealth{}

		scorecard := EvaluationInputDependencyHealthScorecard{}
		found := false
		if row.ScorecardScore.Valid {
			found = true
			scorecard.Score = &row.ScorecardScore.Float64
		}
		if row.ScorecardCodereview.Valid {
			found = true
			tmp := int(row.ScorecardCodereview.Int64)
			scorecard.CodeReview = &tmp
		}
		if row.ScorecardMaintained.Valid {
			found = true
			tmp := int(row.ScorecardMaintained.Int64)
			scorecard.Maintained = &tmp
		}
		if row.ScorecardCiibestpractices.Valid {
			found = true
			tmp := int(row.ScorecardCiibestpractices.Int64)
			scorecard.CiiBestPractices = &tmp
		}
		if row.ScorecardLicense.Valid {
			found = true
			tmp := int(row.ScorecardLicense.Int64)
			scorecard.License = &tmp
		}
		if row.ScorecardDangerousworkflow.Valid {
			found = true
			tmp := int(row.ScorecardDangerousworkflow.Int64)
			scorecard.DangerousWorkflow = &tmp
		}
		if row.ScorecardPackaging.Valid {
			found = true
			tmp := int(row.ScorecardPackaging.Int64)
			scorecard.Packaging = &tmp
		}
		if row.ScorecardTokenpermissions.Valid {
			found = true
			tmp := int(row.ScorecardTokenpermissions.Int64)
			scorecard.TokenPermissions = &tmp
		}
		if row.ScorecardSignedreleases.Valid {
			found = true
			tmp := int(row.ScorecardSignedreleases.Int64)
			scorecard.SignedReleases = &tmp
		}
		if row.ScorecardBranchprotection.Valid {
			found = true
			tmp := int(row.ScorecardBranchprotection.Int64)
			scorecard.BranchProtection = &tmp
		}
		if row.ScorecardBinaryartifacts.Valid {
			found = true
			tmp := int(row.ScorecardBinaryartifacts.Int64)
			scorecard.BinaryArtifacts = &tmp
		}
		if row.ScorecardFuzzing.Valid {
			found = true
			tmp := int(row.ScorecardFuzzing.Int64)
			scorecard.Fuzzing = &tmp
		}
		if row.ScorecardSecuritypolicy.Valid {
			found = true
			tmp := int(row.ScorecardSecuritypolicy.Int64)
			scorecard.SecurityPolicy = &tmp
		}
		if row.ScorecardSast.Valid {
			found = true
			tmp := int(row.ScorecardSast.Int64)
			scorecard.Sast = &tmp
		}
		if row.ScorecardVulnerabilities.Valid {
			found = true
			tmp := int(row.ScorecardVulnerabilities.Int64)
			scorecard.Vulnerabilities = &tmp
		}
		if row.ScorecardPinneddependencies.Valid {
			found = true
			tmp := int(row.ScorecardPinneddependencies.Int64)
			scorecard.PinnedDependencies = &tmp
		}

		if found {
			i.Dependency.Health.SecurityScorecard = &scorecard
		}

		// reset
		found = false

		pack := EvaluationInputDependencyHealthEcosystemsPackage{}
		repo := EvaluationInputDependencyHealthEcosystemsRepo{}

		if row.EcosystemsRepoArchived.Valid {
			found = true
			repo.Archived = &row.EcosystemsRepoArchived.Bool
		}
		if row.EcosystemsRepoPushedAt.Valid {
			found = true
			repo.PushedAt = &row.EcosystemsRepoPushedAt.String
		}
		if row.EcosystemsRepoUpdatedAt.Valid {
			found = true
			repo.UpdatedAt = &row.EcosystemsRepoUpdatedAt.String
		}
		if row.EcosystemsRepoLastSyncedAt.Valid {
			found = true
			repo.LastSyncedAt = &row.EcosystemsRepoLastSyncedAt.String
		}

		if found {
			pack.Repo = &repo
		}

		// reset
		found = false

		if row.EcosystemsLastSyncedAt.Valid {
			found = true
			pack.LastSyncedAt = &row.EcosystemsLastSyncedAt.String
		}
		if row.EcosystemsLatestReleasePublishedAt.Valid {
			found = true
			pack.LatestReleasePublishedAt = &row.EcosystemsLatestReleasePublishedAt.String
		}
		if row.EcosystemsStatus.Valid {
			found = true
			pack.Status = &row.EcosystemsStatus.String
		}

		if found || pack.Repo != nil {
			i.Dependency.Health.Ecosystems = &pack
		}
	}

	return i
}

func PreparePackageQuery(ctx context.Context, modules []Module) (rego.PreparedEvalQuery, error) {
	r := rego.New(
		rego.Query(`
				denied = count(object.get(data.policy, "deny", [])) != 0
				deny_msg = object.get(data.policy, "deny", [])
				warning = count(object.get(data.policy, "warn", [])) != 0
				warning_msg = object.get(data.policy, "warn", [])
				advisory_type = object.get(data.policy, "advisory_type", "POLICY")
			`),
	)

	for _, mod := range modules {
		m := rego.Module(mod.Filepath, mod.Contents)
		m(r)
	}

	query, err := r.PrepareForEval(ctx)
	if err != nil {
		return rego.PreparedEvalQuery{}, err
	}

	var errs []error
	for filepath, m := range query.Modules() {
		packagePath := m.Package.Path
		packageName := strings.Replace(packagePath.String(), "data.", "", 1)
		if packageName != "policy" {
			errs = append(errs, fmt.Errorf("OpenPolicyAgent policy files must have their package defined as `package policy`. The package provided in %s was set to `package %v`", filepath, packageName))
		}
	}

	err = errors.Join(errs...)
	if err != nil {
		return rego.PreparedEvalQuery{}, err
	}

	return query, nil
}

func processViolationsForEvaluation(input EvaluationInput, bindings rego.Vars, msgVar string, name string, level Level) ([]PolicyViolation, error) {
	advisoryType, ok := bindings["advisory_type"].(string)
	if !ok {
		return nil, fmt.Errorf("expected the policy to return an `advisory_type` of type `string`, but instead received %#v. Check the code again, run `dmd policy evaluate`, and raise an issue if you believe this was flagged in error", advisoryType)
	}

	rawMsg, ok := bindings[msgVar].([]interface{})
	if !ok {
		// note that although it's actually a `[]interface{}`, it's better not to expose that to the user
		return nil, fmt.Errorf("expected the policy to specify `%s contains {var}`, where the value of {var} is a `[]string`, but instead received %#v. Check the code again, run `dmd policy evaluate`, and raise an issue if you believe this was flagged in error", name, bindings[msgVar])
	}

	var violations []PolicyViolation
	for _, m := range rawMsg {
		advisoryDescription, ok := m.(string)
		if !ok {
			// note that although it's actually a `[]interface{}`, it's better not to expose that to the user
			return nil, fmt.Errorf("expected the policy to specify `%s contains {var}`, where the value of {var} is a `[]string`, but instead received %#v. Check the code again, run `dmd policy evaluate`, and raise an issue if you believe this was flagged in error", name, bindings[msgVar])
		}

		v := PolicyViolation{
			Dependency: domain.Dependency{
				Platform:     input.Project.Platform,
				Organisation: input.Project.Organisation,
				Repo:         input.Project.Repo,
				PackageName:  input.Dependency.PackageName,
				Version:      input.Dependency.Version,
				// CurrentVersion below
				PackageManager:  input.Dependency.PackageManager,
				PackageFilePath: input.Dependency.PackageFilePath,
			},
			DepTypes:     input.Dependency.DepTypes,
			Level:        level,
			AdvisoryType: advisoryType,
			Description:  advisoryDescription,
		}

		if input.Dependency.CurrentVersion != "" {
			var tmp = input.Dependency.CurrentVersion
			v.CurrentVersion = &tmp
		}

		violations = append(violations, v)
	}

	return violations, nil
}

func PreparePolicyEvaluationInputs(ctx context.Context, sqlDB *sql.DB) ([]EvaluationInput, error) {
	q := db.New(sqlDB)

	var repositoryMetadata map[string]db.RepositoryMetadatum

	metadataRows, err := q.RetrieveRepositoryMetadata(ctx)
	if err != nil {
		return nil, fmt.Errorf("failed to list repository metadata, to apply policy against it: %w", err)
	}

	if len(metadataRows) > 0 {
		repositoryMetadata = make(map[string]db.RepositoryMetadatum)
	}

	for _, rm := range metadataRows {
		key := fmt.Sprintf("%s/%s/%s", rm.Platform, rm.Organisation, rm.Repo)
		repositoryMetadata[key] = rm
	}

	rows, err := q.RetrieveDistinctReposAndPackages(ctx)
	if err != nil {
		return nil, fmt.Errorf("failed to list all distinct package data, to apply policy against it: %w", err)
	}

	var inputs []EvaluationInput
	for _, row := range rows {

		key := fmt.Sprintf("%s/%s/%s", row.Platform, row.Organisation, row.Repo)
		var metadata *db.RepositoryMetadatum
		m, ok := repositoryMetadata[key]
		if ok {
			metadata = &m
		}

		input := newEvaluationInput(row, metadata)
		inputs = append(inputs, input)
	}

	return inputs, nil
}

func EvaluatePolicies(ctx context.Context, query rego.PreparedEvalQuery, inputs []EvaluationInput) ([]PolicyViolation, int, error) {
	if len(inputs) == 0 {
		return nil, 0, nil
	}

	var violations []PolicyViolation
	var advisoriesMutex sync.Mutex
	var errs []error
	var errsMutex sync.Mutex

	var wg sync.WaitGroup

	for _, input := range inputs {
		wg.Add(1)

		go func(input EvaluationInput) {
			defer wg.Done()

			results, err := query.Eval(ctx,
				rego.EvalParsedInput(input.AsAstValue()))
			if err != nil {
				errsMutex.Lock()
				errs = append(errs, fmt.Errorf("failed to process the policies: %w", err))
				errsMutex.Unlock()
				return
			} else if len(results) == 0 {
				errsMutex.Lock()
				errs = append(errs, fmt.Errorf("failed to process any results for these policies. Check that you're returning the right variables"))
				errsMutex.Unlock()
				return
			}
			denied, ok := results[0].Bindings["denied"].(bool)
			if !ok {
				errsMutex.Lock()
				errs = append(errs, fmt.Errorf("expected the policy to evaluate a variable `denied` of type `bool`, but instead received %#v. This is likely a bug in dependency-management-data, please check the code again, run `dmd policy evaluate`, and then raise an issue upstream", denied))
				errsMutex.Unlock()
			}

			warning, ok := results[0].Bindings["warning"].(bool)
			if !ok {
				errsMutex.Lock()
				errs = append(errs, fmt.Errorf("expected the policy to evaluate a variable `warning` of type `bool`, but instead received %#v. This is likely a bug in dependency-management-data, please check the code again, run `dmd policy evaluate`, and then raise an issue upstream", denied))
				errsMutex.Unlock()
			}

			if denied {
				vs, err := processViolationsForEvaluation(input, results[0].Bindings, "deny_msg", "deny", LevelError)
				if err != nil {
					errsMutex.Lock()
					errs = append(errs, err)
					errsMutex.Unlock()
				}

				advisoriesMutex.Lock()
				violations = append(violations, vs...)
				advisoriesMutex.Unlock()
			}

			if warning {
				vs, err := processViolationsForEvaluation(input, results[0].Bindings, "warning_msg", "warn", LevelWarning)
				if err != nil {
					errsMutex.Lock()
					errs = append(errs, err)
					errsMutex.Unlock()
				}

				advisoriesMutex.Lock()
				violations = append(violations, vs...)
				advisoriesMutex.Unlock()
			}
		}(input)
	}

	wg.Wait()

	err := errors.Join(unique(errs)...)
	if err != nil {
		return nil, 0, err
	}

	return violations, len(inputs), nil

}

//go:embed linting/*.rego
var lintingRules embed.FS

func NewLinter(paths []string) (linter.Linter, error) {
	input, err := rules.InputFromPaths(paths)
	if err != nil {
		return linter.Linter{}, err
	}

	l := linter.
		NewLinter().
		WithInputModules(&input).
		WithCustomRulesFromFS(lintingRules, "linting")

	return l, nil
}

func unique(t []error) []error {
	m := make(map[string]error)

	var errs []error
	for _, v := range t {
		m[v.Error()] = v
	}

	for _, err := range m {
		errs = append(errs, err)
	}
	return errs
}
