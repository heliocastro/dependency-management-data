-- name: RetrieveDistinctReposAndPackages :many
select
  distinct
  platform,
  organisation,
  repo,
  renovate.package_name,
  renovate.version as version,
  current_version as current_version,
  renovate.package_manager as package_manager,
  dep_types as dep_types,
  package_file_path as package_file_path,
  group_concat(distinct
    (case
      when external_licenses.license IS NOT NULL then external_licenses.license
      when depsdev_licenses.license IS NOT NULL then depsdev_licenses.license
      else '' -- we want to still collect dependencies, even if there's no license available
    end)
  ) as licenses,
  dependency_health.*
from
  renovate
  left join external_licenses
  on renovate.package_name = external_licenses.package_name
  and (
    case
      when renovate.current_version is not null then renovate.current_version = external_licenses.version
      else renovate.version = external_licenses.version
    end
  )
  and (
    renovate.package_manager = external_licenses.package_manager
    OR
    renovate.datasource = external_licenses.package_manager
  )
  left join depsdev_licenses
  on renovate.package_name = depsdev_licenses.package_name
  and (
    case
      when renovate.current_version is not null then renovate.current_version = depsdev_licenses.version
      else renovate.version = depsdev_licenses.version
    end
  )
  left join dependency_health
  on  renovate.package_name = dependency_health.package_name
  and renovate.package_manager = dependency_health.package_manager
group by platform, organisation, repo, renovate.package_name, renovate.version, renovate.current_version, dep_types, package_file_path
union
select
  distinct
  platform,
  organisation,
  repo,
  sboms.package_name as package_name,
  sboms.version as version,
  current_version as current_version,
  package_type as package_manager,
  -- as SBOMs don't make this available, default to an empty array
  '[]' as dep_types,
  -- as SBOMs don't make this available, default to an empty string
  '' as package_file_path,
  group_concat(distinct
    (case
      when external_licenses.license IS NOT NULL then external_licenses.license
      when depsdev_licenses.license IS NOT NULL then depsdev_licenses.license
      else '' -- we want to still collect dependencies, even if there's no license available
    end)
  ) as licenses,
  dependency_health.*
from
  sboms
  left join external_licenses
  on sboms.package_name = external_licenses.package_name
  and (
    case
      when sboms.current_version is not null then sboms.current_version = external_licenses.version
      else sboms.version = external_licenses.version
    end
  )
  and sboms.package_type = external_licenses.package_manager
  left join depsdev_licenses
  on sboms.package_name = depsdev_licenses.package_name
  and (
    case
      when sboms.current_version is not null then sboms.current_version = depsdev_licenses.version
      else sboms.version = depsdev_licenses.version
    end
  )
  left join dependency_health
  on  sboms.package_name = dependency_health.package_name
  and sboms.package_type = dependency_health.package_manager
where
  sboms.version is not null
  or
  sboms.current_version is not null
group by platform, organisation, repo, sboms.package_name, sboms.version, sboms.current_version, dep_types, package_file_path
order by platform, organisation, repo
;

-- name: RetrievePolicyViolations :many
select
  advisories.platform,
  advisories.organisation,
  advisories.repo,
  package_name,
  version,
  current_version,
  package_manager,
  dep_types,
  package_file_path,
  owners.owner,
  level,
  description
from advisories
  left join owners on
  advisories.platform = owners.platform and
  advisories.organisation = owners.organisation and
  advisories.repo = owners.repo
where
  advisory_type = 'POLICY'
order by
  platform,
  organisation,
  repo,
  level;

-- name: RetrievePolicyViolationsLike :many
select
  advisories.platform,
  advisories.organisation,
  advisories.repo,
  package_name,
  version,
  current_version,
  package_manager,
  dep_types,
  package_file_path,
  owners.owner,
  level,
  advisory_type,
  description
from advisories
  left join owners on
  advisories.platform = owners.platform and
  advisories.organisation = owners.organisation and
  advisories.repo = owners.repo
where
advisories.platform like sqlc.arg(platform) and
advisories.organisation like sqlc.arg(org) and
advisories.repo like sqlc.arg(repo) and
advisory_type = 'POLICY' and
(
  -- checking for the owner is a little more complicated due to the fact that
  -- it's a nullable field
  -- TODO We may be able to simplify this.
  (
    sqlc.arg(owner) == '%' and
    (
      owners.owner like sqlc.arg(owner) or
      owners.owner IS NULL
    )
  ) or
  (
    sqlc.arg(owner) != '%' and
    owners.owner like sqlc.arg(owner)
  )
)
and
level like sqlc.arg(level)
and
advisory_type like sqlc.arg(advisory_type)
order by
  advisories.platform,
  advisories.organisation,
  advisories.repo,
  level,
  advisory_type;

-- name: RetrieveRepositoryMetadata :many
select * from repository_metadata;
