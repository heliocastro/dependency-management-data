package db

import (
	"encoding/json"
	"strings"
)

func NewRetrievePolicyViolationsLikeParams(platform string, org string, repo string, owner string, level string) RetrievePolicyViolationsLikeParams {
	params := RetrievePolicyViolationsLikeParams{
		Platform: "%",
		Org:      "%",
		Repo:     "%",
		Owner:    "%",
		Level:    "%",
	}

	if platform != "" {
		params.Platform = strings.ReplaceAll(platform, "*", "%")
	}

	if org != "" {
		params.Org = strings.ReplaceAll(org, "*", "%")
	}

	if repo != "" {
		params.Repo = strings.ReplaceAll(repo, "*", "%")
	}

	if owner != "" {
		params.Owner = strings.ReplaceAll(owner, "*", "%")
	}

	if level != "" {
		params.Level = strings.ReplaceAll(level, "*", "%")
	}

	return params

}

func (row RetrieveDistinctReposAndPackagesRow) DepTypesAsString() string {
	bytes, _ := json.Marshal(row.DepTypes)
	return string(bytes)
}
