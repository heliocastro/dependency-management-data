package policies

import (
	"context"
	"database/sql"
	"fmt"
	"log/slog"

	"dmd.tanna.dev/internal/policies/db"
	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/mitchellh/go-wordwrap"
)

func ReportPackages(ctx context.Context, logger *slog.Logger, sqlDB *sql.DB, platform string, org string, repo string, owner string, level string) (table.Writer, error) {
	q := db.New(sqlDB)

	params := db.NewRetrievePolicyViolationsLikeParams(platform, org, repo, owner, level)

	rows, err := q.RetrievePolicyViolationsLike(ctx, params)
	if err != nil {
		return nil, err
	}

	tw := table.NewWriter()
	tw.AppendHeader(table.Row{
		"Platform",
		"Organisation",
		"Repo",
		"Package",
		"Version",
		"Dependency Types",
		"Filepath",
		"Owner",
		"Level",
		"Description",
	})

	for _, violation := range rows {
		ver := violation.Version
		if violation.CurrentVersion.Valid {
			ver = fmt.Sprintf("%s / %s", ver, violation.CurrentVersion.String)
		}
		tw.AppendRow(table.Row{
			violation.Platform,
			violation.Organisation,
			violation.Repo,
			violation.PackageName,
			wordwrap.WrapString(ver, 10),
			violation.DepTypes,
			violation.PackageFilePath,
			violation.Owner.String,
			violation.Level,
			wordwrap.WrapString(violation.Description, 60),
		})
	}

	return tw, nil
}
