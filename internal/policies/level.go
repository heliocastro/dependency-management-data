package policies

type Level string

func (l Level) String() string {
	return string(l)
}

const (
	LevelError   Level = "ERROR"
	LevelWarning Level = "WARN"
)
