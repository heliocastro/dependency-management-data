package policies

import (
	"encoding/json"

	"dmd.tanna.dev/internal/advisory"
	"dmd.tanna.dev/internal/domain"
)

type PolicyViolation struct {
	domain.Dependency
	DepTypes []string

	Level        Level
	AdvisoryType advisory.AdvisoryType
	Description  string
}

func (v PolicyViolation) DepTypesAsString() string {
	if v.DepTypes == nil {
		return "[]"
	}
	bytes, _ := json.Marshal(v.DepTypes)
	return string(bytes)
}
