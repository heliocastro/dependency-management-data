-- name: RetrieveLicenseOverview :many
select
  r.platform as _plat,
  r.organisation as _org,
  r.repo as _repo,
  group_concat(distinct
    (case
      when external_licenses.license IS NOT NULL then external_licenses.license
      when depsdev_licenses.license IS NOT NULL then depsdev_licenses.license
      else '' -- this shouldn't trigger due to the WHERE clause below
    end)
  ) as licenses,
  owner
from
  renovate r
  left join external_licenses
  on r.package_name = external_licenses.package_name
  and (
    case
      when r.current_version is not null then r.current_version = external_licenses.version
      else r.version = external_licenses.version
    end
  )
  and (
    r.package_manager = external_licenses.package_manager
    OR
    r.datasource = external_licenses.package_manager
  )
  left join depsdev_licenses
  on r.package_name = depsdev_licenses.package_name
  and (
    case
      when r.current_version is not null then r.current_version = depsdev_licenses.version
      else r.version = depsdev_licenses.version
    end
  )
  left join owners on
  r.platform = owners.platform and
  r.organisation = owners.organisation and
  r.repo = owners.repo
where
r.platform like sqlc.arg(platform) and
r.organisation like sqlc.arg(org) and
r.repo like sqlc.arg(repo) and
(
  -- checking for the owner is a little more complicated due to the fact that
  -- it's a nullable field
  -- TODO We may be able to simplify this.
  (
    sqlc.arg(owner) == '%' and
    (
      owner like sqlc.arg(owner) or
      owner IS NULL
    )
  ) or
  (
    sqlc.arg(owner) != '%' and
    owner like sqlc.arg(owner)
  )
)
AND
(
  external_licenses.license IS NOT NULL
  OR
  depsdev_licenses.license IS NOT NULL
)
group by _plat, _org, _repo
union
select
  s.platform as _plat,
  s.organisation as _org,
  s.repo as _repo,
  group_concat(distinct
    (case
      when external_licenses.license IS NOT NULL then external_licenses.license
      when depsdev_licenses.license IS NOT NULL then depsdev_licenses.license
      else '' -- this shouldn't trigger due to the WHERE clause below
    end)
  ) as licenses,
  owner
from
  sboms s
  left join external_licenses
  on s.package_name = external_licenses.package_name
  and (
    case
      when s.current_version is not null then s.current_version = external_licenses.version
      else s.version = external_licenses.version
    end
  )
  and s.package_type = external_licenses.package_manager
  left join depsdev_licenses
  on s.package_name = depsdev_licenses.package_name
  and (
    case
      when s.current_version is not null then s.current_version = depsdev_licenses.version
      else s.version = depsdev_licenses.version
    end
  )
  left join owners on
  s.platform = owners.platform and
  s.organisation = owners.organisation and
  s.repo = owners.repo
where
(
  s.version is not null or
  s.current_version is not null
)
and
s.platform like sqlc.arg(platform) and
s.organisation like sqlc.arg(org) and
s.repo like sqlc.arg(repo) and
(
  -- checking for the owner is a little more complicated due to the fact that
  -- it's a nullable field
  -- TODO We may be able to simplify this.
  (
    sqlc.arg(owner) == '%' and
    (
      owner like sqlc.arg(owner) or
      owner IS NULL
    )
  ) or
  (
    sqlc.arg(owner) != '%' and
    owner like sqlc.arg(owner)
  )
)
AND
(
  external_licenses.license IS NOT NULL
  OR
  depsdev_licenses.license IS NOT NULL
)
group by _plat, _org, _repo
order by _plat, _org, _repo
;
