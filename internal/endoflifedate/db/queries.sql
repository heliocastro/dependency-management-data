-- name: InsertProductCycle :exec
insert into endoflifedate_products (
  product_name,
  cycle,
  supported_until,
  eol_from,
  inserted_at
  ) VALUES (
  ?,
  ?,
  ?,
  ?,
  ?
);

------------ Renovate

-- name: RetrieveDistinctRenovateDeps :many
select
  distinct package_name,
  version,
  current_version,
  renovate.package_manager,
  datasource
from
  renovate
  left join sensitive_packages as sp on
    renovate.package_name like replace(sp.package_pattern, '*', '%')
where
  (
    case
      when sp.package_manager IS NOT NULL then sp.package_manager == renovate.package_manager
      else true
    end
  )
  and (
    case
      when sp.match_strategy = 'MATCHES' then false
      when sp.match_strategy = 'DOES_NOT_MATCH' then true
      else true
    end
  );

-- name: InsertRenovateEndOfLife :exec
insert into renovate_endoflife (
  package_name,
  version,
  current_version,

  package_manager,

  datasource,

  product_name,
  cycle
  ) VALUES (
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?
);

------------ SBOMs

-- name: RetrieveDistinctSBOMDeps :many
select
  distinct package_name,
  version,
  current_version,
  sboms.package_type
from
  sboms
  left join sensitive_packages as sp on
    sboms.package_name like replace(sp.package_pattern, '*', '%')
where
  (
    version is not null
    or
    current_version is not null
  )
  and
  (
    case
      when sp.package_manager IS NOT NULL then sp.package_manager == sboms.package_type
      else true
    end
  )
  and (
    case
      when sp.match_strategy = 'MATCHES' then false
      when sp.match_strategy = 'DOES_NOT_MATCH' then true
      else true
    end
  );


-- name: InsertSBOMEndOfLife :exec
insert into sboms_endoflife (
  package_name,
  version,
  current_version,
  package_type,

  product_name,
  cycle
  ) VALUES (
  ?,
  ?,
  ?,
  ?,
  ?,
  ?
);
