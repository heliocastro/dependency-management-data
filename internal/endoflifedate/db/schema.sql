-- endoflifedate_products contains an internal representation of Products and
-- Cycles that https://EndOfLife.date supports.
--
-- This table can be used on its own to perform ad-hoc lookups of end-of-life
-- dates for given dependencies.
--
-- However, this table is primarily a utility table, and is expected to be
-- `JOIN`'d with Datasources (https://dmd.tanna.dev/concepts/datasource/).
--
-- For example, if you are using the SBOMs datasource, you would perform a
-- `JOIN` between the `sboms`, `sboms_endoflife` and `endoflifedate_products`
-- tables.
CREATE TABLE IF NOT EXISTS endoflifedate_products (
  -- product_name describes an EndOfLife.date Product name.
  -- For example:
  --   go
  --   ansible
  --   sqlite
  product_name TEXT NOT NULL,

  -- cycle of the given product, as defined by EndOfLife.date, and the product
  -- itself.
  --
  -- For example (non-exhaustively):
  --   sqlite has cycles: `3`, `2`, `1`
  --   go     has cycles: `1.21`, `1.20`, `1.5`
  cycle TEXT NOT NULL,

  -- supported_until describes the date that this release is (actively)
  -- supported until
  supported_until TEXT,
  -- eol_from describes the date that this release will be marked as End of
  -- Life, and will no longer be maintained from
  eol_from TEXT,

  -- inserted_at indicates when this row was inserted into the database
  inserted_at TEXT NOT NULL,

  UNIQUE (product_name, cycle) ON CONFLICT REPLACE,
  CHECK(product_name <> ''),
  CHECK(cycle <> '')
);


-- renovate_endoflife contains a mapping between packages found in the
-- `renovate` table, and `endoflifedate_products`.
--
-- This table is a utility table, and is expected to be `JOIN`d to other
-- tables. See `endoflifedate_products` for more details.
CREATE TABLE IF NOT EXISTS renovate_endoflife (
  -- what package is this?
  --
  -- Foreign key: `renovate.package_name`
  package_name TEXT NOT NULL,
  -- version indicates the version of this dependency.
  --
  -- NOTE this could be a version constraint, such as any of:
  --
  --   <=1.3.4,>=1.3.0
  --   "~> 0.9"
  --   latest
  --   ^2.0.6
  --   =1.0.4
  --
  -- As well as a specific value, such as:
  --
  --   1.0.4
  --   10
  --   latest
  --
  -- This versioning will be implementation-specific for the `package_manager`
  -- in use.
  --
  -- Foreign key: `renovate.version`
  version TEXT NOT NULL,
  -- current_version defines the current version that this package's `version`
  -- resolves to.
  --
  -- If the `version` is an exact version number, such as `1.0.4`, then
  -- `current_version` will usually be the same value, `1.0.4`.
  --
  -- If the `version` is a version constraint, then this column MAY indicate
  -- the exact version that was resolved at the time of dependency analysis.
  --
  -- The `current_version` is derived from:
  -- - the `locked_version` or `fixed_version`, if there is a lockfile or some
  --   pinning set
  -- - the `current_version` if present, and can be recalculated each time the
  --   data is generated
  --
  -- Foreign key: `renovate.current_version`
  current_version TEXT,

  -- package_manager indicates the package manager that the dependency is from,
  -- which corresponds to Renovate's managers
  -- (https://docs.renovatebot.com/modules/manager/).
  --
  -- For example:
  --
  --  gomod
  --  pip_setup
  --  pip_requirements
  --  maven-wrapper
  --  html
  --  maven
  --  gradle
  --
  -- Foreign key: `renovate.package_manager`
  package_manager TEXT NOT NULL,

  -- datasource indicates the datasource that the dependency has been sourced
  -- from, which corresponds to Renovate's datasources
  -- (https://docs.renovatebot.com/modules/datasource/).
  --
  -- For example:
  --
  --   go
  --   github-tags
  --   gitlab-tags
  --   maven
  --   gradle-version
  --
  -- Foreign key: `renovate.datasource`
  datasource TEXT NOT NULL,

  -- product_name describes an EndOfLife.date Product name.
  -- For example:
  --   go
  --   ansible
  --   sqlite
  --
  -- Foreign key: `endoflifedate_products.product_name`
  product_name TEXT NOT NULL,
  -- cycle of the given product, as defined by EndOfLife.date, and the product
  -- itself.
  --
  -- For example (non-exhaustively):
  --   sqlite has cycles: `3`, `2`, `1`
  --   go     has cycles: `1.21`, `1.20`, `1.5`
  --
  -- Foreign key: `endoflifedate_products.cycle`
  cycle TEXT NOT NULL,

  UNIQUE (package_name, version, current_version, package_manager, datasource) ON CONFLICT REPLACE
);

-- renovate_endoflife contains a mapping between packages found in the `sboms`
-- table, and `endoflifedate_products`.
--
-- This table is a utility table, and is expected to be `JOIN`d to other
-- tables. See `endoflifedate_products` for more details.
CREATE TABLE IF NOT EXISTS sboms_endoflife (
  -- what package is this?
  --
  -- Foreign key: `sboms.package_name`
  package_name TEXT NOT NULL,
  -- version indicates the version of this dependency.
  --
  -- NOTE this could be a version constraint, such as any of:
  --
  --   <=1.3.4,>=1.3.0
  --   "~> 0.9"
  --   latest
  --   ^2.0.6
  --   =1.0.4
  --                   (NULL)
  --
  -- As well as a specific value, such as:
  --
  --   1.0.4
  --   10
  --   latest
  --
  -- This versioning will be implementation-specific for the `package_manager`
  -- in use.
  --
  -- NOTE that due to the quality of the tool producing the SBOM, this field
  -- may be NULL.
  --
  -- Foreign key: `sboms.version`
  version TEXT,
  -- current_version defines the current version that this package's `version`
  -- resolves to.
  --
  -- If the `version` is an exact version number, such as `1.0.4`, then
  -- `current_version` will usually be the same value, `1.0.4`.
  --
  -- NOTE that due to the quality of the tool producing the SBOM, this field
  -- may be NULL, or this may not be an exact value, but a version constraint
  -- similar to `version`.
  --
  -- Foreign key: `sboms.current_version`
  current_version TEXT,

  -- package_type most commonly relates to the "Type" field of a Package URL
  -- (as defined by https://github.com/package-url/purl-spec), which may be a
  -- package ecosystem or package manager type
  --
  -- Foreign key: `sboms.package_type`
  package_type TEXT NOT NULL,

  -- product_name describes an EndOfLife.date Product name.
  -- For example:
  --   go
  --   ansible
  --   sqlite
  --
  -- Foreign key: `endoflifedate_products.product_name`
  product_name TEXT NOT NULL,
  -- cycle of the given product, as defined by EndOfLife.date, and the product
  -- itself.
  --
  -- For example (non-exhaustively):
  --   sqlite has cycles: `3`, `2`, `1`
  --   go     has cycles: `1.21`, `1.20`, `1.5`
  --
  -- Foreign key: `endoflifedate_products.cycle`
  cycle TEXT NOT NULL,


  UNIQUE (package_name, version, package_type) ON CONFLICT REPLACE
);
