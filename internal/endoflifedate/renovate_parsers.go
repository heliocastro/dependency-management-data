package endoflifedate

import (
	"log/slog"

	"dmd.tanna.dev/internal/endoflifedate/db"
)

var renovateParsers = []renovateParser{}

type renovateParser interface {
	Handled(dep db.RetrieveDistinctRenovateDepsRow) bool
	ParseProductAndCycle(logger *slog.Logger, dep db.RetrieveDistinctRenovateDepsRow) (p string, c string, ok bool)
}
