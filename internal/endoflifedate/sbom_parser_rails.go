package endoflifedate

import (
	"regexp"

	"log/slog"

	"dmd.tanna.dev/internal/endoflifedate/db"
)

func init() {
	sbomParsers = append(sbomParsers, &sbomParserRails{})
}

type sbomParserRails struct{}

func (*sbomParserRails) Handled(dep db.RetrieveDistinctSBOMDepsRow) bool {
	return dep.PackageName == "rails" && dep.PackageType == "gem"
}

func (*sbomParserRails) ParseProductAndCycle(logger *slog.Logger, dep db.RetrieveDistinctSBOMDepsRow) (p string, c string, ok bool) {
	p = "rails"

	version := getSBOMDepVersion(dep.Version, dep.CurrentVersion)

	match, _ := regexp.MatchString("^[0-9]+\\.[0-9]+", version)
	if match {
		cycle := majorDotMinor(version)

		return p, cycle, true
	}
	return
}
