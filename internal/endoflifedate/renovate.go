package endoflifedate

import (
	"context"
	"database/sql"
	"fmt"
	"strings"
	"time"

	"dmd.tanna.dev/internal/endoflifedate/client"
	"dmd.tanna.dev/internal/endoflifedate/db"
	"log/slog"
)

type Renovate struct {
	sqlDB    *sql.DB
	queries  db.Queries
	client   *client.ClientWithResponses
	logger   *slog.Logger
	products map[string][]client.Cycle
}

func NewRenovate(sqlDB *sql.DB, theClient *client.ClientWithResponses, logger *slog.Logger) Renovate {
	return Renovate{
		sqlDB:    sqlDB,
		queries:  *db.New(sqlDB),
		client:   theClient,
		logger:   logger,
		products: make(map[string][]client.Cycle),
	}
}

func (r Renovate) ProcessDependency(ctx context.Context, tx *sql.Tx, dep db.RetrieveDistinctRenovateDepsRow) error {
	product, cycle, ok := r.RetrieveEOLStatus(ctx, dep)
	if !ok {
		r.logger.Debug(fmt.Sprintf("Could not parse dependency %s as an endoflife.date Product/Cycle, returned (%v, %v, %v)", dep.DependencyDetails(), product, cycle, ok))
		return nil
	}

	if cycle.Cycle == "" {
		r.logger.Debug(fmt.Sprintf("Could not parse dependency %s as an endoflife.date Cycle, returned (%v, %v, %v)", dep.DependencyDetails(), product, cycle, ok))
		return nil
	}

	productCycleParams := db.InsertProductCycleParams{
		ProductName:    product.Name,
		Cycle:          cycle.Cycle,
		EolFrom:        emptyOrNullString(cycle.EolFrom),
		SupportedUntil: emptyOrNullString(cycle.SupportedUntil),
		InsertedAt:     time.Now().Format(time.RFC3339),
	}

	err := r.queries.WithTx(tx).InsertProductCycle(ctx, productCycleParams)
	if err != nil {
		return err
	}

	renovateParams := db.InsertRenovateEndOfLifeParams{
		PackageName:    dep.PackageName,
		Version:        dep.Version,
		CurrentVersion: dep.CurrentVersion,
		PackageManager: dep.PackageManager,
		Datasource:     dep.Datasource,
		ProductName:    product.Name,
		Cycle:          cycle.Cycle,
	}

	err = r.queries.WithTx(tx).InsertRenovateEndOfLife(ctx, renovateParams)
	if err != nil {
		return err
	}

	return nil
}

func (r Renovate) retrieveProductInformation(ctx context.Context, dep db.RetrieveDistinctRenovateDepsRow, product string) ([]client.Cycle, error) {
	cycles, ok := r.products[product]
	if ok {
		r.logger.Debug(fmt.Sprintf("Using cached endoflife.date product %s with %d cycles", product, len(cycles)))
		return cycles, nil
	}

	r.logger.Debug(fmt.Sprintf("Looking up endoflife.date product information for %s for dependency %s", product, dep.DependencyDetails()))
	resp, err := r.client.GetApiProductJsonWithResponse(ctx, product)
	if err != nil {
		return nil, err
	}

	r.logger.Debug(fmt.Sprintf("Received an HTTP %d from endoflife.date product information for %s for dependency %s", resp.StatusCode(), product, dep.DependencyDetails()))
	if resp.JSON200 == nil {
		return nil, fmt.Errorf("Received an HTTP %d from endoflife.date, with no JSON200 response body", resp.StatusCode())
	}

	r.products[product] = *resp.JSON200
	return r.products[product], nil
}

func (r Renovate) RetrieveEOLStatus(ctx context.Context, dep db.RetrieveDistinctRenovateDepsRow) (p Product, c Cycle, ok bool) {
	product, cycle, ok := r.parseProductAndCycle(dep)
	if !ok {
		return
	}

	cycles, err := r.retrieveProductInformation(ctx, dep, product)
	if err != nil {
		r.logger.Warn(fmt.Sprintf("Received an error while looking up %s's data in endoflife.date: %v", product, err))
		return
	}
	p.Name = product

	for _, cy := range cycles {
		if cy.Cycle != nil {
			cycleStr, err := cy.Cycle.AsCycleCycle0()
			if err == nil {
				if cycle == cycleStr {
					ok = true
					c.Cycle = cycleStr

					if cy.Eol != nil {
						eol, err := cy.Eol.AsCycleEol0()
						if err == nil {
							c.EolFrom = eol
						}
					}

					if cy.Support != nil {
						supported, err := cy.Support.AsCycleSupport0()
						if err == nil {
							c.SupportedUntil = supported.Format("2006-01-02")
						}
					}
				}
			}
		}
	}

	return
}

func (r Renovate) parseProductAndCycle(dep db.RetrieveDistinctRenovateDepsRow) (p string, c string, ok bool) {
	if dep.Version == "latest" {
		return
	}

	for _, rp := range renovateParsers {
		if rp.Handled(dep) {
			p, c, ok = rp.ParseProductAndCycle(r.logger, dep)
			if ok {
				return
			}
		}
	}

	return
}

func major(s string) string {
	return part(strings.Split(s, "."), 0)
}

func majorDotMinor(s string) string {
	return fmt.Sprintf("%s.%s",
		part(strings.Split(s, "."), 0),
		part(strings.Split(s, "."), 1),
	)
}

func trimDockerSuffix(s string) string {
	return part(strings.Split(s, "-"), 0)
}

func part(parts []string, n int) string {
	if n > len(parts) {
		return ""
	}

	return parts[n]
}

func emptyOrNullString(s string) sql.NullString {
	return sql.NullString{
		String: s,
		Valid:  s != "",
	}
}
