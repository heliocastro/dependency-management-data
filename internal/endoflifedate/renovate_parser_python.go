package endoflifedate

import (
	"fmt"
	"regexp"

	"dmd.tanna.dev/internal/endoflifedate/db"
	"log/slog"
)

func init() {
	renovateParsers = append(renovateParsers, &renovateParserPython{})
}

type renovateParserPython struct{}

func (*renovateParserPython) Handled(dep db.RetrieveDistinctRenovateDepsRow) bool {
	return dep.PackageName == "python" || dep.PackageManager == "pyenv"
}

func (*renovateParserPython) ParseProductAndCycle(logger *slog.Logger, dep db.RetrieveDistinctRenovateDepsRow) (p string, c string, ok bool) {
	p = "python"

	r_ := regexp.MustCompile("[v >=<~^]*")
	version := r_.ReplaceAllString(getDepVersion(dep.Version, dep.CurrentVersion), "")

	match, _ := regexp.MatchString("^[0-9]+$", version)
	if match {
		logger.Warn(fmt.Sprintf("Couldn't assume cycle of %s that %s corresponds to", p, dep.DependencyDetails()))
		return
	}

	match, _ = regexp.MatchString("^[0-9]+\\.[0-9]+", version)
	if match {
		cycle := majorDotMinor(version)

		return p, cycle, true
	}

	return
}
