package endoflifedate

import (
	"context"
	"database/sql"
	"fmt"
	"log/slog"
	"net/http"

	"dmd.tanna.dev/internal/endoflifedate/client"
	"dmd.tanna.dev/internal/endoflifedate/db"
	"github.com/jedib0t/go-pretty/v6/progress"
)

func Generate(ctx context.Context, logger *slog.Logger, sqlDB *sql.DB, pw progress.Writer, httpClient *http.Client) error {
	client, err := client.NewClientWithResponses("https://endoflife.date", client.WithHTTPClient(httpClient))
	if err != nil {
		return err
	}

	err = generateRenovate(ctx, logger, sqlDB, pw, client)
	if err != nil {
		return err
	}

	err = generateSBOM(ctx, logger, sqlDB, pw, client)
	if err != nil {
		return err
	}

	return nil
}

func generateRenovate(ctx context.Context, logger *slog.Logger, sqlDB *sql.DB, pw progress.Writer, client *client.ClientWithResponses) error {
	queries := db.New(sqlDB)

	deps, err := queries.RetrieveDistinctRenovateDeps(ctx)
	if err != nil {
		return err
	}

	renovateEOL := NewRenovate(sqlDB, client, logger)

	tx, err := sqlDB.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	tracker := progress.Tracker{
		Message: fmt.Sprintf("Processing %d Renovate dependencies for end-of-life status", len(deps)),
		Total:   int64(len(deps)),
	}
	pw.AppendTracker(&tracker)

	go pw.Render()

	errors := 0
	for _, d := range deps {
		tracker.Increment(1)
		err = renovateEOL.ProcessDependency(ctx, tx, d)
		if err != nil {
			errors++

			logger.Warn(fmt.Sprintf("Failed to process dependency (%s@%s): %v", d.PackageName, d.Version, err))
		}
	}

	tracker.MarkAsDone()

	if errors > 0 {
		return fmt.Errorf("%d dependencies failed to process", errors)
	}

	return tx.Commit()
}

func generateSBOM(ctx context.Context, logger *slog.Logger, sqlDB *sql.DB, pw progress.Writer, client *client.ClientWithResponses) error {
	queries := db.New(sqlDB)

	deps, err := queries.RetrieveDistinctSBOMDeps(ctx)
	if err != nil {
		return err
	}

	sbomEOL := NewSBOM(sqlDB, client, logger)

	tx, err := sqlDB.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	tracker := progress.Tracker{
		Message: fmt.Sprintf("Processing %d SBOM dependencies for end-of-life status", len(deps)),
		Total:   int64(len(deps)),
	}
	pw.AppendTracker(&tracker)

	go pw.Render()

	errors := 0
	for _, d := range deps {
		tracker.Increment(1)
		err = sbomEOL.ProcessDependency(ctx, tx, d)
		if err != nil {
			errors++

			logger.Warn(fmt.Sprintf("Failed to process dependency (%s@%v): %v", d.PackageName, d.Version, err))
		}
	}

	tracker.MarkAsDone()

	if errors > 0 {
		return fmt.Errorf("%d dependencies failed to process", errors)
	}

	return tx.Commit()
}
