package endoflifedate

import (
	"log/slog"

	"dmd.tanna.dev/internal/endoflifedate/db"
)

var sbomParsers = []sbomParser{}

type sbomParser interface {
	Handled(dep db.RetrieveDistinctSBOMDepsRow) bool
	ParseProductAndCycle(logger *slog.Logger, dep db.RetrieveDistinctSBOMDepsRow) (p string, c string, ok bool)
}
